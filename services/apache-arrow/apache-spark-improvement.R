data_frame = data.frame(case=c("未使用", "使用"),
                        elapsed=c(20.7, 0.737))
ggplot2::ggplot(data_frame) +
  ggplot2::ggtitle("Apache Arrowによる処理時間の違い") +
  ggplot2::labs(x="処理秒数（短いほど速い）",
                y="Apache Arrowを使用しているかどうか",
                caption="https://arrow.apache.org/blog/2017/07/26/spark-arrow/ のデータを使用") +
  ggplot2::geom_bar(ggplot2::aes(y=case, weight=elapsed)) +
  ggplot2::ggsave("apache-spark-improvement.png",
                  dpi=100,
                  height=3.8)
