module Jekyll
  class Document
    module PostURL
      def url_template
        if data["slug"] == "index"
          "/blog/:year/:i_month/:i_day:output_ext"
        else
          super
        end
      end
    end

    prepend PostURL
  end
end
