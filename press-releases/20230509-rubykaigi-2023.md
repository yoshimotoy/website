---
layout: default
main_title: RubyKaigi 2023をシルバースポンサーとして支援し、5月13日に須藤功平が登壇
sub_title:
type: press
---

<p class='press-release-subtitle-small'>


<div class="press-release-signature">
  <p class="date">2023年5月9日</p>
  <p>株式会社クリアコード
</div>

株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、RubyKaigi 2023をシルバースポンサーとして支援します。
あわせて、代表取締役 須藤功平が2023年5月13日(土)に「Ruby + ADBC - A single API between Ruby and DBs」と題して登壇することをお知らせします。

## RubyKaigi 2023
RubyKaigi 2023は、国内最大規模で開催されるRubyの国際カンファレンスです。今回は、長野県松本市でのオフライン開催とともに、オンラインでの参加も可能な形で開催されます。期間中は、関連のイベントなども多く開催され多くのRubyistたちが交流を深める場となっています。クリアコードは、2009年の日本Ruby会議2009からRubyKaigiをスポンサーしています。

* 開催期間：2023年5月11日（木）-13日（土）9:40-17:40（初日は10:30から）
* 会場：まつもと市民芸術館＋オンライン
* 公式サイト：https://rubykaigi.org/2023/
* 参加申し込み：https://ti.to/rubykaigi/2023


### 講演の概要

* 講演日時：2023年5月12日（土）13時30分-14時00分
* 講演タイトル：Ruby + ADBC - A single API between Ruby and DBs
* 講演内容：本講演では、大量のカラム指向データを高速にやり取りするためのAPIであるADBC(Apache Arrow Database Connectivity)を紹介します。ADBCは各種データベースとの接続方法を抽象化しており、同じAPIで異なるデータベースと効率的にデータをやり取りできます。Ruby on Railsを使ったWebアプリケーションをはじめ、Rubyでデータベースとやりとりする機会は多いです。これまで大量データのやり取りは他の言語で実装していたようなケースでもADBCを使うことによりRubyで実装できるようになります。

詳細：{{ site.url }}{% post_url 2023-05-08-rubykaigi-2023-announce %}

* 講演者プロフィール：須藤功平 (Rubyコミッター)

1981年生まれ。大学在学時からフリーソフトウェアの開発を始め、2004年にはRubyのコミッターになる。2006年にクリアコードを立ち上げ、2008年代表取締役に就任。

2016年より新しいデータ処理基盤として開発が進むApache Arrowの開発に参加し、2017年5月にコミッターに就任、2017年9月にプロジェクト管理委員会メンバーに就任。2022年1月から1年間プロジェクト管理委員会のchairを担った。並行してApache Arrowも活用してRuby用のデータ処理ツールを提供するプロジェクトRed Data Toolsも進めている。

参考：https://magazine.rubyist.net/articles/0053/0053-Hotlinks.html


### Ruby用のデータ処理ツールを提供するプロジェクト『Red Data Tools』

日本生まれで、世界中に利用者のいるオブジェクト指向スクリプト言語Ruby用のデータ処理ツールを提供するプロジェクトです。
各種ツールの開発のほか、月に一度の開発イベントや、新規参加者のサポートもしています。新規参加者をサポートする様子はYouTubeで配信しているので未参加の人も視聴することができます。

* プロジェクトページ：https://red-data-tools.github.io/ja/


## クリアコードについて

クリアコードは、2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。

Fluentd/Fluent Bit/Apache Arrow/Groonga/組み込みシステム向け日本語入力ソフトウェアなど、多岐にわたるソフトウェアの開発やソースコードレベルでの技術支援を行っています。あわせてフリーソフトウェア開発の推進も行っています。

クリアコードでは、フリーソフトウェアとビジネスの両立を理念としています。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。

業務でフリーソフトウェアに関わりたい、プログラミングが楽しい[開発者を募集]({% link recruitment/index.md %})しています。

### 当リリースに関するお問合せ先
* 株式会社クリアコード
* 電話：04-2907-4726
* メール：info@clear-code.com

### 参考URL

* 【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})
* 【採用情報】[{{ site.url }}{% link recruitment/index.md %}]({% link recruitment/index.md %})

<small>
Apache Arrow, the Apache Arrow project logo are either registered trademarks or trademarks of The Apache Software Foundation in the United States and other countries.（Apache Arrowの商標およびロゴはApache Software Foundationの商標です。）
</small>

