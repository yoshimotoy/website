---
layout: default
main_title: 代表取締役　須藤功平が次世代データ処理基盤Apache Arrowのカンファレンス『The Data Thread』に登壇
sub_title:
type: press
---


<div class="press-release-signature">
  <p class="date">2022年6月22日</p>
  <p>株式会社クリアコード</p>
</div>



株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平)は、代表取締役　須藤功平が2022年6月24日1:00am-4:00amにオンライン開催される 『The Data Thread』に「Why Apache Arrow is important for Ruby」と題し、ビデオ登壇することをお知らせします。『The Data Thread』は、Apache Arrow*1の開発をリードするVoltron Data社が主催するカンファレンスイベントで、Apache Arrowとそのエコシステムに関連する革新的な最新情報や技術が紹介されます。 

[^1]: Apache Arrow, the Apache Arrow project logo are either registered trademarks or trademarks of The Apache Software Foundation in the United States and other countries.（Apache Arrowの商標およびロゴはApache Software Foundationの商標です。）

  <p>
  <img src= {% link press-releases/20220622the-data-thread/TDT1500x500.png %}>
  </p>


### 講演の概要
* 視聴方法：イベント公式ページにて視聴登録(Register)をいただくことで、視聴用URLが発行されます。本講演はBreak Out Sessionとして、Key Note Session終了後自由に、視聴することが可能になります。
* 講演タイトル：「Why Apache Arrow is important for Ruby」（RubyにとってApache Arrowはなぜ重要なのか）

次世代データ処理基盤として期待されているApache Arrowは10以上のプログラミング言語で利用できるため、異なるプログラミング言語で実装された各種データ処理プロダクトを効率よく連携できます。長らくRubyコミッターとして活躍してきた須藤が、Apache ArrowがRubyでのデータ処理にもたらす可能性や、Apache Arrowに関わる取り組みについて紹介します。

* 講演者プロフィール：須藤功平 (Apache Arrow プロジェクト管理委員会 chair、Rubyコミッター)

2016年よりApache Arrowの開発に参加し、2017年5月にコミッターに就任、2017年9月にプロジェクト管理委員会メンバーに就任。2022年3月時点でコミット数は3位。2022年1月からプロジェクト管理委員会のchairに就任。日本でのApache Arrow普及を目的として、開発のみならず各所でのApache Arrowの
紹介を精力的に行っている。また[Apache Arrowの開発状況を定期的にまとめ、日本語の記事として公開している。]({% link _posts/2022-05-13-latest-apache-arrow-information.md %})も公開している。

* Apache Arrow関連講演事例
  * [『Apache Arrowフォーマットはなぜ速いのか』](https://bit.ly/3wnOwLH)
  * [『Apache Arrow 1.0 - A cross-language development platform for in-memory data』](https://bit.ly/3mTK58j)
  * [『Red Arrow - Ruby and Apache Arrow』](https://bit.ly/3mR9QWH)


### The Data Threadの概要
* 開催日時：2022年6月24日1:00 am～4:00 am（2022年6月23日12:00 pm～3:00 pm(東部夏時間)）
* 会場：オンライン
* 参加費：無料
* 公式ページ：https://thedatathread.com/
* 主催：[Voltron Data社](https://voltrondata.com/)

イベントは英語を主言語としています。登壇分は日本語音声と英語字幕です。


## Apache Arrowの概要
Apache Arrowは、大規模データの交換処理を効率化するソフトウェアで次世代データ処理基盤として期待されています。Apache Arrowは10以上のプログラミング言語で利用できるため、異なるプログラミング言語で実装された各種データ処理プロダクトを効率よく連携できます。2016年から開発が進み、2020年7月の1.0.0リリースをきっかけに様々なプロジェクトで採用が進みました。2022年6月現在の最新バージョンは8.0.0です。

Apache Arrowの主な機能には次のような機能があります。
* CPU上でSIMD・JITコンパイルを使って高速にデータを処理する機能
* GPU上で高速にデータを処理する機能
* CSVやApache Parquetなど既存のデータフォーマットと相互変換する機能
* 高速なRPC機能
従来はこのような機能は各データ処理プロダクトで個別に実装されていましたが、Apache Arrowは各データ処理プロダクトで共有できる高速な実装を提供します。


#### 活用事例
Apache Spark™をはじめApache Arrowを利用して高速化したデータ処理プロダクトが増えています。Apache ArrowのPythonライブラリーであるpyarrowだけでも、2020年8月23日の1日のダウンロード数は約23万件です。

以下にApache Arrowの利用例を紹介します。

* [Amazon Athenaの新しいフェデレーテッド・クエリによる複数データソースの検索 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/query-any-data-source-with-amazon-athenas-new-federated-query/)：Amazon Athenaへのデータ提供時にApache Arrowを利用
* [PostgreSQLだってビッグデータ処理したい！！～GPUとNVMEを駆使して毎秒10億レコードを処理する技術～ ](https://www.slideshare.net/kaigai/20191115pgconfjapan)：PostgreSQLで毎秒10億レコードを処理するためにApache Arrowを利用
* [Vaex: A DataFrame with super strings | by Maarten Breddels | Towards Data Science](https://towardsdatascience.com/vaex-a-dataframe-with-super-strings-789b92e8d861)：Pythonのデータフレームの文字列処理の高速化にApache Arrowを利用
* [Powered by | Apache Arrow](https://arrow.apache.org/powered_by/)：Apache Arrow公式サイトに集められた利用例集


## クリアコードについて
クリアコードは、2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。クリアコードの目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。現在、Apache Arrow・Groonga・Fluentdといった様々なソフトウェアのコアメンテナが在籍しています。


### クリアコードのApache Arrowコンサルティングサポート
クリアコードでは、Apache Arrowによる大規模データ交換の効率化を目指すお客様や、Apache Arrowをどのように活用したらよいか相談に乗って欲しい、Apache Arrowを使っていて解決したい点があるお客様に対して、コンサルティングサポートを提供しています。

開発当初からプロジェクトに関わっている須藤をはじめ、FluentdやGroongaといったデータを扱う様々なOSS開発・メンテナンスに関わっている経験豊かなエンジニアが、Apache Arrowの活用だけではなく、実際の運用におけるデータ処理に関する課題や期待をヒアリングしたうえで、データ処理ツールの開発などを含めたコンサルティングサポートを提供します。まずは、お気軽にお問合せください。

### 参考URL
【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20220622-the-data-thread.md %}]({% link press-releases/20220622-the-data-thread.md %})

【関連サービス】[{{ site.url }}{% link services/apache-arrow.md %}]({% link services/apache-arrow.md %})

### 当リリースに関するお問合せ先
株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com
