---
layout: default
main_title: 会社紹介資料を更新しました。
sub_title:
type: topic
---

クリアコードの会社紹介資料を更新しました。
会社情報、沿革、事業の領域および概要紹介、また、これまでサポート実績のあるOSSなどを紹介している資料です。
興味のある方はぜひご覧ください。

資料は下記フォームからダウンロード可能です。

[{{ site.url }}{% link about/company-profile.md %}]({% link about/company-profile.md %})
