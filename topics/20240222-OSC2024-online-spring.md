---
layout: default
main_title: Open Source Conference 2024 Online/Springにて、Fluentdの新しいパッケージと最新動向を発表します。
sub_title:
type: topic
---

2024年3月1日（金）～2日（土）に開催される、Open Source Conference 2024 Online/Springにて、『Fluentd Update – Fluentdとパッケージの最新動向について』と題して発表します。

クリアコードでは2021年から、世界的にも多くのユーザーを誇るログ収集・転送オープンソースソフトウェア「Fluentd」のメンテナンス及びコミュニティ運営を中心となって行っています。OSC2022 Online/Springでは、「OSSを継続的にメンテナンスしていく仕組みづくり – Fluentdの事例とその最新情報」と題して、Treasure Data社中心だった開発から、コミュニティへの移行などについての発表を行いました。

Fluentdはプラグインアーキテクチャを採用しており、様々なサービスからログを収集したり、収集したログをデータベースや検索エンジンなどへ転送・保存するためのプラグインの開発などが継続的に行われています。
2年ぶりの登壇となる本セミナーでは、前回の発表時点からのアップデートを含めた最新のFluentdのトピック及び今後の展望などを紹介します。
2023年8月には、パッケージ名称の変更及びサポートサイクルの見直しが行われ、より長期のサポートを必要とするケースにあわせて、LTS(Long Term Support)版パッケージの提供も始まりました。また、長らく解決していなかったin_tailバグの修正などについて紹介を予定しています。

## 発表概要
* タイトル:『Fluentd Update – Fluentdとパッケージの最新動向について』
* 日時: 2024年3月1日(金) 14:00 〜 14:45
* 場所： C会場　（詳しくはイベントページをご確認ください）
* 発表資料:
  https://slide.rabbit-shocker.org/authors/kenhys/osc2024-online-spring-fluentd/
* 対象者:
  * Fluentdの最新情報を知りたい人 
  * Fluentd・ログ収集ソフトウェアを利用している・したことのある人 

* 前提知識:
  * 特筆すべき前提知識なし

* レベル：
  * 入門


## Open Source Conference 2022 Online Springイベント概要
* 日時: 2024年3月1日（金）～2日（土）
* 会場: オンライン会場（Zoom＆YouTube Live）
* 参加費: 無料
* web: https://event.ospn.jp/osc2024-online-spring
* 参加申し込み（connpass）: https://ospn.connpass.com/event/303178/
