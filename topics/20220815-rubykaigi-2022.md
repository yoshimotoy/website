---
layout: default
main_title: RubyKaigi 2022のシルバースポンサーになりました。今年は、二人が登壇します。
sub_title:
type: topic
---

クリアコードは、2022年9月8日(木)～10日（土）の間にオフラインとオンラインで同時開催される、[RubyKaigi 2022をシルバースポンサー](https://rubykaigi.org/2022/sponsors/#sponsor-333)として支援しています。


今回のイベントで、会期2日目9月9日(金)にFluentdのメンテナーを務める藤本誠二が[「How fast really is Ruby 3.x?」](https://rubykaigi.org/2022/presentations/fujimotos.html)と題して登壇します。また、会期3日目9月10日（土）にRubyのコミッターであり、Apache ArrowのPMC chairを務める須藤功平が[「Fast data processing with Ruby and Apache Arrow」](https://rubykaigi.org/2022/presentations/ktou.html)と題して登壇します。


参加をご検討の方は、下記公式ページをご確認ください。
RubyKaigi 2022公式：https://rubykaigi.org/2022/

画像の[ライセンス情報]({% link license/index.html %})

<p>
![]({% link /images/sidebar/rubykaigi-2022.svg %})
</p>




