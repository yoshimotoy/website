---
title: Fluent Package v5.2.0 リリース - ゼロダウンタイム・アップデート
author: daipom
tags:
  - fluentd
---

こんにちは。[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

2024年12月14日に Fluent Package v5.2.0 をリリースしました。

本リリースでは、ゼロダウンタイム・アップデート機能を追加しました。
これによって、今後のバージョンではリスタートやアップデートをゼロダウンタイムで実行できるようになります。

この記事では、メンテナーの観点から Fluentd の最新パッケージの動向を詳しく解説します。

<!--more-->

### 前置き: td-agent のEOLと Fluent Package

Fluent Package とは、コミュニティーが提供している Fluentd のパッケージです。

それまでコミュニティーが提供していた Fluentd のパッケージとして td-agent がありましたが、
td-agent は2023年12月31日限りでサポート終了となりました。

* [Drop schedule announcement about EOL of Treasure Agent (td-agent) 4](https://www.fluentd.org/blog/schedule-for-td-agent-4-eol)

Fluent Package は td-agent v4 の後継パッケージであり、ある程度の互換性を保っています。
多くのケースでは td-agent v4 から簡単にアップデート可能です。

また、今回リリースした v5.2.0 は通常版となりますが、並行してLTS版も提供しています。
新機能をすぐに使う必要がない、長期に渡って安定して使いたい、という場合はLTS版がおすすめです。

現在 td-agent をお使いの方は、ぜひ Fluent Package へのアップデートをご検討ください。

詳しくは、次の資料をご覧ください。

* 公式ブログ(英語): [Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)
* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド ]({% link downloads/fluent-package-lts-guide.md %})
* 弊社ブログ: [Fluent Package v5.0.2リリース - in_tailプラグインの重大な不具合の修正]({% post_url 2023-12-15-fluent-package-v5.0.2 %})
  * （前半で Fluent Package についての基本的な情報を紹介しています）

### Fluent Package v5.2.0

2024年12月14日に最新の通常版として Fluent Package v5.2.0 をリリースしました。
（現時点でのLTS版の最新は v5.0.5 となっています）

本バージョンでは、同梱する Fluentd のバージョンを v1.18.0 にアップデートしました。
Fluentd v1.18.0 では、ゼロダウンタイムで Fluentd をリスタートする機能を追加したり、他にも多数の機能改善を行っています。
これについては別記事[「Fluentd v1.18.0をリリース」]({% post_url 2024-12-05-fluentd-v1.18.0 %})で詳しく紹介していますので、そちらもぜひご覧ください。

Fluent Package v5.2.0 では、この Fluentd の新機能を活用し、ゼロダウンタイムでのパッケージアップデートをサポートしました。
（Windows版は本機能をサポートしていません）

なお、この新機能は、クリアコードがサポートしているお客様からのご要望に基づいて開発したものです。

本記事では、この新機能について詳しく紹介し、最後にその他の改善点についても簡単に紹介します。

### ゼロダウンタイム・アップデート / リスタート（設定リロード）

本バージョンから、Fluentdをゼロダウンタイムでリスタートしたり、パッケージをゼロダウンタイムでアップデートすることが可能になります。
リスタートとアップデートの双方を説明します。

#### ゼロダウンタイム・リスタート（設定リロード）

Fluentd v1.18.0 の新機能により、 Fluentd をゼロダウンタイムでリスタートすることが可能になりました。
これはつまり、Fluentdの動作を止めることなく設定のリロードを行える、ということです。

次の2通りの方法で、 Fluentd をゼロダウンタイムでリスタートすることができます。

* `SIGUSR2`をSupervisorプロセスへ送信
* [RPCサーバー](https://docs.fluentd.org/deployment/rpc)に`/api/processes.zeroDowntimeRestart`をリクエスト

ゼロダウンタイム・リスタートとは、 Fluentd がダウンする時間を生じさせず、リスタートを完遂する、ということです。
次の図のように、リスタートする際に新旧プロセスが並列して存在するため、ダウンする時間帯が生じません。

![zerodowntime-mechanism]({% link /images/blog/fluent-package-v5.2.0/zerodowntime-mechanism.png %})

ただし Fluentd の全ての機能がゼロダウンタイムというわけではありません。
厳密には、リスタートの際に次のプラグインのデータ受信が止まらない、ということになります。

* [in_udp](https://docs.fluentd.org/input/udp)
* [in_tcp](https://docs.fluentd.org/input/tcp)
* [in_syslog](https://docs.fluentd.org/input/syslog)

「他はダウンするんかい！」と思われた方もいらっしゃるかもしれませんが、ダウンされて困るのが基本的にこれらの受信なのです。

例えば、ログファイルからログを収集する[in_tail](https://docs.fluentd.org/input/tail)プラグインは、収集位置をファイルに記録しておくため、リスタートによって多少のダウンタイムが生じても問題にはなりません。
一方で、`in_udp`などのサーバー型のプラグインは事情が異なります。
こういったサーバー型のInputプラグインがダウンすると、その間クライアント側（例えば`syslog`など）のデータ送信が失敗することになります。
後で再送してくれれば問題ありませんが、クライアントに再送機能がない場合は、受信できなかったデータをロストすることになります。
そのため、これらのプラグインの受信を停止せずにリスタートできることが、 Fluentd にとってとても重要なことなのです。

「[in_forward](https://docs.fluentd.org/input/forward)と[in_http](https://docs.fluentd.org/input/http)は？」と思われたかもいらっしゃるかもしれません。
`in_forward`については、クライアントが[out_forward](https://docs.fluentd.org/output/forward)であれば再送できるため、問題になりません。
`in_http`は今後本機能に対応したいです。

その他のInputプラグインを本機能に対応させる方法については、別の記事で紹介予定です。

##### 従来のGraceful Reload機能（SIGUSR2）について

Fluentd は従来からGraceful Reload機能を持っており、`SIGUSR2`はそのためのシグナルでした。

この従来機能で、 Fluentd をリスタートせずに設定をリロードすることができました。
しかし、この機能には次のような制限事項や不具合があり、本番環境では使いにくいという問題がありました。

* [System設定](https://docs.fluentd.org/deployment/system-config)を反映することができない
* プラグインの実装次第ではうまく動かず、その判定が難しい
* [データロスの発生](https://github.com/fluent/fluentd/issues/2259)や[メモリリーク](https://github.com/fluent/fluentd/issues/3549)といった問題が報告されている

そのため、本バージョンから、Supervisorプロセスへの`SIGUSR2`を、ゼロダウンタイム・リスタート機能に置き換えることにしました。

なお、この従来のGraceful Reload機能は、以下の方法で引き続き使えます。

* Workerプロセスに直接`SIGUSR2`を送信
* RPCサーバーに従来通り`/api/config.gracefulReload`をリクエスト

### ゼロダウンタイム・アップデート

リスタートだけでなく、パッケージのアップデートもゼロダウンタイムで実行できるようにしました。

パッケージをアップデートする際には、当然Fluentdのリスタートが必要になります。
それにこの機能を使うことで、ゼロダウンタイムでパッケージを更新することができるわけです。

ただし、本機能を使うためには、更新元と更新先の双方のバージョンが本機能をサポートしている必要があります。
そのため、従来のバージョン（例えば Fluent Package v5.1.0）から本バージョンにアップデートする時には、本機能はまだ使えないのでご注意ください。

今後のパッケージのアップデートの仕方は大まかに次のようになります。

* 本機能を使わない => **サービスを停止してから** アップデートを行う
* 本機能を使う => **サービスを停止せずに** アップデートを行う

サービスを停止せずにアップデートし、アップデート後にゼロダウンタイム・リスタートが実行されることで、ゼロダウンタイムのアップデートが完了する、という仕組みです。

アップデート後のゼロダウンタイム・リスタートの実行のされ方には次の2通りがあり、後述する`FLUENT_PACKAGE_SERVICE_RESTART`設定で変更可能です。

* **auto** (デフォルト): アップデート後に**自動で**ゼロダウンタイム・リスタートを実行する
* **manual**: アップデート後に**手動で**ゼロダウンタイム・リスタートを実行する

デフォルトの`auto`では、アップデート後に自動でゼロダウンタイム・リスタートを実行します。
またこの際、ユーザーが追加したプラグインを自動検出して最新版を自動インストールします（オンライン環境であることが必要です）。

`manual`に変更した場合は、アップデート後の自動プラグインインストールや自動リスタートが行われないため、任意のタイミングでユーザーが（`SIGUSR2`を送信するなどして）ゼロダウンタイム・リスタートを実行します。

`manual`は、以下のような場合にご利用ください。

* オフライン環境（厳密には https://rubygems.org/ へアクセスできない環境）の場合
* リスタート前に一部のファイルを再配布・更新する必要がある場合
* 追加プラグインのバージョンを固定化したい場合

#### `FLUENT_PACKAGE_SERVICE_RESTART`設定

次の環境変数ファイルに環境変数として設定できます。

* RPM版: `/etc/sysconfig/fluentd`
* DEB版: `/etc/default/fluentd`

何も設定されていない場合も含め、デフォルトは`auto`です。

`manual`に変更する場合は、次のように設定します。

```
FLUENT_PACKAGE_SERVICE_RESTART=manual
```

この設定変更の反映にリスタートは不要です。
これは、アップデートの際にパッケージのスクリプトが環境変数を読み込むからです。

※ ユニットファイル(`/etc/systemd/system/fluentd.service`など)で`EnvironmentFile`をカスタマイズしている場合も、本設定は必ず上記ファイルパスに行ってください

#### ダウングレード

ダウングレードもゼロダウンタイムで可能です。

通常のアップデートと同様に、古いバージョンへ上書き更新する形で使えます。
（同様に、更新元と更新先の双方のバージョンが本機能をサポートしている必要があります）

例えば、「ゼロダウンタイムでバージョンアップしたが問題が見つかったためダウングレードしたい」という場合に、以下のように一連の処理をゼロダウンタイムで実施できます。

1. v5.2.0 から v5.2.1 (現在は未リリース) へアップグレード（ゼロダウンタイム）
1. 問題を発見
1. v5.2.1 から v5.2.0 へダウングレード(ゼロダウンタイム)

もしアップデートで問題が発生しても、Fluentdの動作を止めずにバージョンを戻すことができるわけです。

安心してアップデートできますね！

#### 注意: ユニットファイルの更新

ユニットファイル(`/etc/systemd/system/fluentd.service`など)を自前管理している場合は、本バージョンにアップデートする前に次の2行を削除してください。

```
Environment=GEM_HOME=/opt/...
Environment=GEM_PATH=/opt/...
```

これらの値が設定されていると、本機能が正しく動作しない可能性があります。
（これらの値はFluentdの動作に不要であり、本バージョンから削除されました）

### その他の改善

Fluent Package v5.2.0 では、ゼロダウンタイム・アップデート機能以外にもいくつかの改善を行っています。

* DEB版: `needrestart` の影響を受けないように改善
* `fluent-plugin-systemd`プラグイン: `SIGABORT`エラーの修正
* 細工されたデータを受信するとプロセスがメモリ不足で終了することがある問題を修正
* Windows版: `Fluent Package Command Prompt`で一部の環境変数がユーザー設定の影響を受けることがある問題を修正

詳しくは次をご覧ください。

* [Fluentd公式ブログ: fluent-package v5.2.0 has been released](https://www.fluentd.org/blog/fluent-package-v5.2.0-has-been-released)
* [弊社ブログ: debパッケージからneedrestartの挙動を抑制する方法]({% post_url 2024-12-26-how-to-control-needrestart-behavior %})

### まとめ

今回は Fluent Package v5.2.0 のリリース情報をお届けしました。

このように、 td-agent の後継パッケージである Fluent Package では、便利な新機能の追加や不具合・脆弱性の対応を行っております。
td-agent をご利用の方は、ぜひ Fluent Package へのアップデートをご検討ください。
今回の新機能をすぐに使いたい、という方は通常版へ、そうではない方はLTS版（安定版）へアップデートしていただくのがおすすめです。

詳しくは、次の資料をご覧ください。

* 公式ブログ(英語): [Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)
* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド ]({% link downloads/fluent-package-lts-guide.md %})
* 弊社ブログ: [Fluent Package v5.0.2リリース - in_tailプラグインの重大な不具合の修正]({% post_url 2023-12-15-fluent-package-v5.0.2 %})
  * （前半で Fluent Package についての基本的な情報を紹介しています）

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
実際に、今回紹介したゼロダウンタイム・アップデート機能は、クリアコードがサポートしているお客様からのご要望に基づいて開発したものです。

クリアコードの理念は、[自由ソフトウェア](https://www.gnu.org/philosophy/free-sw.ja.html)とビジネスの両立です([クリアコードとフリーソフトウェアとビジネス]({% post_url 2015-09-17-index %}))。
クリアコードは、お客様のあるなしに関わらず、Fluentdを含め様々な自由ソフトウェアの開発やメンテナンスを日々行っています。
しかし、会社として継続してフリーソフトウェアの発展に貢献していくためには、会社を継続すること、すなわちビジネスが必要です。
お客様にクリアコードのサービスをご利用いただくことで、
今回のようにお客様にとっての課題を解決しつつ、Fluentdをよりよいソフトウェアへと発展させることができます。

Fluent Package へのアップデート含め様々なサポートを提供しておりますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
