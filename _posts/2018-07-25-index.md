---
tags:
- feedback
title: IBus の最新版を使って問題の切り分けを行うには
---
### はじめに

ソフトウェアを使っていると、意図しない挙動に遭遇することがあります。
ソフトウェアによっては、ディストリビューションで配布されているものが最新とは限りません。
そのため、場合によってはアップストリームではすでに修正済みということもあります。[^0]
せっかく報告しても最新版ですでに直っていたりすると、悲しいですね。そんなことのないように最新版でも確認するのがおすすめです。
<!--more-->


そこで今回は `IBus` を例に、不可解な挙動に遭遇したときに最新版の `IBus`を使っても再現するかどうか確認する方法を紹介します。

### 問題の詳細について

Ubuntu 18.04上のibus-mozcを使っているときに、プロパティパネルから文字種を切り替えると、文字種の変更が追従しないことがあることに気づきました。
Xfce4のパネル1に表示されるアイコンをクリックしたときに表示されるメニューから文字種を変更したときにはそのような挙動はありません。[^1]

Xfce4のパネル1に表示される `IBus` のメニューとプロパティパネルに表示されるメニューはどちらも同じ `IBusProperty` で実装されているので基本的には挙動に違いはないはずです。そこで `IBus` の最新版で挙動を確認することにしました。

### `IBus` の最新版をビルドするには

まずは `IBus` が依存しているパッケージをインストールします。`build-dep`を使うと依存関係をまるごとインストールできるので便利です。

```console
$ sudo apt build-dep ibus
```


次に、 `IBus` が必要としているファイルをあらかじめダウンロードして配置します。[^2]

```console
$ sudo mkdir -p /usr/share/unicode/ucd
$ wget https://www.unicode.org/Public/UNIDATA/NamesList.txt
$ wget https://www.unicode.org/Public/UNIDATA/Blocks.txt
$ sudo cp NamesList.txt Blocks.txt /usr/share/unicode/ucd
```


次に、 `IBus` のソースコードを入手して、`./autogen.sh`を実行します。

```console
$ git clone https://github.com/ibus/ibus.git
$ cd ibus
$ ./autogen.sh
```


最後に、`make` を実行した後にインストールします。

```console
$ make
$ sudo make install
```


これで `/usr/local` 以下へと `IBus` の最新版がインストールされました。

### 最新版のIBusを動かす手順

パッケージで `IBus` をすでに導入している場合、古いライブラリーを見に行ってしまうことがあります。
そこであらかじめ、ライブラリーのパスを環境変数にて指定しておきます。

```console
$ export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBLARY_PATH
```


これで準備ができたので最新版の `IBus` を試せるようになりました。

次のコマンドを実行すると、`ibus-daemon` を起動できます。

```console
$ /usr/local/bin/ibus-daemon -x -r
```


今回追試したいのはパネル1上に表示されるメニューの挙動です。
それを有効にするには、次のコマンドを実行します。

```console
$ /usr/local/libexec/ibus-ui-gtk3
```


これでパネル1に表示されるメニューの挙動を確認できるようになりました。
実際に試したところ、最新版でも同様の問題を抱えていたため、アップストリームに報告しておきました。

  * [Property panel status doesn't sync correctly](https://github.com/ibus/ibus/issues/2026)

### まとめ

今回は `IBus` の最新版を使って問題の切り分けを行う方法を紹介しました。

[^0]: 細かいことをいうと、ディストリビューションが配布しているバージョンで特別にパッチを当てることで対応している場合もあります。

[^1]: 実装が異なるibus-anthyやibus-skkにはこの問題はないようです。

[^2]: この作業を忘れるとautogen.shを実行したときにconfigureでエラーになります。
