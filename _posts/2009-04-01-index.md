---
tags:
- mozilla
- test
- uxu
title: UxUで外部テキストエディタを使う時のおすすめ設定
---
みなさん、テストしてますか？（挨拶）
<!--more-->


[UxU](/software/uxu/)では、テスト失敗時に表示されるスタックトレースからテキストエディタを起動することができます。この時、利用するテキストエディタがコマンドライン引数による行指定に対応していれば、エラーが発生した行を直接開いて編集できます。きちんと設定しておけば、テストを実行して、編集して、またテストして、といったサイクルで開発を進められるので非常に便利です。

以下に、有名なテキストエディタ向けの設定の例をいくつか挙げてみました。UxUの設定ダイアログの「MozUnitテストランナー」タブでエディタ起動用のコマンドとして入力してください。（エディタの実行ファイルのパスは必要に応じて読み替えてください）

<dl>






<dt>






秀丸エディタ






</dt>






<dd>


"C:\Program Files\Hidemaru\Hidemaru.exe" /j%L,%C "%F"


</dd>








<dt>






TeraPad






</dt>






<dd>


"C:\Program Files\TeraPad\TeraPad.exe" /j=%L "%F"


</dd>








<dt>






サクラエディタ






</dt>






<dd>


"C:\Program Files\sakura\sakura.exe" "%F" -X=%C -Y=%L


</dd>








<dt>






EmEditor






</dt>






<dd>


"C:\Program Files\EmEditor\EmEditor.exe" /l %L /cl %C "%F"


</dd>








<dt>






xyzzy






</dt>






<dd>


"C:\Program Files\xyzzy\xyzzycli.exe" -l "%F" -g %L -c %C


</dd>








<dt>






萌エディタ






</dt>






<dd>


"C:\Program Files\moeditor\moe.exe" "%F" -m %L,%C


</dd>








<dt>






gedit






</dt>






<dd>


/usr/bin/gedit +%L "%F"


</dd>








<dt>






Vim（vi）






</dt>






<dd>


/usr/bin/vim +%L "%F"


</dd>








<dt>






Emacs






</dt>






<dd>


/usr/bin/gnuclient +%L "%F"


</dd>


</dl>

なお、%Lは行番号、%Cは列番号、%Fはファイルのパスへと、それぞれ自動的に置換されます。
