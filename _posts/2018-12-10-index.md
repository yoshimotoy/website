---
tags:
  - apache-arrow
  - presentation
title: 'Apache Arrow東京ミートアップ2018 - Apache Arrow #ArrowTokyo'
---
[Apache Arrow東京ミートアップ2018](https://speee.connpass.com/event/103514/)を主催したした須藤です。会場提供・飲食物提供など[Speee](https://speee.jp/)さんにいろいろ協力してもらいました。ありがとうございます。
<!--more-->


私はApache Arrow本体のことを網羅的に紹介しました。データの配置のことなど[日本OSS推進フォーラム アプリケーション部会 第10回勉強会]({% post_url 2018-12-05-index %})では触れなかった技術的な詳細についても紹介しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/apache-arrow-tokyo-meetup-2018/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/apache-arrow-tokyo-meetup-2018/" title="Apache Arrow">Apache Arrow</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/apache-arrow-tokyo-meetup-2018/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/apachearrowtokyomeetup2018)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-apache-arrow-tokyo-meetup-2018)

### 集まりの目的

この集まりは勉強会ではありません。勉強をする集まりではなく開発者を増やす集まりです。開発対象のプロダクトはApache Arrowだけでなく、Apache Arrow以外でもデータ処理に関わるプロダクトであればなんでもOKです。

そのため参加枠は次の2つにしました。

  * 開発に参加したい気持ちがある枠

  * 開発に参加したい気持ちがなくはない枠

開発に参加する気がある人だけが参加する集まりということです。

### 目的（開発者を増やす）の実現方法

「開発者を増やす」という目的を実現するために時間の使い方を次の2つにわけました。

  * 前半：開発を始めるための情報提供の時間

  * 後半：開発を始める時間

後半では「開発の1歩目を踏み出せる」ことを目指しました。ポイントは「1歩目」です。この集まりの時間内で「バリバリ開発する」を目指していないということです。この集まりの時間で「なにに取り組もうか」が決まれば十分です。取り組むと決めたことに実際に取り組み始められればなおよしです。

「何に取り組もうか」を決めるために必要そうな情報を前半に提供しました。

#### 前半：開発を始めるための情報提供の時間

前半では、まず私がApache Arrowの関する全体的な情報を提供しました。今回はApache Arrowを軸にいろいろなプロダクトの開発者を増やしたかったので、どのプロジェクトでも参考になりそうな予備知識としてApache Arrowの情報を最初に提供しました。

その後は次のテーマごとに詳しい人から情報を提供しました。適切な人に情報提供してもらえて本当によかったなぁと思っています。

  * Apache Spark

    * 資料：[Apache Arrow and Pandas UDF on Apache Arrow](https://www.slideshare.net/ueshin/apache-arrow-and-pandas-udf-on-apache-spark)

    * 情報提供者：[上新さん](https://twitter.com/ueshin/)（Apache SparkのコミッターでApache SparkのApache Arrow対応の開発にも参加）

  * [Multiple-Dimension-Spread](https://github.com/yahoojapan/multiple-dimension-spread)

    * 資料：[Multiple-Dimension-Spread と Apache Arrow](https://www.slideshare.net/techblogyahoo/multipledimensionspread-apache-arrow)

    * 情報提供者：[井島さん](https://github.com/koijima)（Multiple-Dimension-Spreadの開発者でApache Arrow対応部分も実装）

  * R

    * 資料：[RとApache Arrow](https://speakerdeck.com/yutannihilation/rtoapache-arrow)

    * 情報提供者：[湯谷さん](https://twitter.com/yutannihilation)（日本のR界隈で一番前からApache Arrowのことを調べている）

    * ブログ：[Apache Arrow東京ミートアップ2018で「RとApache Arrow」について話してきました - Technically, technophobic.](https://notchained.hatenablog.com/entry/2018/12/09/001907)

  * Ruby

    * 資料：[RubyとApache Arrow](https://speakerdeck.com/hatappi/rubytoapache-arrow)

    * 情報提供者：

      * [畑中さん](https://twitter.com/hatappi)（Red Data ToolsメンバーとしてRuby用のデータ処理ツールを開発）

      * [橋立さん](https://twitter.com/joker1007)（Ruby用ログ収集基盤のFluentdでApache Arrowを活用するプラグインを開発）

    * ブログ：[Apache Arrow東京ミートアップ2018で「RubyとApache Arrow」というタイトルで発表してきました！！！ - hatappi.blog](https://blog.hatappi.me/entry/2018/12/09/002707)

  * Python

    * 資料：[PythonとApache Arrow](https://speakerdeck.com/sinhrks/pythontoapache-arrow-eaf72479-ce30-4161-8c73-15b555cc56c7)

    * 情報提供者：[堀越さん](https://github.com/sinhrks)（pandas・Daskのコミッター）

  * テンソル

    * 資料：[Tensor and Apache Arrow](https://speakerdeck.com/mrkn/tensor-and-arrow)

    * 情報提供者：[村田さん](https://twitter.com/mrkn)（数値計算に詳しくApache Arrowの疎なテンソルを実装中）

みなさんには次の2点を話してくださいとお願いしていました。

  * 現状のまとめ

  * 今後こうなるとよさそう

「今後こうなるとよさそう」は「開発の1歩目」のヒントになることを期待していました。

私も新しく知ったことが多くあったのでみなさんにお願いして本当によかったなぁと思っています。Rubyまわりを開発している人視点で言うと、特にRとPythonの情報は興味深かったです。RubyでもRのALTREP・pandasのExtensionArrayのようなものが必要になるときは来るのだろうか、とか。

#### 後半：開発を始める時間

前半で開発を始めるための情報を得られたので後半では実際に開発を始める時間です。

次のグループにわかれてグループの人と相談しながら各人が「まずはなにに取り組むか」を決めていきます。

  * Apache Arrow（Multiple-Dimension-Spread・Ruby関連を含む）

  * Apache Spark

  * Python

  * R

各グループには詳しい人たちがいるのでその人たちと相談しながら「なにに取り組むか」を決めます。私は各人に「まずはなにに取り組むか決まりましたかー？」と聞いてまわったのですが、だいたいみなさん決められたようでした。最初の1歩を踏み出せましたね。

すでにpull requestを出して2歩目・3歩目を踏み出せている人たちもいるのでいくつかリンクを置いておきますね。

  * [ARROW-3962: [Go] Support null value while reading CSV #3129](https://github.com/apache/arrow/pull/3129)

  * [ARROW-3964: [Go] Refactor examples of csv reader #3131](https://github.com/apache/arrow/pull/3131)

  * [ARROW-3961: [Python/Documentation] Fix wrong path in the pyarrow README #3127](https://github.com/apache/arrow/pull/3127)

  * [Apache Arrow Meetup行ってきた結果OSS活動の一歩を踏み出した - Diary over Finite Fields](https://blog.515hikaru.net/entry/2018/12/10/005937)

### まとめ

勉強会ではなく「開発者を増やす」ことを目的にした集まりを開催しました。この集まりをきっかけに開発に参加した人がいたのでよい集まりだったなぁと思っています。「開発者を増やす」ことを目的にした集まりを開催したい人は参考にしてください。

参加した人たちには集まりの最後にアンケートに答えてもらいました。アンケートの結果は[GitHubのspeee/code-party/apache-arrow-tokyo-meetup-2018/feedback/](https://github.com/speee/code-party/tree/master/apache-arrow-tokyo-meetup-2018/feedback)から参照できるので、同様の集まりを開催したい人はこちらも参考にしてください。

引き続き開発に参加していきましょう！

なお、クリアコードはApache Arrowを活用して[データ処理ツールの開発をしたい]({% post_url 2018-07-11-index %})のでデータ処理ツールを開発していてApache Arrowを活用したい方は[お問い合わせ](/contact/?type=data-processing-tool)ください。また、一緒にデータ処理ツールの開発をしたい人も募集しているのでわれこそはという方は[ご応募](/recruitment/)ください。
