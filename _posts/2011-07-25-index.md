---
tags:
- ruby
title: 伝えることを伝えること
---
しばらく人前で話す予定がないので、講演者が本当に話したかったスピーチを残しておきます。
<!--more-->


実は、[日本Ruby会議2011で話したこと]({% post_url 2011-07-19-index %})は[札幌Ruby会議02で話したこと]({% post_url 2009-12-07-index %})と[札幌Ruby会議03で話しかけたこと]({% post_url 2010-12-08-index %})の続きでした。話の流れも札幌Ruby会議02と同じ流れにしました。一番伝えたいことは話しの真ん中に持っていき、その後に実例を伝えるという流れです。

札幌Ruby会議のころは「伝えること」について考えていました。

私が[プログラミングを始めたのは大学のころ](http://www.ogis-ri.co.jp/otc/hiroba/others/OORing/interview39.html)[^0]で、世間で活躍しているプログラマーよりだいぶ遅いです。でも、プログラミングが好きでたくさんプログラムを書いてきました。今では、プログラミングをはじめて[10年くらい経ち、だいぶ上手く](http://route477.net/d/?date=20110718#p01)なってきました。でも、このままだとよくないなぁと思うようになりました。

私は独学でプログラミングを学んできました。本を読んだりコードを書いたり、他の人のよいところを盗んだり。でも、他の人も同じようにやる必要はないなぁと思うようになりました。自分がよいとわかったことを厳選して伝えられて、その分、他の人がもっと別のことをできるなら、そっちの方がいいなぁと思ったのです[^1]。

そのため、札幌では自分がプログラミングで大事だと思っていることを伝えようとしました。

それから、普段でもいろいろ伝えてみました[^2]。
そしたら、伝えるだけでも足りないと思うようになりました。「魚をあげるんじゃなくて、魚の捕り方を教えなよ」では足りないのです。これでは、魚を捕れる人は増えるかもしれませんが、「魚の捕り方を教えなよ」という人は増えないのではないかと思うようになりました。「伝えること」だけではなく「伝えることを伝えること」をしなければいけないのではないか。

「伝えることを伝えること」は日本Ruby会議2011では落とした話題ですが、拾ってくれた人がいて嬉しかったです[^3]。いつかリベンジしたい話題です。

まだ、「伝えること」も伝えきれていないので、「伝えることを伝えること」はもう少し先の話になると思いますが、実現できたらいいなぁと思っています。まずは、クリアコード内で「伝えること」をまとめて、ここで公開しようとしているところです[^4]。公開したものが誰かの役にたつといいなぁ。

日本Ruby会議2011のことはもう書いたので、ここに書くことは技術的な話に戻そう[^5]と思っていました。でも、日本Ruby会議2011関連のWeb日記を読んでいたら、「書いておかないと」と思ったので、書いてしまいました[^6]。

[^0]: 今、ざっと読みなおしてみたら札幌で話したこととか練馬で話したことと同じことを話していました。変わっていないですね。

[^1]: ソフトウェアに関してはプログラミングをはじめたころからそんな風に思っていた気がします。自分がよいと思ったソフトウェアがあったら他の人にも「これを使いなよ」と渡せるとよいなぁと思います。だから、自由に使えるソフトウェアが好きなんだと思います。

[^2]: そして、いろいろうまくいきませんでした。

[^3]: 参考文献を教えてもらいました。手元に用意したので、そっちも少し調べてみようと思います。

[^4]: 自由に利用できるようにする予定です。

[^5]: RDocとYARDとI18Nの話とか。

[^6]: ここでは「思いました。」という風には書かないようにしているのですが、Ruby会議関連の話題のときだけは勝手に「特別に使ってよい」ことにしています。
