---
tags:
- mozilla
title: Mozilla Firefox ESR60でのPolicy Engineによるポリシー設定の方法と設定項目のまとめ
---
FirefoxはESR60以降のバージョンにおいて、法人利用者向けに設定を集中管理する新しい仕組みである「Policy Engine」が導入・有効化されています。Policy Engineで可能な設定の一部は従来のMCD（AutoConfig）と重複していますが、Policy Engineには従来型アドオンによるカスタマイズの代替手段としての性質もあり、MCDではできなかった設定も存在しています。
<!--more-->


過去の記事（[第一報]({% post_url 2018-02-19-index %})、[グループポリシーについての続報]({% post_url 2018-03-23-index %})）において、Firefox 60のベータ版を対象として設定の手順やその時点で使用できるようになっていた設定項目を紹介してきましたが、この度のFirefox ESR60正式版のリリースに伴い、改めて最新の状況について情報を整理します。

### ポリシー設定を反映する2つの方法

WindowsでPolicy Engineを使って設定を集中管理する方法には、以下の2通りがあります。

  * ポリシー定義ファイル（`policies.json`）を使う

  * Active Directoryのグループポリシーを使う（Windowsのみ）

#### `policies.json`を使う

この方法を使う場合には、各クライアントPC上のFirefoxのインストール先フォルダ（`C:\Program Files\Mozilla Firefox` や `C:\Program Files (x86)\Mozilla Firefox` など）配下に`distribution`という名前でフォルダを作成し、さらにその中に`policies.json`という名前で以下の内容のテキストファイル（JSON形式）を設置します。

```javascript
{
  "policies": {
    （ここにポリシー設定を記述する）
  }
}
```


例えば、後述の`"BlockAboutAddons"`と`"BlockAboutConfig"`を両方とも設定する場合は以下の要領です。

```javascript
{
  "policies": {
    "BlockAboutAddons": true,
    "BlockAboutConfig": true
  }
}
```


この方法の利点は、ファイルを配置する事さえできればLinux版およびmacOS版（ファイルは`Firefox.app`をディレクトリと見なして、その配下に設置する事になります）においても使用できるという点です。

欠点は、設定ファイルを各クライアントに適切に配置しなくてはならないという事と、この方法で設定しただけでは充分に機能しない設定があるという事です。一部の設定項目は、期待通りの結果を得るためには後述のグループポリシーで設定する必要があります。

本記事では、各設定項目の指定はこちらの方法で設定する想定で解説します。

#### Active Directoryのグループポリシーを使う

Windows版Firefoxでは、Active Directoryのグループポリシーを使ってPolicy Engineの設定を制御することができます。[Mozillaの外部プロジェクトとして公開されているポリシーテンプレート](https://github.com/mozilla/policy-templates/releases)[^0]をダウンロードして展開し、ドメインコントローラの `C:\Windows\SYSVOL\domain\Policies\PolicyDefinitions` 配下に各ファイルを配置すると、グループポリシーの管理画面上で「コンピューターの構成」および「ユーザーの構成」の「管理用テンプレート」配下にFirefox向けの設定項目が表示されるようになります。各設定項目の働きは、`policies.json`で設定可能な項目に対応しています。

Firefoxの設計上は、同じポリシーがユーザー向けとコンピューター向けの両方に設定されていた場合、ユーザーに設定されたポリシーよりもコンピューターに設定されたポリシーの方が優先的に反映されます。例えば「Block About Config」というポリシーについて、ユーザー向けのポリシーで「有効」と設定していても、コンピューターのポリシーで「無効」と設定していると、このポリシーは「無効」と扱われます。基本的にはコンピューター向けのポリシーのみ設定して運用するとよいでしょう。

なお、2018年5月14日時点では、ポリシーテンプレートはWindows Server 2008以降で使用できるadmx形式のファイルのみが提供されています[^1]。Windows 2003 ServerをドメインコントローラとしてActive Directoryを運用している場合には、上記のポリシーテンプレートを参考にadm形式のポリシーテンプレートを作成する必要があります。ポリシーテンプレートを用意できない場合は、`policies.json`を各クライアントに設置する方法を取らなくてはなりません。

この方法の利点は、ドメインコントローラ上での操作のみで管理を完結できるという事です。また、こちらの方法で設定した場合には、全ての設定項目が期待通りに機能するというメリットもあります。

欠点は、当然ですが、Active Directoryでドメインを運用中でないと利用できないという点です。ドメインを構築していないような小規模の組織においては、`policies.json`を使う他ないという事になります。

### Firefox ESR60で使用できる設定項目

Policy Engineで設定可能な項目はいくつかのカテゴリに分類できます。以下、カテゴリごとにご紹介します。

  * 2018年11月30日：Firefox ESR60.4までのバージョンでの以下のポリシー設定の追加・変更点について追記しました。

    * `AppUpdateURL`（自動更新の制御）

    * `Authentication.AllowNonFQDN`（シングルサインオンを許可するための設定）

    * `Certificates.Install`（証明書のインポート）

    * `HardwareAcceleration`（様々な機能の無効化）

    * `Homepage.StartPage`（ホームページの設定）

    * `Permissions`（Webサイトに与える様々な権限の設定）

    * `RequestedLocales`（UIの初期状態）

    * `SearchEngines.Remove`（検索エンジンの設定）

    * `SecurityDevices`（セキュリティデバイスの設定）

  * 2019年5月15日：Firefox ESR60.6.3までのバージョンでの以下のポリシー設定の追加・変更点について追記しました。

    * `3rdparty`（アドオンの制御）

    * `CaptivePortal`（ネットワーク関係の設定）

    * `DefaultDownloadDirectory`（UIの初期状態）

    * `DNSOverHTTPS`（ネットワーク関係の設定）

    * `DownloadDirectory`（UIの初期状態）

    * `ExtensionSettings.*.blocked_install_message`（アドオンの制御）

    * `ExtensionUpdate`（自動更新の制御）

    * `FirefoxHome`（新規タブページの設定）

    * `LocalFileLinks`（ネットワーク関係の設定）

    * `NetworkPrediction`（ネットワーク関係の設定）

    * `NewTabPage`（新規タブページの設定）

    * `OverridePostUpdatePage`（ホームページの設定）

    * `Preferences`（その他の細々とした設定）

    * `PromptForDownloadLocation`（UIの初期状態）

    * `SupportMenu`（UIの初期状態）

    * `SSLVersionMax`（ネットワーク関係の設定）

    * `SSLVersionMin`（ネットワーク関係の設定）

#### シングルサインオンを許可するための設定

シングルサインオンに関する設定としては、以下の設定が可能です。
設定は全て`"Authentication"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "Authentication": {
      // SPNEGO認証を許可するサイトのリスト。
      // （MCDでの network.negotiate-auth.trusted-uris に相当）
      "SPNEGO": ["www.example.com", "http://www.example.org/", ...],

      // 認証情報の委任を許可するサイトのリスト。
      // （MCDでの network.negotiate-auth.delegation-uris に相当）
      "Delegated": ["www.example.com", "http://www.example.org/", ...],

      // NTLM認証を許可するサイトのリスト。
      // （MCDでの network.automatic-ntlm-auth.trusted-uris に相当）
      "NTLM": ["www.example.com", "http://www.example.org/", ...],

      // 確認証方式でFQDN以外の使用を許可する
      // （Firefox ESR60.2以降）
      "AllowNonFQDN": {
        // SPNEGO認証でFQDN以外の使用を許可
        // （MCDでの network.negotiate-auth.allow-non-fqdn に相当）
        "SPNEGO": true,

        // NTLM認証でFQDN以外の使用を許可
        // （MCDでの network.automatic-ntlm-auth.allow-non-fqdn に相当）
        "NTLM":   true
      }
    },

    ...
  }
}
```


`"SPNEGO"`、`"Delegated"`、および`"NTLM"`の各設定は、必要な物だけを設定します。不要な設定は省略して構いません。

#### 自動更新の制御

自動更新の制御に関する設定には、以下の項目があります。

```javascript
{
  "policies": {
    ...

    // Firefoxの自動更新を禁止する。
    "DisableAppUpdate": true,

    // Firefoxの更新情報の取得先URL
    // （Firefox ESR60.2以降）
    // （MCDでの app.update.url に相当）
    "AppUpdateURL": "https://example.com/update.xml",

    // アドオンの自動更新を禁止する
    // （Firefox ESR60.6.2以降）
    // （MCDでの extensions.update.enabled に相当）
    "ExtensionUpdate": false,

    // システムアドオンの更新を禁止する。
    "DisableSystemAddonUpdate": true,

    ...
  }
}
```


#### `about:` で始まるURIのページへのアクセスを禁止

URIが`about:` で始まる特別なページのうちいくつかは、以下の設定でアクセスを禁止する事ができます。

```javascript
{
  "policies": {
    ...

    // about:addons（アドオンマネージャ）の表示を禁止する。
    // アドオンの設定変更、有効・無効の切り替え、インストール、
    // アンインストールなどの操作を全面的に禁止することになる。
    "BlockAboutAddons": true,

    // about:config（設定エディタ）の表示を禁止する。
    // 設定画面に現れない詳細な設定の変更を禁止することになる。
    // 副作用として、ブラウザコンソールの入力欄も強制的に無効化される。
    "BlockAboutConfig": true,

    // about:profiles（プロファイル管理画面）の表示を禁止する。
    // プロファイルを指定しての起動、アドオンを無効にしての起動などを
    // 禁止することになる。
    "BlockAboutProfiles": true,

    // about:support（トラブルシューティング情報）の表示を禁止する。
    // アドオンを無効にしての起動などを禁止することになる。
    "BlockAboutSupport": true,

    ...
  }
}
```


#### 初期ブックマーク項目の作成

ポリシー設定を使って、初期状態で任意のブックマーク項目やフォルダを登録済みの状態にする事ができます。

```javascript
{
  "policies": {
    ...

   // 初期状態で登録しておく追加のブックマーク項目の定義。任意の個数登録可能。
   "Bookmarks": [
     { // ブックマークツールバーにブックマークを作成する場合
       "Title":     "example.com",
       "URL":       "http://www.example.com/",
       "Favicon":   "http://www.example.com/favicon.ico",
       "Placement": "toolbar"
     },

     { // ブックマークツールバーの中にフォルダを作って、その中にブックマークを作成する場合
       "Title":     "example.com",
       "URL":       "http://www.example.com/",
       "Favicon":   "http://www.example.com/favicon.ico",
       "Placement": "toolbar",
       "Folder":    "ツールバー初期項目"
     },

     { // ブックマークメニューにブックマークを作成する場合
       "Title":     "example.org",
       "URL":       "http://www.example.org/",
       "Favicon":   "http://www.example.org/favicon.ico",
       "Placement": "menu"
     },

     { // ブックマークメニューの中にフォルダを作って、その中にブックマークを作成する場合
       "Title":     "example.org",
       "URL":       "http://www.example.org/",
       "Favicon":   "http://www.example.org/favicon.ico",
       "Placement": "menu",
       "Folder":    "メニュー初期項目"
     },

     ...
   ],

   // Firefox自体の初期ブックマーク項目を作成しない。
   // （「よく見るページ」などのスマートブックマーク項目を除く）
   // この設定は初回起動時・新規プロファイル作成時にのみ反映され、
   // 既に運用中のプロファイルには反映されない（作成済みの初期ブックマーク項目は削除されない）。
   "NoDefaultBookmarks": true,

    ...
  }
}
```


#### 証明書のインポート

ルート証明書の自動インポートに関しては、以下の設定が可能です。
設定は全て`"Certificates"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "Certificates": {
      // 管理者によってシステムに配布されたルート証明書をFirefoxの証明書データベースにインポートする。
      // （WindowsのActive Directoryのグループポリシーで配布されたルート証明書をインポートする）
      "ImportEnterpriseRoots": true,

      // Firefoxの証明書データベースにインポートするルート証明書のファイル名
      // （Firefox ESR60.4以降）
      "Install": [
        "example1.pem",
        "example2.crt",
        ...
      ]
    },

    ...
  }
}
```


`"ImportEnterpriseRoots"`による自動インポートは、[Windowsのレジストリ上の特定の位置に存在するルート証明書（Active Directoryのグループポリシーで配布された証明書）]({% post_url 2017-06-01-index %})のみが対象となります。
ユーザーが自分の権限で証明書データベースに登録したルート証明書はインポートされません。

Firefox ESR60.4以降のバージョンでは、Active Directoryを運用していない環境やWindows以外の環境でも、ファイルからルート証明書を自動インポートさせることができます。

  * Windows:

    * `%AppData%\Mozilla`（`C:\Users\(ユーザー名)\AppData\Roaming\Mozilla\Certificates`）

    * `%LocalAppData%\Mozilla`（`C:\Users\(ユーザー名)\AppData\Local\Mozilla\Certificates`）

  * macOS

    * `/Users/(username)/Library/Application Support/Mozilla/Certificates`

    * `/Library/Application Support/Mozilla/Certificates`

  * Linux

    * `/home/(username)/.mozilla/certificates`

    * `/user/lib/mozilla/certificates`

上記のいずれかの位置にPEM形式の証明書ファイルを設置し、ファイル名を`"Install"`に列挙しておくと、それらの証明書がFirefoxの証明書データベースへ自動的にインポートされます。

#### Cookieの保存の可否と有効期限

Cookieに関する設定は、以下の物があります。
設定は全て`"Cookies"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "Cookies": {
      // 指定のWebサイト（オリジンで指定）でCookie、IndexedDB、
      // Web Storage、およびService Worker用Cacheを保存する。
      // （任意に無効化はできない）
      "Allow": ["http://example.com", "https://example.org:8080", ...],

      // 指定のWebサイト（オリジンで指定）でCookie、IndexedDB、
      // Web Storage、およびService Worker用Cacheを保存する。
      // （任意に無効化はできない）
      // また、これらのホストに保存済みのCookieがあった場合、それらは削除される。
      "Block": ["http://example.com", "https://example.org:8080", ...],

      // サードパーティCookieかどうかに関わらず、全てのCookieを一律で拒否する。
      "Default": false,

      // サードパーティによるCookieの受け入れ可否。
      // 以下の3つの中からいずれかを指定する。
      //  * "always":       全て受け入れる。
      //  * "never":        全く受け入れない。
      //  * "from-visited": 既に保存済みのサードパーティCookieがある場合のみ受け入れる。
      // （MCDでの network.cookie.cookieBehavior に相当）
      "AcceptThirdParty": "always",

      // 全てのCookieについて「Firefoxを終了するまでを有効期限とし、
      // Webサイトが指定した保持期限を無視する。
      // （MCDでの network.cookie.lifetimePolicy に相当）
      "ExpireAtSessionEnd": true,

      // Cookieに関する設定のユーザーによる変更を禁止する。
      "Locked": true
    },

    ...
  }
}
```


#### 様々な機能の無効化

機能の無効化に関する設定には、以下の項目があります。

```javascript
{
  "policies": {
    ...

    // PDF.jsを無効化する。
    // （MCDでの pdfjs.disabled に相当）
    "DisableBuiltinPDFViewer": true,

    // 開発ツールを無効化する。
    // メニュー項目、キーボードショートカットの両方とも無効化する。
    // （MCDでの devtools.policy.disabled および
    //   devtools.chrome.enabled に相当）
    "DisableDeveloperTools": true,

    // フィードバックの送信、詐欺サイトの誤検出の報告を禁止する。
    "DisableFeedbackCommands": true,

    // FirefoxアカウントとSyncを無効化する。
    "DisableFirefoxAccounts": true,

    // Firefox Screenshotsを無効化する。
    "DisableFirefoxScreenshots": true,

    // Firefoxの新機能のテストへの参加（ https://support.mozilla.org/ja/kb/shield ）を禁止する。
    "DisableFirefoxStudies": true,

    // 「忘れる」ボタンを無効化する。
    "DisableForgetButton": true,

    // フォームの入力履歴の保存とオートコンプリートを無効化する。
    // （MCDでの browser.formfill.enable に相当）
    "DisableFormHistory": true,

    // マスターパスワードの設定を禁止する。
    "DisableMasterPasswordCreation": true,

    // Pocketを無効化する。
    // （MCDでの extensions.pocket.enabled に相当）
    "DisablePocket": true,

    // プライベートブラウジング機能の使用を禁止する。
    "DisablePrivateBrowsing": true,

    // 他のブラウザからのブックマークや設定のインポート機能を無効化する。
    // （ただし、ESR60時点では初回起動時の設定インポートは無効化されない）
    "DisableProfileImport": true,

    // しばらくぶりにFirefoxを起動した際のプロファイルのリフレッシュ操作（データの消去）の提案と、
    // about:support（トラブルシューティング情報）からの手動でのリフレッシュ操作を無効化する。
    // （MCDでの browser.disableResetPrompt に相当）
    "DisableProfileRefresh": true,

    // クラッシュからの復帰時と自動更新の時以外で、
    // セーフモードでの起動（アドオンを無効化しての起動）を禁止する。
    // ただし、コマンドライン引数でのセーフモードまで無効化するにはグループポリシーでの設定が必要。
    "DisableSafeMode": true,

    // セキュリティ警告を敢えて無視することによる危険な操作の実行を、完全に禁止する。
    "DisableSecurityBypass": {
      // 証明書の警告の例外を登録するボタンを隠して、
      // 例外を絶対に登録できないようにする。
      // （MCDでの security.certerror.hideAddException に相当）
      "InvalidCertificate": true,

      // 詐欺サイト等の警告における「危険性を無視」リンクを隠して
      // コンテンツを絶対に表示できないようにする。
      // （MCDでの browser.safebrowsing.allowOverride=false に相当）
      "SafeBrowsing": true
    },

    // 画像をコンテキストメニューからデスクトップの壁紙に設定することを禁止する。
    "DisableSetDesktopBackground": true,

    // 統計情報の収集・送信を禁止する。
    // （MCDでの datareporting.healthreport.uploadEnabled および
    //   datareporting.policy.dataSubmissionEnabled に相当）
    "DisableTelemetry": true,

    // 起動時に既定のブラウザにするかどうかを確認しない。
    // （MCDでの browser.shell.checkDefaultBrowser に相当）
    // 既定のブラウザに設定できなくする設定、ではないことに注意。
    "DontCheckDefaultBrowser": true,

    // パスワードマネージャによるパスワードの保存を禁止する。
    // （MCDでの signon.rememberSignons に相当）
    "OfferToSaveLogins": false,

    // ハードウェアアクセラレーションを無効化する。
    // （Firefox ESR60.2以降）
    // （MCDでの layers.acceleration.disabled に相当）
    "HardwareAcceleration": false,

    ...
  }
}
```


`DisableSafeMode`のみ、`policies.json`で設定した場合とグループポリシーで設定した場合の効果に差異があり、セーフモードでの起動をより広範に禁止したい場合にはグループポリシーで設定する必要があります。
何らかの理由でグループポリシーを使用できないものの、セーフモードでの起動をより広範に禁止したいという場合は、レジストリを編集してグループポリシーが使われている状況を再現する必要があります。
具体的には、`HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Mozilla\Firefox`または
`HKEY_CURRENT_USER\SOFTWARE\Policies\Mozilla\Firefox`に`DWORD`型で `DisableSafeMode`という名前の値を作成し、データを`1`に設定して下さい。

[![（実際にレジストリに情報を登録した状態）]({{ "/images/blog/20180512_0.png" | relative_url }} "（実際にレジストリに情報を登録した状態）")]({{ "/images/blog/20180512_0.png" | relative_url }})

#### UIの初期状態

FirefoxのUIの初期状態を変更する設定としては、以下の項目があります。

```javascript
{
  "policies": {
    ...

    // 初期状態でブックマークツールバーを表示する。
    "DisplayBookmarksToolbar": true,

    // 初期状態でメニューバーを表示する。
    "DisplayMenuBar": true,

    // 検索ツールバーをロケーションバーと統合するかどうか。
    //  * "unified":  統合する（Firefox ESR60以降の初期状態）
    //  * "separate": 分離する（Firefox ESR52以前の初期状態）
    "SearchBar": "separate",

    // UIに使用するロケール
    // （Firefox ESR60.3以降）
    // http://l10n.mozilla-community.org/webdashboard/ に列挙されている言語名を指定し、
    // その中で言語パックがすでにインストール済みの物が使われる。
    // （MCDでの intl.locale.requested に相当）
    "RequestedLocales": [
      "ja",
      "en-US",
      ...
    ],

    // ファイルの既定のダウンロード先フォルダ
    // （Firefox ESR68以降）
    // （MCDでの browser.download.dir に相当）
    "DefaultDownloadDirectory": "（フォルダのパス）",

    // ファイルの固定のダウンロード先フォルダ
    // （Firefox ESR68以降）
    // （MCDでの browser.download.dir に相当）
    "DownloadDirectory": "（フォルダのパス）",

    // 常にダウンロード先フォルダを尋ねる
    // （Firefox ESR68以降）
    // （MCDでの browser.download.useDownloadDir=false に相当）
    "PromptForDownloadLocation": true,

    // 「ヘルプ」メニュー配下から辿れる独自のサポートページへのリンク
    // （Firefox ESR60.6.2以降）
    "SupportMenu": {
      "Title":     "タイトル",
      "URL":       "https://example.com/",
      "AccessKey": "H"
    },

    ...
  }
}
```


`"DisplayBookmarksToolbar"`、`"DisplayMenuBar"`、および`"SearchBar"`については、ユーザーが任意に状態を変更した場合はそちらが優先されます。
（強制的にその状態に固定する、ということはできません。）

#### プライバシー情報の取り扱いに関わる設定

プライバシー情報に関する設定は、以下の項目があります。
トラッキング防止機能の設定は`"EnableTrackingProtection"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "EnableTrackingProtection": {
      // トラッキング防止機能の有効無効。
      //  * true:  有効（トラッキングを防止する）
      //  * false: 無効（トラッキングを許容する）
      // （MCDでの privacy.trackingprotection.enabled および
      //   privacy.trackingprotection.pbmode.enabled に相当）
      "Value": false,

      // トラッキング防止機能の、ユーザーによる設定変更の可否。
      //  * true:  ユーザーの設定変更を禁止（設定を固定する）
      //  * false: ユーザーの設定変更を許可
      "Locked": true
    },

    // Firefox終了時に、その都度履歴やCookieなどのプライバシー情報を全て消去する。
    // （MCDでの privacy.sanitize.sanitizeOnShutdown に相当）
    "SanitizeOnShutdown": true,

    ...
}
```


#### アドオンの制御

アドオンの制御に関する設定には、以下の項目があります。
管理者によるアドオンのインストールやアンインストールの制御は`"Extensions"`配下に、ユーザーによるインストールの可否に関する設定は`"InstallAddonsPermission"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "Extensions": {
      // 自動的にインストールするアドオンのXPIファイルの位置。
      // （"InstallAddonsPermission"配下の"Default"がfalseであってもインストールされる）
      "Install": [
        // URLでの指定
        "https://example.com/my-addon.xpi",
        // ファイルパスでの指定
        "C:\\Path\\To\\XPI\\File\\my-another-addon.xpi",
        ...
      ],

      // インストールされていた場合に自動的にアンインストールするアドオンのID。
      "Uninstall": [
        "my-legacy-addon@example.com",
        "my-obsolete-addon@example.org",
        ...
      ],

      // アンインストール、無効化を禁止するアドオンのID。
      // （設定の変更は可能）
      "Locked": [
        "my-security-addon@example.com",
        "my-privacy-addon@example.org",
        ...
      ]
    },

    // アドオンのインストールの可否に関する制御。
    "InstallAddonsPermission": {
      // アドオンのインストール時に警告を行わないページ。
      // （オリジンで指定、`https`のみ有効）
      "Allow": ["https://example.com", "https://example.org:8080", ...],

      // ユーザーによるアドオンのインストールを禁止する。
      // （MCDでの xpinstall.enabled に相当）
      "Default": false
    },

    // アドオンごとの固有の設定
    "ExtensionSettings": {
      "example@example.com": { // アドオンのIDを指定
        // インストールをブロックした時の特別なメッセージ
        // （Firefox ESR68以降）
        "blocked_install_message": "このアドオンは使用禁止です"
      }
    }

    // アドオンごとのManaged Storage
    // （Firefox ESR68以降）
    "3rdparty": {
      "Extensions": {
        "ieview-we@clear-code.com": { // アドオンのIDを指定
          "ieapp": "C:\\Program Files (x86)\\Internet　Explorer\\iexplore.exe",
          "ieargs": ""
        }
      }
    }

    ...
}
```


`"Extensions"`の`"Install"`で指定したアドオンは、実行時のユーザープロファイル内にインストールされます。`"Extensions"`の`"Uninstall"`も、ユーザープロファイル内にあるアドオンが対象となります。

すべてのユーザーで常に特定のアドオンがインストールされた状態としたい場合は、[特定のフォルダにファイルを設置することによるサイドローディング](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Alternative_distribution_options/Sideloading_add-ons#Installation_using_the_standard_extension_folders)でインストールする必要があります。

これらの方法でインストールおよびアンインストールしたアドオンは、Firefox ESR60.4以降のバージョンであればポリシーの再設定によって再インストールさせることができます。
それ以前のバージョンでは、一度アンインストールしたアドオンをこの方法で再インストールさせることはできません。

#### Adobe Flashプラグインの制御

Flashプラグインの制御については、以下の項目があります。
設定は全て`"FlashPlugin"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "FlashPlugin": {
      // 指定のWebサイト（オリジンで指定）でFlashを
      // Click to Play無しで自動実行する。
      // （任意に無効化はできない）
      "Allow": ["http://example.com", "https://example.org:8080"],

      // 指定のWebサイト（オリジンで指定）でFlashの実行を禁止する。
      // （任意に無効化はできない）
      "Block": ["http://example.com", "https://example.org:8080"],

      // 初期状態でClick to Play無しで自動実行するかどうか。
      // （MCDでの plugin.state.flash に相当、未定義でClick to Play）
      "Default": true,

      // 設定の変更を許容するかどうか
      // （MCDでの plugin.state.flash の固定に相当）
      "Locked": true
    },

    ...
}
```


#### ホームページの設定

起動時に表示されるページの設定には、以下の項目があります。
ホームページに関する設定は全て`"Homepage"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "Homepage": {
      // ホームページのURI（複数タブを設定する場合は最初のタブの内容）
      // （MCDでの browser.startup.homepage および
      //    に対応）
      "URL": "http://example.com",

      // ホームページを複数設定する場合の、2番目以降のタブの内容
      "Additional": ["https://example.org:8080", ...],

      // Firefoxの毎起動時の初期状態
      // （Firefox ESR60.4以降）
      // 以下のいずれかを指定する。
      //   "none"：            空のページを表示（MCDでの browser.startup.page=0 に対応）
      //   "homepage"：        ホームページを表示（MCDでの browser.startup.page=1 に対応）
      //   "previous-session"：前回終了時の状態を復元（MCDでの browser.startup.page=3 に対応）
      // Firefox ESR60.4以前のバージョンでは、"URL" が設定されていると強制的に
      // "StartPage":"homepage" に相当する状態となります。
      "StartPage": "homepage",

      // これらの設定のユーザーによる変更を禁止する
      "Locked": true
    },

    // 初回起動後に1回だけ表示されるページのURL。
    // （MCDでの startup.homepage_welcome_url に相当）
    "OverrideFirstRunPage": "http://www.example.com/",

    // 更新後に1回だけ表示されるページのURL。空にすると、タブを開かない。
    // （MCDでの startup.homepage_override_url に相当）
    "OverridePostUpdatePage": "http://www.example.com/",

    ...
}
```


#### 新規タブページの設定

新しい空のタブを開いた時の内容の設定には、以下の項目があります。

```javascript
{
  "policies": {
    ...

    // 新規タブを完全な空白ページにする
    // （Firefox ESR68以降）
    // （MCDでの browser.newtabpage.enabled に相当）
    "NewTabPage": false,

    // Activity Streamの制御
    "FirefoxHome": {
      // 検索窓を非表示にする
      // （Firefox ESR68以降）
      // （MCDでの browser.newtabpage.activity-stream.showSearch に相当）
      "Search": false,

      // 「トップサイト」を非表示にする
      // （Firefox ESR68以降）
      // （MCDでの browser.newtabpage.activity-stream.feeds.topsites に相当）
      "TopSites": false,

      // 「ハイライト」を非表示にする
      // （Firefox ESR68以降）
      // （MCDでの browser.newtabpage.activity-stream.feeds.section.highlights に相当）
      "Highlights": false,

      // 「トップストーリー」を非表示にする
      // （Firefox ESR68以降）
      // （MCDでの browser.newtabpage.activity-stream.feeds.section.topstories に相当）
      "Pocket": false,

      // 「スニペット」を非表示にする
      // （Firefox ESR68以降）
      // （MCDでの browser.newtabpage.activity-stream.feeds.snippets に相当）
      "Snippets": false,

      // ユーザーによる設定の変更を禁止する。
      // （Firefox ESR68以降）
      "Locked": true
    },

    ...
}
```


#### ポップアップブロックの制御

ポップアップブロック機能の制御に関しては、以下の設定項目があります。
設定は全て`"PopupBlocking"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "PopupBlocking": {
      // window.open() によるポップアップを確認なしで許可するWebサイト。
      // オリジンで指定し、任意に無効化はできない。
      "Allow": ["http://example.com", "https://example.org:8080"],

      // ポップアップブロック機能を初期状態で無効化する。
      // （MCDでの dom.disable_open_during_load に相当）
      "Default": false,

      // ユーザーによる設定の変更を禁止する。
      "Locked": true
    },

    ...
}
```


#### Webサイトに与える様々な権限の設定

ポップアップブロックやアドオンのインストール可否以外の、Webサイトに与える様々な権限の設定には、以下の項目があります。

```javascript
{
  "policies": {
    ...

    "Permissions": {

      // Webカメラ
      // （Firefox ESR60.2以降）
      "Camera": {
        // Webカメラの権限要求無しで使用を許可するWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Allow": ["https://example.com", "https://example.org:8080", ...],

        // Webカメラの使用を禁止し、権限の確認もさせないWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Deny": ["https://example.com", "https://example.org:8080", ...],

        // Webカメラの使用を初期状態で禁止し、権限の確認もしないようにする。
        // （true はMCDでの permissions.default.camera=2 に、
        //   false は permissions.default.camera=0 に相当）
        "BlockNewRequests": true,

        // BlockNewRequestsの設定をユーザーが変更して上書きする事を禁止する。
        "Locked": true
      },

      // マイク
      // （Firefox ESR60.2以降）
      "Microphone": {
        // マイクの権限要求無しで使用を許可するWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Allow": ["https://example.com", "https://example.org:8080", ...],

        // マイクの使用を禁止し、権限の確認もさせないWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Deny": ["https://example.com", "https://example.org:8080", ...],

        // マイクの使用を初期状態で禁止し、権限の確認もしないようにする。
        // （true はMCDでの permissions.default.microphone=2 に、
        //   false は permissions.default.microphone=0 に相当）
        "BlockNewRequests": true,

        // BlockNewRequestsの設定をユーザーが変更して上書きする事を禁止する。
        "Locked": true
      },

      // 位置情報
      // （Firefox ESR60.2以降）
      "Location": {
        // 位置情報の権限要求無しで使用を許可するWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Allow": ["https://example.com", "https://example.org:8080", ...],

        // 位置情報の使用を禁止し、権限の確認もさせないWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Deny": ["https://example.com", "https://example.org:8080", ...],

        // 位置情報の使用を初期状態で禁止し、権限の確認もしないようにする。
        // （true はMCDでの permissions.default.geo=2 に、
        //   false は permissions.default.geo=0 に相当）
        "BlockNewRequests": true,

        // BlockNewRequestsの設定をユーザーが変更して上書きする事を禁止する。
        "Locked": true
      },

      // デスクトップ通知
      // （Firefox ESR60.2以降）
      "Notifications": {
        // デスクトップ通知の権限要求無しで使用を許可するWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Allow": ["https://example.com", "https://example.org:8080", ...],

        // デスクトップ通知を禁止し、権限の確認もさせないWebサイト。
        // オリジンで指定し、任意に無効化はできない。
        "Deny": ["https://example.com", "https://example.org:8080", ...],

        // デスクトップ通知を初期状態で禁止し、権限の確認もしないようにする。
        // （true はMCDでの permissions.default.desktop-notification=2 に、
        //   false は permissions.default.desktop-notification=0 に相当）
        "BlockNewRequests": true,

        // BlockNewRequestsの設定をユーザーが変更して上書きする事を禁止する。
        "Locked": true
      }

    },

    ...
}
```


#### ネットワーク関係の設定

ネットワーク接続、通信に関する設定は以下の項目があります。

```javascript
{
  "policies": {
    ...

    // 認証が必要なWiFiアクセスポイントを検出した時に、認証用のタブを自動的に開かない
    // （Firefox ESR60.6.2以降）
    // （MCDでの network.captive-portal-service.enabled に相当）
    "CaptivePortal": false,

    // DNS over HTTPSの設定
    // （Firefox ESR68以降）
    "DNSOverHTTPS": {
      // DNS over HTTPSを有効にする
      // （true はMCDでの network.trr.mode=2 に、
      //   false は network.trr.mode=5 に相当）
      "Enabled": true,

      // DNS over HTTPSの問い合わせ先
      // （MCDでの network.trr.uri に相当）
      "ProviderURL": "https://mozilla.cloudflare-dns.com/dns-query".

      // DNS over HTTPSの設定をユーザーが変更して上書きする事を禁止する。
      "Locked": true
    },

    // DNSプリフェッチを無効化する
    // （Firefox ESR60.6.2以降）
    // （MCDでの network.dns.disablePrefetch=true,
    //   network.dns.disablePrefetchFromHTTPS=true に相当）
    "NetworkPrediction": false,

    // リンク先としてローカルファイルの読み込みを許可するページ
    // （MCDでの capability.policy.policynames=localfilelinks_policy,
    //   capability.policy.localfilelinks_policy.checkloaduri.enabled=allAccess,
    //   capability.policy.localfilelinks_policy.sites=(スペース区切りのURL列) に相当）
    "LocalFileLinks": [
      "http://example.com",
      "http://example.net",
      ...
    ],

    // 使用を許可するSSL/TLSの最大バージョン
    // （Firefox ESR60.5.3以降）
    // （MCDでの security.tls.version.max=1～4 に相当）
    "SSLVersionMax": "tls1.3", // "tls1", "tls1.1", "tls1.2", "tls1.3" のいずれか

    // 使用を許可するSSL/TLSの最小バージョン
    // （Firefox ESR60.5.3以降）
    // （MCDでの security.tls.version.min=1～4 に相当）
    "SSLVersionMin": "tls1", // "tls1", "tls1.1", "tls1.2", "tls1.3" のいずれか

    ...
  }
}
```


#### プロキシの設定

プロキシに関する設定は以下の項目があります。
設定は全て`"Proxy"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "Proxy": {
      // プロキシを使用するかどうか。
      //  * "none":       プロキシを使用しない。
      //  * "system":     システムで設定されたプロキシ設定に従う。
      //                  （Windowsであれば「インターネットオプション」の設定）
      //  * "autoDetect": 接続しているネットワークのDHCPで通知されたWPADの設定に従う。
      //  * "autoConfig"  以降の項目で指定されたPACファイルを使用する。
      //  * "manual":     以降の項目での個別の設定に従う。
      // （MCDでの network.proxy.type に相当）
      "Mode": "none",

      // "Mode":"autoConfig"の時に使用するPACファイルの取得先URL。
      // （MCDでの network.proxy.autoconfig_url に相当）
      "AutoConfigURL": "file://///fileserver/proxyl.pac",

      // HTTPで使用するプロキシのホストとポート番号。
      // （MCDでの network.proxy.http と
      //   network.proxy.http_port に相当）
      "HTTPProxy": "192.168.0.1:50080",

      // SSL/TLSで使用するプロキシのホストとポート番号。
      // （MCDでの network.proxy.ssl と
      //   network.proxy.ssl_port に相当）
      "SSLProxy": "192.168.0.1:50443",

      // FTPで使用するプロキシのホストとポート番号。
      // （MCDでの network.proxy.ftp と
      //   network.proxy.ftp_port に相当）
      "FTPProxy": "192.168.0.1:50022",

      // SOCKSで使用するプロキシのホストとポート番号。
      // （MCDでの network.proxy.socks と
      //   network.proxy.socks_port に相当）
      "SOCKSProxy": "192.168.0.1:51080",

      // SOCKSのバージョン。4または5のみ有効。
      // （MCDでの network.proxy.socks_version に相当）
      "SOCKSVersion": 4,

      // HTTPのプロキシを他の全てのプロトコルに使用する。
      // （MCDでの network.proxy.share_proxy_settings に相当）
      "UseHTTPProxyForAllProtocols": true,

      // プロキシを使用しないホスト。
      // （MCDでの network.proxy.no_proxies_on に相当）
      "Passthrough": "localhost,127.0.0.1,...",

      // DNSへの問い合わせにもプロキシを使用する。
      // （MCDでの network.proxy.socks_remote_dns に相当）
      "UseProxyForDNS": true,

      // プロキシに自動的にログインする。
      // （MCDでの signon.autologin.proxy に相当）
      "AutoLogin": true,

      // ユーザーによる設定の変更を禁止する。
      "Locked": true
    },

    ...
}
```


#### 検索エンジンの設定

設定は全て`"SearchEngines"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "SearchEngines": {
      // 初期状態で検索バー用に登録しておく検索エンジンのリスト。
      "Add": [
        {
          "Name":               "検索エンジンの表示名",
          "IconURL":            "https://example.com/favicon.ico",
          "Alias":              "ex",
          "Description":        "検索エンジンの説明文",
          "Method":             "GET",
          // POSTメソッドの場合の送信内容
          // （Firefox ESR68以降）
          "PostData":           "q={searchTerms}&...",
          "URLTemplate":        "https://example.com/search?q={searchTerms}",
          "SuggestURLTemplate": "https://example.com/suggest?q={searchTerms}"
        },
        ...
      ],

      // 削除する検索エンジンのリスト。
      // （Firefox ESR60.2以降）
      "Remove": [
        "削除する検索エンジンの表示名1",
        "削除する検索エンジンの表示名2",
        ...
      ],

      // 指定した名前の検索エンジンを規定の検索エンジンにする。
      "Default": "Google",

      // 検索エンジンの追加を禁止する。
      "PreventInstalls": true
    },

    ...
}
```


検索エンジンは、ESR60.xではHTTP Methodが`GET`である物のみ使用可能です（ESR68以降からは`POST`も使用可能です）。

検索語句は、Firefox ESR60.3.1以降では常にUTF-8でエンコードされます。それ以前のバージョンでは常に[CP1252](https://ja.wikipedia.org/wiki/Windows-1252)となる不具合（[Bug 1463684](https://bugzilla.mozilla.org/show_bug.cgi?id=1463684)）があるため、残念ながら日本語の検索クエリは文字化けしてしまい検索できません。日本語の検索を行う前提であれば、Firefox ESR60.3.1以降は必須という事になります。

`"Alias"`は、検索エンジンに設定するキーワードです。キーワードが設定されている検索エンジンは、ロケーションバーで`ex 検索したい語句`のようにキーワードに続けて語句を入力する形でも呼び出せるようになります。

Firefox ESR60.1以前のバージョンでは、一度インストールされた検索エンジンの設定はポリシー設定を変えても反映されません（[Bug 1453350](https://bugzilla.mozilla.org/show_bug.cgi?id=1453350)）。文字化けの件と併せても、検索エンジン関係のポリシー設定はFirefox ESR60.3.1以降でのみ実用に足ると言えます。

#### セキュリティデバイスの設定

ポリシー設定を使うと、ICカードリーダーなどをFirefoxから使用するために必要なセキュリティデバイス（を使用するために必要なライブラリ）の登録を行えます。
設定は全て`"SecurityDevices"`配下に定義します。

```javascript
{
  "policies": {
    ...

    // 自動的に登録するセキュリティデバイス名と、
    // ライブラリのフルパスの一覧
    // （Firefox 60.4以降）
    "SecurityDevices": {
      "デバイス名1": "C:\\Path\\To\\Library.dll",
      "デバイス名2": "/path/to/library.so",
      ...
    },

    ...
}
```


#### 簡易的なアクセス制限

設定は全て`"WebsiteFilter"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "WebsiteFilter": {
      // ブロックする対象のURLを示すマッチパターン。
      // （詳細は https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Match_patterns を参照）
      // ESR60時点では、http:またはhttps:で始まるサイトのみが対象で、
      // 最大1000項目まで指定可能。
      "Block": [
        "https://twitter.com/*",
        "https://*.twitter.com/*",
        ...
      ],

      // ブロック対象の例外とするURLを示すマッチパターン。
      "Exceptions": [
        "https://twitter.com/about",
        ...
      ]
    },

    ...
}
```


#### その他の細々とした設定

いくつかの設定項目については、MCDの設定名をそのまま使用して設定を固定できます。
設定は全て`"Preferences"`配下に定義します。

```javascript
{
  "policies": {
    ...

    "Preferences": {
      // 国際化ドメインを必ずpunycode形式で表示する
      // （Firefox ESR68以降）
      "network.IDN_show_punycode": true,

      // ロケーションバーへの入力内容が1単語のみの時、検索を実行する前にローカルドメインのホスト名として名前解決を試みる
      // （Firefox ESR68以降）
      "browser.fixup.dns_first_for_single_words": true,

      // ディスクキャッシュの格納先フォルダ
      // （Firefox ESR68以降）
      "browser.cache.disk.parent_directory": "C:\\Path\\To\\Folder",

      // ロケーションバーの候補に現在開かれているタブを表示しない
      // （Firefox ESR68以降）
      "browser.urlbar.suggest.openpage": false,

      // ロケーションバーの候補に履歴項目を表示しない
      // （Firefox ESR68以降）
      "browser.urlbar.suggest.history": false,

      // ロケーションバーの候補にブックマーク項目を表示しない
      // （Firefox ESR68以降）
      "browser.urlbar.suggest.bookmark": false
    },

    ...
}
```


### まとめ

以上、Firefox ESR60から使用可能となったPolicy Engineによるポリシー設定について、ESR60.0時点での状況を解説しました。

Policy Engine機能の実装はFirefox 59のリリース以後から急ピッチで進められたため、Firefox ESR60.0の時点では機能不足や不具合がまだまだ目立つ状況と言わざるを得ません。ただ、通常Firefoxのセキュリティアップデートはセキュリティに関わる変更のみが反映されますが、Policy EngineもESR60も法人利用者のための機能である事から、Policy Engineの不具合は、今後ESR60のセキュリティアップデートの中で修正されていく可能性もあります[^2]。最新の動向に注意しながら使っていきましょう。

ちなみに、ESR60時点で実装されているポリシー設定のうち、以下の機能はクリアコード在籍のエンジニアによって実装されました。
日本での法人利用の中で大きな需要があるポリシー設定については、今後も積極的に実装の提案を行っていきたいと考えています。

  * [1440573 - Policy: Disable safe mode](https://bugzilla.mozilla.org/show_bug.cgi?id=1440573) セーフモードの無効化機能。

  * [1440574 - Policy: Disable commands to send feedback](https://bugzilla.mozilla.org/show_bug.cgi?id=1440574) フィードバック送信系メニューの無効化機能。

  * [1440578 - Policy: "block" under "cookies" should clear already-stored cookies](https://bugzilla.mozilla.org/show_bug.cgi?id=1440578) Cookieを保存しないよう設定されたホストについて、保存済みのCookieがある場合はそれを自動消去する機能。

[^0]: 現時点では言語リソースは英語用のみが提供されています。

[^1]: Windows Server 2003ドメインでもこのポリシーテンプレートで行われた設定は有効ですが、ドメインコントローラはWindows Server 2008以降のバージョンである必要があります。

[^2]: <a href='https://mike.kaply.com/2018/05/09/an-enterprising-future/'>Policy Engineの開発を主導していた方によるPolicy Engineの紹介記事</a>でも、ESR60のセキュリティアップデートの中でポリシーの追加や修正を行っていくことが予告されています。実際に、<a href='https://hg.mozilla.org/releases/mozilla-esr60/log/tip/browser/components/enterprisepolicies/Policies.jsm'>ESR60向けのブランチにおけるポリシーの実装ファイルの変更履歴</a>を見ると、ESR60のリリース以後も様々な変更が反映されています。
