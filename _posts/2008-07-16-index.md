---
tags:
- cutter
- test
title: Cutter 1.0.3リリース
---
昨日、C言語用の単体テストフレームワークである
[Cutter](http://cutter.sourceforge.net/)の1.0.3がリリースされま
した。
<!--more-->


実は、[Cutter-1.0リリース]({% post_url 2008-05-21-index %})から3回リリースしていま
す。1.0.0以降はマイクロバージョンだけを上げていますが、新しく
追加された機能はマイクロとは思えません。例えば、Windows
([MinGW](http://ja.wikipedia.org/wiki/MinGW))でのビルド
に対応、[GStreamer](http://ja.wikipedia.org/wiki/GStreamer)
のサポートなどといった機能が含まれていました。過去のリリースに
ついては[NEWS](http://cutter.sourceforge.net/reference/ja/news.html)
を見てください。

### Cutterとは

Cutterはテストの書きやすさ・テスト結果からのデバッグのしやす
さを重視したC言語用の単体テストフレームワークです。今回のリリー
スから[Cutterの機能を説明したページ](http://cutter.sourceforge.net/reference/ja/features.html)を用意
しました。

### データ駆動テスト対応

同じテストを条件を変えて実行したい時があります。例えば、以下
のような場合です。

  * 複数の入力パターンがあり、それらを網羅的にテストする場合
  * 複数のバックエンドを抽象化し、どのバックエンドを利用して
    いる場合でも同じインターフェイスで扱えるライブラリをテス
    トする場合（Cでのcairo、Perl/Ruby/GaucheなどでのDBI、
    RubyでのActiveRecordなど）

このような場合、必要な分だけテストコードをコピー&ペーストして
テストを作成するよりも、以下のように書けるとテスト記述・管理
のコストを下げることができます。

  * テストは1つだけ用意
  * テスト条件、つまり、入力データを複数用意
  * 各入力データに対してそれぞれテストを実行

このようなテストの方法をデータ駆動テストと呼びます。

データ駆動テストではデータの用意の仕方にはいくつかの方法があ
り、それぞれ利点があります。

<dl>






<dt>






データベースに保存された入力データを利用






</dt>






<dd>


大量のデータを用意したり、データを一括変更できるなどデー
タ管理機能が豊富


</dd>








<dt>






CSVなど表形式の入力データを利用






</dt>






<dd>


Excelなどを利用して入力データを用意することができる


</dd>








<dt>






プログラム内で入力データを生成






</dt>






<dd>


動的にデータを用意するので、柔軟にデータを生成すること
ができる。例えば、文字'a', 'b', 'c'を使って作られる長さ
が3の文字列すべて（"abc", "acb", ...）、などというデータ
を用意できる。


</dd>


</dl>

Cutterでは今回のリリースで、最後の「プログラム内で入力データ
を生成」する方法をサポートしました。使い方は以下の通りです。

  * data_XXX(void)を定義
  * data_XXX()中でcut_add_data()を使ってデータを登録
  * test_XXX(const void *data)を定義
    * dataにはcut_add_data()で登録したデータの1つが渡る

今までどおり、関数を定義するだけでよく、他のC言語用の単体テ
ストフレームワークにあるような「登録処理」のようなことは必要
ありません。Cutterが自動で見つけてくれます。

コードにすると以下のようになります。

{% raw %}
```c
void
data_XXX(void)
{
    cut_add_data("データ1の名前", data1, data1_free_function,
                 "データ2の名前", data2, data2_free_function,
                 "データの例", strdup("test data"), free,
                 ...)
}

void
test_XXX(const void *data)
{
    /* dataはdata_XXX()で登録した「data1」か「data2」
       か「strdup("test data")」。test_XXX()はそれぞれに対
       して1回ずつ、計3回呼ばれる。
     */
    cut_assert_equal_string("test data", data);
}
```
{% endraw %}

具体例は
[cut_add_data()](http://cutter.sourceforge.net/reference/ja/cutter-cutter.html#cut-add-data)
を見てください。

### まとめ

Cutter 1.0.3ではデータ駆動テストをサポートし、より簡単にテス
トがかけるようになりました。
