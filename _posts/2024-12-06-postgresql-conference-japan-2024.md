---
tags:
- apache-arrow
title: "PostgreSQL Conference Japan 2024 - ADBC: Connecting PostgreSQL with Analytics #pgcon24j"
author: kou
---

[PostgreSQL Conference Japan 2024](https://www.postgresql.jp/jpug-pgcon2024)で[ADBCの話（リンク先にはベンチマークスクリプトや実行結果もあるよ）](https://gist.github.com/lidavidm/0752ac1d42dde6dbd785b9dd791be387)をした[@lidavidm](https://github.com/lidavidm)のお手伝いをした須藤です。

<!--more-->

### 内容

[ADBC（Arrow DataBase Connectivity）](https://arrow.apache.org/adbc/)はODBCのApache Arrowバージョンみたいなやつです。ODBCは接続先のデータベースの詳細を（ほとんど）気にせずに同じAPIでデータベースを使える仕組みです。ADBCも同じように接続先のデータベースの詳細を（ほとんど）気にせずに同じAPIでデータベースを使える仕組みです。

違いは何かというと、データをどのように扱うかです。ODBCは行ベースで扱い、ADBCは列ベースで扱います。もう少し言うと、ADBCはApache Arrowを使います。

ADBCがなにを目指しているかというと、分析アプリケーションが高速かつ便利にデータベースにアクセスできるようになることです。これを実現するために重要なところがApache Arrowです。Apache Arrowは標準化されているので、すでに各種分析アプリケーションが対応しています。ADBCはデータをApache Arrow形式で扱うので、各種分析アプリケーションはデータベースから取得したデータをそのまま使えます。そのまま使えると便利ですし、変換が不要なので速いです。

既存の類似実装よりも効率的な実装になっているので、今後は分析アプリケーション用にデータベースにアクセスしたいときはADBCを使うといいですよ！

ADBCは各種データベースごとにドライバーというものを用意してデータベース固有の処理を実装しています。PostgreSQL用のドライバーが用意されているのでPostgreSQLに対してもADBCを使えます。そこのドライバーが効率的な実装になっているのです。たとえば、`SELECT`の結果をやりとりするところでは、普通に`SELECT`をしてその結果をパースするのではなく、`COPY (SELECT ...) TO STDOUT (FORMAT binary)`にして`COPY`のバイナリーフォーマットでやりとりするようにしています。

さらに高速にするために`COPY`のフォーマットを拡張可能にすることに取り組んでいたりします。これが実現されるとバイナリーフォーマットではなく、直接Apache ArrowデータをADBCでやりとりできるのでさらに高速になるはずです！

### まとめ

PostgreSQL Conference Japan 2024で[@lidavidm](https://github.com/lidavidm)が話したADBCの話を紹介しました。
