---
title: Debianパッケージ利用調査に参加するとどうなるのか？
tags:
  - debian
---
### はじめに

Debianを対話的にインストールすると、Debianパッケージ利用調査への参加の可否がインストール途中で確認されます。
<!--more-->


今回は、このDebianパッケージ利用調査とはどういう仕組みになっているのか、また、どのようなデータが送られることになっているのかについて説明します。

### popularity-contestの仕組み

Debianパッケージ利用調査では、どんなパッケージがよく使われているのかを定期的にサーバーに送信するようになっています。
この役割を担っているのが、popularity-contestパッケージに含まれるプログラムです。

popularity-contestを有効にすると、cronで一定期間ごとにデータを収集して、[popconサーバー](http://popcon.debian.org/cgi-bin/popcon.cgi) へgzip圧縮したデータを送信するようになっています。

このときHTTP経由でデータを送信 [^0] しますが、あらかじめこのデータは専用のGPG鍵（/usr/share/popularity-contest/debian-popcon.gpg）を使って暗号化しているものを送信しており、秘匿性を担保するようになっています。
あらかじめ、`tor` パッケージをインストールしていると、送信時に `Tor` を経由させることもできます。

なお既定ではこの送信する頻度は7.5日に1回です。こうして集められたデータは、 https://popcon.debian.org/ にて公開されています。

### popularity-contestで送られるデータとは

popularity-contestの仕組みがわかったところで、実際にどんな情報が送られているのでしょうか。

実際に送られている情報は次のコマンドで収集しています。[^1]

```
/usr/sbin/popularity-contest --su-nobody
```


ここで収集したデータは、以下のフォーマットで出力されます。

```
POPULARITY-CONTEST-0 TIME:1597988034 ID:(HOSTID) ARCH:amd64 POPCONVER:1.70 VENDOR:Debian
...
1597968000 1588723200 libprotobuf22 /usr/lib/x86_64-linux-gnu/libprotobuf.so.22.0.4
...
END-POPULARITY-CONTEST-0 TIME:1597988093
```


上記をみるとなんとなく想像がつくかと思いますが、次のデータが含まれています。

  * 最終アクセス時刻

  * 最終変更時刻

  * パッケージ名

  * ファイル名

プライバシーの観点から、最終アクセス時刻は丸められています。

(HOSTID)は実際には自動的に生成された128bitのUUIDが含まれます。
ここで使用されるHOSTIDは `/etc/popularity-contest.conf` をみるとわかります。

注意すべきはカスタムパッケージをインストールしている場合（例えば、自社でパッケージングしたものなど）、popconデータに含まれます。
いまのところ、特定のパッケージだけ除外するような設定は用意されていません。

### すぐに試してみるには

通常は不必要に送信しないようになっているので、 `/var/lib/popularity-contest/lastsub` に記録されている時刻を戻してあげると `popularity-contest` による利用調査の参加スクリプトを実行できるようになります。例えば、次のようにして時刻を戻してあげると試せるようになります。

```
echo $(echo $(date +%s) - 648000 | bc) | sudo tee /var/lib/popularity-contest/lastsub
1597149912
sudo /etc/cron.daily/popularity-contest 
```


popularity-contest 1.69以前では、 `/var/lib/popularity-contest/lastsub` ではなく、 `/var/log/popularity-contest` の最終変更時刻を戻してあげる必要があります。

### 複数のサーバーにデータを送信するには

既定では、http://popcon.debian.org/cgi-bin/popcon.cgi にデータを送信しますが、複数のサーバーに送信することもできます。

```
% cat /etc/popularity-contest.d/example.conf
KEYRING="$KEYRING --keyring /usr/share/keyrings/xxxxxxxx.gpg"
POPCONKEY="$POPCONKEY -r XXXXXXXXXXXXXXX"
SUBMITURLS="$SUBMITURLS http://exampl.com/popcon.cgi"
```


例えば、上記のような設定ファイルを `/etc/popularity-contest.d` 以下に配置すると、指定したGPG鍵束に含まれる鍵IDを使って暗号化した
データをgzip圧縮して `http://exampl.com/popcon.cgi` へ送信するということができます。
ここで、送信先に HTTPS を指定することはできません。

### popularity-contestを再度有効にする方法について

ここまで読んで、インストール時に無効にしていたけど、Debianパッケージ利用調査に参加してもいいかな、と思えたら再度有効にしてみてください。次のコマンドで対話的に再度有効にできます。

```
sudo dpkg-reconfigure popularity-contest
```


### まとめ

今回は、Debianパッケージ利用調査に参加すると何が起こるのか？どんな仕組みでデータが送られているのかについて説明してみました。
どれくらい広く使われているかというのは、そのパッケージをメンテナンスし続けるかどうかの一つの指標にもなりえます。
Debianインストール時に問答無用で不参加にするのではなく、参加することで自由なソフトウェアを使っていることを示してみるのはいかがでしょうか。

[^0]: HTTP経由で送信できなかった場合はデータをメールにて送信を試みるようになっています。

[^1]: popularity-contest 1.70の場合。1.67以前はまたちょっと違う。
