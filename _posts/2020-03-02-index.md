---
tags:
- feedback
title: ノータブルフィードバック5 - 開発者が把握していない使い方についての報告と、そこからのプルリクエスト
author: piro_or
---
結城です。
<!--more-->


[ノータブルコード](/blog/category/notable-code.html)に便乗して、実際のOSSのフィードバックを見て「ノータブルフィードバック」と題してお届けする記事の5つ目は、当社の畑ケさん[^0]が[meta-clangというプロジェクトに対して行ったフィードバック](https://github.com/kraj/meta-clang/issues/247)です。

### 実際の報告

> ■タイトル
> meta-clang's llvm-config is not compatible with MULTILIBS
≪meta-clangのllvm-configが、MULTILIBSと互換性がない≫
> ■説明
> ≪不具合の説明≫
One of the our target boards(RZ/G2E)'s Yocto default conf/local.conf specifies MULTILIBS = "multilib:lib32" and   DEFAULTTUNE_virtclass  -multilib-lib32 = "armv7vethf-neon" to be able to run 32bit ARMv7 binaries.
≪私達が開発している対象のボード（RZ/G2E）の一つのYoctoレシピの既定のconf/local.confには、32bit ARMv7バイナリを動かすために、  「MULTILIBS = "multilib:lib32"」と「DEFAULTTUNE_virtclass-multilib-lib32 = "armv7vethf-neon"」という設定が含まれています。≫
So, built binaries will be installed in /usr/lib64/ instead of /usr/lib.
≪そのため、ビルドされたバイナリは/usr/libではなく/usr/lib64/の中にインストールされます。≫
> Because our SDK environment does not assume /usr/lib for library installation directory.
≪なぜなら、私達のSDKの環境は/usr/libをライブラリのインストール先ディレクトリーとして想定していません。≫
Instead, /usr/lib64 is used for 64bit libraries and shared object. And /usr/lib32 is used for 32bit objects.
≪その代わりに、/usr/lib64は64bitライブラリと共有オブジェクトに使われます。また、/usr/lib32は32bit版オブジェクトに使われます。≫
> ref: https://llvm.org/docs/CMake.html#frequently-used-cmake-variables
> To Reproduce
≪再現するには≫
> Steps to reproduce the behavior:
≪この動作を再現するための手順：≫
> [["  1. Specify MULTILIBS = \"multilib:lib32\" and DEFAULTTUNE_virtclass-multilib-lib32 = \"armv7vethf-neon\" in local.conf\n≪「MULTILIBS = \"multilib:lib32\"」と「DEFAULTTUNE_virtclass-multilib-lib32 = \"armv7vethf-neon\"」をlocal.confの中で設定する≫"], ["  1. Add meta-clang layer\n≪meta-clangレイヤを追加する≫"], ["  1. bitbake clang-cross-aarch64\n≪bitbake clang-cross-aarch64を実行する≫"], ["  1. add meta-browser and meta-rust layer\n≪meta-browserとmeta-rustのレイヤを追加する≫"], ["  1. bitbake firefox\n≪bitbake firefoxを実行する≫"], ["  1. See error\n≪エラーが表示される≫"]]
> Error
≪エラー≫
> ≪実際に出力されたエラーの全文が貼り付けられているが、ここでは省略。≫
> Expected behavior
≪期待される挙動≫
> meta-clang's llvm-config can work with DEFAULTTUNE_virtclass-multilib-lib32 specified environment.
≪meta-clangのllvm-configが、「DEFAULTTUNE_virtclass-multilib-lib32」が指定された環境で動作すること。≫
> llvm-config points to ${RECIPE_SYSROOT}/usr/lib/clang/8.0.1/lib/linux/ but actual libclang libraries are put in   ${RECIPE_SYSROOT}/usr/lib64/clang/8.0.1/lib/linux/
≪llvm-configは「${RECIPE_SYSROOT}/usr/lib/clang/8.0.1/lib/linux/」を指定しますが、実際のlibclangライブラリは「${RECIPE_SYSROOT}/usr/lib64/clang/8.0.1/lib/linux/」に置かれます。≫
> LLVM insists that using LLVM_LIBDIR_SUFFIX to control installation directory suffix such as lib64 or lib32.
≪LLVMでは、インストール先ディレクトリーの末尾をlib64やlib32のように変えたい場合、LLVM_LIBDIR_SUFFIXを使う必要があります。≫
> We should handle library directory glitch it llvm-config with LLVM_LIBDIR_SUFFIX.
≪私達はLLVM_LIBDIR_SUFFIXを伴ったllvm-configのライブラリーの配置先ディレクトリーのずれに対処するべきでしょう。≫
> Desktop (please complete the following information):
≪ビルド環境のデスクトップ機（以下の情報を埋めてください）≫
> [["  * OS: Ubuntu"], ["  * Version 16.04.6 LTS"]]
> Additional context
≪追加の情報≫
> (Updated) I'd encountered this issue during meta-browser's firefox recipe building.
≪（追記）私はこの問題に、meta-browserのFirefoxのレシピを使ってのビルド作業中に遭遇しました。≫


### フィードバックの経緯

組み込み機器向けにカスタマイズしたLinuxディストリビューションを作成するためのツールセットの一種であるYoctoでは、そのLinuxディストリビューションに組み込めるパッケージが多数公開されています。その中で、C言語のコンパイラであるclangを組み込むための設定ファイルやスクリプトを提供しているのが、meta-clangというプロジェクトです。畑ケさんがとある組み込みボード[^1]用にFirefoxをビルドしようとして、Firefoxのビルドに必要なmeta-clangを構成に入れたところ、*ビルド結果が想定通りにならず、Firefoxパッケージをビルドできない*という状況に遭遇しました。

この時点で原因がmeta-clangにあるということは明らかだったため、畑ケさんは*手元でとりあえずの回避策を講じた*上で、問題に遭遇したということをmeta-clangにフィードバックしました。すると、その報告に対してプロジェクトオーナーから「Can you cook up a patch?（パッチを作ってもらえませんか？）」とコメントが返ってきました。そこで、畑ケさんは「I'm cooking up patches....（今パッチを作成中です……）」とコメントした上で、*手元で行っていた暫定的な回避策を一般公開できるようにより洗練させ、プルリクエストにしました*。

その後パッチがマージされたことで、この報告もクローズされています。

### 注目したい点

これも、「自分の手元でなんとかして動くようにした」というところで終わらせないで、開発元にエスカレーションした事例です。

最終的にプルリクエストを出すところにまで至っていますが、畑ケさんは当初はそこまでするつもりはありませんでした。しかし *「パッチを作ってもらえませんか？」と逆に依頼された* ことがきっかけとなり、プルリクエストの作成に至りました。フィードバックは「自分にできることをまずはやる」所から始めるとよいですが、*やろうと思っていなかったことに挑戦する機会にもなる*ということの好例と言えるでしょう。

この報告はプロジェクトのイシューテンプレートに基づいて書かれていていますが、「To Reproduce」には再現手順と実際の結果が、「Expected behavior」には期待される結果が書かれており、*問題の報告の基本の3要素がきちんと含まれている*ことが分かります。

フィードバックする前の予備調査の時点で、畑ケさんはこの作者の人が、ここで使おうとしている「MULTILIBS」という機能を使っていないようだ、ということを把握していました。そのため、再現手順や環境の作り方をより細かく具体的に書き、*作者が容易に現象を確認できるようにすること*を意識したそうです。[CJKの言語に特有の事情を詳しく説明した前回の例]({% post_url 2020-02-29-index %})と、考え方は同じです。

文中に登場している「glitch」という単語は、辞書では「故障」「誤動作」「異常」といった意味と出ますが、語源は「slip（滑る）」や「slide（ずらす）」と同じで、ニュアンスとしては「本来の正常な状態からずれてしまっている」状態を表すそうです。インデントのずれや画像の位置ずれなどにも使える表現ということで、覚えておくとよいでしょう。

ところで、この例も報告文の中にミスタイプがあります。

> We should handle library directory glitch it llvm-config with LLVM_LIBDIR_SUFFIX.


この文の「it」は誤記で、「in」が正しいです。先の筆者の例と併せて見ると、誤記はありふれたものだということをなんとなく感じて頂けるのではないでしょうか[^2]。

### まとめ

ノータブルフィードバックの5回目として、開発者が想定していない使い方での不具合を報告するフィードバック例をご紹介しました。

このような「身近なところで遭遇したつまずきをOSS開発プロジェクトにフィードバックする」ということをテーマに、*まだOSSにフィードバックをしたことがない人の背中を押す解説書 「これでできる！ はじめてのOSSフィードバックガイド ～ #駆け出しエンジニアと繋がりたい と言ってた私が野生のつよいエンジニアとつながるのに必要だったこと～」を、電子書籍としてリリースしました*。本記事の内容はこの本からの抜粋となっています。ダウンロード購入のチャンネル、紙媒体版などの詳細については、[「ノータブルフィードバック」第4回目の記事]({% post_url 2020-02-29-index %})を併せてご参照下さい。

[^0]:  「はたけ」と読む珍しい名字のため、社内でもよく「ケ」が行方不明になりがちです。

[^1]: 複合機やカーナビなどの制御に使われるコンピューター。

[^2]: 畑ケさん本人にこの事を知らせると「やっちまったぁぁ」と恥ずかしがっておられましたが、本書でフィードバック初心者の方に勇気を持ってもらうための礎になっていただきました。合掌。
