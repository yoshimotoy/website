---
tags: []
title: 'commit-email.info: コミットメール配信サービス'
---
コミットメール配信機能を提供するサービス[commit-email.info](http://www.commit-email.info/)を紹介します。
<!--more-->


（commit-email.infoはクリアコードが運営しているサービスではなくクリアコードのメンバーが個人で運営しているサービスです。）

フリーソフトウェアの開発で学んだことのひとつに「コミットを共有する」ということがあります。

「コミットを共有する」とは、他の開発者のコミットを自分が読む、自分のコミットを他の開発者が読む、ということです。最近はpull requestを使った開発がよい開発というイメージが広がっているようなので、「pull requestをしたら他の開発者が自分のコミットを見てくれる」と言えばどういうことかピンときやすいかもしれません。ただ、「コミットを共有する」は「requestされなくても自分が読みたいから読む」という点が「pull requestを使った開発」のケースと違います。

「自分が読みたいからコミットを読む」ことを支援するサービスがcommit-email.infoです。pull requestきっかけでコミットを読みたい場合は[GitHubのwatch機能](https://help.github.com/articles/watching-repositories/)を使うとよいでしょう。

### 機能

commit-email.infoの機能を説明します。

commit-email.infoはリポジトリーにコミットがpushされたらそのコミットの情報（diff入り）をEメールで配信するサービスです。このEメールのことをコミットメールと呼びます。コミットメールにはdiffが入っているので別途Webブラウザーを開いたり`git log -p`を実行しなくても変更の詳細まで確認できるところがポイントです。diffが入っていると確認するひと手間が減るのです。

diffは変更点がわかりやすくなるように工夫しています。次のように色付きでフォーマットしています。これはdiffをわかりやすく表示するツールがよく使うやり方です。

たとえば、[GitHubにミラーされたRubyのr52117](https://github.com/ruby/ruby/commit/61053459cfa5bf55210c5411652ac039dd07c703)は次のようになります。

最初にコミットのメタデータがあります。

![コミットメールのヘッダー部分]({{ "/images/blog/20151013_0.png" | relative_url }} "コミットメールのヘッダー部分")

続いてdiffがあります。このスクリーンショットでは`AF_UNSPEC`を引数として追加したことがわかりやすくなっています。

![コミットメールのdiff部分]({{ "/images/blog/20151013_1.png" | relative_url }} "コミットメールのdiff部分")

コミットメールはメーリングリストで配信します。コミットメールを受信したい場合はメーリングリストを購読し、受信をやめたい場合はメーリングリストから退会します。購読・退会処理は自動化されているため、ユーザーは自分の好きなタイミングで実行できます。

このサービスが提供する機能はこれだけです。

### 使い方

commit-email.infoの使い方を説明します。

まず、自分がコミットを読みたいプロジェクトのコミットメールを配信しているメーリングリストを[リスト](http://www.commit-email.info/#repositories)から探します。たとえば、[Rubyのリポジトリー](https://github.com/ruby/ruby/)のコミットメールなら`ruby@ml.commit-email.info`です。

このメーリングリストに次の内容のメールを送ります。`Cc`に`null@commit-email.info`を指定することを忘れないでください。

```text
To: ruby@ml.commit-email.info
Cc: null@commit-email.info
Subject: Subscribe
--
subscribe
```


このメールを送ると購読完了です。それ以降にpushされたコミットはコミットメールとして届きます。

もし、リストにコミットを読みたいプロジェクトがなく、そのプロジェクトのリポジトリーがGitHubにある場合はメーリングリストを購読する前にそのプロジェクトをリストに追加する必要があります。リストに追加する方法は次の通りです。

  1. [kou/commit-email.infoにある`ansible/files/web-hooks-receiver/config.yaml`](https://github.com/kou/commit-email.info/blob/master/ansible/files/web-hooks-receiver/config.yaml)にそのリポジトリーを追加するpull requestを送ります。

  1. もし、自分がそのリポジトリーの管理者権限を持っているなら、そのリポジトリーのWebフックに`http://web-hooks-receiver.commit-email.info/`を追加します。（リポジトリー単位のWebフックでもorganization単位のWebフックでも構いません。複数のリポジトリーを対象にするならorganization単位のWebフックが便利です。）

  1. もし、自分がそのリポジトリーの管理者権限を持っていないなら、[kou/commit-email.infoにある`ansible/files/github-event-watcher/config.yaml`](https://github.com/kou/commit-email.info/blob/master/ansible/files/github-event-watcher/config.yaml)にそのリポジトリーを追加するpull requestを送ります。

どのケースでも「1.」は必須です。「2.」と「3.」はどちらかをやればいいです。「2.」の方がおすすめです。理由はタイムラグが小さいからです。

### まとめ

コミットメール配信機能を提供するサービスcommit-email.infoを紹介しました。GitHubにあるリポジトリーに対応しています。[GitLab.com](https://about.gitlab.com/gitlab-com/)に対応することは容易なので、必要な人は[issueを作って](https://github.com/kou/commit-email.info/issues/new)相談してください。

commit-email.infoを使って自分が使っているプロダクトのコミットを読み始めてみてはいかがでしょうか？きっと学ぶことがたくさんあるはずです。

そういえば、[www.commit-email.info](http://www.commit-email.info/)がやけに質素なデザインだと思いませんでしたか？実は[スタイルシートが空っぽ](https://github.com/kou/commit-email.info/blob/master/ansible/files/apache/www/style.css)なのです。

関連情報：

  * [Git でコミットメールを配信する方法 - ククログ(2015-03-27)]({% post_url 2015-03-27-index %})：commit-email.infoではここで紹介しているソフトウェアを使っています。サーバーは[ConoHa](https://www.conoha.jp/)で動いています。

  * [Mewで色付きでコミットメールを表示する方法 - ククログ(2015-03-17)]({% post_url 2015-03-17-index %})：前述のスクリーンショットはGmailでコミットメールを表示しているところですが、Mewでも同様に色付きで表示できます。

  * [クリアコードの開発の様子をフォローしやすくしました - ククログ(2013-07-11)]({% post_url 2013-07-11-index %})：このサービスの元になった仕組みです。`clear-code@ml.commit-email.info`に移行したので`commit@clear-code.com`は廃止しました。

  * [「チームメンバーのリーダブルコードを見つけて共有」を体験 - ピクシブさんでリーダブルコードワークショップを開催 - ククログ(2015-08-13)]({% post_url 2015-08-13-index %})：このワークショップでもcommit-email.infoを使いました。

  * [[Book] リーダブルコード - より良いコードを書くためのシンプルで実践的なテクニック - ふぇみにん日記(2012-07-11)](http://kazuhiko.tdiary.net/20120711.html#p01)：diff入りコミットメールが好きな人の日記です。
