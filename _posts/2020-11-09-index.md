---
tags:
- mozilla
- redmine
title: Flex Confirm MailとRedmine連携のThunderbird 78対応版をリリースしました
author: piro_or
---
結城です。以前に書いた[アドオンのThunderbird 78対応状況についての記事]({% post_url 2020-08-07-index %})の続報です。
<!--more-->


  * *メール送信前に宛先等を再確認するアドオン[Flex Confirm Mail](https://addons.thunderbird.net/thunderbird/addon/flex-confirm-mail/)の、Tb78対応版をリリースしました。*

    * ただし、自動更新を通じてインストールされた場合の予期しないトラブルが懸念されることから、現時点ではThunderbird Add-ons Webサイト上では公開していません。

    * 使用するには、[GitHub上のリリースページ](https://github.com/clear-code/flex-confirm-mail/releases)からパッケージをダウンロードして頂く必要があります。

    * 旧バージョン（バージョン1系、バージョン2系）の設定は自動的には引き継がれません。お手数ですが、手動操作での再設定をお願いします。

  * *メールとRedmineのチケットを関連付けるアドオン[RedThunderMineBird Plus](https://addons.thunderbird.net/thunderbird/addon/redthunderminebird-plus/)のTb78対応版をリリースしました。*

    * こちらはThunderbird Add-onsで公開済みとなっております。

    * フォーク元の「Redmine連携」、およびTb68以前での設定は自動的には引き継がれません。お手数ですが、手動操作での再設定をお願いします。

現在世に出ているThundebrirdアドオンの中で、従来から人気があったアドオンの「Tb78対応版」は、[WebExtensions Experiments](https://thunderbird-webextensions.readthedocs.io/en/78/how-to/experiments.html)という仕組みを使った暫定的な対応に留まっている例が多いのが現状です。この手法はアドオンを比較的小さな工数でTb78に対応させられますが、Tb78の次のメジャーリリース以降でアドオンが動作しなくなるリスクがあります。

それに対し、この度リリースしたこれらのアドオンのTb78対応版は、[前回記事]({% post_url 2020-08-07-index %})で述べた「WebExtensions APIに基づいて全面的に再実装する」手法で一から作り直した物となっています。APIの互換性が保たれる限りにおいては、今後のバージョンのThunderbirdでも安定して使い続けられる事が期待できます。

以下、Tb78用のアドオンを開発される方向けの参考情報として、それぞれのアドオンのTb78対応版開発における技術的な特記事項を簡単に紹介します。

### Flex Confirm MailのTb78対応

本アドオンの技術的な特記事項は以下の2点です。

  * メールの送信直前に割り込んで、許可・不許可の判断を非同期に行い、許可が得られなければ送信を中断する。

  * 送信しようとしているメールがどういう文脈に属するかを判断する。

1点目の動作は、Tb78から使用可能となった新APIの[`compose.onBeforeSend`](https://thunderbird-webextensions.readthedocs.io/en/78/compose.html#onbeforesend-tab-details)に依存しています。そのため、WebExtensionsベースのFlex Confirm MailはTb68以前のバージョンでは動作しません。

`compose.onBeforeSend`は、登録したリスナーが返す[`webRequest.BlockingResponse`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/BlockingResponse)と同形式のオブジェクトによって、処理の継続の可否を指定することができます。このとき、リスナーから通常の`Object`を返す代わりに`Promise`を返すか、同様の効果を得られる物として非同期関数をリスナーにすると、`Promise`が解決されるまで（非同期関数が結果をreturnするまで）送信が待機されます。本アドオンではこの仕様を利用し、[非同期関数のリスナー](https://github.com/clear-code/flex-confirm-mail/blob/b32dd6b3fa078882b5b86ae19b4a4cbf6815bc13/webextensions/background/background.js#L351)で[非同期的に開いた独自の確認ダイアログ](https://github.com/clear-code/flex-confirm-mail/blob/b32dd6b3fa078882b5b86ae19b4a4cbf6815bc13/webextensions/background/background.js#L268)での確認結果が得られてから結果を返すことで、期待される動作を実現しています。

2点目の特記事項の「文脈」とは、ここでは「新規作成」「返信」「転送」「テンプレートからの作成」「下書きの編集」「既存メールの編集」のうちのどの方法で作成されたメールかという意味です。

本アドオンのように「事前に警告するUI」は、あまりに頻繁に警告されすぎると、そのうち警告を読まずに盲目的に許可するようになってしまうジレンマがあります。その一方で、筆者の経験上は、メールの誤送信が発生しやすい場面は「新規にメールを書いた場合や、手動操作で宛先を記入・変更した場合」であるのに対し、実際の運用上では「受信したメールにそのまま返信し、宛先は編集しない」という、宛先内に無関係のアドレスが混入している可能性が低いケースが大半である、という印象がありました。そのため、本アドオンには「安全である可能性が高いケース＝既存メールへの返信で宛先が編集されていない場面では警告を行わないようにする」設定があります。

そのような動作を実現するには「返信かどうか」および「宛先が編集されているかどうか」を判別する必要がありますが、`compose.onBeforeSend`のリスナーにはそのような情報は渡されません。そのため、何らかの方法でこれらを把握する工夫が必要となります。本アドオンでは、[メール編集画面で自動実行されるコンテントスクリプトを登録](https://github.com/clear-code/flex-confirm-mail/blob/b32dd6b3fa078882b5b86ae19b4a4cbf6815bc13/webextensions/background/background.js#L84)しておき、その中で[メッセージを送出](https://github.com/clear-code/flex-confirm-mail/blob/b32dd6b3fa078882b5b86ae19b4a4cbf6815bc13/webextensions/resources/compose.js#L8)して、そのメッセージを受け取ったタイミングで[現在開かれているウィンドウを走査したり、メール編集画面にその時点で入力されている宛先を収集したり](https://github.com/clear-code/flex-confirm-mail/blob/master/webextensions/background/background.js#L47)することによって、その後の`compose.onBeforeSend`のリスナーで必要な判断を行えるようにしています。

### RedThunderMineBird PlusのTb78対応

本アドオンの技術的な特記事項は以下の2点です。

  * メールのスレッドとチケットを関連付けて、その情報を保存する。

  * 受信メールの本文を確実に抽出する。

1点目は、そもそもThunderbirdのWebExtensions APIは存在しない「スレッド」という概念をどのように把握するかと、個々のメールに対する任意のメタ情報の紐付けをどのように保存するかという話です。

従来型のアドオンが参照できるThunderbird内部のAPIには、そのものズバリ「スレッドに対して任意の情報を紐付けて保存する」機能があり、従来はこれによって、スレッドの親のメールがチケットに関連付け済みであれば、返信メールについていちいち「チケットに関連付ける」操作をしなくても良いようになっていました。現時点のWebExtensions APIには相当する機能が無いので、すでにある機能の組み合わせで代替しなくてはいけません。

スレッドは、ヘッダの情報から推測できます。[`messages.getFull()`](https://thunderbird-webextensions.readthedocs.io/en/78/messages.html#getfull-messageid)で得られる[`messages.MessagePart`](https://thunderbird-webextensions.readthedocs.io/en/78/messages.html#messages-messagepart)を参照すると、`headers`というプロパティですべての生のヘッダを参照できます。その中の`references`ヘッダにスレッドの祖先のメールの'message-id'ヘッダの値が含まれているので、本アドオンでは[それを取得して](https://github.com/clear-code/tb-redthunderminebird/blob/c1dae47c2b55ac74a6dfe5bf6750d26c69b12cb6/webextensions/common/Message.js#L43)、[各`message-id`をキーとしてチケットの`id`をIndexedDBに保存](https://github.com/clear-code/tb-redthunderminebird/blob/c1dae47c2b55ac74a6dfe5bf6750d26c69b12cb6/webextensions/common/db.js#L32)しています。こうしておけば、まだチケットに直接関連付けられていないメールでも、スレッドの祖先の`message-id`に関連付けられたチケットの`id`を取得できる、という具合です。

2点目の受信メールの本文の抽出は、従来アドオンで使用できた「指定されたメールの本文として適切な内容を収集して返す」Thunderbird内部のユーティリティを何らかの方法で代替するという事です。

WebExtensions APIでは、[`messages.MessagePart`](https://thunderbird-webextensions.readthedocs.io/en/78/messages.html#messages-messagepart)の`parts`としてマルチパートの各パートを再帰的に辿ることができます。本アドオンでは、[その全パートを再帰的に走査](https://github.com/clear-code/tb-redthunderminebird/blob/c1dae47c2b55ac74a6dfe5bf6750d26c69b12cb6/webextensions/common/Message.js#L100)して、本文と思われるパートを抽出するようにしました。HTMLメールの本文をプレーンテキストの本文に変換する方法としては、[`DOMParser`でDOMツリーを作り](https://github.com/clear-code/tb-redthunderminebird/blob/c1dae47c2b55ac74a6dfe5bf6750d26c69b12cb6/webextensions/common/format.js#L9)、そのツリーの[各ノードを再帰的に走査](https://github.com/clear-code/tb-redthunderminebird/blob/c1dae47c2b55ac74a6dfe5bf6750d26c69b12cb6/webextensions/common/format.js#L13)して、テキスト形式の戻り値を組み立てるようにしました。

2点目の処理は他のアドオンでも必要になる可能性があり、そうなった時にはライブラリとして独立してメンテナンスするようになるかもしれません。

### まとめ

以上、Tb78以降のバージョン向けにWebExtensions APIベースで作り直した2つのアドオンのリリースをお知らせし、併せて実装上の工夫もご紹介しました。

今回リリースした2つのアドオンは、どちらも元々は、[confirm-mail](https://addons.thunderbird.net/ja/thunderbird/addon/confirm-mail/)および[Redmine連携](https://addons.thunderbird.net/thunderbird/addon/redmine%E9%80%A3%E6%90%BA/)という、第三者の開発者の方々によって作成されたアドオンでした。既存ソフトウェアに対してお客さまから機能追加の要望を頂いた際、当社では基本的にはその改善をそのソフトウェアの開発プロジェクトにフィードバックする方針としています。ですが、これらについては業務で必要とされる更新頻度がアドオン側の元々の更新頻度と大きく乖離してしまっていたことから、プロジェクトをフォークし、独自の改修版として当社でメンテナンスを行っている状況となっています。

当社ではThunderbirdの法人向けサポートサービスの一環として、ご要望に応じたアドオンの開発や既存アドオンの改修も承っております。今回リリースした2件については、ある程度のニーズが見込まれたことや、社内でも業務上で必要としていたことから、現時点で最も大きな工数がかかるものの将来的なメンテナンスコストを抑えられると考えられる、「Tb78用に完全に再設計する」やり方で対応を行いました。しかしながら、ご要望の要件次第では、多くのアドオンが暫定的な方法として採用しているWebExtensions Experimentsに基づく対応を行うことも可能です。業務上の必要で採用しているアドオンが最新のThunderbirdに対応していないために、脆弱性を抱えている古いバージョンのThundebrirdを使わざるを得ない、という状況でお困りの企業ご担当者の方は、[お問い合わせフォーム](/contact/)からご連絡を頂けましたら幸いです。
