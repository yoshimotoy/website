---
title: "Fcitx5ベースの仮想キーボードのレイアウトカスタマイズ機能を解説"
author: kenhys
tags:
  - embedded
  - use-case
---

以前、[サイバートラスト](https://www.cybertrust.co.jp/)様と共同で[Fcitx5](https://github.com/fcitx/fcitx5)をベースとして開発した仮想キーボードとそれに関連する開発環境を整える方法について紹介しました。

* [PC上でWeston + WaylandをターゲットとしたFcitx5ベースの仮想キーボードの開発環境を構築する方法 (2024-08-07)](https://www.clear-code.com/blog/2024/8/7/virtualkeyboardui-how-to-setup-devenv.html)

今回は、Fcitx5ベースの仮想キーボードに最近追加されたレイアウトカスタマイズ機能について紹介します。

<!--more-->

### レイアウトカスタマイズ機能とは

[PC上でWeston + WaylandをターゲットとしたFcitx5ベースの仮想キーボードの開発環境を構築する方法](https://www.clear-code.com/blog/2024/8/7/virtualkeyboardui-how-to-setup-devenv.html)の記事では、レイアウトカスタマイズ機能について次のようにその存在について言及していましたが、詳細については解説していませんでした。

```text
最近より自由に仮想キーボードのレイアウトを定義できるようにレイアウトカスタマイズ機能を実装しました。
レイアウトカスタマイズ機能は、従来C++で実装する必要のあったキーボードを、所定のJSONファイルを配置することで実現できるようにするものです。
こちらについては、別の機会に紹介したいと考えています。
```

この機能の導入前後でどのように変わったのかをそれぞれ説明します。

### 従来のFcitx5ベースの仮想キーボードの課題

Fcitx5ベースの仮想キーボードでは、既定で英語、日本語、中国語(繁体字)、中国語(簡体字)、韓国語、ロシア語の6種類に対応しています。
複数の言語に対応するために、それぞれの言語ごとに仮想キーボードを実装するという設計になっていました。

そのため、新たな言語に対応するためには、C++で新規仮想キーボードを実装してあげる必要があります。
また、既存の仮想キーボードのレイアウトを一部変更するのにも、仮想キーボードの再ビルドが必要となります。

CJKなどに代表される特定の言語では、入力できる文字を切り替えるために煩雑な状態の制御が必要です。
そのような処理を必要とする言語では、言語ごとに仮想キーボードを実装せざるをえない事情があります。
しかし、そうでない場合でも言語ごとに似たような仮想キーボードを実装するコストがかかっていました。

そのため、仮想キーボードのキーレイアウトをより簡単な方法でカスタマイズできることが求められていました。

### レイアウトカスタマイズ機能導入後の仮想キーボード

fcitx5-virtualkeyboard-uiのレイアウトカスタマイズ機能では、次の点を重視して設計しました。

* 設定ファイルを配置するだけで仮想キーボードを追加できること
* 設定ファイルはキーレイアウトの配置の試行錯誤がしやすいこと

前者の設定ファイルを配置するだけで仮想キーボードを追加できるようにするためには、仮想キーボードの振る舞いを
設定ファイルに記述できる必要があります。そして、設定ファイルを解釈して動作する汎用のカスタムキーボードが必要です。

また、キーレイアウトの配置を試行錯誤できるようにするためには、レイアウト結果を可視化して確認するための手段が必要です。

上記を実現するために、[Keyboard Layout Editor](http://www.keyboard-layout-editor.com/)を利用しレイアウトを試せるようにしました。[^kle]

[^kle]: 自作キーボード界隈ではよくみかける、レイアウトの検討に用いられるキーレイアウトエディター。

Keyboard Layout Editorでは、[Serialized Data Format](https://github.com/ijprest/keyboard-layout-editor/wiki/Serialized-Data-Format)が定められており、
メタデータを付与して拡張する余地があります。
厳密には互換性がなくなるのですが、メタデータだけでなく、キーレイアウトの定義箇所の仕様(JSON)を仮想キーボード向けに拡張することで、キーの振る舞いを定義するだけでなく、変更したキーのレイアウトを確認できるようにしました。[^incompatibility]

[^incompatibility]: 拡張した仕様のJSONをKeyboard Layout Editorに読み込ませると拡張部分のデータが失われますが、あくまでレイアウトの確認用としてこの点については制限事項としました。

### レイアウトカスタマイズ機能によるキーレイアウトのデザイン

英語キーボードをレイアウトカスタマイズ機能で実現したときのJSONの定義は次のようになります。

```json
[
  {
    "// The layout definition for English keyboard (ext)": "Designed for fcitx5-virtualkeyboard-ui",
    "Keyboard": {
      "label": "US",
      "languageCode": "us",
      "numberOfLayers": 2,
      "layerOffset": [
        1,
        5
      ]
    },
    "ModeSwitchKey": {
      "numberOfStates": 2,
      "stateLabel": [
        "A",
        "#"
      ],
      "mode": [
        {
          "label": "text",
          "next": "mark",
          "layer": 0
        },
        {
          "label": "mark",
          "next": "text",
          "layer": 1
        }
      ]
    },
    "NormalKey": {
      "width": 60.0,
      "useLabelAsKeyName": true
    }
  },
  [
    {
      "//Text": "R1",
      "a": 7,
      "f": 9,
      "class": "NormalKey",
      "code": 24,
      "upperLabel": "Q"
    },
    "q",
    {
      "class": "NormalKey",
      "code": 25,
      "upperLabel": "W"
    },
    "w",
    {
      "class": "NormalKey",
      "code": 26,
      "upperLabel": "E"
    },
    "e",
    {
      "class": "NormalKey",
      "code": 27,
      "upperLabel": "R"
    },
    ...
```

[拡張仕様](https://github.com/clear-code/fcitx5-virtualkeyboard-ui/blob/fcitx-5.0.8/src/keyboards/README.ja.md)に従って、英字入力のレイヤーだけでなく、記号入力のためのレイヤーもまとめて定義してあるので、Keyboard Layout Editorに読み込ませると複数のレイヤーが連結された状態で描画されています。

![拡張仕様のレイアウトファイルをKLEに読み込ませた画面]({% link /images/blog/virtualkeyboardui-layout-customization/kle-us.png %})

このレイアウト定義ファイルを仮想キーボードに読み込ませると、仮想キーボード(英語)に切り替えたときには次のように描画されます。

![仮想キーボードのレイアウト結果]({% link /images/blog/virtualkeyboardui-how-to-setup-devenv/keyboard-us.png %})

### おわりに

今回は、Fcitx5ベースの仮想キーボードに最近追加されたレイアウトカスタマイズ機能について紹介しました。

レイアウトカスタマイズ機能といっても、ちまたにあるような各種キーボードカスタマイズツールほど高度なカスタマイズ機能は提供していません。
しかし、（本来の用途である）組み込みLinux向け多言語入力のための仮想キーボードを提供するという観点からは、
より独自レイアウトを試しやすくなるというのがメリットとなっています。

クリアコードでは組み込みLinux向け多言語入力や、Webブラウザを含むアプリケーションカスタマイズに関するコンサルティングを請け負っています。
なにかお困りの点があれば、[お問い合わせフォーム]({% link contact/index.md %})よりお気楽にお問い合わせ下さい。
