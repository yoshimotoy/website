---
tags: []
title: logalimacs 1.0.1 をリリースしました
---
2012.6.7に1.0.0をリリースしたばかりですがMermaladeからMELPAにリポジトリサイトを変更したのでお知らせします。
<!--more-->


[logaling-command](http://logaling.github.com/) のEmacs用のフロントエンドプログラム
[logalimacs](https://github.com/logaling/logalimacs) 1.0.1 をリリースしました。
Emacsのキーボードショートカットでlogaling-commandを利用できます。

### logaling-commandとは

logaling-commandは翻訳作業に欠かせない訳語の確認や選定をサポートする CUI ツールです。
対訳用語集を簡単に作成、編集、検索することができます。

### logalimacs 1.0.0 から 1.0.1 への変更点

#### EmacsのリポジトリサイトのMELPAに対応しました。

以前はMermaladeというリポジトリサイトを利用していましたが、
最近はメンテナンスされていないようでアップロードできませんでした。
そのため今回のアップデートで
他のEmacsリポジトリサイトのMELPAに変更しました。
ちなみにMELPAは最新のmasterを持ってきて日付をバージョン替わりにします。
なのでMELPAにしておけばEmacs-Lispのパッケージ職人の方は
masterにpushするだけで最新のものをアップロードしたことになります
[^0]。

#### loga-popup-output-typeに`'max`の様なシンボル記法も対応しました

popup表示の時の幅を最大にするか自動にするかの設定変数ですが、`:max`より`'max`の表記のほうが見慣れている方もいるのでどちらでも設定できるようにしました。

#### バッファ表示の時の不具合を修正しました。

`M-x loga-lookup-in-buffer`コマンドでバッファに翻訳候補を表示して
画面移動できますが、入力した文字がミニバッファに表示されていました。
ミニバッファに出力しないように修正しました。

### package.elを利用したインストール方法

今回のリリースでMermaladeからMELPAにリポジトリサイトが変更になりました。
今まではEmacs23ユーザーの方には、GitHubからのcloneと.emacsなどへの設定で
インストールして頂いていたのですが、
今までアナウンスしていなかったので、この機会にpackage.elを導入してlogalimacsをインストールする方法を紹介したいと思います。
（Emacs24は標準添付なのでpackage.elの導入の説明は、Emacs23のユーザー用です）

#### package.elの導入

package.elを~/.emacs.d/package23/以下にダウンロードする例です
作成するディレクトリは適宜変更してください。

{% raw %}
```
$ makedir -p ~/.emacs.d/package23
$ cd ~/.emacs.d/package23
$ wget http://repo.or.cz/w/emacs.git/blob_plain/1a0a666f941c99882093d7bd08ced15033bc3f0c:/lisp/emacs-lisp/package.el
```
{% endraw %}

上記を行うと~/.emacs.d/package23ディレクトリの下にpackage.elが作成されるはずです。

#### ~/.emacs.d/init.elなどにpackage.el用の設定とlogalimacs用の設定を追加

package.elはEmacs23用とEmacs24用で違うので
Emacs24になった時には必要なくなるので条件分岐しています。

{% raw %}
```
;;; Emacs24未満の場合だけEmacs23用のpackage.elを読み込む
(when (version< emacs-version "24.0.0")
  (load "~/.emacs.d/package23/package"))
;;; リポジトリにMELPAを追加
(add-to-list 'package-archives ("melpa" . "http://melpa.milkbox.net/packages/"))
;;; package.elの設定
;; インストールするディレクトリを指定(お好みで)
(setq package-user-dir "インストールされるディレクトリ")
;; インストールしたパッケージにロードパスを通してロードする
(package-initialize)
;;; logalimacsの設定
;; keybinds
(global-set-key (kbd "M-g M-i") 'loga-interactive-command)
(global-set-key (kbd "M-g M-l") 'loga-lookup-at-manually)
(global-set-key (kbd "M-g M-a") 'loga-add)
(global-set-key (kbd "C-:") 'loga-lookup-in-popup)
;; 以下はお好みで設定してください
;; lookupに--dictionaryオプションを利用する場合
(setq loga-use-dictionary-option t)
;; :autoがデフォルトでカーソル位置の近くにpopup
;; :maxにするとbuffer一杯までpopupをのばし行の最初から表示します
;; Version: 1.0.1から'maxの様なシンボル記法も対応しました
(setq loga-popup-output-type 'max)
```
{% endraw %}

詳しいlogaling-commandの設定やインストールの説明はlogalimacsの
[チュートリアルページ](http://logaling.github.com/logalimacs/tutorial.html) で紹介しています。

#### `M-x list-packages`コマンドでインストール

`M-x list-packages`コマンドを実行すると
現在登録しているリポジトリサイトから候補を表示します。
`C-s`でlogalimacsを検索してみてください。
`"logalimacs   20120611 ~~~~"`のような行がみつかるので
この行にカーソルをあわせて`"i"`を押してください。
行のはじめに`"I"`というマークがつくので、
`"x"`を押してインストールを実行してください。
これで
`(setq package-user-dir "インストールされるディレクトリ")`
で指定したディレクトリか~/.emacs.d/elpaの下にインストールされます。
もし既にlogalimacsをGitHub経由でインストールしていて、`load`関数や`require`関数で
読み込んでいた場合、正常にインストールされない場合があります。
この場合はその`load`関数や、`require`関数を消してEmacsを再起動してから試してみてください。

無事インストールできれば、
`C-:`でポップアップを表示できるはずです。

### 終わりに

以上で本日リリースのlogalimacs 1.0.1の紹介を終わります。
2012/6/10にEmacs24.1も正式にリリースされましたので、
`M-x list-packsges`を試すついでにlogalimacsもインストールしてみてはいかがでしょうか?

[^0]: MELPAの由来:Milkypostman's ELPA or Milkypostman's Experimental Lisp Package Repository
