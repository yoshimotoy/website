---
tags:
  - groonga
title: Mroongaでlock failedとなったときの復旧方法2021 - clearlockしてはいけない！
author: kou
---
[Mroonga](https://mroonga.org/ja/)の開発をしている須藤です。

2013年に書かれた[groonga/mroongaの.mrnファイルがlock failedとなった場合の復旧方法 - Y-Ken Studio](http://y-ken.hatenablog.com/entry/how-to-unlock-mroonga-database)という記事があるのですが、この記事に書かれているように`clearlock`を使ってはいけません！

<!--more-->

Mroongaは更新中（`INSERT`/`UPDATE`/`DELETE`中とかスキーマ変更中とか）にクラッシュすると更新の前に獲得しておいたロックがDB中に残留してしまい、再起動後にロック待ちで固まってしまいます。このとき、`groonga.log`には`lock failed`というようなログが残ります。

前述の記事にはこんなときは`select mroonga_command('clearlock')`して復旧できると書かれているのですが、基本的に`clearlock`はしないでください！

ロックが残留しているということは更新処理が中途半端になっている可能性が非常に高いです。これはデータがおかしくなっている可能性が非常に高いということです。`clearlock`するとこのおかしくなっている状態で使い続けることになるので、使えるようになったと思ってもすぐにまたクラッシュする・検索結果がおかしくなるといったことが発生しやすいです。

では、どうすればよいかというと、一旦MySQLを止めて[`grndb`コマンド](https://groonga.org/ja/docs/reference/executables/grndb.html)を使います。`grndb`コマンドはMroongaのDBがどう壊れているかを確認したり、壊れているDBを復旧したりできます。

`grndb`コマンドはDebian/Ubuntuなら`groonga-bin`パッケージ、CentOSなら`groonga`パッケージに含まれています。

大事なことなのでもう一度書いておきますが、`grndb`コマンドを使うときは*MySQLを止めて*ください。MySQLを止めないと誤検知したり、逆にDBを壊してしまうこともあります。

`grndb`コマンドを使うときは次のことに注意してください。

* MySQLを動かしているユーザーと同じユーザーで実行する。（たとえば`mysql`ユーザー。）
* `--log-path`で確実にログを記録しておく。

たとえば、MySQLのデータディレクトリーが`/var/lib/mysql/`でデータベース名が`db`なら次のようにDBがどう破損しているかをチェックできます。どのように破損しているかは`grndb check`の出力あるいはログを確認してください。

```console
$ sudo -u mysql -H grndb check --log-path /tmp/grndb.check.log /var/lib/mysql/db.mrn
```

自動で復旧できる場合は`grndb recover`で復旧できるというメッセージが残っています。自動で復旧できない場合は手動での復旧方法が残っています。

ざっくりいうと、インデックスのみが壊れている場合は自動で復旧でき、データが壊れている場合はバックアップからの復旧になります。インデックスのみが壊れている場合はどうして自動で復旧できるのかというとインデックスを再構築するためのデータがDB内にあるからです。データが壊れている場合はDB内に正しいデータがないので自動で復旧できないのです。

自動で復旧できる場合は`grndb recover`の実行方法がメッセージとして残っているはずです。次のようなコマンドラインになるはずです。

```console
$ sudo -u mysql -H grndb recover --log-path /tmp/grndb.recover.log /var/lib/mysql/db.mrn
```

自動で復旧できない場合はバックアップから復旧してください。たとえば、`mysqldump`しておいたデータをリストアします。

復旧したら[Mroongaのissue](https://github.com/mroonga/mroonga/issues)にクラッシュしたことを報告してください。報告してくれないと開発者（私とか）が問題に気づいていないため、問題が直りません。問題が直らないと再発する可能性があるため運用の負荷が下がりません。

多くの場合、問題を再現できれば数時間から1日くらいで問題を修正できます。問題が収載されれば再発に怯えなくてすみます。（万が一、別の問題でクラッシュしたときのためにバックアップをとることはやめないでください。）

問題を報告するときは次の情報を添付してください。

* MySQLのバージョン
* Mroongaのバージョン
* Groongaのバージョン
* OSの名前
* OSのバージョン
* MySQLのエラーログ（特にバックトレースが見たい）
* `groonga.log`（特にバックトレースが見たい）

必要に応じて追加の情報（たとえば問題が発生するデータ）をお願いすることもありますが、基本的にやりたいことは「問題を再現する」ことです。問題を再現できれば直せるからです。なお、問題を再現できればよいので実際に問題が発生したデータでなくても再現用に作成したテストデータでも大丈夫です。

公開の場で情報を提供できない場合は[Groongaサポートサービス]({{ "/services/groonga.html" | relative_url }})の申込みを検討してください。NDAを結んだ上での情報提供が可能になりますし、優先して修正します。

できるだけ安心してMroongaを使えるように日々開発を進めています。たとえば、先日リリースしたGroonga 11.0.3でもクラッシュにつながる問題を1つ修正しています。この問題は、Mroongaを使っているユーザーに報告してもらい、再現できたので修正できました。次のMroonga 11.04ではクラッシュ時の悪影響が大きい[`mroonga_enable_operations_recording`](https://mroonga.org/ja/docs/reference/server_variables.html#mroonga-enable-operations-recording)がデフォルトで無効になる予定です。今年の後半には、クラッシュ時にデータが壊れた場合でもできるだけ自動で復旧できるような機能を実装する予定です。Mroongaをより安心して使えるようにするためにユーザーのみなさんに協力してもらえると助かります！

ところで、今日リリースした[Mroonga 11.03以降へのアップグレードは重大な注意点がある](https://mroonga.org/ja/docs/news.html#release-11-03)ので、必ずアップグレード前に確認してください！！！（11.02以前から11.04とかへのアップグレードでも影響があります。）
