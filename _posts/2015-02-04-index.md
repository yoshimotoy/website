---
tags: []
title: Effective Ruby
---
2015年1月に翔泳社から「Effective Ruby」という中級者・上級者向けのRubyの本が出版されました。
<!--more-->


[https://amazon.co.jp/dp/9784798139821](https://amazon.co.jp/dp/9784798139821)

### 内容

とても主張が強い本です。そのため、何でも真に受ける人にはオススメできません。（初級者とか。）他の人の主張を聞いて、自分で咀嚼してよい悪いを判断できるようになってから読むべきです。

「項目5 実行時の警告に注意しよう」で警告を有効にしようと書いているなど、よいことを書いているところはあるのですが、ここでは、著者の主張が気に入らなかったところを紹介します。よいところは自分で読んで確認してください。

「項目2 オブジェクトを扱うときには`nil`かもしれないことを忘れないようにしよう」で、「適切なら」と断りをいれていますが、`to_s`や`to_i`などで`nil`を強制的に型変換しようと書いていることが気に入りません。不正なオブジェクトが見つかる時期が遅くなるので気に入りません。なぜか空文字がある、0がある、どうして？という状態になりやすくなります。

「項目3 Rubyの暗号めいたPerl風機能を避けよう」で次のように`if`の条件式のところで代入しているのが気に入りません。しかも、一般的であると書いています。代入（`=`）と比較（`==`）は紛らわしいので条件式で代入するのはやめて欲しいです。

{% raw %}
```ruby
def extract_error (message)
  if m = message.match(/^ERROR:\s+(.+)$/)
    m[1]
  else
    "no error"
  end
end
```
{% endraw %}

「項目19 `reduce`を使ってコレクションを畳み込む方法を身に付けよう」で次のようなコードを書いています。`select`よりも`reduce`の方がよいそうです。理由は効率的だからだそうです。

{% raw %}
```ruby
users.select {|u| u.age >= 21}.map(&:name)

users.reduce([]) do |names, user|
  names << user.name if user.age >= 21
  names
end
```
{% endraw %}

たしかに効率的ですが、そうまでして`reduce`を使う必要はあるのでしょうか。この例なら`each`で十分です。

{% raw %}
```ruby
names = []
users.each do |user|
  names << user.name if user.age >= 21
end
names
```
{% endraw %}

`reduce`のブロックの中で破壊的な操作をするのが気に入らないのです。次のように使うならいいです。[「あるルールに従ってデータ構造内の要素数を縮小していき、最後に残った値を返す」](http://magazine.rubyist.net/?0038-MapAndCollect#l10)という`reduce`の発想に沿った使い方だからです。ただ、効率はよくありません。

{% raw %}
```ruby
users.reduce([]) do |names, user|
  if user.age >= 21
    names + [user.name]
  else
    names
  end
end
```
{% endraw %}

「項目33 エイリアスチェイニングで書き換えたメソッドを呼び出そう」のところは気に入らないというより著者の勘違いですが、演算子でも動きます。`send(:"*_without_logging")`は動くのです。

あと、些細なことですが、コーディングスタイルは気に入りませんでした。

### まとめ

主張の強い中級者・上級者向けのRubyの本、Effective Rubyを紹介しました。他の人の主張の良し悪しを自分で判断できるようになったら読んでみてはいかがでしょうか。ここでは気に入らないところを紹介しましたが、まっとうなことを書いているところもいろいろあります。自分で考えながら読んでみてください。

Effective Rubyを読んだ後は[Rubyのテスティングフレームワークの歴史（2014年版）]({% post_url 2014-11-06-index %})を読みたくなることでしょう。

そういえば、最近のRubyの本はGCのことを説明することが当たり前になってきたのでしょうか[^0]。この本でもGCのことを説明していました。

[^0]: 将来、RubyがJVMのように自分でいろいろチューニングしないといけなくなったらどうしましょう。。。
