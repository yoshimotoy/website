---
tags:
- groonga
- presentation
title: 'PostgreSQLカンファレンス2015：PGroongaの実装 #pgcon15j'
---
[PostgreSQLカンファレンス2015](https://www.postgresql.jp/events/jpug-pgcon2015/)の[PGroongaの実装](https://www.postgresql.jp/events/jpug-pgcon2015/detail#M4)というセッションで[PGroonga](http://pgroonga.github.io/ja/)について自慢しました。PGroongaはPostgreSQLに超高速日本語全文検索機能を追加する拡張機能です。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-2015/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-2015/" title="PGroongaの実装">PGroongaの実装</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show](https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-2015/)

  * [スライド（SlideShare）](http://www.slideshare.net/kou/postgresql-conference-2015)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-postgresql-conference-2015)

### 内容

内容は基本的にPGroongaの自慢です。（amindexを実装する人向けのヒントも要所要所で入っています。）

自慢なのでPGroongaのよいところを中心に紹介しています。もちろん苦手なところもあるのですが、それはまた別の機会にしました。このセッションに参加した人の半分くらいは全文検索を使っていなかったので、どうして苦手なのかという踏み込んだ説明をすると伝わりにくくなっていたかもしれません。そういう意味で、今回はこの配分でよかったです。

今回紹介したよいところは大きくわけて次の2つです。詳細は前述のスライドを参照してください。

  * 速い

  * 全文検索のことをよく知らなくてもいい感じに日本語全文検索を実現できるようになっている

セッション前には「PGroongaをインストールしたことがある人」・「PGroongaを使ったことがある人」はほとんどいなかったのですが、セッション後にはほとんどの人が「PGroongaを使ってみよう！」という気になっていたので発表してよかったです。（セッション前後に質問をして手を挙げて回答してもらいました。）

### 他のセッション

PostgreSQLカンファレンス2015には他にもいろいろセッションがありました。その中でもPivotal Greenplumのセッションは参加してよかったセッションでした。

Pivotal Greenplumは外部の全文検索サーバーを使った全文検索の仕組みを独自に実現しているようだったので、セッションが終わった後に「PGroongaというPostgreSQLと親和性の高い全文検索拡張があるよ！使いませんか！？」とアピールしました。よさそうだね！という反応をもらったので、近いうちにPivotal GreenplumでもPGroongaを使った高速な全文検索機能が実装されるかもしれません。

参考：Pivotal GreenplumはPostgreSQLベースの（forkして実装した）データベースです。最近OSSになりました。（[リポジトリー](https://github.com/greenplum-db/gpdb)）

### まとめ

PostgreSQLカンファレンス2015でPGroongaを自慢してきました。PostgreSQLユーザーの人に知ってもらう機会ができてよかったです。主催の日本PostgreSQLユーザ会さん、ありがとうございます。

### おしらせ

2015-11-29にPGroongaが使っている全文検索エンジンであるGroongaのイベント「[Groonga Meatup 2015](https://manage.doorkeeper.jp/groups/groonga/events/31482)」があります。PGroongaの話題もあるのでPGroonga・Groongaに興味ある方はぜひお越しください。
