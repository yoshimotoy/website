---
tags:
- milter-manager
title: milter manager 2.0.0 リリース
---
2013年7月29日に[milter manager](http://milter-manager.sourceforge.net/index.html.ja)の約2年ぶりのメジャーバージョンアップである2.0.0をリリースをしました。
<!--more-->


前回ククログでmilter managerを紹介したのは[milter manager 1.6.9をリリースしたとき]({% post_url 2011-04-27-index %})である約2年前なので、milter managerとはどういったものなのか[milter managerのウェブサイト](http://milter-manager.sourceforge.net/index.html.ja)から引用します。

<blockquote>
milter managerはmilterを使って効果的に迷惑メール対策（スパムメール対策・ウィルスメール対策）を行うフリーソフトウェアです。

milter managerはmilterをサポートしているSendmailやPostfixといったMTAと一緒に使うことができます。Ubuntu、CentOS、FreeBSDなどのプラットフォーム上で動作します。 
</blockquote>

メジャーバージョンアップですが、1.8系とは互換性があるため、設定ファイルを変更せずにそのまま簡単にアップデートすることができます。前回のリリースから大きな変更もないのにメジャーバージョンをあげて2.0.0にした理由は、開発が継続していることと動作が安定していることをアピールするためです。

milter managerの前回のメジャーアップデートは2011年6月10日でした。そこから、10回目のリリースが今回のメジャーバージョンアップです。リリースの間隔が6ヶ月あいてしまったこともありましたが、こつこつと改良を続けてきました。マイナーバージョンアップリリースはmilter managerのメーリングリストだけでアナウンスしていたため、メーリングリストに参加していない方々には開発の様子が見えづらいものでした。しかし、メジャーバージョンアップとなると大きなイベントですので、これを機に、メーリングリストに参加していない方々にも、開発が継続していること、milter managerがより便利になり、より安定したことをアピールします。

実際、動作実績が増え[^0]、ユーザーのみなさんが問題を報告してくださったおかげで、1.8.0の頃よりさらに動作が安定しました。milter manager本体はもちろんですが、Rubyでmilterを書くための機能であるRuby/milterもかなり安定しました。Rubyでmilterを実装する機会が増え、さまざまなノウハウが溜まりました。これらのノウハウがRuby/milterに反映されています。

2.0.0は1.8.0よりも確実によくなっていると自信を持って言えます。これまでmilter managerを使ったことがなかったみなさんもぜひ試してみてください！

### milter manager 2.0.0 リリースに対する反応

リリースメールを以下のMLに流したところ、いくつか反応があったので紹介します。

日本語:

  * [milter-manager-users-ja](https://lists.sourceforge.net/lists/listinfo/milter-manager-users-ja)
  * [Postfixの日本語のメーリングリスト](http://lists.sourceforge.jp/mailman/listinfo/postfix-jp-list)
  * [SpamAssassinの日本語のメーリングリスト](http://www.apache.jp/mailman/listinfo/spamassassin-jp)

英語:

  * [milter-manager-users-en](https://lists.sourceforge.net/lists/listinfo/milter-manager-users-en)
  * [Postfixのメーリングリスト postfix-users@postfix.org](http://www.postfix.org/lists.html)

全て個人宛てに返信があったので、メールをくれた方の名前やメールアドレスは伏せます。

#### 海外からの反応1:

<blockquote>
I've noticed milter manager, now I will package it into Fedora.
</blockquote>

Fedora にパッケージを追加するための作業をしてくれるそうです。いつかやりたいと思っていたことなので大変嬉しいご提案です。

#### 海外からの反応2:

<blockquote>
How about writing milters in Python, rather than just Ruby?
</blockquote>

Ruby だけではなく Python で milter を書くのはどう？とのことですが、既に [pymilter](http://www.bmsi.com/python/) というプロジェクトがあるのでご紹介しておきました[^1]。

#### 海外からの反応3:

<blockquote>
Use milter in postfix will cause the performance drop about 20% due to the implement in Postfix compared to policyD(tested on Postfix 2.3.5, I am not sure if it have been improved in latest Postfix version).  That's why I prefer to PolicyD (greylist, blacklist, whitelist, IP reputation/based detection, etc). Content-based scanning will cost a lot of time which may caused session time-out issue. My suggestion is to let MTA do the basic/fast checking and ask other back-end system/module to scan the content.

BTW, it's a great idea to consolidate those milters into one milter mgr. I will try it later.
</blockquote>

自分が知っている情報を共有してくれています。[PolicyD](http://www.policyd.org/)というプロダクトは知らなかったので、参考になりました。milter manager のアイデアをとても褒めてくれています。こういったコメントはとても励みになりますね！

### test-mails プロジェクトを始めました

milter manager 2.0.0 リリースとは直接関係ないのですが、最近milter managerの関連プロジェクトとして始めた test-mails というプロジェクトがあるので紹介します。

  * [test-mailsプロジェクト](https://github.com/milter-manager/test-mails)

test-mailsプロジェクトは、メールシステムのテストに必要なメールを共有することを目的としたプロジェクトです。共有したメールはオープンソースとして利用できるので、だれでも自分のメールシステムのテストに有効活用できます。

メールシステムの開発をしていると、テストのためにとても多くのパターンのメールが必要になります。そのようなメールを個人で集めたり作成したりするのは大変です。そんなとき、自由に再利用できるテストメール集があると便利だと思いませんか？このようなメール集を共有すると、自分で多くのテスト用メールを作成したり管理したりするコストが削減でき、網羅的なテストをしやすくなることが期待できます。

興味のある人は[milter-manager/test-mails](https://github.com/milter-manager/test-mails)を参照してください。現在はまだ少しだけですが、テストで使用可能なメールを追加してあります。

[^0]: 10万mails/day以上のメールを処理するシステムでも使われています

[^1]: もちろん、milter managerはPythonで書かれたmilterも組合せて使うことができます。
