---
tags: []
title: CentOS上にDebian GNU/Linux lennyをインストール（Xen）
---
UbuntuやFedoraなど最近のディストリビューションでは、仮想化技術としてXenではなくKVMを採用していますが、CentOS 5.2やDebian GNU/Linux lennyではまだXenがサポートされています。
<!--more-->


ここではCentOS 5.2をDom0とし、その上でDomUとしてDebian GNU/Linux lennyをインストールする方法を紹介します。手順をたどっていくと、インストールしたDebianにsshでログインできる状態になります。

インストール後は通常のホストと同様にsshでログインし、使用することができます。

### CentOS上でのXenの設定

root権限の必要なコマンドは一般ユーザがsudoで行う前提で書いています。sudoの設定を行っていない場合は、suなどでrootユーザになって実行してください。

まず、Xen用のカーネルをインストールし、起動時にXen用カーネルから起動するようにします。

/etc/sysconfig/kernelを編集し、デフォルトのカーネルを変更します。これはXen用カーネルをインストールする前に行う必要があります。

変更前:

{% raw %}
```
DEFAULTKERNEL=kernel
```
{% endraw %}

変更後:

{% raw %}
```
DEFAULTKERNEL=kernel-xen
```
{% endraw %}

次に、Xen関連パッケージ（Xen用カーネルも含む）をインストールします。

{% raw %}
```
% sudo yum install -y xen
```
{% endraw %}

インストールするDebianをCentOSと同じネットワークにおきたい場合はこのまま再起動し、Xen用カーネルで起動してください。このような構成はブリッジモードと呼ばれ、例えば、CentOSのIPアドレスが192.168.1.2でDebian lennyのIPアドレスが192.168.1.3というようなネットワーク構成になります。（参考: [第3回　Xen環境で仮想ネットワークを構築：ITpro](http://itpro.nikkeibp.co.jp/article/COLUMN/20070910/281597/?ST=virtual&P=1)）

そうではなく、DebianをCentOSの後ろにあるネットワークに置く場合は設定を変更する必要があります。このようなネットワーク構成のためにNATモードとルーティングモードがあります。違いはDebianが外部ネットワークから見えるか見えないかです。NATモードでは見えませんし、ルーティングモードでは見えます。（参考: [Xen Networking - Wiki](http://wiki.kartbuilding.net/index.php/Xen_Networking): 英語ですがネットワーク構成を示した図があります）

ここでは、より安全なNATモードにします。

ネットワーク構成を変更するには/etc/xen/xend-config.sxpを編集します。デフォルトではブリッジモードになっているので、それを無効にして、NATモードにします。

変更前:

{% raw %}
```
...
(network-script network-bridge)
...
(vif-script vif-bridge)
...
#(network-script network-nat)
#(vif-script     vif-nat)
...
```
{% endraw %}

変更後:

{% raw %}
```
...
#(network-script network-bridge)
...
#(vif-script vif-bridge)
...
(network-script network-nat)
(vif-script     vif-nat)
```
{% endraw %}

設定を変更したら再起動し、Xen用カーネルで起動します。

{% raw %}
```
% sudo /sbin/shutdown -r now
```
{% endraw %}

起動後、xendサービスが起動し、ネットワークの設定が反映されます。

### Debian GNU/Linux lennyのインストール

CentOSにはvirt-installというDomUをセットアップするコマンドがありますが、このコマンドではDebianをインストールできません。Debianをインストールするにはdebootstrapを使います。

Debian関連のファイルは/var/xen/lenny以下に置くことにします。

{% raw %}
```
% sudo mkdir -p /var/xen/lenny
```
{% endraw %}

まず、Debian用のファイルシステムとスワップを用意します。ここではディスク容量を10GB、スワップサイズ512MBとしていますが、必要に応じて増やしてください。

{% raw %}
```
% cd /var/xen/lenny
% sudo dd if=/dev/zero of=disk.img bs=1M count=10240
% sudo /sbin/mkfs -t ext3 disk.img
% sudo dd if=/dev/zero of=swap.img bs=1M count=512
% sudo /sbin/mkswap swap.img
```
{% endraw %}

作成したディスクファイルをマウントし、debootstrapでDebianをインストールします。ここではi386のシステムをインストールしています。amd64のシステムをインストールしたい場合は、最後のコマンドの中の「--arch i386」を「--arch amd64」に変更してください。

{% raw %}
```
% cd /tmp
% wget http://ftp.de.debian.org/debian/pool/main/d/debootstrap/debootstrap_1.0.10lenny1.tar.gz
% tar xvzf debootstrap_1.0.10lenny1.tar.gz
% cd debootstrap
% sudo make
% sudo mount -o loop /var/xen/lenny/disk.img /mnt
% sudo su - -c "DEBOOTSTRAP_DIR=$PWD $PWD/debootstrap --arch i386 lenny /mnt http://ftp.jp.debian.org/debian/ $PWD/scripts/debian/sid"
```
{% endraw %}

ベースシステムをインストールしたら、chrootし基本的な設定を行います。

{% raw %}
```
% sudo su - -c "/sbin/chroot /mnt"
```
{% endraw %}

必要パッケージをインストール:

{% raw %}
```
lenny# apt-key update
lenny# aptitude update
lenny# aptitude install -V -D -y sudo ssh linux-image-xen-686 lv vim
```
{% endraw %}

ホスト名の設定（環境に応じて書き換えること）:

{% raw %}
```
lenny# echo 'lenny' > /etc/hostname
```
{% endraw %}

/etc/hosts（環境に応じて127.0.1.1の行を書き換えること）:

{% raw %}
```
127.0.0.1 localhost
127.0.1.1 lenny.example.com lenny

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
```
{% endraw %}

/etc/fstab:

{% raw %}
```
# /etc/fstab: static file system information.
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
proc            /proc           proc    defaults        0       0
/dev/sda1       /               ext3    defaults,errors=remount-ro 0       1
/dev/sda2       none            swap    sw              0       0
```
{% endraw %}

最後にネットワークの設定です。

ここでは、DebianのIPアドレスとして192.168.1.2を使います。このアドレスは後でXenの設定をするときに使います。

デフォルトゲートウェイはIPアドレスの最後の数字に127を足したものになります。この場合は192.168.1.129になります。

/etc/network/interfaces:

{% raw %}
```
# Used by ifup(8) and ifdown(8). See the interfaces(5) manpage or
# /usr/share/doc/ifupdown/examples for more information.

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
  address 192.168.1.2
  netmask 255.255.255.0
  gateway 192.168.1.129
```
{% endraw %}

設定が完了したら、chrootを抜けて、CentOS側にDebianのXen用カーネルを取り出します。

{% raw %}
```
lenny# exit
% sudo cp /mnt/boot/{vmlinuz,initrd.img}-2.6.26-1-xen-686 /var/xen/lenny/
% cd /var/xen/lenny
% sudo ln -s vmlinuz-2.6.26-1-xen-686 vmlinuz
% sudo ln -s initrd.img-2.6.26-1-xen-686 initrd.img
```
{% endraw %}

作業が完了したので、Debian用ディスクシステムをアンマウントします。

{% raw %}
```
% sudo umount /mnt
```
{% endraw %}

### DomUとして登録

インストールしたDebianをDomUとして登録します。

Debian用の設定は/etc/xen/lenny.cfgに書くことします。以下の設定では、メモリを512MB割り当てています。

/etc/xen/lenny.cfg:

{% raw %}
```
kernel  = '/var/xen/lenny/vmlinuz'
ramdisk = '/var/xen/lenny/initrd.img'
memory  = '512'
root    = '/dev/sda1 ro'
disk    = ['file:/var/xen/lenny/disk.img,sda1,w', 'file:/var/xen/lenny/swap.img,sda2,w']
name    = 'lenny'
vif     = ['ip=192.168.1.2']
on_poweroff = 'destroy'
on_reboot   = 'restart'
on_crash    = 'restart'
extra   = 'xencons=tty clock=jiffies'
```
{% endraw %}

vifのipパラメータの値をDebianで指定したIPアドレスと同じ値にすることに注意してください。

また、extraに'xencons=tty clock=jiffies'を設定しているところもポイントです。xencons=ttyをつけないと、起動時に止まってしまうかもしれません。close=jiffiesをつけないと、/var/log/messagesに以下のようなメッセージが大量に出力され、システムが動かなくなってしまうかもしれません。

{% raw %}
```
clocksource/0: Time went backwards: ret=5af6d65a24f delta=-312972075237201 shadow=5af3252f37f offset=3b1338fe
```
{% endraw %}

Xenの設定が完了したら試しに起動してみます。

{% raw %}
```
% sudo /usr/sbin/xm create /etc/xen/lenny.cfg -c
```
{% endraw %}

ログインプロンプトがでたら成功です。rootユーザのパスワードが設定されていないので、パスワード無しでrootユーザとしてログインできます。

ログインしたら、ユーザを作成し、sudoの設定をします。

{% raw %}
```
lenny# adduser user # ←適切なユーザ名に変えること
lenny# visudo
```
{% endraw %}

sudoの設定が完了したら、rootのパスワードはロックし、exitします。

{% raw %}
```
lenny# passwd -l
lenny# exit
```
{% endraw %}

exitすると、またログインプロンプトがでます。Ctrl+]でDebianのコンソールから抜けましょう。ターミナルの幅や高さがおかしくなっている場合はresetコマンドで直るかもしれません。

{% raw %}
```
% reset
```
{% endraw %}

この状態でsshでDebianにログインできます。

{% raw %}
```
% ssh 192.168.1.2
```
{% endraw %}

後は通常のホストのように運用できます。

### 運用時に便利な設定

CentOSが起動したときにDebianも起動するようにするには/etc/xen/auto/以下にDebianの設定ファイルへのシンボリックリンクを置きます。

{% raw %}
```
% sudo ln -s /etc/xen/lenny.cfg /etc/xen/auto/
```
{% endraw %}

CentOSに届いたパケットをDebianに転送し、実際のサービスはDebianが提供する場合はiptablesを使用します。実際のサービスは仮想化された環境上で提供するという使い方です。

以下の内容のXXX.XXX.XXX.XXXの部分をCentOSのIPアドレスに変更し、/etc/sysconfig/iptablesに保存すると、次回起動時からCentOS宛のHTTPとHTTPSのパケットはDebianが処理します。[^0]

/etc/sysconfig/iptables:

{% raw %}
```
#
*nat
:PREROUTING ACCEPT [258:20744]
:POSTROUTING ACCEPT [18:1152]
:OUTPUT ACCEPT [233:17232]
# HTTP
-A PREROUTING -d XXX.XXX.XXX.XXX -p tcp -m tcp --dport 80 -j DNAT --to-destination 192.168.1.2:80
# HTTPS
-A PREROUTING -d XXX.XXX.XXX.XXX -p tcp -m tcp --dport 443 -j DNAT --to-destination 192.168.1.2:443
COMMIT
```
{% endraw %}

### まとめ

KVMではなくXenを利用した仮想化環境へのDebian GNU/Linux lennyのインストール方法を紹介しました。

CentOS 6ではKVMが利用できるようになっているかもしれませんが、CentOS 5.2がよく使われている現在においてはまだXenを使用する機会も多そうです。CentOSがあるけど実際に使いたいのはDebian系、という場合はCentOS Dom0 + Debian GNU/Linux lenny DomUの組み合わせが使えます。

[^0]: 再起動しなくても「sudo /sbin/service iptables restart」で反映させることができます。
