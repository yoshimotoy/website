---
title: "サポート事例紹介: nginxの最大接続数とreuseport"
author: daipom
tags:
  - free-software
---

こんにちは。データ収集ツール[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

今回の記事では、有名なWebサーバーアプリケーションである[nginx](https://nginx.org/en/)の最大接続数のチューニングについて調査した事例を紹介します。

最大接続数をチューニングするにあたり、[reuseport](http://nginx.org/en/docs/http/ngx_http_core_module.html#reuseport)設定とファイルディスクリプター数上限([worker_rlimit_nofile](http://nginx.org/en/docs/ngx_core_module.html#worker_rlimit_nofile))を考慮する必要がある、という点が重要です。

この調査は[クリアコードのFluentd法人様向けサポートサービス]({% link services/fluentd-service.md %})の一貫で実施したもので、
Fluentdとセットで周辺のフリーソフトウェアのサポートを実施した事例となっています。

nginxなどのwebサーバーの性能チューニングや、クリアコードの法人様向けサポートサービスに興味のある方は、ぜひ本記事をご覧ください。

<!--more-->

### 背景

今回のサポート事例は、nginxの限界性能についてお客様からご質問をいただき、最大接続数のチューニングの仕方について調査した、というものになります。

nginxでは、同時に多くの接続が生じると、次のように`worker_connections are not enough`や`Too many open files`といったエラーが発生します。

```
[alert] xxxx: *xxxx 1024 worker_connections are not enough while connecting
 to upstream, client: xxxx, server: xxxx, request: "xxxx", upstream: "xxxx", host: "xxxx"
```

```
[alert] xxxx: *xxxx socket() failed (24: Too many open files) while
 connecting to upstream, client: xxxx, server: xxxx, request: "xxxx", upstream: "xxxx", host: "xxxx"
```

```
[crit] xxxx: accept4() failed (24: Too many open files)
```

このようなエラーが発生せずに処理できる最大の接続数はどう決まるのか、およびその正しいチューニング方法について、調査しました。

なお、本記事は以下の環境を前提とします。

* nginx: version 1.16.1
* OS: RHEL 8.6

### 基本的な考え方と計算式

今回の事例では、nginxをフォワードプロキシとして利用していました。

検証したところ、およその最大接続数は基本的には次の計算式で求められました。

* およその最大接続数 = [worker_processes](http://nginx.org/en/docs/ngx_core_module.html#worker_processes)(ワーカープロセス数) * [worker_connections](http://nginx.org/en/docs/ngx_core_module.html#worker_connections)(各ワーカーの最大接続数) / 2

2で割るのは、1接続あたりプロキシ元とプロキシ先の2つ接続を持つ必要があるからだと推測できます（これは設定によって変わるかもしれません）。

しかし、この値付近までエラーなく接続を安定して処理するには、後述する次の2つ設定が必要でした。

* [reuseport](http://nginx.org/en/docs/http/ngx_http_core_module.html#reuseport)
* ファイルディスクリプター数([worker_rlimit_nofile](http://nginx.org/en/docs/ngx_core_module.html#worker_rlimit_nofile))

### reuseport設定で各ワーカーに接続を分散させる

同時に大量の接続を行う検証をすると、1つのワーカーに接続が偏る傾向が分かりました。
このため、接続が偏ったワーカーが先行して限界([worker_connections](http://nginx.org/en/docs/ngx_core_module.html#worker_connections))に達し、とても早い段階で`worker_connections are not enough`エラーが発生してしまいました。

```
[alert] xxxx: *xxxx 1024 worker_connections are not enough while connecting
 to upstream, client: xxxx, server: xxxx, request: "xxxx", upstream: "xxxx", host: "xxxx"
```

この接続の偏りを解決する方法を調べたところ、[reuseport](http://nginx.org/en/docs/http/ngx_http_core_module.html#reuseport)設定を使うことで解決することが分かりました。
`resuseport`設定を使うと、ソケットの[SO_REUSEPORT](https://man7.org/linux/man-pages/man7/socket.7.html)オプションを利用し、各ワーカーが独自に同じポートに対してソケットをバインドするようになります。
これにより、さきほどの計算式の最大接続数の値の90%程度までエラーなく接続を処理できるようになることを確かめられました。

以下、`reuseport`設定の注意点です。

`reuseport`は、[listen](http://nginx.org/en/docs/http/ngx_http_core_module.html#listen)設定に付与する形になります。
ただし同じIPとポート番号のセットに重複して設定をすると`duplicate listen options`の設定エラーになるので、IPとポート番号のセット毎に1回だけ設定する必要があります。
`listen`のIPを省略する場合は、`0.0.0.0`を指定したことになります。

```console
$ nginx -t
nginx: [emerg] duplicate listen options for ...
```

(`-t`オプションで設定のバリデーションを行えます)

例:

```nginx
# IPとポートの組み合わせ毎にreuseportを付与する
server {
    listen 8080 reuseport;
}

server {
    listen 192.168.0.1:8080 reuseport;
}

server {
    listen 192.168.0.2:8080 reuseport;
}

# 重複して付与しないように注意する
server {
    listen 8080;
}
```

また、[reuseport](http://nginx.org/en/docs/http/ngx_http_core_module.html#reuseport)の説明文にセキュリティーに関する注意があります。

> Inappropriate use of this option may have its security implications. 

この点について説明します。
`reuseport`を使うと、nginxのワーカープロセスと同じユーザーで実行されている他のプロセスも、そのポートのデータを読めてしまいます。
正しく使用しないとセキュリティー上のリスクとなるため、注意が必要です。
好ましい対策としては、[user](http://nginx.org/en/docs/ngx_core_module.html#user)設定を利用するなどして、nginxのワーカープロセスをnginx専用ユーザーで動作させるように徹底してください。

### ファイルディスクリプター数を十分大きくしておく

ファイルディスクリプター数上限の限界に達すると、最大接続数まで余裕があっても次のような`Too many open files`のエラーになってしまいます。

```
[alert] xxxx: *xxxx socket() failed (24: Too many open files) while
 connecting to upstream, client: xxxx, server: xxxx, request: "xxxx", upstream: "xxxx", host: "xxxx"
```

```
[crit] xxxx: accept4() failed (24: Too many open files)
```

対策として、[worker_rlimit_nofile](http://nginx.org/en/docs/ngx_core_module.html#worker_rlimit_nofile)を十分大きな値にしておく（少なくとも想定する最大接続数よりも大きい値にしておく）のが良いです。
この設定により、ワーカープロセスのファイルディスクリプター数上限のソフトリミットとハードリミットの双方を変更することができます。
上限値は、システムとしての1プロセスの上限である`/proc/sys/fs/nr_open`で定義されている値となります。

例:

```console
$ cat /proc/sys/fs/nr_open
1048576
```

また、unitファイルにおける`LimitNOFILE`でも同様の設定が可能です。
SELinuxの`httpd_setrlimit`の制限を受ける場合は`worker_rlimit_nofile`の設定が失敗するらしいので、その場合は`LimitNOFILE`の方が良さそうです。

### まとめ

本記事では、有名なWebサーバーアプリケーションである[nginx](https://nginx.org/en/)の最大接続数のチューニングについて調査した事例を紹介しました。

以下の点を考慮してチューニングしてください！

* およその最大接続数 = [worker_processes](http://nginx.org/en/docs/ngx_core_module.html#worker_processes)(ワーカープロセス数) * [worker_connections](http://nginx.org/en/docs/ngx_core_module.html#worker_connections)(各ワーカーの最大接続数) / 2
  * 2で割る点は、設定によって異なる可能性がある
* [reuseport](http://nginx.org/en/docs/http/ngx_http_core_module.html#reuseport)の設定をしないと、特定のワーカーに接続が偏って早い段階でエラーになることがある
* [worker_rlimit_nofile](http://nginx.org/en/docs/ngx_core_module.html#worker_rlimit_nofile)などでファイルディスクリプター数上限を大きくしておく必要がある

この調査は[クリアコードのFluentd法人様向けサポートサービス]({% link services/fluentd-service.md %})の一貫で実施したもので、
Fluentd単体に限らず、このようにFluentdとその周辺のフリーソフトウェア（Redis, Postfix, K8s, Embulkなど）をセットでサポートする事例やご相談もあります。

このようにクリアコードはフリーソフトウェア全般を得意としておりますので、
もしFluentdや周辺のアプリケーションの運用でお困りのことがありましたら、
ぜひ[お問い合わせフォーム]({% link contact/index.md %})からお気軽にご相談ください。

同様のサポート事例として次の記事もあるので、ぜひご覧ください。

* [Fluentdサポート事例: Postfixのログが途中で途切れてしまう]({% post_url 2024-06-26-fluentd-support-case-postfix-log-truncated %})
* [サポート事例紹介: Redisのフェールオーバーの原因調査]({% post_url 2024-09-06-fluentd-support-case-redis-failover %})
