---
tags:
- milter-manager
title: milter manager 0.9.0リリース
---
迷惑メール対策ソフトウェア、[milter manager](http://milter-manager.sourceforge.net/index.html.ja) 0.9.0をリリースしました。
<!--more-->


[変更点](http://milter-manager.sourceforge.net/reference/ja/news.html)にもある通り、前のリリースである0.8.0より処理速度と安定性が向上しています。手元のメールシステムでも安定して動作しており、興味のある方に試してもらえる品質になってきたと思います。

### 今回のリリースについて

このリリースは1.0.0のリリース候補版という位置付けです。0.9.0に問題が見つからなかった場合は0.9.0が1.0.0としてリリースされます。

最初のリリース時にもらったフィードバック[^0]を元に[0.8.0]({% post_url 2009-02-06-index %})でいくつか機能を拡張しましたが、今回はmilter manager本体に目立った機能追加はありません。これは、いつまでも完成しない1.0.0を待ってもらうより、まずは安定して動作するmilter managerを提供して、有効に使ってもらう方が有用だろうという判断からです。

いくつか追加機能の案はあるのですが、それは1.0.0のリリース後に実装する予定です。0.9.0のリリースでは組み込みの適用規則の追加と周辺ツールの機能追加にとどめました。

### 新適用規則

今回は以下の2つの適用規則が追加されました。

  * sendmail-compatible: Sendmailでしか動かないmilterをPostfixでも動くようにする機能を提供
  * authentication: 以下の2つの適用規則を含む
    * 認証されて*いる*ときのみmilterを適用する規則
    * 認証されて*いない*ときのみmilterを適用する規則

sendmail-compatibleは常にmilterを適用するので適用規則ではないのですが、適用規則の枠組みを使った応用例ということで追加しました。

適用規則中ではMTAからmilterに渡されるマクロを参照・変更することができます。この機能をフィルタのように利用しているのがsendmail-compatibleです。

SendmailとPostfixはともにmilter対応しているMTAですが、milterに渡すマクロの実装に互換性がありません。例えば、SendmailとPostfixではマクロ名が異なる、SendmailとPostfixでマクロが渡ってくるタイミングが違う、ということがあります。（詳しくは[Postfix before-queue Milterサポート](http://www.postfix-jp.info/trans-2.3/jhtml/MILTER_README.html#workarounds)にあります）

この非互換の部分に依存しているmilterはSendmailでは動いてもPostfixでは動かない場合があります。例えば、[dnsbl-milter](http://dnsbl-milter.sourceforge.net/)がそのようなmilterです。[^1]

sendmail-compatibleは、Postfixが渡してきたマクロをSendmailが使っているマクロ名でもアクセスできるようにしたり、ダミーの値を入れたマクロをmilterに渡すなどして、できるだけ非互換の部分を利用しているmilterも動作するようにします。

適用規則の部分はmilter managerの動作の中で、もっとも特徴的で応用範囲が広い部分です。1.0.0リリース後はこれ以外にも応用できるような機能を追加する予定です。

なにかアイディアがあれば[メーリングリスト](https://lists.sourceforge.net/lists/listinfo/milter-manager-users-ja)などで共有してもらえると、より有用な迷惑メール対策システムを構築できるようになるかもしれません。お待ちしています。

#### まとめ

0.9.0ではこれまでよりも速度と安定性が向上していて、実際に試してみることができる品質になっています。milter managerに興味のある方はぜひ試してみてください。もし、問題が見つかった場合はその問題を修正してから1.0.0をリリースします。（問題の修正が大きな変更を伴う場合は既知の問題として修正を先送りする可能性もあります）

機能追加は1.0.0リリース後に行うので、もうしばらくお待ちください。

参考: [milter managerのインストールドキュメント](http://milter-manager.sourceforge.net/reference/ja/install.html)

[^0]: 例えば[milter managerを利用したRgrey - モーグルとカバとパウダーの日記](http://d.hatena.ne.jp/stealthinu/20090126/p5)。ありがとうございます。

[^1]: この問題を修正するパッチは[提出済み](http://sourceforge.net/tracker/?func=detail&atid=1015126&aid=2594714&group_id=210782)。
