---
tags: []
title: git-utils 0.0.1リリース
---
git関連の様々なユーティリティソフトウェアのパッケージであるgit-utilsのバージョン0.0.1をリリースしました。このパッケージには、gitリポジトリ用のコミットメール送信スクリプトcommit-email.rbが入っています。以下からダウンロードできます。
<!--more-->


  * [packages.clear-code.comからダウンロード](http://packages.clear-code.com/git-utils/git-utils-0.0.1.tar.gz)

### 概要

今回のリリースはgit-utilsの最初のリリースです。

このパッケージに同梱されているcommit-email.rbを使うことで、gitリポジトリにpushされたコミットのコミットメールを送信することができます。

gitでは複数のコミットを一度にリポジトリに追加することができます。これはpushと呼ばれています。commit-email.rbはインストールしたリポジトリに対してpushが発生する度に実行されます。そして、そのpushの中にあるコミットごとに差分付きでメールを送信します。gitで標準でインストールされるコミットメール送信ソフトウェアだとpushごとに一通のメールが送信されます。

差分も付いているのでコミットの中身を調べ直すためにローカルのリポジトリを参照する必要はありません。なので、しっかりとソースコードの修正を含めてリポジトリの変更をフォローしていきたい、尚且つ、メールのみでリポジトリの変更を把握したい場合に適しています。

### 使い方

それでは早速実際に使ってみましょう。今回は、コミットメールの送り先として、Unix-likeなOSに標準で付いているスプール方式のメールボックスにコミットメールを送ることにします。メールアドレスが「ローカルマシンのユーザー名@localhost」となります。

まずはコミットメールを送信するためのgitリポジトリを作成します。今回はローカルで作業しますが、このリポジトリはサーバー上にある公開用のgitリポジトリだと思って下さい。

{% raw %}
```
$ git clone --bare http://git.clear-code.com/git-utils git-utils
```
{% endraw %}

次に新しく作成したgitリポジトリでcommit-email.rbを動かすように設定します。<var>user</var>の部分をローカルマシンで使っているあなたのユーザー名に置き換えてください。

{% raw %}
```
$ cd git-utils
$ cat <<'EOC' > ./hooks/post-receive
#!/bin/bash
/usr/bin/ruby ./hooks/commit-email.rb --repository="$(pwd)" --to user@localhost
EOC
$ chmod +x ./hooks/post-receive
```
{% endraw %}

上の2行のコマンドではbareなgitリポジトリに移動し、commit-email.rbを実行するpost-receiveと呼ばれるフックスクリプトを作成し、実行権限を付与しています。

次にcommit-email.rbをpost-receiveから実行できるようにファイルを用意する必要があります。上のリンクからgit-utils-0.0.1.tar.gzをダウンロードし、展開後、commit-email.rbというファイルを./hooksディレクトリにコピーして下さい。

これでコミットメールを送信するリポジトリ側の設定は完了です。次にこのリポジトリからcloneして作業用のリポジトリを作成します。そして、コミットを作成し、pushして実際にメールが送信されるかを確認してみましょう。

{% raw %}
```
$ cd ..
$ git clone git-utils git-utils-work
$ cd git-utils-work
```
{% endraw %}

./test-commit-email.rbを最新開発版のRuby 1.9.3devで動かしてみると分かりますが、`Time#succ`はobsoleteされ`time + 1`を使うように警告されます。今回はこれを直しましょう。git diffすると以下のようになるように編集してください。

{% raw %}
```diff
diff --git a/test-commit-email.rb b/test-commit-email.rb
index c4a7024..8f1857f 100755
--- a/test-commit-email.rb
+++ b/test-commit-email.rb
@@ -658,7 +658,7 @@ module HookModeTest
     end

     def advance_timestamp
-      @timestamp = @timestamp.succ
+      @timestamp += 1
     end

     def delete_output_from_hook
```
{% endraw %}

それではいよいよコミットしpushして見ましょう。

{% raw %}
```
$ git commit -m "use time + 1 instead of Time#succ" ./test-commit-email.rb
$ git push origin master
```
{% endraw %}

これで以下のようなメールがメールボックスに追加されているはずです。

{% raw %}
```
Ryo Onodera     2010-05-18 13:05:56 +0900 (Tue, 18 May 2010)

  New Revision: a6841ded1d2cc70d6d98d03de023c5a57f3c0f85

  Log:
    use time + 1 instead of Time#succ

  Modified files:
    test-commit-email.rb

  Modified: test-commit-email.rb (+1 -1)
===================================================================
--- test-commit-email.rb    2010-05-13 18:59:25 +0900 (c4a7024)
+++ test-commit-email.rb    2010-05-18 13:05:56 +0900 (8f1857f)
@@ -658,7 +658,7 @@ module HookModeTest
     end

     def advance_timestamp
-      @timestamp = @timestamp.succ
+      @timestamp += 1
     end

     def delete_output_from_hook
```
{% endraw %}

### まとめ

git-utils 0.0.1の初リリースの告知とその中に同梱されているcommit-email.rbの説明とインストール方法を紹介しました。
