---
tags:
- fluentd
- presentation
title: 'Prometheus Meetup Tokyo #3でLTをしました'
---
### はじめに

Prometheus Meetup Tokyo #3でLTをしてきた畑ケです。
[Prometheus](https://prometheus.io/)は近年注目されるメトリクスモニタリングツールです。特徴としては非常にスケーラビリティが高く、Pull型でメトリクスを収集するため、Prometheusのインスタンス1つで1万インスタンスのサーバー群を監視できます。この[Prometheus Meetup Tokyo #3](https://prometheus.connpass.com/event/157721/)ではPrometheus本体というよりもそれを取り巻くエコシステムを対象としたミートアップでした。
<!--more-->


筆者は最近Prometheusのエコシステムの一つでGrafana Labが出しているLokiというログ基盤を触ったことがあり、触った時の成果の一つがLTのネタになるのではないかとのことでLTに応募してみたところ、LTに採択されました。

### イベントの内容

まず他の方のされた発表をご紹介します。

#### Remote Write API と Thanos を活用したメトリクス永続化

  * Moto Ishizawa 氏(@summerwind), Z Lab Corporation

コンテナを基盤とする環境でPrometheusを運用していると、例えば、k8sのクラスターを作り直したときにPrometheusで収集したメトリクスが消失してしまう問題があるそうです。
このメトリクスがk8sクラスターに紐づいてしまっている問題を解消するのに永続化ストレージを採用し、永続化を試みていました。[Thanos](https://github.com/thanos-io/thanos)というPrometheusを高可用性にし、長期間のログ保存を可能にするソフトウェアを用いて方法をPrometheusで収集したメトリクスを永続化する方法を丹念に調査していた発表でした。

#### Victoria Metricsで作りあげる大規模・超負荷システムモニタリング基盤

  * 入江 順也 (GitHub: inletorder)氏, 株式会社コロプラ

[Victoria Metrics](https://github.com/VictoriaMetrics/VictoriaMetrics)というこちらもPrometheusで収集したメトリクスを長期間永続化するソフトウェアを使って、Prometheusを高負荷環境にも耐えられえるようにした試行錯誤を発表されていました。このソフトウェアにたどり着くまでにいくつかのPrometheus関連のメトリクス永続化ストレージを試されたそうです。Victoria Metricsはいくつかのコンポーネント（VMStorage, VMSelect, VMInsert）に分かれており、そのうちVMStorageは持つデータによって状態を持つのでk8sではStatefulSetとしてデプロイする必要があるそうです。k8sのマルチテナント構成では合計1万Pod以上の監視を安定的に行えるようになったそうです。

#### 次世代のログ基盤 Grafana Lokiを始めよう！

  * 仲亀 拓馬氏(@kameneko1004, さくらインターネット 株式会社), 上村 真也氏(@uesyn, Z Lab Corporation)

本ククログで何回か筆者が開発者視点で取り上げているGrafana Lokiについての発表です。Lokiについてのデモを通じてどのようなソフトウェアなのかを解説するのが前半の発表でした。後半はpromtail特集でした。promtailはpromtail.yamlにてPrometheusの設定と同様の設定を流し込むことで設定できるそうです。Prometheusと同様にGrafana Lokiも時系列データを保持するのにTSDBを使用しており、これに入れたデータをクエリするのにラベルが必要になるのですが、このラベル設計がうまくないと後に目的の時系列データをクエリするのに苦労するようです。

### LTの内容

以下は筆者が行ったLTの内容です。

Grafana Lokiの開発元にFluent BitのGo製のLokiプラグインをフィードバックしてみた話をしました。
このFluent BitのGo製Lokiプラグインは[Fluent BitからGrafana Lokiに転送する方法]({% post_url 2019-07-31-index %})をまとめたときに紹介しました。
Grafana Lokiは開発が活発なソフトウェアということもあり、ドキュメントがあまり見当たりませんでした。
そのため、promtailがやっていることをソースコードを見つつFluent BitのGo製Lokiプラグインとして仕上げました。

<div class="slideshare-wide">
<iframe src="//www.slideshare.net/slideshow/embed_code/key/ozvdphrJENaZoC" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/cosmo0920/prometheus-meetup-tokyo3-slide-220572881" title="Prometheus meetup tokyo#3 slide" target="_blank">Prometheus meetup tokyo#3 slide</a> </strong> from <strong><a href="https://www.slideshare.net/cosmo0920" target="_blank">cosmo0920</a></strong> </div>
</div>


LTではフリーソフトウェアにフィードバックする作法を軽く触れました。
Grafana Lokiもフリーソフトウェアです。フリーソフトウェアの開発は通常開発者同士が同時に同じ場所に集まっているのではなく、住んでいる国や文化、ひいては暮らしているタイムゾーンも異なる場合があります。
フィードバックするには言葉で説明しなければいけません。

まずは、動機や今困っていることを説明します。

  * なぜこの機能が必要か？なぜこの問題を報告したのか？

  * このIssueチケットやプルリクエストでは何を問題にするのか？

プルリクエストは小粒なtypo修正ではない限り、Issueチケットに関連付けられるものとして出す方が良いと筆者は考えています。しかし、この方針はプロジェクトによって異なります。プルリクエストを出す時はプロジェクトの方針を確認してみてください。

  * プルリクエストでは方針を議論するよりも提出したパッチが前もって議論した方針に合っているか、このプロジェクトに受け入れられる品質となっているか？を議論する場だからです。

プルリクエストやIssueチケットにチェックリストが付いているのであれば一通り確認すべきです。

  * 問題が発生している報告者の環境を開発者が再現するには十分な情報が書き込まれている必要があります。

  * よいIssueチケットは開発者が見た時にどのようにすればこの問題が再現できるか？がチケットを見ただけで理解できるチケットです。

  * レビュアーが見るべき箇所が発散しておらず、実現したい機能が実現できているか、パッチの変更は妥当かのレビューに集中できるものがよいプルリクエストです。

Grafana LokiにFluent BitのGo製プラグインをフィードバックしてみたところ、ユーザーもそこそこ出てきたようです。

Grafana Loki自体は一個のバイナリですし、promtailもバイナリ一個で済みます。そのため、Dockerコンテナに載せやすいです。
これらの特徴に加え、Grafana LokiはPrometheusファミリーということもありk8sと非常に親和性がよいです。

k8sに載せるにはまずDockerコンテナ化しないといけないということで、Dockerコンテナ化の要望が新たにIssueチケットとして切られました。
このFluent BitのGo製のLokiプラグインDockerイメージもGrafana Lokiの公式イメージとして提供されることになりました。

k8sでは複数のサービスを連携して動作させる必要がありますが、手動で連携させるには面倒な場合があります。
この煩雑さを解決するソフトウェアはいくつか出ています。Grafana Lokiの開発元からはこの煩雑さを解決するソフトウェアの[helm](https://helm.sh/)を用いたFluent BitのGo製Lokiプラグインのレシピが提供されることになりました。

筆者はこのhelmのレシピがきっかけでk8sをより深く理解することになりました。自身の成果をフリーソフトウェアにフィードバックするだけで終わりではなく、フィードバックすることにより学びのきっかけを頂けました。

### まとめ

日本ではあまり事例の少ないFluent BitとGrafana Lokiを題材にしてフリーソフトウェアの開発元にフィードバックする作法をLTしました。
フリーソフトウェアの問題を手元で回避するのではなく、開発元にフィードバックするのは[クリアコードが普段実践している開発スタイル](https://www.clear-code.com/philosophy/development/style.html#fix-in-upstream)です。

フリーソフトウェアを普段使っている方でもフリーソフトウェアの開発元にフィードバックする方法が分からず、手元で問題を回避していたり、手元でパッチを持ったままになっている方もいると思います。
その時には本記事のフリーソフトウェアであるGrafana Lokiの開発元へフィードバックした事例をヒントにしてフィードバックに挑戦してみてはいかがでしょうか？

また、フリーソフトウェアの開発にまだ参加したことがない人を対象にして[OSS Gateワークショップ](https://oss-gate.doorkeeper.jp/)を開催しています。こちらも併せて検討してみてください。
