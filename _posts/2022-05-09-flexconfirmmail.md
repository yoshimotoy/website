---
tags:
  - free-software
author: fujimotos
title: "メール誤送信対策製品「FlexConfirmMail」のMicrosoft Outlook版をリリースしました"
---

### 1. 概要

クリアコードでは [FlexConfirmMail](https://github.com/FlexConfirmMail) というフリーソフトウェアを開発しています。もともと2013年からThunderbird向けに開発を始めたメール誤送信対策製品で[^1]、現在も国内外で数万ユーザーに利用されている割とポピュラーな製品です。

[^1]: 2013年にconfirm-mailをフォークする形で開発を始めました。最初のコミットは [2013年9月6日](https://github.com/FlexConfirmMail/Thunderbird/commit/1f1b20481) です。

[先週、FlexConfirmMailをMicrosoft Outlook向けに移植したバージョンを公開しました。]({% link press-releases/20220427-flexconfirmmail-outlook.md %})MicrosoftのOffice APIを利用して一から実装したもので、Thunderbirdで実証済みの誤送信対策がMicrosoft Outlookでも使えるようになりました。

この記事では「誤送信対策を始めたい」・「会社で対策が必要だがどこから手を付けていいか分からない」といった企業の担当者の方を主なターゲットとして、この新しいアドインについてご紹介します。

<!--more-->

### 2. 「メール誤送信問題」入門

メール誤送信は企業のシステム運用の中でも非常に重要な問題です。
誤送信に起因するトラブルは実は国内外の様々な組織で毎週のように起きており、
それが報道に載ることも決して珍しくありません。

一つの事例として、ちょうど先月ニュースになったデジタル庁の事例を紹介します。

> (1) 発生事案
>
> e-Gov 利用者サポートデスクの運用をデジタル庁が委託している委託先の事業者において、
> 令和4年4月4日（月）18時54分、利用者A氏からの問合せについて、
> A氏に対して回答すべきメール本文の内容を、誤って利用者B氏に対してメールを送付したため、
> B氏に対して送付したメールの本文中に、A氏のメールアドレスが記載されている事案が生じました。
>
> [デジタル庁「委託事業者におけるメールの誤送信について」 (2022年4月6日)](https://www.digital.go.jp/press/a264aa83-154e-4677-8481-d29dcab34eed/)

この事例を読んで「ああこれはありがちだな」あるいは「似たような事故が身近で起きたことがある」という感想を覚えた方も少なくないはずです。

その感覚は間違いではありません。というのも、この手の事故は **本当に** たくさん起きているからです。その一つの証拠として、直近1〜2ヶ月で発生した事故（以下はすべて公表されている事例です）を以下の表にまとめました。このように、メールの送信ミスに起因する事故は際限なく起きており、枚挙に暇がありません。

**（表）2022年3月〜4月に発生した誤送信の事例**

| 発表日        | 件名   |
| ------------- | ------ |
| 2022年3月11日 | [内閣官房内閣人事局におけるメールアドレス取扱いの不備について](https://www.cas.go.jp/jp/gaiyou/jimu/jinjikyoku/files/20220311_kouhyousiryou.pdf)
| 2022年3月22日 | [委託事業受託者による個人情報漏えい事案について](https://jsite.mhlw.go.jp/iwate-roudoukyoku/content/contents/001114957.pdf)
| 2022年4月03日 | [県民安全・安心メールの誤配信について（お詫びと報告）](https://www.pref.oita.jp/soshiki/13550/260407-mailerror.html)
| 2022年4月10日 | [図書の団体貸出しにおける事務連絡に係るメール誤送信について](https://www.city.chiba.jp/somu/shichokoshitsu/hisho/hodo/documents/220410-1.pdf)
| 2022年4月13日 | [個人情報の漏洩について（メールアドレスの流出）](https://www.metro.tokyo.lg.jp/tosei/hodohappyo/press/2022/04/14/15.html) |
| 2022年4月25日 | [生野区役所におけるメール誤送信による個人情報等の漏えいについて](https://www.city.osaka.lg.jp/hodoshiryo/ikuno/0000565531.html)

### 3. FlexConfirmMailとは何か？

FlexConfirmMailはメールクライアント用の誤送信対策アドインです。メールの送信時に宛先情報をチェックするポップアップを表示することで送信ミスを予防する製品です。

言葉で説明するよりは、動作を見た方がコンセプトを掴みやすいので、以下に実際の操作イメージをご用意しました。一連の「送信ボタン→ポップアップ確認→カウントダウン」という処理の流れを確認いただけるはずです。

**（図）FlexConfirmMailはどのように誤送信を防ぐのか**

![FlexConfirmMail Dialog Example]({% link images/blog/20220509/fcm.gif %})

このポップアップ方式の誤送信対策の有効性は、様々な業種・規模の企業で裏付けられています。

クリアコードが直接把握している利用事例はごく一部に過ぎませんが[^2]、
これまで通信キャリア・金融会社・電子部品製造・設備メーカー・鉄鋼メーカなどから
サポートや開発のご依頼を承ってきました。
企業の規模も様々で、100名程度の企業から、グローバルに拠点を持つ数万名規模の企業まで、
幅広い組織で運用されてきた実績があります。

また、FlexConfirmMailが非常に優れたメール誤送信対策であることは、
実際のユーザーの声でも裏付けられています。現場の方から「このアドオンがないと
怖くてメールを使えない」と言った感想をいただくことは珍しくありません。
具体例として、Thunderbrid版のレビューから寄せられた感想を紹介します：

* [by Firefox user 44f41e on Oct. 23, 2019](https://addons.thunderbird.net/en-US/thunderbird/addon/flex-confirm-mail/reviews/1158979/)
  ```
  ★★★★★
  このアドオン無しには怖くてメールが送信できないほどになりました。
  Ver68以降のバージョンへの対応を心待ちにしております！
  ```
* [by 506ue on Sept. 18, 2020](https://addons.thunderbird.net/en-US/thunderbird/addon/flex-confirm-mail/reviews/1163262/)
  ```
  ★★★★★
  78.2.2 (64 ビット)への対応をよろしくお願いします。
  このアドオンが無いと、メールを安心して送信できません。

  要望なのですが、送信時に複数のSMTPアカウントを登録してある場合、
  どのアカウントから送信しているのかをチェックボックスを入れて送信できるようになると大変助かります。
  ```
* [by Firefox user 5bfe72 on April 28, 2021](https://addons.thunderbird.net/en-US/thunderbird/addon/flex-confirm-mail/reviews/1166457/)
  ```
  ★★★★★
  Ver78の対応ありがとうございます。
  本当に感謝しています。

  １点要望ですが、可能でしたら添付ファイル確認時にファイルサイズも
  表示されると非常にありがたいです。よろしくお願いします。
  ```

[^2]: FlexConfirmMailはフリーソフトウェアとして配布している製品のため、
利用の有無を知る機会は「FlexConfirmMailを使っているが
機能拡張の改修をお願いしたい」といった開発・サポートのご相談をいただくケースに限られます。

### 4. FlexConfirmMailを使い始めるには

今回公開したOutlook版のインストーラは公式サイトで配布しています。具体的なセットアップ手順と設定方法は「クイックスタート」にまとめてあります。

* [FlexConfirmMail 最新版インストーラをダウンロードする](https://www.flexconfirmmail.com/download.html)
* [FlexConfirmMail クイックスタート](https://www.flexconfirmmail.com/quickstart.html)

手順をたどれば、数分で端末のMicrosoft Outlook上でセットアップが完了しますので、
ぜひご活用ください！

### 5. まとめ

この記事では、2022年4月に新しくリリースしたFlexConfirmMailアドインについて紹介しました。

フリーソフトウェアライセンスで公開している製品ですので「誰でも自由に使える誤送信対策」として幅広い企業で活用いただけることを期待しています。製品に関する質問や意見などあれば [GitHub Issue](https://github.com/FlexConfirmMail/Outlook/issues) か [お問い合わせフォーム](https://www.clear-code.com/contact/) からお寄せください。
