---
tags:
- embedded
title: ChainerのYoctoレシピを公開
---
### はじめに

クリアコードは組み込みLinux向けにMozilla Firefoxを移植するプロジェクト[Gecko Embedded](https://github.com/webdino/gecko-embedded/wiki)を[WebDINO Japan（旧Mozilla Japan）](https://www.webdino.org/)様と共同で立ち上げ、開発を進めております。
この組み込みLinux向けに対して、深層学習のフレームワークの一つの[Chainer](https://chainer.org)を載せようという話がありましたので、ChainerのYoctoレシピを作成しました。
<!--more-->


### 現在のステータス

Yoctoの2.0または2.1で動作を確認しています。
動作は[iWave RainboW-G20D Q7](http://www.iwavejapan.co.jp/product/RENESASRZG1M_DEV.html)および[Renesas R-Car Gen3](https://elinux.org/R-Car/Boards/Yocto-Gen3)で確認しています。

レシピはGitHubにて公開しています。https://github.com/clear-code/meta-chainer
Yoctoに組み込むには、meta-chainerを `git clone` したのち、(bitbakeのビルドディレクトリ)/conf/local.confへ

```
IMAGE_INSTALL_append = " python-chainer "
```


(bitbakeのビルドディレクトリ)/conf/bblayers.confへ

```
BBLAYEYS += " ${TOPDIR}/../meta-chainer "
```


をそれぞれ追加し、bitbakeを実行します。

### 諸注意

動作対象の組み込みボードではCUDAが動作しないため、作成したモデルの互換性に注意する必要があります。
Chainer 1系の時に作成されたコードではモデルの保存に `pickle.dump(...)` が使用されていましたが、
この形式では保存する環境に存在するcupyなどの情報も保存されてしまいます。
この場合は、[chainerに付属するserializerを用いて保存するように書き換え](https://github.com/yusuketomoto/chainer-char-rnn/commit/131f7c0c051ec10322f580dd61435bf9ce8dbafd)てください。

### まとめ

組み込みLinuxに対してのChainerを載せた取り組みを紹介しました。
ChainerはPythonで書かれており、深層学習のモデルを使った予測であれば組み込み機器でも乗っているメモリの範囲内で動かせる可能性があります。
興味がある方は協力して頂けるとありがたいです。問題を発見した場合は[meta-chainerプロジェクトのIssueページ](https://github.com/clear-code/meta-chainer/issues)に報告して下さい。
