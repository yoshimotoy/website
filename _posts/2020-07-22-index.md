---
title: gbpを使ったDebianパッケージでパッチのファイル名を明示的に指定するには
tags:
  - debian
---
### はじめに

Debianパッケージのメンテナンスでは`gbp`コマンドがしばしば使われます。
`gbp`を使ったワークフローでは、従来やや面倒だった`quilt`を意識せずにパッチを管理できます。
<!--more-->


ただし、特に指定をしなければコミットメッセージからファイル名が自動的に設定されてしまいます。[^0]

`debian/patches`以下にあるパッチのファイル名を明示的に指定するにはどうすればよいでしょうか。

### gbp pqコマンドを利用したパッチ管理をする

`gbp`には`gbp pq`というパッチ管理のためのコマンドがあります。
`gbp pq`を使うと、冒頭で述べたように`quilt`を意識せずにパッチを管理できます。[^1]

仮に`debian/unstable`ブランチでunstable向けのパッケージをメンテンナンスをしているときに次のコマンドを実行すると、
パッチを管理するためのブランチをrebaseできます。

```
gbp pq rebase
```


rebase後のブランチ名は`patch-queue/debian/unstable`です。
このブランチでは`debian/unstable`ブランチに`debian/patches/`以下のパッチがコミットされている状態となります。

### Gbp-Pq:タグを使う

`gbp-pq(1)`を読むと書いてありますが、コミットメッセージに`Gbp-Pq:`タグを含めます。

`Gbp-Pq:`タグを含んだコミットとは例えば次のようなコミットです。

```
commit 9f956f8232cce926eed52ab3fb2ed3f951d40652
Author:     Steven Chamberlain <stevenc@debian.org>
AuthorDate: Sat Jul 16 23:52:50 2016 +0100
Commit:     Kentaro Hayashi <kenhys@gmail.com>
CommitDate: Mon Jul 20 21:37:15 2020 +0900

    Use _GNU_SOURCE on GNU/kFreeBSD

    Define _GNU_SOURCE not only on GNU/Hurd, but also other glibc-based
    platforms including GNU/kFreeBSD.

    See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=826061#10

    Gbp-Pq: Name fix-nginx-FTBFS-on-kfreebsd.patch
---
 vendor/nginx-1.17.9/src/os/unix/ngx_posix_config.h | 5 ++++-
 1 file changed, 4 insertions(+), 1 deletion(-)
diff --git a/vendor/nginx-1.17.9/src/os/unix/ngx_posix_config.h b/vendor/nginx-1.17.9/src/os/unix/ngx_posix_config.h
index 2a8c413e..03f7e0a5 100644
--- a/vendor/nginx-1.17.9/src/os/unix/ngx_posix_config.h
+++ b/vendor/nginx-1.17.9/src/os/unix/ngx_posix_config.h
@@ -21,10 +21,13 @@
 #endif

-#if (NGX_GNU_HURD)
+#if defined(__GLIBC__)
 #ifndef _GNU_SOURCE
 #define _GNU_SOURCE             /* accept4() */
 #endif
+#endif
+
+#if (NGX_GNU_HURD)
 #define _FILE_OFFSET_BITS       64
 #endif
```


この状態で`gbp pq export`を実行すると、`debian/patches/fix-nginx-FTBFS-on-kfreebsd.patch`が生成されます。簡単ですね。

### まとめ

今回は、`gbp`を使ったパッチ管理においてファイル名を明示的に指定する方法（`Gbp-Pq: Name`）を紹介しました。
patches以下にパッチを貯めるのはあまりおすすめできませんが、Debianパッケージを`gbp`でメンテナンスするときに必要になったら参考にしてみてください。

[^0]: git format-patchみたいな挙動といえばわかりやすいかもしれません。

[^1]: 普通にgitのブランチを扱う感覚でパッチも管理できる
