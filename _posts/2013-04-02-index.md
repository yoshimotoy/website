---
tags:
- ruby
title: RubyKaigi 2013にSilverスポンサーとして参加
---
[おととし]({% post_url 2011-05-19-index %})に引き続き、クリアコードは今年も[RubyKaigi 2013のスポンサー](http://rubykaigi.org/2013/sponsors#silverSponsor)になりました。
<!--more-->


また、[須藤と沖元は発表者として参加](http://rubykaigi.org/2013/speakers#committers)する予定なので、会場で見かけたら声でもかけてください。
