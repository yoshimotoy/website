---
tags: []
title: Debian GNU/LinuxでWindows用バイナリをビルドする方法
---
2011/10/13時点でのDebian GNU/Linux sidでの話です。
<!--more-->


Debian GNU/Linux上では、KVMやXenなどの仮想マシンを使わなくても[Deiban GNU/LinuxやUbuntu用のパッケージのビルド]({% post_url 2010-01-18-index %})をできますし、[CentOSやFedora用のパッケージのビルド]({% post_url 2010-03-03-index %})もできます。Windows用のバイナリもDebian GNU/Linux上でビルドできると1台のマシンですべてのパッケージをビルドできるので[groonga](http://groonga.org/ja/)のようにたくさんの環境向けのパッケージを用意する場合に便利です。

以前、Rubyでは[rake-compilerを使ってWindows用のバイナリ入りgemをビルド]({% post_url 2010-04-21-index %})できることを紹介しましたが、ここで紹介するのはrake-compilerが中で何をしているかについてです。rake-compilerがやっていることがわかると、rake-compilerなしでも同じことができるため、Rubyの拡張ライブラリ以外でもDebian GNU/Linux上でWindows用バイナリをビルドできるようになります。

### 用意するもの

Debian GNU/Linux上でWindows用バイナリをビルドするためには[クロスコンパイラ](https://ja.wikipedia.org/wiki/%E3%82%AF%E3%83%AD%E3%82%B9%E3%82%B3%E3%83%B3%E3%83%91%E3%82%A4%E3%83%A9)する必要があります。そのために、x86（32bit版Windows）にもx64（64bit版Windows）にも対応した[MinGW-w64](http://mingw-w64.sf.net/)を使います。MinGW-w64はGCCでWindows用バイナリをビルドするために必要なヘッダーファイルやライブラリなど一式を提供します。

mingw-w64パッケージをインストールしてMinGW-w64環境を作ります。

{% raw %}
```
% sudo aptitude -V -D -y install mingw-w64
```
{% endraw %}

準備はこれで完了です。

### クロスコンパイルしやすいソフトウェア

`./configure; make; make install`でビルドするGNUビルドシステムを使っているソフトウェアはクロスコンパイルしやすいです。これは、`configure`内にクロスコンパイル用の処理が含まれているからです。

ここでは、GNUビルドシステムを使っているgroongaを使って、どのようにビルドするかを紹介します。ソースコードをダウンロードして展開しておきましょう。

{% raw %}
```
% wget http://packages.groonga.org/source/groonga/groonga-1.2.6.tar.gz
% tar xvzf groonga-1.2.6.tar.gz
% cd groonga-1.2.6
```
{% endraw %}

### ビルド

クロスコンパイルするときに一番大事なのが`configure`のところです。むしろ、`configure`をうまくやれたら後は`make; make install`するだけなので違いは`configure`だけになるべきです。

`configure`を実行するときのポイントは`--host`オプションです。ここにビルド対象のプラットフォームを指定します。x64用にビルドするときは以下のように`--host=x86_64-w64-mingw32`を指定します。

{% raw %}
```
% ./configure --host=x86_64-w64-mingw32
```
{% endraw %}

もし、システムにlibmecab-devパッケージがあるとLinux用のMeCabが使われてしまいます。libglib2.0-devパッケージも同様です。そのような場合は、以下のように`configure`で自動検出している部分を無効にしてください。

{% raw %}
```
% ./configure --host=x86_64-w64-mingw32 --without-mecab --disable-benchmark
```
{% endraw %}

`configure`が成功したらいつも通り`make`します。

{% raw %}
```
% make
```
{% endraw %}

パッケージを作るときは`make install`に`DESTDIR`変数を指定して一時的な場所にインストールしてからアーカイブするとよいでしょう。

{% raw %}
```
% make install DESTDIR=/tmp/groonga
% cd /tmp/groonga/
% mv usr/local groonga-1.2.6
% zip -r groonga-1.2.6.zip groonga-1.2.6
```
{% endraw %}

簡単ですね。

なお、x86用にビルドする場合は`--host=i686-w64-mingw32`を指定してください。

システムにインストールされているMinGWを確認する（つまり、`--host`に指定できる値を調べる）には以下のようにします。

{% raw %}
```
% (cd /usr; echo *mingw*)
i686-w64-mingw32/ x86_64-w64-mingw32/
```
{% endraw %}

この場合は`--host=i686-w64-mingw32`または`--host=x86_64-w64-mingw32`を指定できます。

### まとめ

groongaを例にしてDebian GNU/Linux上でWindows用のバイナリをビルドする方法を紹介しました。これで、Debian GNU/Linux上でDebianパッケージもRPMもWindows用バイナリもビルドできてリリース作業が楽になりますね。
