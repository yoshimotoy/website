---
tags:
- mozilla
title: Windows XPでThunderbirdのLDAP関連機能をテストする
---
多数のユーザを抱える企業や大学などでは、ユーザの情報をディレクトリサーバに保存していることが多いと思われますが、Thunderbirdには、そのようなディレクトリサーバにLDAP（Lightweight Directory Access Protocol）で接続して必要な情報を取得する機能があります。具体的には、Thunderbirdでは以下の場面でLDAPが利用できます。
<!--more-->


  * アドレス帳の共有。
  * 宛先などのメールアドレス入力時の補完候補。
  * AutoConfigを利用した設定の自動適用。

これらの機能をテストするためには、当然ながらディレクトリサーバを用意する必要があります。ここでは、インストールが簡単な「ApacheDS」を利用して、Windows XP環境下でThunderbirdでのLDAP関連機能をテストする手順を解説します。

### ApacheDSのセットアップ

[ApacheDS（Apache Directory Server）](http://directory.apache.org/apacheds/1.5/)は、Webサーバ「Apache」で有名なApache Foundationで開発されているオープンソースのディレクトリサーバです。Windows用にはインストーラが用意されており、特別な知識が無くてもインストールしてすぐに利用を始められます。

  1. ダウンロードしたインストーラからApacheDSをインストールする。

  1. 「コントロールパネル」→「管理ツール」→「サービス」で、サービス管理ツールを起動する。

  1. サービス一覧の中で「Apache Directory Service - default」を右クリックして、「開始」を選択する。


### Apache Directory Studioのセットアップとダミーのユーザ情報の登録

Apache Directoryプロジェクトではディレクトリサーバの内容をGUIで管理する「[Apache Directory Studio](http://directory.apache.org/studio/)」というEclipseベースのツールも開発されています。これを使って、テストのためのダミーのユーザ情報をディレクトリサーバに登録します。

  1. ダウンロードしたインストーラからApache Directory Studioをインストールする。

  1. 「すべてのプログラム」→「Apache Directory Suite」→「Studio」→「Studio」でApache Directory Studioを起動する。

  1. 「Window」→「Open Perspective」→「LDAP」を選択する。

  1. 左下の「Connections」パネルにある「Example」をダブルクリックする。これで、ApacheDSに最初から用意されているサンプル設定のディレクトリサーバに接続される。

  1. 左上に表示されている「LDAP Browser」のパネル内にツリーが表示されるので、「Root DSE」→「ou=system」→「ou=users」と辿る。

  1. 「ou=users」を右クリックして「New Entry」を選択。

  1. 「Create entry from scratch」を選択。

  1. 「Object Classes」で左のリストの「inetOrgPerson」と「uidObject」をダブルクリックする。これで右側のリストに「inetOrgPerson」「organizationalPerson」「person」「top」「uid」が加わるので、そのまま「Next」をクリック。

  1. 「RDN:」欄で左のボックスに「uid」、右のボックスにそのユーザを識別するための一意な文字列（例えば「user001」など）と入力して「Next」をクリックする。

  1. 「cn」欄にユーザの表示名（例：Yamada Taro）、「sn」欄にユーザの名字（例：Yamada）を入力する。また、表の上で右クリックして「New Attribute」を選択し、「Attribute type:」欄に「mail」と入力して「Finish」をクリックすると表に新しく「mail」欄の行が増えるので、「mail」欄にE-mailアドレス（例：yamada@example.com）を入力する。すべて入力し終えたら「Finish」をクリックする。


6から10の操作を繰り返して、さらに何人分かユーザ情報を登録します。「uid」はユーザの識別子となりますので、他と重複しない値を入力してください。

なお、9のステップで「RDN:」欄の右にある「+」ボタンをクリックして入力欄を増やすと以下の操作が期待通りには動かなくなる場合がありますので、ここでは必ず「uid」のみを設定して下さい。

### Thunderbirdにディレクトリサーバの設定を追加する

ここまでで入力した情報を、Thunderbirdで実際に利用してみましょう。

  1. Thunderbirdを起動し、「ツール」→「アドレス帳」でアドレス帳を起動する。

  1. 「ファイル」→「新規作成」→「LDAPディレクトリ」を選択する。

  1. LDAPサーバの設定を入力するダイアログが開かれるので、「名前」に「ApacheDS」、「ホスト名」に「localhost」、「ベース識別名」に「ou=users,ou=system」、「ポート番号」に「10389」、「バインド識別名」に「uid=admin,ou=system」と入力して「OK」をクリックする。


これで、Thunderbirdでディレクトリサーバに接続する準備が整いました。

試しに、ディレクトリサーバの内容をアドレス帳として表示してみましょう。アドレス帳ウィンドウの左のアドレス帳一覧から「ApacheDS」を選んで、ウィンドウ右上の検索欄に先ほど登録したダミーのユーザの名前やメールアドレスの一部を入力します。ディレクトリサーバに接続するためのパスワードの入力を求められますので、「secret」（ApacheDSに最初から用意されているサンプル設定のディレクトリサーバのパスワードです）と入力すると、ダミーのユーザ情報のうち今入力した文字列にヒットした物がリストアップされます。また、新規にメール作成ウィンドウを開き、宛先の入力欄に先ほど登録したユーザの名前の先頭数文字を入力すると、該当するユーザのメールアドレスが補完候補として表示されます。

以上で、基本的な手順の解説は終わりです。LDAP関連の各種の隠し設定をテストする場合などに利用してみると良いでしょう。
