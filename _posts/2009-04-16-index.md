---
tags:
- milter-manager
title: milter manager 1.0.0リリース
---
milter manager初の安定版1.0.0をリリースしました。→[milter managerの紹介](/software/milter-manager.html)
<!--more-->


また、クリアコード初のプレスリリースもしました。→[迷惑メール対策システムの構築を簡単・低コストにする『milter manager』をリリース](/press-releases/20090416-milter-manager-1-0-0.html)

今日をリリース日にしたのは、今日が大安だったからです。リリース作業はそれほど大きなトラブルもなく行えたので、よい日だったと思います。みなさんも、大安にリリースしてみてはいかがでしょうか。
