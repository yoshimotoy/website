---
tags:
- mozilla
title: CPUの使用率とメモリの使用量を表示するFirefoxアドオン「システムモニター」を更新しました
---
システムの情報をFirefoxのツールバー上に表示するアドオン「システムモニター」のバージョン0.4.1をリリースしました。以下のリンク先からダウンロードできます。
<!--more-->


  * [packages.clear-code.comからダウンロード](http://packages.clear-code.com/xul/extensions/system-monitor/system-monitor-0.4.1.xpi)
  * [addons.mozilla.orgからダウンロード](https://addons.mozilla.org/firefox/addon/15093)
  * [Gitリポジトリ](http://git.clear-code.com/xul/extensions/system-monitor/)

前のバージョンからの主な変更点は以下の通りです。

  * CPUの使用率に加えて、メモリの使用量を表示できるようになりました。
  * 64bit版Linux向けのバイナリを同梱するようにしました。
  * Firefox 3.6およびThunderbird 3.1で動作します。Firefox 3.5およびThunderbird 3.0では動作しません。

### Web APIの利用

Web APIの利用形式は[前のバージョン]({% post_url 2009-10-20-index %})と同様ですが、このバージョンでは以下の点が改良されました。

  * メモリの使用量を表示および監視できるようになりました。
  * リスナを登録したページがアンロードされた際に、リスナの登録を自動的に解除するようになりました。

メモリの使用量を監視する場合は、system.addMonitorの第1引数に「memory-usage」を指定します。この時、リスナには以下のプロパティを持つオブジェクトが渡されます。

  * used（使用済みのメモリ領域のバイト数：64bit整数）
  * free（未使用のメモリ領域のバイト数：64bit整数）
  * total（全体のメモリ領域のバイト数：64bit整数）

また、以前のバージョンでは登録したリスナは明示的にsystem.removeMonitorで登録を解除するまで情報を受け取り続ける使用でしたが、バージョン0.4.1からは、ページがアンロードされると自動的にリスナの登録も解除されるようになりました。

以下は、メモリの使用量を監視する例です。

{% raw %}
```html
<div id="system-monitor-demo"></div>
<script type="text/javascript"><!--
var container = document.getElementById("system-monitor-demo");
if (!window.system || !window.system.addMonitor) {
  container.innerHTML = "システムモニターがインストールされていません";
  container.style.color = "red";
} else {
  container.innerHTML = "<div><canvas id='system-monitor' width='300' height='60'></canvas>"+
                        "<br /><span id='system-monitor-console'></span></div>";

  var width = 300;
  var interval = 1000;

  var MemoryArray = [];
  var arrayLength = width / 2;
  while (MemoryArray.length < arrayLength) {
    MemoryArray.push(undefined);
  }

  // リスナとして登録する関数には、メモリの情報を保持したオブジェクトが渡される。
  function onMonitor(aUsage) {
    var console = document.getElementById("system-monitor-console");
    console.textContent = parseInt(aUsage.used/1024/1024)+'MiB / '+parseInt(aUsage.total/1024/1024)+'MiB';

    MemoryArray.shift();
    MemoryArray.push(aUsage.used / aUsage.total);

    var canvasElement = document.getElementById("system-monitor");
    var context = canvasElement.getContext("2d")
    var y = canvasElement.height;
    var x = 0;

    context.fillStyle = "black";
    context.fillRect(0, 0, canvasElement.width, canvasElement.height);

    context.save();
    MemoryArray.forEach(function(aUsage) {
      var y_from = canvasElement.height;
      var y_to = y_from;
      if (aUsage == undefined) {
        drawLine(context, "black", x, y_from, 0);
      } else {
        y_to = y_to - (y * aUsage);
        y_from = drawLine(context, "aqua", x, y_from, y_to);
        drawLine(context, "black", x, y_from, y_to);
      }
      x = x + 2;
    }, this);
    context.restore();
  }

  function drawLine(aContext, aColor, aX, aBeginY, aEndY) {
    aContext.beginPath();
    aContext.strokeStyle = aColor;
    aContext.lineWidth = 1.0;
    aContext.lineCap = "square";
    aContext.moveTo(aX, aBeginY);
    aContext.lineTo(aX, aEndY);
    aContext.closePath();
    aContext.stroke();
    return aEndY - 1;
  }

  // リスナを登録する。
  window.system.addMonitor("memory-usage", onMonitor, interval);
}
// --></script>
```
{% endraw %}

### Firefox 3.5およびThunderbird 3.0での動作について

システムモニター0.4.1では、前述したリスナ登録の自動的な解除を実現するためにいくつかの実装が加わりました。しかしながら、この機能が依存しているGecko側の機能についてGecko 1.9.1とGecko 1.9.2の間で互換性が無いため、Gecko 1.9.2のSDKを用いてビルドされたシステムモニターのバイナリはGecko 1.9.1では動作しなくなりました。よって、システムモニター0.4.1からはGecko 1.9.1ベースのFirefox 3.5（Thunderbird 3.0）はサポート対象外となっています。

どうしてもFirefox 3.5（Thunderbird 3.0）でシステムモニター0.4.1を利用したい場合は、Gecko 1.9.1のSDKを使ってソースからバイナリをビルドし直す必要があります。また、独自に変更を加える場合もバイナリをビルドし直さなくてはなりません。以下に、Windows、LinuxおよびMac OS Xの各環境でのビルド手順を解説します。

#### Ubuntu 10.04LTS Lucid Lynxの場合（Firefox 3.6）

リリース版のシステムモニター0.4.1のLinux用バイナリは、32bit版、64bit版のいずれもUbuntu 10.04LTS上でビルドしています。Ubuntu 10.04LTSでのビルド手順は以下の通りです。

  1. 必要なパッケージをインストールします。

     {% raw %}
```
$ sudo aptitude install automake libtool xulrunner-1.9.2-dev libgtop2-dev zip
```
{% endraw %}
  1. システムモニターのソースをダウンロードして展開します。

     {% raw %}
```
$ wget http://git.clear-code.com/xul/extensions/system-monitor/snapshot/system-monitor-0.4.1.tar.bz2
$ tar xvf system-monitor-0.4.1.tar.bz2
```
{% endraw %}
  1. 作業ディレクトリに入ってビルドします。

     {% raw %}
```
$ cd system-monitor-0.4.1
$ ./automake.sh
$ ./configure
$ make
```
{% endraw %}

これで、作業ディレクトリ直下にインストール用のパッケージ「system-monitor-0.4.1.xpi」が生成されます。

#### Ubuntu 9.04 Jaunty JackalopeおよびUbuntu 9.10 Karmic Koalaの場合（Firefox 3.5）

基本的にはUbuntu 10.04LTSの場合と同じですが、Gecko SDK（XULRunner SDK）のバージョンを1.9.1にする点が異なります。必要なパッケージの準備の際に、「xulrunner-1.9.2-dev」ではなく「xulrunner-1.9.1-dev」をインストールして下さい。

{% raw %}
```
$ sudo aptitude install automake libtool xulrunner-1.9.1-dev libgtop2-dev zip
```
{% endraw %}

それ以外の手順はUbuntu 10.04LTSの場合と同一です。

#### Windowsの場合

リリース版のシステムモニター0.4.1のWindows（Win32）用バイナリは、64bit版Windows 7でビルドしています。Windowsでのビルド手順は以下の通りです。

  1. 必要なソフトウェアをインストールします。

       * [Cygwin](http://www.cygwin.com/)
       ：以下のパッケージをインストールしておいて下さい。
         * automake
         * gcc
         * gcc4
         * libtool
         * make
         * pkg-config
         * zip
       * [Microsoft Visual C++ 2008 Express Edition](http://www.microsoft.com/express/Downloads/#Visual_Studio_2008_Express_Downloads)
  1. [ソースコードのZIPアーカイブ](http://git.clear-code.com/xul/extensions/system-monitor/snapshot/system-monitor-0.4.1.tar.bz2)をダウンロードし、展開します。展開すると「system-monitor-0.4.1」というフォルダが展開されますので、Cygwinのホーム直下などの位置に移動しておきます。

  1. Firefox 3.5/Thunderbird 3.0用にビルドする場合は[Gecko 1.9.1のSDK](http://releases.mozilla.org/pub/mozilla.org/xulrunner/releases/1.9.1.13/sdk/xulrunner-1.9.1.13.en-US.win32.sdk.zip)を、Firefox 3.6/Thunderbird 3.1用にビルドする場合は[Gecko 1.9.2のSDK](http://releases.mozilla.org/pub/mozilla.org/xulrunner/releases/1.9.2.10/sdk/xulrunner-1.9.2.10.en-US.win32.sdk.zip)をダウンロードし、展開します。展開するとどちらも「xulrunner-sdk」というフォルダが展開されますので、c:\xulrunner に移動しておきます。（c:\xulrunner\xulrunner-sdk に置くのではなく、c:\xulrunner-sdk の位置に移動した上でフォルダ名をxulrunnerに変更します。）

  1. Cygwin Bash Shellを起動し、作業ディレクトリに入ってビルドします。例えば作業ディレクトリが~/system-monitor-0.4.1であれば、以下の通りです。

     {% raw %}
```
$ cd ~/system-monitor-0.4.1
$ ./autogen.sh
$ ./configure --with-libxul-sdk=/cygdrive/c/xulrunner
$ make
```
{% endraw %}
     このビルド操作はエラーで停止しますので、続けて、VC++でバイナリをビルドします。（Cygwin Bash Shellはそのまま置いておいてください。）
  1. system-monitor-0.4.1\components\SystemMonitor\SystemMonitor.vcproj をダブルクリックします。すると、VC++が起動してプロジェクトファイルが読み込まれますので、「ビルド」メニューから「SystemMonitorのビルド」を選択してください。出力の最後の行が <samp>ビルド: 1 正常終了、0 失敗、0 更新不要、0 スキップ</samp>となっていればビルド成功です。

  1. XPIファイルを作成します。Cygwin Bash Shellに戻り、以下のコマンドを実行します。

     {% raw %}
```
$ make xpi
```
{% endraw %}

これで、作業ディレクトリ直下にインストール用のパッケージ「system-monitor-0.4.1.xpi」が生成されます。

c:\xulrunner 以外の位置にGeckoのSDKを置く場合は、SystemMonitor.vcproj の内容を修正する必要があります。VC++で SystemMonitor.vcproj を開いてプロジェクトのプロパティを編集するか、SystemMonitor.vcproj をテキストエディタで開いて c:\xulrunner を指している箇所を置換して下さい。

#### Mac OS Xの場合

リリース版のシステムモニター0.4.1のMac OS X（Intelプロセッサ・32bit）用バイナリは、Mac OS X 10.6.4 Snow Leopardでビルドしています。

  1. 必要なソフトウェアをインストールします。

       * Xcode：[Webサイト](http://developer.apple.com/technologies/tools/xcode.html)またはOSのインストールディスクからインストールしておいて下さい。
       * [MacPorts](http://www.macports.org/)
       ：以下のパッケージをインストールしておいて下さい。
         * pkg-config
  1. [ソースコードのアーカイブ](http://git.clear-code.com/xul/extensions/system-monitor/snapshot/system-monitor-0.4.1.tar.bz2)をダウンロードし、展開します。展開すると「system-monitor-0.4.1」というフォルダが展開されますので、ホーム直下などの位置に移動しておきます。

  1. Firefox 3.5/Thunderbird 3.0用にビルドする場合は[Gecko 1.9.1のSDK](http://releases.mozilla.org/pub/mozilla.org/xulrunner/releases/1.9.1.13/sdk/xulrunner-1.9.1.13.en-US.mac-i386.sdk.tar.bz2)を、Firefox 3.6/Thunderbird 3.1用にビルドする場合は[Gecko 1.9.2のSDK](http://releases.mozilla.org/pub/mozilla.org/xulrunner/releases/1.9.2.10/sdk/xulrunner-1.9.2.10.en-US.mac-i386.sdk.tar.bz2)をダウンロードし、展開します。展開するとどちらも「xulrunner-sdk」というフォルダが展開されますので、/opt/xulrunner-sdk に移動しておきます（別の場所に置いても構いません）。

  1. アプリケーション→ユーティリティ→ターミナルを起動し、作業ディレクトリに入ってビルドします。例えば作業ディレクトリが~/system-monitor-0.4.1であれば、以下の通りです。

     {% raw %}
```
$ cd ~/system-monitor-0.4.1
$ ./autogen.sh
$ ./configure --with-libxul-sdk=/opt/xulrunner-sdk CXXFLAGS="-arch i386"
$ make
```
{% endraw %}

これで、作業ディレクトリ直下にインストール用のパッケージ「system-monitor-0.4.1.xpi」が生成されます。

Snow Leopardでは初期状態では64bit用のバイナリが作成されてしまいます。./configureの際に明示的に CXXFLAGS="-arch i386" と指定することで、32bit用のバイナリを作成できます。

ビルドに使用するGecko SDKは ./configure の --with-libxul-sdk オプションで指定したパスにある物が使われます。パスをその都度明示的に指定すれば、Gecko 1.9.1用とGecko 1.9.2用のそれぞれのバイナリを同一環境上でビルドすることもできます。

### まとめ

今回、システムの情報をFirefoxのツールバー上に表示するアドオン「システムモニター」のバージョン0.4.1をリリースしました。また、Firefox 3.5向けにソースコードからバイナリをビルドする手順を紹介しました。
