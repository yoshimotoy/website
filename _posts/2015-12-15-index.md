---
tags:
- mozilla
title: Thunderbirdのエラーコンソールの出力結果を効率よく収集する方法
---
FirefoxやThunderbirdには、内部で発生したエラーの情報やデバッグ用の情報を表示するためのコンソールが内蔵されています。
FirefoxでもThunderbirdでも、「Ctrl-Shift-J」というキーボードショートカットでこのコンソールを開く事ができます。
<!--more-->


FirefoxのエラーコンソールはWeb開発ツール由来の物となっており、表示された内容を簡単に選択してクリップボードにコピーできます。
しかしながら、Thunderbirdのエラーコンソールは設計上の都合から、1つ1つのログをコピーする事しかできず、また表示可能なログの最大件数も250件に制限されています。
大量のメッセージが発生する状況ではすぐにログが流れてしまって、有効な情報を収集することができません。

以下は、この制限を実証するための「300件のエラーを報告する」というサンプルコードです。

```js
for (var i = 1; i <= 300; i++) { Components.utils.reportError('Error '+i); }; 
```


これをエラーコンソール内の「コード」欄にコピー＆ペーストして「コードを評価」ボタンをクリックして実行すると、それまで表示されていたログや新規のログの300件のうち最初の50件までは消えてしまい、「Error 51」から「Error 300」までの250件だけが見えるという結果になることが分かります。
実際の運用上で問題が発生している時に、エラーコンソール内をすさまじい速度でログが流れていってしまうと、原因の切り分けのための調査自体もはかどりません。

### エラーコンソールの最大表示ログ件数を増やす

幸い、Thunderbirdのエラーコンソールでは任意のコードを実行できます。
この機能を使えば、エラーコンソール自身の最大表示件数を変える事ができます。

以下のコードをエラーコンソール内の「コード」欄にコピー＆ペーストし、「コードを評価」ボタンをクリックして実行すると、ログの最大件数が99999件に拡大されます。

```js
Components.utils.import('resource://gre/modules/Services.jsm');var box = Services.wm.getMostRecentWindow('global:console').document.getElementById('ConsoleBox'); var descriptor = { value: 99999 }; Object.defineProperty(box, 'limit', descriptor); Object.defineProperty(box, 'fieldMaxLength', descriptor);
```


実際に、続けて以下のようなコードを実行してみると、それまでのログと併せてすべてのログが表示されることを確認できます。

```js
for (var i = 1; i <= 300; i++) { Components.utils.reportError('New Error '+i); }; 
```


この方法は、エラーコンソールを開いた時点より後に出力されるログに対してしか利用できません。
（エラーコンソールを開く前に大量にログが出ている場合、それらは既に制限を超えて流れてしまっているために、この方法では見る事ができません。）
とはいえ、エラーコンソールを開いた後の任意のタイミングで再現試験を行える状況であれば十分に有用でしょう。

なお、この変更は「エラーコンソール」のウィンドウを閉じるまでの間のみ有効で、エラーコンソールを開き直したりすると最初の状態（最大250件のみ表示）に戻ります。

### エラーコンソールの内容をクリップボードにコピーする

また、以下のコードを実行すれば、その時点でコンソールに表示されているすべてのログをまとめてクリップボードにコピーできます。

```js
Components.utils.import('resource://gre/modules/Services.jsm');var text=[];for (var row of Services.wm.getMostRecentWindow('global:console').document.getElementById('ConsoleBox').mConsoleRowBox.children) { if (row.boxObject.height > 0) { text.push(row.toString()); } }; Components.classes['@mozilla.org/widget/clipboardhelper;1'].getService(Components.interfaces.nsIClipboardHelper).copyString(text.join('\n---\n'));
```


これなら、ログの件数が多い場合でも容易にログを保存することができます。
アドオンのエラー情報を作者に伝える場合や、バグトラッキングシステムに問題を報告する時などにご活用下さい。
