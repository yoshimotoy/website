---
tags:
- ruby
title: 日本Ruby会議2009の発表セッション
---
[日本Ruby会議2009のセッション詳細が公開](http://rubykaigi.tdiary.net/20090706.html#p01)されました。
<!--more-->


日本Ruby会議2009は3トラックで3日間の開催のため、たくさんのセッションがありますが、このうち、2セッションで発表します。

### ActiveLdap - 2009年07月18日土曜日 14:30-15:30 Lightning Talks

1つ目は[2回目のライトニングトーク](http://rubykaigi.org/2009/ja/talks/18H06)の一番最後です。

ライトニングトークでは[ActiveLdap](http://ruby-activeldap.rubyforge.org/)（[チュートリアル](http://code.google.com/p/ruby-activeldap/wiki/TutorialJa)）について話します。ActiveLdapの開発に関わるようになってから約3年経ちますが、ついに発表する機会に恵まれました。

クリアコードは今月から第四期に入っていますが、ActiveLdapはクリアコード設立初期にLDAPを利用する機会があったことがきっかけで開発に関わるようになったフリーソフトウェアです。当時は多くの問題を抱えていたActiveLdapですが、そのときからコツコツ開発を続けていたため現在では当時よりもかなり便利なライブラリとなりました。その成果を日本Ruby会議で発表できることはとても感慨深いものです。

### CとRubyとその間 - 2009年07月18日土曜日 16:00 - 18:30

2つ目は同じ日の同じ会場の次の枠で、[CとRubyとその間](http://rubykaigi.org/2009/ja/talks/18H11)です。

クリアコードではActiveLdap以外にもたくさんのフリーソフトウェアの開発に関わっていますが、その中でもCとRubyそれぞれのよいところを活かしたフリーソフトウェアについて話します。この発表ではそのようなフリーソフトウェアの例として[milter manager](/software/milter-manager.html)と[ActiveGroonga](http://groonga.rubyforge.org/)を紹介しながら、CとRubyを活かすことのメリットについて話します。

Rubyから使える高速なkey-valueストアとしては[Tokyo Cabinet](http://tokyocabinet.sourceforge.net/)や
[Localmemcache](http://localmemcache.rubyforge.org/)が有名です。

ActiveGroonga（とその下の層のRuby/groongaは）これらと同様に高速なkey-valueストア機能も備える[groonga](http://groonga.org/)をよりRubyらしく使いやすいAPIで提供します。

milter managerについては最近いろいろなところで話しましたが、ActiveGroongaについて話すことは今回が初めてです。

### まとめ

日本Ruby会議2009で発表するセッションを紹介しました。他にも興味深いセッションがたくさんあるので迷うと思いますが、興味があれば上記のセッションにも参加してみてください。

クリアコードは[スポンサー](http://rubykaigi.org/2009/ja/Sponsors)となっているため、スポンサーブースを出します。セッションには参加できなかった方も、ぜひ、足を運んでください。
