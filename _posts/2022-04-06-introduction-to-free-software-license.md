---
title: フリーソフトウェアライセンス（オープンソースソフトウェアライセンス）入門
author: daipom
tags:
  - free-software
---

最近、フリーソフトウェア（オープンソースソフトウェア）をいざ公開しようと思った時に、ライセンスの扱いに困った福田です。

ライセンスには様々な種類があり、シンプルなライセンスもあれば制限の強そうなライセンスもありますよね。私は最初、自分で選ぶならシンプルで制限が少ないライセンスの方が良いかな、と深く考えずに思ってしまいました。しかし、制限が強いということは、それだけ強い保護が受けられる、ということでもあると知り、世界がひっくり返ったような衝撃を受けました。

ライセンスには様々な種類があり、そこには様々な考え方がありますが、今回はフリーソフトウェアを大事にしたい場合のお勧めのライセンスの選び方や、使い方を紹介します。

<!--more-->

### フリーソフトウェアとは

フリーソフトウェア[^free]とは、ユーザーが自由に使えるソフトウェアのことで、自由（な）ソフトウェア、などとも呼ばれます[^freesoft]。オープンソースソフトウェア（OSS）という言葉の方が聞き慣れている方が多いかもしれません。フリーソフトウェアとOSSはほぼ同じソフトウェアを指しますが、考え方が違います[^diff-to-oss]。

詳しく知りたい方は脚注のサイトをご覧頂きたいのですが、「OSSは分かるけどフリーソフトウェアという言葉がピンと来ない」、という方のためにここで簡単に概念を紹介します。フリーソフトウェアは、ユーザーがソフトウェアをコントロールできるべきだ、という考え方に基づいています。一方、OSSはユーザーがソフトウェアをコントロールできることによるメリットを重視しています。結局ほぼ同じソフトウェアを指すのですが、考え方の違いで言葉が使い分けられているのです。

クリアコードは、フリーソフトウェアとビジネスの両立を理念としており、フリーソフトウェアの考え方を大事にし、推進している会社です[^clearcode-philosophy]。そのため、本記事ではOSSではなく、フリーソフトウェアという言葉を使います。

[^freesoft]: 自由ソフトウェアとは?: https://www.gnu.org/philosophy/free-sw.ja.html
[^free]: 「フリー」（「自由」、「free」）という言葉は「無料」を意味しているわけではありません。
[^diff-to-oss]: なぜ、オープンソースは自由ソフトウェアの的を外すのか: https://www.gnu.org/philosophy/open-source-misses-the-point.html
[^clearcode-philosophy]: [クリアコードとフリーソフトウェアとビジネス]({% post_url 2015-09-17-index %})

### フリーソフトウェアとソフトウェアライセンス

自分の作ったソフトウェアをフリーソフトウェアとして公開するためには、ソフトウェアライセンスについての理解が欠かせません。

[フリーソフトウェアの定義](https://www.gnu.org/philosophy/free-sw.ja.html#four-freedoms)を満たすだけであれば、実は著作権を放棄するだけでも構いません。しかしそれだけでは、後世そのソフトウェアの開発を引き継いだ人たちがそれをフリーソフトウェアではなくしてしまったり、その二次的著作物がフリーソフトウェアではなくなってしまったりする可能性があります。せっかくフリーソフトウェアとして世に出すのですから、そういった事態は防ぎたいですよね。これを実現する、つまりそのソフトウェアの自由を永続的に保護するために、コピーレフト[^copyleft]という概念に基づいたライセンスを用いることができます。

フリーソフトウェアを推進する我々がここで言っている「自由」とは、「ユーザーが自分の使うソフトウェアをコントロールできること」です。一方で、これを永続的に保護しようとするコピーレフトの考え方について、「自由に使えなくする」という選択肢を奪っている、という考え方もあります。こういった考え方における「自由」は、「オリジナルバージョンのユーザーが作った二次的著作物のユーザーにコントロール権を与えないこともできる」ということであり、我々の言う「自由」とは異なります。これが行使されてしまうと、「二次的著作物のユーザー」には我々の言う「自由」はありません。

[^copyleft]: コピーレフト: https://www.gnu.org/licenses/copyleft.html

#### 著作権の放棄

実は、何もライセンスを付けず、著作権を放棄して公開すれば、フリーソフトウェアになります。これはパブリックドメイン・ソフトウェアと呼ばれます[^public-domain]。

しかし、上述の通り、この方法はお勧めできません。

永続的な自由を保護するため、コピーレフトのあるライセンスを用いることを推奨します。

[^public-domain]: パブリックドメイン・ソフトウェア: https://www.gnu.org/philosophy/categories.html#PublicDomainSoftware

#### コピーレフトのあるライセンス

コピーレフト(Copyleft)とは、コピーライト(Copyright)、すなわち著作権をもじった概念です。著作権は、利用者の自由を制限するために使われることがあります。フリーソフトウェアにおいてはそういったケースとは逆に、著作権を利用者の自由を保護するために用います。例えば、二次的著作物のユーザーもそれをコントロールできるようにするならば二次的著作物を作れるようにして、さらにその二次的著作物の二次的著作物にも同様の約束を設定します。こうすることで、再帰的に全ての二次的著作物のユーザーがソフトウェアをコントロールできます。

コピーレフトのあるライセンスは、ソフトウェアをフリーソフトウェアであり続けさせるための保護の仕組みを提供してくれます。そのため、フリーソフトウェアを推進する我々としては、この種類のライセンスから選ぶことをお勧めします。

#### コピーレフトのないライセンス

ライセンスには、コピーレフトではないものも存在します。MITライセンスが有名でしょう。

こういったライセンスは、制限が緩く使いやすいと認識されることもありますが、「制限が緩い」ということは「保護が弱い」ということでもあり、立場や状況によってメリットにもなればデメリットにもなります。「制限が緩い」ということをメリットとだけ考えて採用することは、フリーソフトウェアを推進する我々としては推奨できません。今後非公開のソフトウェアにする可能性がある場合や、脅威となる競合があり、できるだけ広く利用してもらうことを優先したい場合などに、採用することを推奨します。

### 代表的なライセンス

フリーソフトウェアに使われるライセンス一覧は、 https://www.gnu.org/licenses/license-list.html で確認できます。

見ての通り多くの種類がありますが、まずはコピーレフトの強さという観点で代表的なライセンスを把握することをお勧めします。

#### GPLv3 (GNU GENERAL PUBLIC LICENSE Version 3)

しっかりしたコピーレフトのライセンスです。

ソフトウェアをフリーソフトウェアであり続けさせるための保護が受けられるので、最もお勧めするライセンスです。

ライセンス内容: https://www.gnu.org/licenses/gpl-3.0.html

#### LGPLv3 (GNU LESSER GENERAL PUBLIC LICENSE Version 3)

GPLv3に比べ、弱い(LESSERな)コピーレフトのライセンスです。GPLv3と同等にユーザーの自由を保護しつつ、GPLv3を採用したくない他のソフトウェアからの利用を許します。

ライブラリーを作成する時に、GPLv3の代わりに用いることがあります。GPLv3のソフトウェアであればGPLv3のライブラリーを利用できます。つまり、GPLv3を採用したくないソフトウェアからは利用してもらえません。もしくは他に競合となるライブラリーがある場合、GPLv3を用いることはソフトウェア側に他のライブラリーを選ばせ、そのライブラリーを使ってもらえない原因になり得ます。こういった場合にGPLv3の代わりに使用できるものがLGPLv3です。

ライセンス内容: https://www.gnu.org/licenses/lgpl-3.0.html

#### Apache-2.0

コピーレフトではないライセンスです。

コピーレフトではないライセンスの中では、「特許ライセンスの付与(Grant of Patent License)」条項があり、開発元から特許権を行使される心配なくユーザーに利用してもらえるため、他のコピーレフトではないライセンスよりお勧めです。

ライセンス内容: https://www.apache.org/licenses/LICENSE-2.0

#### MIT

コピーレフトではないライセンスです。

シンプルなライセンスです。一方で、Apache-2.0もそうですが、ソフトウェアをフリーソフトウェアであり続けさせるための保護が受けられない、という側面もあります。よって、他のライセンスは難しそう、という理由だけで選ぶのはお勧めできません。

ライセンス内容: https://opensource.org/licenses/MIT

### ライセンスの選び方

以下の手順でライセンスを選択することをお勧めします。

1. 関連するフリーソフトウェアがあるならそのソフトウェアと同じライセンスを選ぶ
1. 関連するフリーソフトウェアがなく、単独で動くプログラムならGPLv3を選ぶ
1. 関連するフリーソフトウェアがなく、ライブラリーならLGPLv3を選ぶ

また、もしコピーレフトを含められない事情がある場合は、Apache-2.0を選びます。ただし、Apache-2.0はGPLv2[^gplv2]とのライセンス互換性[^license-compat]がないため、それが問題となる場合のみMITを選びます。

まず、関連するフリーソフトウェアと同じライセンスを選ぶことをお勧めするのは、ライセンス互換性[^license-compat]の問題などを気にする必要がなくなり、そのプロジェクトへの貢献がしやすくなるからです。

より詳しく知りたい方は、 https://www.gnu.org/licenses/license-recommendations.html をご覧下さい。

[^gplv2]: GPLv2とは、GPLv3の前のバージョンです: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
[^license-compat]: ライセンス互換性: https://www.gnu.org/licenses/gpl-faq.html#WhatIsCompatible

### ライセンスの使い方

以上の通り、関連するフリーソフトウェアがある場合にはそれと同じライセンスを選ぶべきです。この場合は、その関連ソフトウェアがどうライセンスを適用しているのかを参考にしつつ、そのライセンスの正しい使い方を調べることになります。

そうでない場合は、GPLv3かLGPLv3を選ぶことをお勧めします。その使い方の1例を紹介します。厳密には、 https://www.gnu.org/licenses/gpl-3.0.html の末尾にある`How to Apply These Terms to Your New Programs`の内容や https://www.gnu.org/licenses/gpl-howto.html をご覧下さい。

以下の手順でプログラムにライセンスを適用することができます。

1. `COPYING`という名前のファイルをプログラムに追加し、[GPLv3の原文](https://www.gnu.org/licenses/gpl-3.0.txt)をコピーする(LGPLv3でもGPLv3の原文をコピーする)
1. LGPLv3の場合は、さらに`COPYING.LESSER`という名前のファイルを加えて、[LGPLv3の原文](https://www.gnu.org/licenses/lgpl-3.0.txt)をコピーする
1. 主要な各ファイルの冒頭にライセンス告知(後述)をつける

#### ライセンス告知

[GPLv3の原文](https://www.gnu.org/licenses/gpl-3.0.txt)の末尾の`How to Apply These Terms to Your New Programs`に、告知の原文があります。ここで記載されている通り、この告知文は必要に応じてアレンジ可能であり、著作権者と完全な原文の入手方法が明記されていれば良いです。LGPLv3の場合は、3箇所ある`GNU General Public License`の表記を`GNU Lesser General Public License`に変更します。また、ソースコードファイル上におけるコメントアウト方法はプログラム言語によって変わります。

原文では最後に「プログラムと一緒にライセンスのコピーが渡されているはずです。もしそうでなければ、 https://www.gnu.org/licenses/ を見てください。」という趣旨の文が書いてあり、このため`COPYING`というファイルを用意するわけです。

また、 https://www.gnu.org/licenses/gpl-howto.html に記載されている通り、複数ファイルからなるプログラムの場合は、`This program`をプログラム名に変更し、`This file is part of {プログラム名}.`という一行を追加することを推奨します。

GPLv3の告知文は例えば下記のようになります。

```
# Copyright (C) {著作権発生年} {ファイル作成者名} <{メールアドレス}>

# This file is part of {プログラム名}.

# {プログラム名} is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# {プログラム名} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

LGPLv3の告知文は例えば下記のようになります。

```
# Copyright (C) {著作権発生年} {ファイル作成者名} <{メールアドレス}>

# This file is part of {プログラム名}.

# {プログラム名} is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# {プログラム名} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

#### or later と only について

GPLv3やLGPLv3などのライセンスでは、今後ライセンスが新しいバージョンに更新された場合に、その新しい方を選べるようにするかどうかを決められます。

ライセンスの新しいバージョンを選べる場合は`GPLv3 or later`、現バージョンに限定する場合は`GPLv3 only`、のように`or later`か`only`を付けて区別します。

これはライセンス告知の内容によって決まり、デフォルトの内容では`or later`になっています。`only`にしたい場合は告知文を変更する必要があります。

例えば`GPLv3 or later`では以下のようになる部分を、

```
# {プログラム名} is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
```

`GPLv3 only`にする場合は以下のように変更します。

```
# {プログラム名} is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as 
# published by the Free Software Foundation.
```

今後作られる新しいバージョンのライセンスがどのような内容になるのか分からないため、それを気にする場合は`only`にすることを検討します。

### まとめ

フリーソフトウェアを公開する際のライセンスの選び方や使い方を、フリーソフトウェアやライセンスの基本的な事柄も含めて簡単に紹介しました。

ライセンスはたくさん種類があり、それぞれ内容が難しいので、いざ自分で使おうとするとハードルが高いですよね。

コピーレフトのあるライセンスは、制限が強いと評されることもありますが、ソフトウェアをフリーソフトウェアであり続けさせるための保護を提供してくれます。フリーソフトウェアを大事にしたい場合には、こういったライセンスの採用を検討することをお勧めします。

本記事の内容が少しでも参考になり、状況に応じて適切なライセンスを選んで頂けたら幸いです。

より詳しく厳密に知りたい方は、 https://www.gnu.org/licenses/licenses.html をご覧下さい。
