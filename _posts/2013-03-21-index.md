---
tags: []
title: 日本OSS奨励賞受賞！
---
先日、[オープンソースカンファレンス2013 Tokyo/Spring](http://www.ospn.jp/osc2013-spring/)にて[第8回 日本OSS貢献者賞および第4回 日本OSS奨励賞](http://ossforum.jp/ossaward8th2)の授賞式が行われました。今回、クリアコードのメンバーおよび関係プロジェクトが日本OSS奨励賞を受賞いたしました。それぞれ推薦して下さった皆様、誠にありがとうございます。
<!--more-->


### 個人での受賞

クリアコードのメンバーでは、Mozillaサポート業務を主に担当している結城が、個人で日本OSS奨励賞を受賞いたしました。個人としてのOSS分野での開発活動については、この1年でめざましい成果があったという訳ではありませんが、これまでのOSSの開発・公開の活動に加えて、[Webサイト](http://piro.sakura.ne.jp/)（ブログ）や雑誌記事執筆等を通じて技術情報を公開する活動についても評価していただけたとのことです。

若手への規範として今後も積極的な活動を続けていく事を奨励する、という意味を込めての受賞ということで、今まであまり実践できていなかった「[アップストリームへの還元](/philosophy/development/style.html#fix-in-upstream)」にも力を入れていきたい所存です。

### チームでの受賞

クリアコードが関わっているプロジェクトの1つである[groonga](http://groonga.org/ja/)も、groonga開発チームが日本OSS奨励賞を受賞いたしました。毎月コンスタントにリリースを行う「肉の日リリース」文化の実践と、ユーザーコミュニティや開発者コミュニティなどを牽引するコミュニティ活動を積極的に行っている点をご評価いただけたとのことです。

[Senna](http://qwik.jp/senna/FrontPageJ.html)の後継ということで、日本国内では認知されつつあるものの、残念ながら英語圏へはなかなかリーチできていないという現状があり、今後は世界的な普及に繋げていきたいところです。また、[分散化プロジェクト](https://github.com/groonga/fluent-plugin-droonga)などの新しい取り組みもあります。今後も定期的なリリースを続けながら、継続的な開発を実践し続けていければと、チーム一同思っております。

なお、groonga開発チームの一員として主に[mroonga](http://mroonga.github.com/ja/)の開発に関わっておられる斯波健徳氏も、同時に個人で日本OSS奨励賞を受賞されています。

### まとめ

クリアコードの関係者・関係プロジェクトの日本OSS奨励賞受賞について、簡単ながらご報告させていただきました。

なお、授賞式の様子については技術評論社さまのレポート記事が公開されています。そちらも是非とも併せてご覧下さい。

  * [第8回 日本OSS貢献者賞・日本OSS奨励賞授賞式を開催：レポート｜gihyo.jp … 技術評論社](http://gihyo.jp/news/report/2013/02/2501)
  * [groonga - 第8回OSS奨励賞を受賞](http://groonga.org/ja/blog/2013/02/25/oss-incentive-award.html)
