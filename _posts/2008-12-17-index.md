---
tags:
- presentation
title: 会社紹介は一番最後
---
2週間ほど前になりますが、[オブジェクト倶楽部2008冬イベント『オブラブ忘年会 〜ふりかえり2008〜』](http://www.objectclub.jp/event/2008winter/)のライトニングトークスで話してきました。
<!--more-->


発表資料: [xUnit 2008](http://pub.cozmixng.org/~kou/archives/object-club-2008-winter/)

一番伝えたかったことは「テスティングフレームワークはデバッグ支援の機能を提供することが重要」だということです。時間もないので、その中でも「期待値と実際の値の違いの見せ方」についてだけ触れました。（資料でいうと[24枚目から](http://pub.cozmixng.org/~kou/archives/object-club-2008-winter/xunit-2008-24.html)の話）

表向きは上の通りなのですが、今回の発表では1つ試してみたことがあります。それは、「会社紹介は一番最後がよいのではないか」ということです。一般的には自己紹介・会社紹介をしてから内容に入ることが多い気がしますが、それとは少し異なります。

### 最後の会社紹介は次のアクションの誘導

先日、[Email Security Expo & Conference 2008](http://www.cmptech.jp/esc/)（来年の今頃は[このURL](http://www.cmptech.jp/esc/2008/)になっていそう）に参加しました。参加の目的は情報収集です。商用の迷惑メール対策用製品がどこをウリにしているのかとどのようなWebインターフェイスを提供しているかに興味がありました。

現在、IPAの[２００８年度 オープンソフトウェア利用促進事業 上期テーマ型（開発） 公募](http://www.ipa.go.jp/software/open/ossc/2008/theme/koubo1.html)に採択された「迷惑メール対策ポリシーを柔軟に実現するためのmilterの開発」のために、[milter manager](http://sourceforge.net/projects/milter-manager/)を開発しているのですが、それを開発する上で参考にするためです。

イベントでは製品紹介のセッションを中心に受講したのですが、その中で気づいたことがありました。
プレゼン資料の中で、一番最後に会社紹介を持ってきているセッションが多かったのです。そして、いくつかのセッションを聴いているうちにこれはとてもよい方法だということがわかりました。

この方法が有効なのは、セッションを聴いて、この製品がよさそうだなぁと思ったときです。プレゼンの一番最後に会社紹介を持ってくることで、よさそうだと思ってもらった後のアクションを誘導することができるのです。

プレゼンしている側としては、製品を紹介し、興味を持ってもらった人に問い合わせてもらいたいものです。一番最後に会社紹介を持ってくることで問い合わせ先を伝え、問い合わせるというアクションを誘導しているのです。（イベントではブースコーナーもあったので会社名さえわかれば、ブースコーナーに行って、デモを試したりより詳しい説明を受けることができた）

一番最後に会社紹介がないセッションを聴いているときにも、この製品はよさそうだなぁと思い、デモを見にいこうと思うことがありました。しかし、プレゼンの最後の頃には最初の方にあった会社紹介の内容はすでに覚えていなかったため、配布されていた資料を見直して、自分でどの会社かを調べ直す必要がありました。興味を持った人にこのような手間をかけさせると、その手間のために次のアクションを起こさないかもしれません。せっかく興味を持った人がスムーズに次のアクションに進めるように、ちょうど良いタイミングで次のアクションのための情報を提供することは有効だと思います。

### 試したこと

このような気づきがあったので、オブジェクト倶楽部でのライトニングトークスでは「一番最後に会社紹介をする」ということを試してみました。今回は、話す内容のベースに「動作するきれいなコードを維持し続けることが重要」ということがあったので、それと[会社紹介](http://pub.cozmixng.org/~kou/archives/object-club-2008-winter/xunit-2008-31.html)をつなげやすそうだったということもあります。（クリアコード→きれいなコード）

今回は、発表の内容で「動作するきれいなコードを維持し続けることが重要」だと思ってくれた人が「話している人の会社もきれいなコードを維持し続けるのか、よさそうな会社だな」と思ってくれるかどうかを試してみました。つまり、今回の「次のアクション」は「動作するきれいなコードを維持し続ける」人たちがいる会社としてクリアコードを認識してもらうということでした。

何人かには成功したようなので、次に話すときも試してみようと思っています。そして、次の「次のアクション」はもう少し行動を促すことにしようと思っています。
