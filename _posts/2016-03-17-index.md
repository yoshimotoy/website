---
tags:
- mozilla
- presentation
title: 第1回 法人向けFirefox導入セミナーで発表を行いました
---
2016年3月16日、[Mozilla Japan主催の法人向けFirefox導入セミナー](https://mozilla.doorkeeper.jp/events/40854)でFirefoxの法人利用についての紹介を行いました。
<!--more-->


発表資料は[GitHubの発表資料のリポジトリにてPDFを公開しています](https://github.com/clear-code/firefox-in-corporations-presentation/releases/tag/v20160316)。
また、PDF版ではリンクになっていない参考情報などについては[Markdown形式の元ファイル](https://github.com/clear-code/firefox-in-corporations-presentation/blob/master/firefox-in-corporations.md)をご参照下さい。

### Firefoxの法人向けカスタマイズメニューについて

発表の中で、法人利用で頻出の設定項目を一覧にしたカスタマイズメニューをご紹介しました。

Firefoxはカスタマイズ性の高さが特長の製品ですが、「なんでもできます」と言われても普通は途方に暮れてしまいますし、実際に法人で利用する場面では、行いたいカスタマイズの内容はある程度パターン化されています。
そこでクリアコードでは、業務の効率化を兼ねて、カテゴリ別にカスタマイズ項目とその実現方法を整理したカスタマイズメニューを作成・利用しています。

こちらも発表資料と同様に[GitHubで公開](https://github.com/clear-code/firefox-support-common)しており、全設定項目の一覧は[OpenDocument Formatのファイル](https://github.com/clear-code/firefox-support-common/blob/master/configurations/customization-items.ods)としてダウンロードしていただけます。
また、発表では触れませんでしたが、[各カスタマイズ項目の検証手順のドキュメント（※現状では未完成）](https://github.com/clear-code/firefox-support-common/blob/master/configurations/verification_manual.odt)や、[検証用のテストケース群](https://github.com/clear-code/firefox-support-common/tree/master/testcases)もあります。

設定項目の一覧のODSファイルには、以下の3つのシートが含まれています。

  * 全般的な設定項目の一覧（メタインストーラの設定を含む）

  * キーボードショートカットやメニュー項目のON/OFFに関する設定の一覧

  * その他のUI項目の非表示化に関する設定の一覧

それぞれの設定項目には、設定項目の識別用のIDと共に、簡単な説明と、各選択肢の具体的な反映方法を記してあります。
ここに書かれている内容に従って[MCD用の集中管理用設定ファイル](https://github.com/clear-code/firefox-support-common/blob/master/demonstration/resources/autoconfig.cfg)や[globalChrome.css用のスタイルシートファイル](https://github.com/clear-code/firefox-support-common/blob/master/demonstration/resources/globalChrome.css)を作成すれば、それらのカスタマイズが反映された状態のFirefoxを迷い無く作る事ができます。

リポジトリにあるファイルは一般的な内容になっており、実際の業務では、ここからお客様ごとのニーズに合った選択肢を選び出して使用しています。
現在はちょうどFirefox 38ESRからFirefox 45ESRへの更新が発生している時期なので、その過程で得られた知見を随時フィードバックしつつ、各項目や対応する検証手順をアップデートしているという段階です。

検証が完了していない箇所については、内容に間違いや見落としがある可能性がありますので、その場合はもし宜しければissueでの指摘やプルリクエストを頂けましたら幸いです。

### カスタマイズのデモンストレーション用メタインストーラについて

発表の中で、実際のメタインストーラを実行してカスタマイズ済みのFirefoxをインストールする様子をご紹介しました。
こちらの実際のファイルも[GitHubリポジトリのリリースの一部として公開しています](https://github.com/clear-code/firefox-support-common/releases/tag/v45.0)。

ただし、Firefoxのインストーラそのものの再配布はできないため、実際には「メタインストーラ作成キット一式」の状態の物をアップロードしています。
こちらのZIP形式のファイルを展開すると「FxDemoInstaller-source」というフォルダが取り出されます。この中の「resources」フォルダにFirefox 45ESRのインストーラの実行ファイルをコピーして、「FxDemoInstaller.bat」というバッチファイルを実行する事で、最終的なインストーラの実行ファイル「FxDemoInstaller-45.0.exe」が作成されます。
この「FxDemoInstaller-45.0.exe」をWindowsクライアントにコピーして実行することで、カスタマイズ済みのFirefoxが実際にインストールされます（それ以外のファイルはコピーしなくても大丈夫です）。

[このデモ用メタインストーラで行われているカスタマイズ内容](https://github.com/clear-code/firefox-support-common/blob/master/demonstration/README.md)は、上記のカスタマイズメニューからいくつか項目をピックアップして作成しています。
カスタマイズの適用方法の例として参考にして下さい。

### カスタマイズメニューとメタインストーラのご利用上の注意

上記のカスタマイズメニューとメタインストーラは、自社内での展開用などにそのままお使いいただいて問題ありません。
ただし、内容については無保証となります。

期待通りの結果を得られないなどのトラブルへの対応、個別の環境に合わせるための調査・修正、既存の項目でカバーされていないカスタマイズのご相談などについては、クリアコードのMozillaサポートサービス（有償）のご利用をご検討下さい。

### 次回のセミナーについて

この法人向けセミナーは*毎月第3水曜日の定期開催*となっており、次回は*4月20日*を予定しています。
参加登録は[DoorkeeperのMozillaグループ](https://mozilla.doorkeeper.jp/)から行うことができ、開催会によっては事前登録時のアンケートで、あらかじめセミナーで聞きたい内容をリクエストすることもできるようになる予定です。
Firefoxの法人利用についてピンポイントで不明な点がある場合には、その旨をお伝えいただければ幸いです。

また、次回開催時には15時から16時までと17時から18時までを個別相談の時間として会場を開放し、セミナーは16時から17時の開催とする予定です。
セミナーの質疑応答で大々的には質問できないことでお困りの場合には、個別相談の時間にご相談頂くこともご検討ください。

以上、Mozilla Japanの第1回法人向けFirefox導入セミナーの発表内容の補足と、次回セミナーのご案内でした。
