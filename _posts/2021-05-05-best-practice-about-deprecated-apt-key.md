---
title: 非推奨となったapt-keyの代わりにsigned-byとgnupgを使う方法
author: kenhys
tags:
  - debian
---

`apt-key(8)`はapt 2.1.8より非推奨となりました。

Ubuntu 20.10 (Groovy)やそろそろリリースの時期が近づいてきたDebian 11(Bullseye)からは`apt-key`を利用すると警告されます。

```text
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
```

今回は`apt-key`を使うのをやめて、代替手段へと(パッケージ提供側が)移行する方法について紹介します。

<!--more-->

### apt-keyはどのような挙動をするのか

ソフトウェアを簡単にインストールできるように、開発元が独自にAPTリポジトリを提供してくれている場合があります。
その場合、パッケージの署名に使っている公開鍵をあらかじめインポートするように指示していることがあります。

`apt-key`はその鍵をインポートするのにこれまでよく使われていました。

ただし、`apt-key`は(対象となるキーリングを指定しない場合には)鍵を`/etc/apt/trusted.gpg`へと取り込みます。
`/etc/apt/trusted.gpg`はAPTが既定で信頼する鍵として扱うことを意味します。
単に特定のリポジトリにひもづいた鍵をそちらへ追加するのは、あまりよいやりかたではありません。

### apt-keyから移行する方法

`apt-key`を使ったやりかたから移行する方法はいくつかあります。

* `apt-key`を使ってキーリングに変換し、対象リポジトリとひもづける方法
* `gpg`を使ってキーリングに変換し、対象リポジトリとひもづける方法(非推奨)
* `gpg`を使ってキーリングを作成したものをエクスポート、対象リポジトリとひもづける方法(おすすめ)

`apt-key`はまだDebian 12 (bookworm)でも使えますが、今後削除される [^no-more-apt-key] ことを踏まえると、最後の`gpg`を使って適宜エクスポートするやりかたがおすすめです。

[^no-more-apt-key]: apt 2.9.17以降でapt-keyが削除されているため、今後リリースされるDebian 13 (trixie)や25.04 (Plucky Puffin)では使えません。


#### apt-keyを使ってキーリングに変換し、対象リポジトリとひもづける方法

インポート元の鍵が`archive.key`で移行先のキーリングを`archive-keyring.gpg`とした場合の変換は次のようにして行います。

```console
$ apt-key --keyring ./archive-keyring.gpg add archive.key
```

`apt-key`が削除され、使えなくなるまでは有効な方法です。[^apt-key]
`apt-key`が削除されたあとに移行する必要がでてきたときは、`gpg`を使いましょう。

[^apt-key]: apt-keyでarchive-keyring.gpgに鍵を追加した場合、keybox形式とはならないため、aptが2.9.16以降に更新されてもそのまま使えます。

キーリングを対象リポジトリとひもづける方法は後述します。

#### `gpg`を使ってキーリングに変換し、対象リポジトリとひもづける方法(非推奨)

<div class="callout alert">
2025-01-06 追記: 以前の記事では、gpgで鍵をインポートしてkeybox形式のキーリングに変換するという方法を紹介していました。

ただし、apt 2.9.16以降のバージョンでは、keybox形式がサポートされなくなり、リポジトリの検証に失敗するようになっています。 [^bug]

[^bug]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1088656

例えば、apt update等を実行すると次のようなエラーが発生し、リポジトリの更新に失敗します。

```bash
Warning: An error occurred during the signature verification.
The repository is not updated and the previous index files will be used. GPG error:
https://packages.treasuredata.com/lts/5/debian/bookworm bookworm InRelease:
Sub-process /usr/bin/sqv returned an error code (1), error message is:
Error: Failed to parse keyring "/usr/share/keyrings/fluent-lts-archive-keyring.gpg"
Caused by:     0: Reading "/usr/share/keyrings/fluent-lts-archive-keyring.gpg": EOF     1: EOF
```

具体的には、来年リリースされるであろうDebian 13 (trixie)やUbuntu 25.04 (Plucky Puffin)以降でこの問題が発生するはずです。
そのため、この方法が非推奨であることを明記しました。
</div>

インポート元の鍵が`archive.key`で移行先のキーリングを`archive-keyring.gpg`とした場合の変換は次のようにして行います。


```console
$ gpg --no-default-keyring --keyring ./archive-keyring.gpg --import archive.key
```

期待通りに変換できたかは、`file`コマンドを使うとわかります。

```console
$ file archive-keyring.gpg
archive-keyring.gpg: GPG keybox database version 1 (省略)
```

上記のように`GPG keybox database version 1`となっていれば期待通りに変換できています。


キーリングを対象リポジトリとひもづける方法はあとで述べます。

<div class="callout secondary">
<p><b>2025-01-06 追記: 上記の説明は現在では古くなっています。過去の時点では利用できたものの、推奨される方法ではありませんでした。
次の.ascにエクスポートする方法を推奨します。あくまで過去の情報としてこのまま残しておきますが、使用しないでください。</b></p>
</div>

#### `gpg`を使ってキーリングを作成したものをエクスポート、対象リポジトリとひもづける方法(おすすめ)

インポート元の鍵が`archive.key`で移行先のキーリングを`archive-keyring.asc`とした場合の変換は次のようにして行います。


```console
$ gpg --no-default-keyring --keyring ./archive-keyring.kbx --import archive.key
$ gpg --no-default-keyring --keyring ./archive-keyring.kbx --armor --export > archive-keyring.asc
```

期待通りに変換できたかは、`file`コマンドを使うとわかります。

```console
$ file archive-keyring.asc
archive-keyring.asc: PGP public key block Public-Key (old)
```

上記のように`PGP public key block Public-Key (old)`となっていれば期待通りに変換できています。

エクスポートしたファイルを対象リポジトリとひもづける方法は次で述べます。

### 変換した鍵を対象リポジトリとひもづける方法

APTの設定ファイルでは、リポジトリがどの鍵で署名されたかを明示できます。

そのため、サードパーティーの鍵は`/usr/share/keyrings/`配下において、
どの鍵で署名されているかを`signed-by`で明示してひもづけるのがおすすめです。

変換した鍵が`archive-keyring.asc`であれば、次のようにして対象リポジトリとひもづけます。

`/etc/sources.list.d/`配下に置かれた設定ファイルの内容は次のようになるはずです。

```
deb [signed-by=/usr/share/keyrings/archive-keyring.asc] https://packages.example.com/debian stable main
```

deb822形式だと次のようになるはずです。

```
Types: deb
URIs: https://packages.example.com/debian/
Suites: bookworm
Components: contrib
Signed-By: /usr/share/keyrings/archive-keyring.asc
```

このようにすることで、`/etc/apt/trusted.gpg.d`配下に変換後の鍵を置かなくても、
サードパーティーが提供するリポジトリの正規パッケージであることを確認してパッケージをインストールできます。
サードパーティーによって提供される鍵によって検証されるリポジトリの範囲を限定できるのでおすすめです。

### まとめ

今回は、非推奨となった`apt-key`から移行する方法について紹介しました。
Debian 11 (Bullseye)が`apt-key`が使える最後のリリースです。Debian 12 (Bookworm)で削除されますので移行はお早めに。[^postponed]

[^postponed]: 本記事の初出はbullseyeリリースよりも前であったため、当時の情勢からそのように書いていましたが、最終的にはDebian 12 (bookworm)がapt-keyが使える最後のリリースになるはずです。

