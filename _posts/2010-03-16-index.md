---
tags:
- ruby
title: るびま0029号
---
[るびま0029号](http://jp.rubyist.net/magazine/?0029)がリリースされていますね。おめでとうございます。
<!--more-->


せっかくなので少し紹介します。

### ActiveLdapを使ってみよう

今回のるびまにはRubyでLDAPを操作するための便利ライブラリ[ActiveLdap](http://rubyforge.org/projects/ruby-activeldap/)の記事の後編[ActiveLdap を使ってみよう（後編）](http://jp.rubyist.net/magazine/?0029-ActiveLdap)が入っています。

後編では今まではあまり文書化されていなかった[フィルタのこと](http://jp.rubyist.net/magazine/?0029-ActiveLdap#l11)や[関連性のこと](http://jp.rubyist.net/magazine/?0029-ActiveLdap#l12)にも触れています。[全てのエントリを扱うクラス](http://jp.rubyist.net/magazine/?0029-ActiveLdap#l26)は、知る人ぞ知るのノウハウではないでしょうか。（ActiveLdap付属のサンプルアプリケーションでは使われていますが文書化はされていなかったはず。）LDAPサーバの設定やデータを確認する場合には[LDAP のスキーマ情報を ActiveLdap から参照する](http://jp.rubyist.net/magazine/?0029-ActiveLdap#l27)あたりの情報が役に立ちます。ldapsearchなどではなく、irbでLDAPサーバの情報を確認できるので、環境構築時やデバッグ時にとても便利です。（irbの補完機能を有効にするとより便利です。）

記事を書いている[高瀬さん](http://d.hatena.ne.jp/tashen)は[ActiveLdapのチュートリアルの翻訳](http://code.google.com/p/ruby-activeldap/wiki/TutorialJa)もしている頼もしい方です。ActiveLdapに興味はあるけどまだよく知らないという方は、まず、 [ActiveLdap を使ってみよう（前編）](http://jp.rubyist.net/magazine/?0027-ActiveLdap)を読んでからチュートリアルを読んで、最後に後編を読むのがよいのではないでしょうか。

[0029-RubyNews](http://jp.rubyist.net/magazine/?0029-RubyNews)にも[ActiveLdap 1.2.1のリリース](http://jp.rubyist.net/magazine/?0029-RubyNews#l5)が載っていますね。

### とちぎRuby会議02

[発表者として参加したとちぎRuby会議02]({% post_url 2009-10-25-index %})のレポート記事[RegionalRubyKaigi レポート (10) とちぎ Ruby 会議 02](http://jp.rubyist.net/magazine/?0029-TochigiRubyKaigi02Report)もあります。レポートにもありますが、とちぎRuby会議02のお題が他のRegionalRubyKaigiと違ったものだったので、独特な内容になっていましたね。

### Rubyベストプラクティス

[0029 号 巻頭言](http://jp.rubyist.net/magazine/?0029-ForeWord)で紹介されているRuby 1.9向けに書かれたRubyベストプラクティスという本がそろそろ発売するようです。テストまわりの章だけレビューに参加しました。

少しくせがある印象なので、Ruby初心者の方にはつらいかもしれません。自分で判断できる程度にRubyを知っている方ならいろいろ考えながら読むとおもしろいかもしれません。

### まとめ

るびまがリリースされていたので紹介しました。

ところで、みなさんは[編集後記](http://jp.rubyist.net/magazine/?0029-EditorsNote)は読んでいるのでしょうか。たまにおもしろかったりするので、読んでいない方は読んでみてはいかがでしょうか。短いのでさっと目を通せます。
