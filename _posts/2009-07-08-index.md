---
tags: []
title: TortoiseGitでOpenSSHの鍵を使う
---
Subversionでバージョン管理されているソフトウェアの開発をWindows上で行う場合に、[TortoiseSVN](http://tortoisesvn.tigris.org/)を使っている人は多いのではないでしょうか。TortoiseSVNはシェル（エクスプローラ）に機能が統合されるため、フォルダ上の右クリックからチェックアウトやコミット、差分の表示などを行うことができ、コマンドライン操作に不慣れな人でも簡単にSubversionを使うことができます。
<!--more-->


このTortoiseSVNと同じ使い勝手で分散型バージョン管理システムのgitを利用できるようにするgitクライアントが、[TortoiseGit](http://sourceforge.jp/projects/tortoisegit/)です。

参考：[実用レベルに達したWindows向けGitクライアント「TortoiseGit」でGitを始めよう](http://sourceforge.jp/magazine/09/06/19/0340248)

TortoiseGitの導入手順は上記リンク先をご覧いただくとして、ここでは、SSHを使用してリポジトリにアクセスする際の手順を紹介したいと思います。なお、以下の説明はWindows XP環境にmsysgit、TortoiseGit 0.8.1.0をインストールした場合に基づいていますので、必要に応じて説明を読み替えてください。

### OpenSSHの秘密鍵をそのまま使ってみる

TortoiseSVNではPuTTY形式とOpenSSH形式の2つの形式のSSH鍵を使うことができます。使いたい鍵がOpenSSH形式である場合、以下のようにすることになります。

  1. C:\Documents and Settings\<var>ユーザ名</var>\.ssh\ の位置にフォルダを作成する。

  1. 「.ssh」フォルダに、OpenSSHの秘密鍵（id_rsaなど）をコピーする。

  1. TortoiseGitの設定ダイアログを開き、「Network」の「SSH client」にmsysgit付属のssh.exeを指定する。


この状態で、適当なフォルダの中でコンテキストメニューから「Git Clone...」を選択してclone用のダイアログを開きます。ここでクローン元のリポジトリのURLにSSHを使用するURL（例： *ssh:*//git.example.com/project ）を入力して「OK」ボタンをクリックすると、秘密鍵を使用するためのパスフレーズの入力を求められます。パスフレーズを入力すれば、データが送信され、リポジトリがローカルに複製されます。この場合、「Pull」でoriginから変更点を取得する際などにも、その都度パスフレーズの入力を求められることになります。

### OpenSSHの秘密鍵をPuTTY形式に変換して使う

上記手順でOpenSSHの秘密鍵を使う場合、pullやpushなどの度に毎回パスフレーズの入力を求められるという問題があります。パスフレーズの入力を最小限だけで済ませたい場合は、秘密鍵をPuTTY形式に変換してPageantに登録すると良いでしょう。

  1. TortoiseGitに付属しているputtygen.exeを起動する。

  1. 「File」→「Load private key」を選択して、ファイルの種類で「All Files」を選択し、先ほど C:\Documents and Settings\<var>ユーザ名</var>\.ssh\ の位置に置いたOpenSSH形式の秘密鍵（id_rsaなど）を開く。

  1. 秘密鍵を使うためのパスフレーズの入力を求められるので、パスフレーズを入力する。

  1. 「Successfully imported foreign key (OpenSSH SSH-2 private key).」といったメッセージが表示され、秘密鍵がインポートされる。

  1. 「Save private key」ボタンをクリックして、PuTTY形式の秘密鍵を「id_rsa.ppk」などのファイル名で保存する。

  1. タスクトレイに表示されている「Pageant（PuTTY authentication agent）」をダブルクリックし、Pageantのキーリストを開く。

  1. 「Add Key」ボタンをクリックし、先ほど保存したPuTTY形式の秘密鍵（id_rsa.ppk など）を選択する。

  1. 秘密鍵を使うためのパスフレーズの入力を求められるので、パスフレーズを入力する。

  1. TortoiseGitの設定ダイアログを開き、「Network」の「SSH client」にTortoiseGit付属のTortoisePlink.exeを指定する。


PageantはOpenSSHでいうssh-agentに相当する常駐型のソフトウェアで、これが常駐している間はパスフレーズの入力の手間を最小限に省くことができます。この状態で、*ssh:*//git.example.com/project のようなSSHを使うURLのリポジトリからcloneする際に、ダイアログで「Load Putty Key」にチェックを入れて先ほどのPuTTY形式の秘密鍵を選択しておくと、cloneやpull、pushなどの実行時にいちいちパスフレーズを聞かれずとも通信できるようになります。

### まとめ

WindowsでTortoiseGitを使う場合、OpenSSH形式の秘密鍵はそのままで使うのではなく、PuTTY形式に変換してから使うようにしましょう。
