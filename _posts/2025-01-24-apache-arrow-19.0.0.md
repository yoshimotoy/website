---
tags:
- apache-arrow
title: Apache Arrow 19.0.0
author: kou
---
[Apache Arrow](https://arrow.apache.org/)の開発に参加している須藤です。[19.0.0がリリース](https://arrow.apache.org/blog/2025/01/16/19.0.0-release/)されたので紹介します。

<!--more-->

### 概要

Apache Arrowは高速に大規模データを処理したり交換したりするアプリケーションを開発するためのプラットフォームです。データフォーマットなどの仕様を標準化したり、それを扱うライブラリーなどを提供したりしています。

Apache Arrowは特定のプログラミング言語のみサポートするのではなく、広く様々なプログラミング言語をサポートすることを重視しています。当初はすべてのプログラミング言語向けのライブラリーをまとめてリリースしていましたが、現在は、まとめてリリースしているものと個別にリリースしているものがあります。今回リリースされた19.0.0はまとめてリリースしているもので、次のプログラミング言語向けになります。

* C
* C#
* C++
* JavaScript
* MATLAB
* Python
* R
* Ruby
* Swift

ちょっと前までGoとJavaもまとめてリリースされていましたが、最近分離されました。

Goは分離されてから何度かリリースされていて、現時点の最新バージョンは10日ほど前にリリースされた[Apache Arrow Go 18.1.0](https://arrow.apache.org/blog/2025/01/13/arrow-go-18.1.0/)です。

Javaは分離されてからまだリリースされていません。今はリリースの仕組みを整備しています。ちなみに、私もやっています。おそらく、来月か再来月にはリリースできるのではないかと思っています。

言語を分離する流れはこれからも続いていくはずです。たくさんの言語をまとめてリリースする場合の課題感は次の通りです。

* リリース作業が大変
* マイナーリリース・パッチリリースをリリースしにくい

リリース作業が大変というのはわかりやすいと思いますが、マイナーリリース・パッチリリースはわかりにくいと思うので少し補足します。まとめて同じバージョンをつけてリリースしているので、どれか1つの言語で非互換の変更がある場合はメジャーリリースにする必要があります。（セマンティックバージョニングを採用しているのでそうなります。）たとえば、C++で非互換の変更があるが、JavaScriptでは非互換の変更がないとします。この場合は、JavaScriptはマイナーリリース・パッチリリースでも大丈夫なのですが、C++にあわせてメジャーリリースにしないといけません。JavaScriptを使ったアプリケーション開発者はメジャーバージョンアップということは非互換があるのか！？と変更内容を確認してから、非互換はなさそうということでアップグレードすることになります。マイナーリリース・パッチリリースの場合はもっと気軽にアップグレードできます。

なお、次はRかJavaScriptが分離されるのではないかと思います。

今は3ヶ月くらいおきにメジャーバージョンアップをしていますが、一通り分離が終わったら、このペースが変わるかもしれません。

ただ、今でもマイナーリリース・パッチリリースをしていないわけではなく、たまにしています。たとえば、すでに19.0.0にいくつかApache Parquet関連の問題が見つかっているので、19.0.1がリリースされる予定にはなっています。

それでは、主な変更点を紹介していきます。

### 仕様

仕様はライブラリーのリリースとは分離されているという体裁なので、仕様の追加・変更をライブラリーのリリースにあわせなくてもよいのですが、ドキュメントの公開がライブラリーのリリースのタイミングなのでここで紹介します。次の2つの新しい仕様が追加されました。どれもまだ「実験的」扱いなので、ぜひ使ってみてフィードバックをしてください。

* [Statistics schema](https://arrow.apache.org/docs/format/StatisticsSchema.html)
* [Async Device Stream Interface](https://arrow.apache.org/docs/format/CDeviceDataInterface.html#async-device-stream-interface)

Statistics schemaは私がやっていたやつで、Apache Arrowデータの統計情報をApache Arrowデータで表現するためのスキーマを標準化しています。標準化することで統計情報を交換しやすくなることを狙っています。Apache Arrowデータで表現することで既存のデータ交換方法（Apache Arrowフォーマットでの交換とかC data interfaceでの交換とか）を活用できることとか、Apache Arrowデータでの表現力（ネストしたデータも表現できる）を活用できること狙っています。統計情報が大きくなることは稀なはずなので、効率のためにApache Arrowデータにしているわけではありません。

あわせて読みたい：[Apache Arrowで統計情報]({% post_url 2024-10-16-apache-arrow-statistics %})

Async Device Stream Interfaceは既存の[C stream interface](https://arrow.apache.org/docs/format/CStreamInterface.html)のGPU（より正確にいうとCPU以外のデバイス）対応版+非同期版です。C stream interfaceは次のデータの取得を要求したら結果が返ってくるまでブロックします。一方、Async Device Stream Interfaceではブロックしません。事前にコールバックを登録しておいて、次のデータの準備ができたタイミングでコールバックが呼ばれる、という使い方になります。

### Apache Arrow Flight RPC

Apache Arrow Flight RPC関連では次のような変更があります。

* タイムアウトで使うタイムスタンプの精度を常にナノ秒にする
* UCXバックエンドを非推奨にする

元々はタイムスタンプの精度はOSの標準の精度にしていたのですが、Windowsなどはナノ秒ではなくマイクロ秒なので、Protocol Bufferのタイムスタンプの精度（ナノ秒）を表現できないことがありました。これ、元々は私が実装したんですが、このときからどの環境でもナノ秒にしておいた方がよかったんだなぁ、失敗したなぁという気持ちになりました。

[UCX](http://www.openucx.org/)はいろんな通信方法を抽象化していてしかも速い、というライブラリーなのですが（私はそう思っているのですが）、それをApache Arrow Flight RPCのバックエンドとして使うことをやめようとしています。gRPCのみになります。

元々、UCXを使うといいんじゃない？ということで実験的に実装されたものでしたが、UCXを使うならApache Arrow Flight RPCベースよりも[Dissociated IPC Protocol](https://arrow.apache.org/docs/format/DissociatedIPC.html)とかの方が向いているんじゃない？ということで、そっちに寄せていくことになりました。

### C++

C++関連では次のような変更があります。PythonとかRubyとかもC++実装を使っているのでこれらはPythonとかRubyとかでも有効です。

* [Compute functions](https://arrow.apache.org/docs/cpp/compute.html)が増えたり新しい32bit/64bit decimalに対応したりと改良した
* [Acero](https://arrow.apache.org/docs/cpp/streaming_execution.html)の高速化
* S3バックエンドでユーザーの鍵を使ってサーバーサイドで暗号化（server-side encryption with customer provided keys）をサポート
* AzureバックエンドでSASトークンを使った認証をサポート

Aceroはストリームベースのデータ処理エンジンで、今後がどうなるか心配だったのですが、Aceroに興味がある人が改良してくれています。その人はコミッターにもなったので、これからもAceroはよくなっていきそうです。

### まとめ

他にもありますが、他のものは[公式のリリースブログ](https://arrow.apache.org/blog/2025/01/16/19.0.0-release/)を参照してください。

なお、[19.0.0のリリースノート](https://arrow.apache.org/release/19.0.0.html)を見ると、今回も私が一番多くコミット・マージしていました。そんながんばっている私にApache Arrow関連のサポートを頼みたいという場合は[クリアコードのApache Arrowサービス]({% link services/apache-arrow.md %})をどうぞ。

