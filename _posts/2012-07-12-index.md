---
tags: []
title: '思い出せるチケットの書き方: 「動機」、「ゴール」、「実現案」'
---
ソフトウェア開発を支援するために、やりたいことや問題を管理するシステムがあります。例えば、[Bugzilla](http://www.bugzilla.org/)[^0]や[Redmine](http://www.redmine.org/)[^1]、[GitHub](https://github.com/)などがそのような機能を持っています。システムごとにやりたいことや問題の呼び方が違います。例えば、Bugzillaでは「バグ」、Redmineでは「チケット」、GitHubでは「Issue」と呼んでいます。ここではRedmineと同じ「チケット」と呼び方を使うことにします。
<!--more-->


今回紹介するのは、後からチケットを見たときに、チケットを書いた時に知っていた情報を思い出せるようなチケットの書き方です。

このような書き方は、プロトタイプのような「作って終わり」とか「短期間の開発」というようなソフトウェアでは必要がないでしょう。また、後述の通り、「チケット駆動開発」にも使えないでしょう。継続的に開発を続けていくソフトウェアの開発のように、やりたいことや問題はあるけどすぐにすべてに着手できない、という開発には使える場面があるはずです。

チケットの書き方というと、問題を報告するときにどのように報告すると開発者に喜ばれるか、という観点での話がありますが、今回の話はそのような話ではありません。開発者自身が開発者自身のためにチケットを書くときの話です。

### 目的

後から見て思い出せるチケットを書く目的は「目の前のやりたいことや問題に集中できるようにすること」です。

継続的に開発を続けていくと、新しいアイディアが浮かんだり、新しい問題が見つかることがよくあります。そのとき、1つのアイディアを実装したら新しいアイディアが浮かんでくるというようにはならず、新しいアイディアはどんどん増えるけど実装が追いつかない、ということがほとんどです。問題についても同じです。

新しいアイディアや問題を全部覚えておくことは大変です。そこで、覚えておかなければいけないことを少なくして、目の前のことに集中できるようにしたいのです。目の前のことに集中して1つずつ解決していきたいのです。

### 目的を実現するために必要な情報

この「目の前のやりたいことや問題に集中できるようにすること」という目的を実現するために、覚えておかなければいけないことをチケットに残して、覚えておかなければいけなかったことを忘れます。では、覚えておかなければいけないことはなんでしょうか。試行錯誤した結果、以下の項目があれば忘れても後で思い出せることがわかりました[^2]。

  1. 動機: どうしてそれをやりたいか。どうしてそれが問題か。

  1. ゴール: どうなったらうれしいのか。

  1. 実現案: こうやったらゴールに辿りつけるんじゃないかという思いつき。


「動機」と「ゴール」は必要な情報で、「実現案」はあればうれしい情報です。

「動機」はチケットの作業をする前に必要で、「ゴール」はチケットの作業を完了するときに必要で、「実現案」はチケットの作業を開始するときに必要です。

「動機」がわからないと、後で見たときに、このチケットの必要性を判断できません。必要かどうかがわからなければ、そもそも解決しなければいけないかどうかということを判断できません。そのため、チケットの作業を始める前に、チケットがどれだけ重要かを判断するために「動機」が必要です。また、「ゴール」が適切かを検証するためにも必要です。

「ゴール」がわからないと、後で見たときに、チケット作成時はどこに向かおうとしていたか、がわかりません。ゴールに到達したかどうかでチケットが完了したかどうかを判断するため、少なくとも作業を始める前には設定する必要があります。実際にゴールに到達したらチケットを完了にします。もちろん、チケット作成時には適切だと思ったゴールが、チケットを見なおしたときに適切ではなくなっているということはあるでしょう。そのときはチケットの作業をする前に新しくゴールを設定する必要があります。

「実現案」があると作業をすぐに始めることができます。なければ、どうやってゴールに到達するかを検討する必要があります。もし、チケットを作成するときに検討したのであれば、その結果を記録しておくと、改めて同じことを検討しなくてもよいためすぐに作業に入れて便利です。

### 実現方法

必要な情報を整理できたので、それらだけをチケットに含めるようにします。このようなときはテンプレート化をするということがよく行われます。今回の場合は以下のようなテンプレートにしました[^3]。

{% raw %}
```
タイトル: 「動機」あるいは「ゴール」の要約

h2. 動機

なぜこれをやりたいか、なぜこれが問題かがわかる情報を書く。
問題の再現手順があるならそれもここに書く。

h2. ゴール

どうなるとうれしいかを書く。
どうしてこのゴールで動機を満足できるかがわかるとよい。

h2. 実現案

ゴールに到達するための案があるならそれを書く。
案がないなら「案が思いつかないので考えないといけない」ということを書く。
ないというのも大事な情報。
書き忘れたのであれば思い出そうとしたほうがよいかもしれないが、
そもそも検討していないのであれば、
検討しなければいけないということがすぐにわかる。
```
{% endraw %}

例えば、「groongaのHTTPサーバー機能を強化すること」をチケットにする場合は以下のようになります。

{% raw %}
```
タイトル: HTTPサーバーの機能が貧弱で不便（「動機」の要約バージョン）
  あるいは
タイトル: ちゃんとしたHTTPサーバー機能が欲しい（「ゴール」の要約バージョン）

h2. 動機

groongaのHTTPサーバーは必要最小限の機能しかもっておらず、
POSTや認証機能などHTTPで使える便利な機能が使えなくて不便である。

ただし、現在のHTTPサーバー機能はやっていることが少ないため速い。
HTTPの便利な機能は欲しいが、速さは譲れないポイントなので、
遅くなるなら便利な機能は諦めたほうがよさそう。

h2. ゴール

ちゃんとしたHTTP機能を提供する。ただし、速度は落とさない。

速度面で現状のHTTPサーバーと同等かそれ以上のものを狙う。
さらにHTTPの仕様をできるだけ満たすこと。

h2. 実現案

groongaのHTTPサーバーをnginxのモジュールとして実装する。
イメージはPassengerのStandaloneモード。

作業の流れは以下の通り。
* groonga用のnginxモジュールを作る。
* nginxをgroongaのソースコードにバンドルして、
  groonga用nginxモジュール付きでビルドできるようにする。
* できあがったバイナリをgroonga-httpdという名前でインストールできるようにする。

Apacheモジュールとすることもできそうだけど、
Apacheよりnginxの方が性能がでるので、nginxの方がよさそう。
```
{% endraw %}

この情報があれば「groongaのHTTPサーバー機能を強化すること」について忘れても後で思い出せます。また、チケットを書いた人とは別の人が作業をすることになったときもすぐに情報を把握できて作業に入りやすいです。

もし、これが「ちゃんとしたHTTPサーバー機能をサポートする」だけであれば、「どうやって実装しよう？」などを改めて検討しないといけません。もしかしたら、「速さを大事にする！」という観点を忘れてしまうかもしれません。そのようなことが続くとボロが増えることになるので、「チケットを使って覚えることを減らしてソフトウェア開発を支援する」というやり方は自分たちには向いていなかったね、ということになるでしょう。

### 問題点

この方法の問題点はチケットを書くのが面倒だということです。例えば、Pivotal Trackerというツールではタイトルだけでチケットを作ることができるため、ちょっとメモをとる感じでどんどんチケットを作っていくことができます。しかし、このテンプレートにあわせて書こうとすると、「どうしてこの作業は必要なんだろう」とか「どうなったらうれしいんだろう」ということを考える必要があり、ちょっとメモをとる感じでは作れません。慣れてくればチケットを作る前から「どうしてこの作業は必要なんだろう」という視点でやりたいことや問題を考えられるようになるため、チケット作成時に悩むことは少なくなりますが、まとまった文を書く必要があるため、ちょっとメモをとるよりも数倍時間がかかります。

チケット駆動開発では[「チケット無しのコミットは禁止」](http://www.machu.jp/diary/20070907.html#p01)というようにチケットを使いますが、それをやるにはこのチケットの書き方は重すぎるでしょう。忘れても思い出せるように記録を残すことが目的なので、チケットを書いているよりコードを書いたほうが早い場合は、忘れなくてもよいためチケットは作りません。これはチケット駆動開発とは方向性が違います。

### まとめ

やりたいことや問題を忘れても後から思い出せるようにチケットに記録する方法を紹介しました。これはやりたいことや問題をたくさん抱えている場合に有効な方法です。すべてのことを覚えておく必要がなくなるため、問題の1つずつに集中して対応することができます。もし、後からチケットを見たときに、よく「なにこのチケット？」となる人は試してみてはいかがでしょうか。

[^0]: [Mozilla関連のプロジェクト](https://bugzilla.mozilla.org/)や[GNOME関連のプロジェクト](https://bugzilla.gnome.org/)などで使われている。

[^1]: [Ruby本体関連のプロジェクト](https://bugs.ruby-lang.org/)や[groonga関連のプロジェクト](http://redmine.groonga.org/)などで使われている。

[^2]: 個人差はあるでしょう。

[^3]: Redmineのデフォルトのマークアップ言語がTextileなので、ここでもTextileを使っています。
