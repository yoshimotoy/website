---
title: collectd 6.0でのAPI変更に対応する方法の概要
author: kenhys
---

システムやアプリケーションのメトリクス情報をあつめるソフトウェアの1つに[collectd](https://collectd.org/)があります。

この記事を書いている時点のcollectdの最新版は5.12ですが、次期バージョンである6.0も並行して開発が進められています。
今回は、collectd 6.0の動向と、それを踏まえてcollectd 6.0でのAPI変更に対応する方法の概要について紹介します。

<!--more-->

### collectd 6.0の現状について

今年の9月の段階では、[年内にリリースしたい意向があった](https://mailman.verplant.org/pipermail/collectd/2020-September/007392.html)ようですが、現在ではいつリリースされるかは未定となっています。
これはほとんどのプラグインが6.0で導入されるAPIの変更に追従できていないためです。
メンテナンスされていないプラグインは削除されることでしょう。

なお現在の対応状況については、 [collectdのプロジェクトボード](https://github.com/orgs/collectd/projects/1)にて一覧できます。

この記事を書いている時点で、6.0の開発に関する167のTODOが残っています。

collectd 6.0の動向を追いかけたい人は、[Community calls](https://collectd.org/wiki/index.php/Community_calls)のページを参照するとよいでしょう。

### collectd 5.xはどうなるのか

5.x系は5.12までとの話もあったようですが、5.13のリリースが予定されています。
ただし、CIの問題があり、レビューやマージ作業がすすんでいません。そのため、リリース次期は未定です。

### collectd 6.0に対応するには

5.xから6.0にプラグインを移行させるために参考になるドキュメントには、[V5 to v6 migration guide](https://collectd.org/wiki/index.php/V5_to_v6_migration_guide)があります。
ただし、これだけだと心もとないので、すでに移植されているcpuプラグインやwritre_stackdriverプラグインを参考にするのがよいようです。

Luaプラグインの場合も概ね上記のガイドにそって移植する必要がありました。

* `data_set_t *`や`value_list_t *`を`metric_family_t *`を使うように置き換える
* `metric_family_t *`を使うようになることにともない、メタデータへのアクセスは`meta_data_xxx`のAPIを使うように書き換える
* メタデータのキーは`meta_data_toc`で取得する
* メタデータの型は`meta_data_type`でキーに対応する型を取得する
* メタデータの値は`meta_data_get_xxx`で型にあわせたAPIを都度呼び出す

上記のようなことに留意して移植する必要があります。

なお、collectd-6.0ブランチに[google-interns-2020ブランチ](https://github.com/collectd/collectd/tree/google-interns-2020)もあとからマージされる予定となっており、プラグイン作者にとっては、さらに追加で修正作業が必要になることも予想されます。

### まとめ

今回は、collectd 6.0でのAPI変更に対応する方法の概要について紹介しました。
[Luaのプラグインを6.0に対応](https://github.com/collectd/collectd/pull/3795)させましたが、まだマージされていません。
collectd 6.0の開発は手がとても足りておらず、他にもたくさんのプラグインが対応待ちの状態なので、興味があったらぜひ開発に参加してみてください。

### 参考となるヘッダファイル

* [メタデータのAPI関連のヘッダファイル](https://github.com/collectd/collectd/blob/collectd-6.0/src/utils/metadata/meta_data.h)
* [metric_family_t関連のヘッダファイル](https://github.com/collectd/collectd/blob/collectd-6.0/src/daemon/metric.h)
