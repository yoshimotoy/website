---
tags:
- notable-code
title: ノータブルコード10 - 文芸的設定ファイル
---
第10回目のノータブルコードで紹介するのは、ドナルド・クヌースの設定ファイルです。
<!--more-->


### 文芸的プログラミングとは何か

ドナルド・クヌースは『The Art of Computer Programming』の作者、そして組版ソフトウェアTeXの開発者として世界的に名を知られた伝説的な人物ですが、彼はまた「文芸的プログラミング」の概念を提唱したことでも知られています。今日の記事のテーマは、彼の業績のうちの文芸的プログラミングに関する部分です。

文芸的プログラミングの哲学を、一言で説明するのは難しいのですが、その基本的な発想は、プログラムを、単に機械が解釈できるフォーマルな命令の羅列ではなく、人間が読むことができる自然な文章としても提示しようというアイデアにありました（クヌース自身はこれを「自然言語である英語と、CやLispのような形式言語の間で切り替えをし、まとめ上げられる自然なフレームワーク」[^0]と表現しています）。 この哲学の実践的なメリットは、その原理が正しく実行されれば、真の意味で「人間が読むことのできるプログラム」を生み出せることです。

クヌースが真に偉大なのは言行一致であること、すなわち自分が提唱しているアイデアを、自ら実践しているところです — これが今日の記事の本題につながります。クヌースはTeXのような公開を意図したプログラムはもちろんのこと、個人的に書くプログラムでも文芸的なスタイルを採用していると語っています。

そして、そのスタイルの適用範囲は、驚くべきことに、設定ファイルにも及ぶのです。

### クヌースの設定ファイル

実際の例を見てみましょう。以下に引用するのは、クヌース自身の手になるFVWM2向け設定ファイルの冒頭部分です。

https://www-cs-faculty.stanford.edu/~knuth/programs/.fvwm2rc

```bash
# This Fvwm2 setup file provides the basic emacs-centered environment
# that I have found most comfortable on my standalone machine at home.
# Basically it gives me a big Emacs window at the left and a slightly
# smaller XTerm at the right, together with a clock and CPU monitor
# and a few buttons for accessing independent desktops.

# I've tried to write lots of comments because I will certainly forget
# most of the details of Fvwm2's syntax and semantics before long.

# My display screen is 1440 pixels wide and 900 high.

# First, make sure that Exec chooses tcsh instead of bash:
ExecUseShell /bin/tcsh

# Next, specify the paths for all icons (.xpm files) that I'm using:
# PixmapPath /usr/share/icons:/home/icons
ImagePath /home/icons:/usr/share/icons

# I tried mxn desktops and didn't like them.
DeskTopSize 1x1
```


全文は274行あるので、ぜひリンク先も見てください。この記事では、次の3つのポイントを指摘しておきたいと思います。

  1. まず、この設定ファイルはごく自然な文章として読むことができます。「文芸的」の名前に違わず、プログラム向けの設定と、人間向けの説明がごく自然に統合されています。

  1. また、設定の背景にある前提は何かが明示されているのもポイントです。例えば、普段はEmacsとXTermを利用していること、設定の対象となるディスプレイが1440x900のサイズであること、などの背後にある隠れた重要な前提がきちんと読者に示されています。

  1. さらに見逃せないポイントは、設定の構成自体が、自然言語の文章の流れに沿うように組み立てられている点です。これは、プログラムのいわば後付けとしてコメントを付与する一般的な流儀とは、明確に発想を異にしている点です。

もちろん、このスタイルをすべての人が実践できるか、というと議論があるところです。というのも、明らかにこの手法は (1) プログラマとしての能力に加えて (2) 優れた文章の書き手としての能力を兼ね備えていないと実践できないからです。

それでも、この設定ファイルが「ちょっと文芸的スタイルを試してみようかな」と読み手に関心を引かせるほどの魅力を持っているのは事実です。ともすると、プログラミングを数理科学の一つとして位置づける現在主流の考え方は根本的に間違っていて、本当は人文科学としての国語の一分野なのかもしれない、そう思わせるほどの説得力が、このコードにはあります。

### まとめ

今日の記事では、文芸的プログラミングの実践例としての（文芸的）設定ファイルを紹介しました。

この設定ファイルを読んで、皆さんはどう思いましたか？ご意見ご感想があればぜひお寄せください。

[^0]: ピーター・サイベル著・青木靖訳「Coders at Work プログラミングの技をめぐる探求」(オーム社, 2011年）p558
