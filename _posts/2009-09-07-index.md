---
tags: []
title: milter-greylistでtaRgrey
---
[milter-greylist](http://hcpnet.free.fr/milter-greylist/)にtarpit機能が取り込まれました。これにより、milter-greylistで[taRgrey](http://k2net.hakuba.jp/targrey/)を実現できるようになりました。
<!--more-->


### taRgrey

taRgreyはメールの送信側情報や送り方などから迷惑メールを排除する方法で、現在のところ90%以上の迷惑メールを排除できます。taRgrey以外にも送信側の情報を利用する迷惑メール対策はありますが、taRgreyは誤検出を抑えることを重視した対策であることが特徴です。（taRgrey関連技術の長所・短所や迷惑メール対策に関する考え方などは[第9回まっちゃ445勉強会でのさとうさんの発表](http://d.hatena.ne.jp/stealthinu/20090829/p1)を参照してください。）

### 導入までの流れ

[taRgreyのページ](http://k2net.hakuba.jp/targrey/)にある通り、現在taRgreyを実現するためには[Postgrey](http://postgrey.schweikert.ch/)にパッチをあてる必要があります。Postgrey自体はパッケージシステムで提供されている環境が多いため導入が容易ですが、その後、手動でパッチをあてなければいけないため、多少、導入の敷居があがっています。

milter-greylistもPostgreyと同様にパッケージシステムで提供されている環境が多く、導入が容易です。今回、milter-greylist本体にtarpit機能が取り込まれたことにより、パッケージシステムでmilter-greylistを導入し、設定を変更するだけでtaRgreyを実現できるようになりました。

とはいえ、tarpit機能が取り込まれたバージョンが安定版としてリリースされるのはもう少し先になるので、今はまだパッケージシステムで導入したmilter-greylistでtaRgreyを実現することはできません。

### 設定

[milter manager](/software/milter-manager.html)と一緒にmilter-greylistを使う場合、milter-greylistの設定はこれだけです。

{% raw %}
```
racl whitelist tarpit 125s
racl greylist default
```
{% endraw %}

非常に簡潔に書けることがわかります。

### まとめ

milter-greylist本体にtarpit機能が取り込まれたことにより、より簡単にtaRgreyが実現できるようになったことを紹介しました。

tarpit機能が取り込まれたバージョンがリリースされるまでの間に変更される部分もあるかもしれないので、ここではあまり詳細に立ち入らずに簡単な紹介にとどめました。安定版としてリリースされたら、より詳細な設定方法や注意点などを紹介する予定です。
