---
tags:
- ruby
- clear-code
title: 札幌Ruby会議2012でバグを修正する方法とクリアなコードの作り方について話します
---
先日、[札幌Ruby会議2012のプログラムが決定](http://rubykaigi.tdiary.net/20120531.html#p02)しました。クリアコードからは以下の2件が採択されました。
<!--more-->


  * [バグを修正する方法](https://github.com/sprk2012/sprk2012-cfp/tree/master/okkez-how-to-fix-bugs)
  * [クリアなコードの作り方](https://github.com/sprk2012/sprk2012-cfp/tree/master/kou-how_to_make_clear_code)

他にも[興味深い話がたくさん採択](https://github.com/sprk2012/sprk2012-cfp)されています。札幌Ruby会議2012は9月14日-16日に開催されるので、興味のある人はそのあたりの予定を空けておくとよいでしょう。

それでは、秋に札幌で会えることを楽しみにしています。
