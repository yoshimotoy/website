---
tags:
- ruby
title: インストールするだけでActiveScaffoldのメニューを日本語化
---
以前、[ActiveScaffoldの地域化]({% post_url 2008-08-12-index %})の中で、[ActiveScaffoldLocalize](http://github.com/edwinmoss/active_scaffold_localize/)を使ってActiveScaffoldのメニューを日本語化する方法を紹介しました。
<!--more-->


時は流れて、ActiveScaffoldLocalizeから日本語メニューのリソースが削除（！）されたり、[ActiveScaffold](http://activescaffold.com/)本体に各言語のリソースが含まれるようになったりしました。しかし、本体には日本語リソースが含まれていなかったため、Web上には、いまだにActiveScaffold Japanese L10Nを使う、自分で日本語リソースを作成して使う、というような情報がでていました。

本家にフィードバックして取り込んでもらえれば、多くの人がより手軽に日本語化されたActiveScaffoldを使えるだろうに、ということで、先日、本家に日本語リソースを取り込んでもらいました。これからは、ActiveScaffoldLocalizeなどを使わずにActiveScaffold本体のみで日本語メニューを使うことができます。

### 手順

簡単に日本語メニューのActiveScaffoldを使う手順を説明します。

まず、ActiveScaffoldに依存しない、一般的なI18nまわりを整備します。

{% raw %}
```
% rails shelf
% cd shelf
% script/generate resource book title:string
% rake db:migrate
% gem install amatsuda-i18n_generators -s http://gems.github.com/
% script/generate i18n ja
```
{% endraw %}

続いて、ActiveScaffoldまわりを整備します。

{% raw %}
```
% script/plugin install git://github.com/activescaffold/active_scaffold.git
```
{% endraw %}

config/routes.rb:

{% raw %}
```diff
-  map.resources :books
+  map.resources :books, :active_scaffold => true
```
{% endraw %}

app/controllers/books_controller.rb:

{% raw %}
```ruby
class BooksController < ApplicationController
  active_scaffold :book
end
```
{% endraw %}

app/views/layouts/application.html.erb:

{% raw %}
```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>ActiveScaffold l10n</title>
    <%= javascript_include_tag(:defaults) %>
    <%= active_scaffold_includes %>
  </head>

  <body>
    <h1>ActiveScaffold l10n</h1>
    <%= yield %>
  </body>
</html>
```
{% endraw %}

これでメニューが日本語化されます。

[![日本語メニューのActiveScaffold]({{ "/images/blog/20090928_2.png" | relative_url }} "日本語メニューのActiveScaffold")]({{ "/images/blog/20090928_0.png" | relative_url }})

日本語関連の手順は最初のI18nまわりのところだけです。ActiveScaffold関連の手順にはまったく日本語関連の手順はありません。通常のインストール手順で日本語メニューが表示されるのは、ActiveScaffold本体に日本語リソースが含まれるようになったおかげです。

データを入れるとこのようになります。

[![日本語メニューのActiveScaffold（データ入り）]({{ "/images/blog/20090928_3.png" | relative_url }} "日本語メニューのActiveScaffold（データ入り）")]({{ "/images/blog/20090928_1.png" | relative_url }})

### まとめ

ActiveScaffold本体に日本語リソースが含まれて、より簡単に日本語メニューのActiveScaffoldを使えるようになったことを紹介しました。

フリーソフトウェアを改良してよりよくした場合は、手元での変更やブログに書くにとどめずに、本家にフィードバックしてみてはいかがでしょうか。より多くの人が便利に使えるようになるだけではなく、多くの場合、自分のメンテナンスコストも下がります。
