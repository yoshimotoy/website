---
tags: []
title: 手動でインベントリーファイルを作らずにVagrantとAnsibleを一緒に使う方法
---
最近、[Hatohol](https://github.com/project-hatohol/hatohol)というオープンソースの運用管理ツールの開発にも参加しています。
<!--more-->


開発環境はDebian GNU/Linux sidなのですが、Hatoholのサポート環境にはCentOS 6も含まれているため、ドキュメントを書くときなどはCentOS 6で動作を確認する必要があります。ただ、「Hatohol用のCentOS 6環境」を常時用意しておくのは管理が面倒[^0]です。そのため、仮想マシン上に何度でも簡単にセットアップできるようにしました。

「何度でも簡単にセットアップできる」の部分にはAnsibleを使いました。これは、[HatoholのリポジトリーにすでにAnsibleのplaybookファイルが存在](https://github.com/project-hatohol/hatohol/tree/master/setup-with-ansible)したためです。playbookファイルはあるので、Vagrantと組み合わせて仮想マシン上にセットアップできるようにしました。

VagrantとAnsibleの連携方法を調べると、日本語の情報のものはだいたい手動でインベントリーファイルを作っています。[Vagrantのサイトにあるドキュメント（英語）](https://docs.vagrantup.com/v2/provisioning/ansible.html)ではインベントリーファイルはVagrantが自動で作ってやるよと書いているのに、です。

インベントリーファイルを自動で作ってもらう方が手間が少なく、IPアドレスやホスト名などを明示しなくてもよいので管理が簡単になります。そのため、Vagrantに自動で作ってもらうときの使い方を日本語で紹介します。

### インベントリーファイルを自動で作る方法

インベントリーファイルを自動で作るには、特別なことを何もしなければよいです。つまり、Vagrantはデフォルトでインベントリーファイルを自動で作るようになっています。

特別なことというのは、例えば、ansible.cfgを作ったり、`ansible.inventory_path`を指定したり、といったことです。

### インベントリーファイルを自動で作ると困ること

インベントリーファイルを自動で作ることは何もしなければよいだけなので簡単なのですが、playbookの書き方によってはうまく動きません。具体的には次のケースはひと手間かけないとうまく動きません。

  * `hosts`で`all`以外を指定している
  * `user`を指定していて、さらに指定した値が`vagrant`ではない（`user`自体を設定していない場合はひと手間は必要ない）
  * root権限で実行することを想定している（`sudo`を指定していない）

それぞれどのようにひと手間かければよいか説明します。なお、どれも[Hatoholの既存のplaybookファイル](https://github.com/project-hatohol/hatohol/blob/master/setup-with-ansible/setup-hatohol-dev.yaml)が該当しているケースです。

#### `hosts`で`all`以外を指定している

`hosts`で`all`以外を指定していると、Vagrantで作った仮想マシンは指定したグループに所属していなければいけません。ホストがどのグループに所属するかはインベントリーファイルで指定します。今はインベントリーファイルはVagrantが自動で作る前提なので、Vagrantにグループとホストの対応を伝えてうまく作ってもらう必要があります。

具体的には次のように`ansible.groups`を使います。この例はplaybookファイルで`hosts: targets`と指定されていたケース用です。

{% raw %}
```ruby
id = "centos-6-x86_64"
box_url = "http://opscode-vm-bento.s3.amazonaws.com/vagrant/virtualbox/opscode_centos-6.5_chef-provisionerless.box"
config.vm.define(id) do |node|
  node.vm.box = id
  node.vm.box_url = box_url
  node.vm.provision("ansible") do |ansible|
    ansible.playbook = "setup-hatohol-dev.yaml"
    ansible.groups = {
      "targets" => [id],
    }
  end
end
```
{% endraw %}

これでインベントリーファイルに次のような内容が入ります。

{% raw %}
```
[targets]
centos-6-x86_64
```
{% endraw %}

playbookファイルが対象としているグループにVagrantで作ったホストが入っているので、playbookファイルで指定したタスクが実行されます。

#### `user`を指定していて、さらに指定した値が`vagrant`ではない

Vagrantが作った仮想マシンにはvagrantユーザーでログインします。playbookファイルで`user`を指定しているとその設定が優先され、仮想マシンにログインできません。この場合は`ansible.extra_vars`で`:ansible_ssh_user`を指定することでSSHするユーザーを変更できます。

具体的には次のように`ansible.extra_vars`を使います。

{% raw %}
```ruby
id = "centos-6-x86_64"
box_url = "http://opscode-vm-bento.s3.amazonaws.com/vagrant/virtualbox/opscode_centos-6.5_chef-provisionerless.box"
config.vm.define(id) do |node|
  node.vm.box = id
  node.vm.box_url = box_url
  node.vm.provision("ansible") do |ansible|
    ansible.playbook = "setup-hatohol-dev.yaml"
    ansible.extra_vars = {
      :ansible_ssh_user => "vagrant",
    }
  end
end
```
{% endraw %}

これでplaybookファイルで指定されているユーザーではなく、vagrantユーザーでSSHしてくれるようになります。

#### root権限で実行することを想定している

vagrantユーザーは一般ユーザーです。root権限が必要な場合は`sudo`を使わなければいけません。playbookファイルに`sudo: true`という設定がない場合は`sudo`を使ってくれないのでVagrant側から`sudo`を使ってほしいということを伝える必要があります。

具体的には次のように`ansible.sudo=`を使います。

{% raw %}
```ruby
id = "centos-6-x86_64"
box_url = "http://opscode-vm-bento.s3.amazonaws.com/vagrant/virtualbox/opscode_centos-6.5_chef-provisionerless.box"
config.vm.define(id) do |node|
  node.vm.box = id
  node.vm.box_url = box_url
  node.vm.provision("ansible") do |ansible|
    ansible.playbook = "setup-hatohol-dev.yaml"
    ansible.sudo = true
  end
end
```
{% endraw %}

これで、たとえplaybookファイル中で`sudo: true`を指定していなくても`sudo`を使って実行してくれるようになります。

### まとめ

VagrantとAnsibleを一緒に使う方法を検索すると、日本語の情報ではインベントリーファイルを手動で作成する情報が多かったので、より手間の少ないVagrantに自動で作成してもらう方法を日本語で紹介しました。自動で作成する方法は[Vagrantのサイトにあるドキュメント（英語）](https://docs.vagrantup.com/v2/provisioning/ansible.html)には書いているので、本家の情報も参考にしてください。よく知らないツールの使い方を調べるときは、念のため、本家の情報も参考にした方がよいでしょう。

VagrantとAnsibleの連携方法は具体的なVagrantファイルの書き方を示しました。実際に使っている[Vagrantファイル](https://github.com/project-hatohol/hatohol/blob/master/setup-with-ansible/Vagrantfile)も参考にしてください。

また、[Hatohol](http://hatohol.org/)というオープンソースの運用管理ツールの開発にも参加していることを匂わしました。

[^0]: 別のマシンで開発しようとした時、「Hatohol用のCentOS 6環境」にアクセスできないかもしれない。
