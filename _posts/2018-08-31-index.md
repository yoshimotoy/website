---
tags:
- mozilla
- feedback
- embedded
title: YoctoのWeston上で日本語入力
---
### はじめに

これまでにも何度か紹介していますが、クリアコードではGecko（Firefox）を組み込み機器向けに移植する取り組みを行っています。
<!--more-->


  * [Gecko Embeddedプロジェクト]({% post_url 2017-07-06-index %})

  * [Gecko Embeddedプロジェクト 11月時点のステータス]({% post_url 2017-11-09-index %})

  * [Gecko Embedded 次期ESR対応]({% post_url 2018-03-30-index %})

  * [Gecko Embedded ESR60]({% post_url 2018-05-30-index %})

その後、課題として残っていたWebRTCも無事に動作するようになり、主だった機能は主ターゲットであるRZ/G1M上で動作するようになっています。

今回は趣向を変えて、同環境上で少しだけ日本語入力を検証してみた経過を紹介します。

![uimでの日本語入力の様子]({{ "/images/blog/20180831_0.png" | relative_url }} "uimでの日本語入力の様子")

### Yoctoでの日本語入力事情

Yoctoの主要なレイヤを概観してみたところ、IMフレームワークとしてはuim、日本語変換エンジンとしてはAnthyが見つかりました。

  * [uimのレシピ](https://github.com/openembedded/meta-openembedded/tree/master/meta-oe/recipes-support/uim)

  * [Anthyのレシピ](https://github.com/openembedded/meta-openembedded/tree/master/meta-oe/recipes-support/anthy)

逆に、これ以外のIMフレームワークや日本語変換エンジンのレシピを見つけることはできませんでしたので、今回はこれを使用してみます（uim-skkでも良いと思いますが、一般向けにはやや紹介しづらいので、今回はAnthyのみを対象とします）。

さて、ウィンドウシステムがX11であれば、おそらく上記レシピをそのままビルドするだけで使用できるでしょう。ですが、今回の対象はYoctoのcore-image-westonであり、ウィンドウシステムはWayland/Westonです。uimに[Waylandサポートが入ったのは1.8.7から](https://github.com/uim/uim/commit/06558e571967f3cb989bdb550d1dea05247cc21d)で、上記レシピは1.8.6ですから、恐らくこれを普通にビルドするだけでは日本語入力できるようにはならないでしょう。実際に試してみましたが、やはりアプリケーションがクラッシュして起動できないという結果となりました。

### レシピの修正

上記仮説が正しければ、単にuimを最新版に上げるだけでWeston上でも動作するでしょう。ですが、実際にuimのバージョンを上げてビルドを試してみたところ、同レシピに含まれるパッチがそのままでは当たらないなどの問題が発生しました。そちらを修正するのも難しくはないでしょうが、まずは手っ取り早く動くか動かないかを確認したかったため、uim-1.5.6のまま最低限のパッチを最新のuimからバックポートしてみることにしました。以下がmeta-openembeddedに対するパッチです。

```diff
commit f3b0e042986a83a767a967ec352c731037693d98
Author: Takuro Ashie <ashie@clear-code.com>
Date:   Fri Aug 17 13:16:21 2018 +0900

    uim: First aid to work with Wayland

diff --git a/meta-oe/recipes-support/uim/uim/0001-Fix-the-problem-that-the-candidate-window-is-not-sho.patch b/meta-oe/recipes-support/uim/uim/0001-Fix-the-problem-that-the-candidate-window-is-not-sho.patch
new file mode 100644
index 0000000..6ebeb21
--- /dev/null
+++ b/meta-oe/recipes-support/uim/uim/0001-Fix-the-problem-that-the-candidate-window-is-not-sho.patch
@@ -0,0 +1,26 @@
+From f266ff2b59bc3b0cd732c62683a1df9672114c1d Mon Sep 17 00:00:00 2001
+From: Konosuke Watanabe <konosuke@media.mit.edu>
+Date: Sat, 20 Feb 2016 12:30:35 +0900
+Subject: [PATCH] Fix the problem that the candidate window is not shown in
+ GTK3 environment.
+
+---
+ gtk2/immodule/uim-cand-win-gtk.c | 2 +-
+ 1 file changed, 1 insertion(+), 1 deletion(-)
+
+diff --git a/gtk2/immodule/uim-cand-win-gtk.c b/gtk2/immodule/uim-cand-win-gtk.c
+index 1bfe759c..41590d06 100644
+--- a/gtk2/immodule/uim-cand-win-gtk.c
++++ b/gtk2/immodule/uim-cand-win-gtk.c
+@@ -225,7 +225,7 @@ uim_cand_win_gtk_init (UIMCandWinGtk *cwin)
+ 
+   gtk_widget_set_size_request(cwin->num_label, DEFAULT_MIN_WINDOW_WIDTH, -1);
+   gtk_window_set_default_size(GTK_WINDOW(cwin), DEFAULT_MIN_WINDOW_WIDTH, -1);
+-  gtk_window_set_resizable(GTK_WINDOW(cwin), FALSE);
++  gtk_window_set_resizable(GTK_WINDOW(cwin), TRUE);
+ }
+ 
+ static void
+-- 
+2.17.1
+
diff --git a/meta-oe/recipes-support/uim/uim/0001-gtk3-support-Wayland-backend.patch b/meta-oe/recipes-support/uim/uim/0001-gtk3-support-Wayland-backend.patch
new file mode 100644
index 0000000..e40caeb
--- /dev/null
+++ b/meta-oe/recipes-support/uim/uim/0001-gtk3-support-Wayland-backend.patch
@@ -0,0 +1,71 @@
+From 06558e571967f3cb989bdb550d1dea05247cc21d Mon Sep 17 00:00:00 2001
+From: Kouhei Sutou <kou@clear-code.com>
+Date: Sat, 30 Dec 2017 21:15:50 +0900
+Subject: [PATCH] gtk3: support Wayland backend
+
+GitHub: fix #71
+
+Debian: 810739
+
+Reported by Thibaut Girka. Thanks!!!
+---
+ gtk2/immodule/gtk-im-uim.c   | 16 ++++++++++++++++
+ gtk2/immodule/key-util-gtk.c |  8 +++++++-
+ 2 files changed, 23 insertions(+), 1 deletion(-)
+
+diff --git a/gtk2/immodule/gtk-im-uim.c b/gtk2/immodule/gtk-im-uim.c
+index ac2918ce..066e5f5b 100644
+--- a/gtk2/immodule/gtk-im-uim.c
++++ b/gtk2/immodule/gtk-im-uim.c
+@@ -535,6 +535,22 @@ layout_candwin(IMUIMContext *uic)
+     gdk_window_get_geometry(uic->win, &x, &y, &width, &height, &depth);
+ #endif
+     gdk_window_get_origin(uic->win, &x, &y);
++    {
++      GtkWindow *window = NULL;
++      GdkWindow *gdk_window = uic->win;
++      while (gdk_window) {
++        gpointer user_data;
++        gdk_window_get_user_data(gdk_window, &user_data);
++        if (user_data && GTK_IS_WINDOW(user_data)) {
++          window = user_data;
++          break;
++        }
++        gdk_window = gdk_window_get_parent(gdk_window);
++      }
++      if (window) {
++        gtk_window_set_transient_for(GTK_WINDOW(uic->cwin), window);
++      }
++    }
+     uim_cand_win_gtk_layout(uic->cwin, x, y, width, height);
+   }
+ }
+diff --git a/gtk2/immodule/key-util-gtk.c b/gtk2/immodule/key-util-gtk.c
+index 27abd834..bd029e73 100644
+--- a/gtk2/immodule/key-util-gtk.c
++++ b/gtk2/immodule/key-util-gtk.c
+@@ -319,6 +319,7 @@ im_uim_init_modifier_keys()
+ #ifdef GDK_WINDOWING_X11
+   int i, k = 0;
+   int min_keycode, max_keycode, keysyms_per_keycode = 0;
++  GdkDisplay *gdk_display;
+   Display *display;
+   GSList *mod1_list, *mod2_list, *mod3_list, *mod4_list, *mod5_list; 
+   XModifierKeymap *map;
+@@ -329,7 +330,12 @@ im_uim_init_modifier_keys()
+ 
+   mod1_list = mod2_list = mod3_list = mod4_list = mod5_list = NULL;
+ 
+-  display = GDK_DISPLAY_XDISPLAY(gdk_display_get_default());
++  gdk_display = gdk_display_get_default();
++  if (!GDK_IS_X11_DISPLAY(gdk_display)) {
++    /* TODO: We may need to something for Wayland. */
++    return;
++  }
++  display = GDK_DISPLAY_XDISPLAY(gdk_display);
+   map = XGetModifierMapping(display);
+   XDisplayKeycodes(display, &min_keycode, &max_keycode);
+   sym = XGetKeyboardMapping(display, min_keycode,
+-- 
+2.17.1
+
diff --git a/meta-oe/recipes-support/uim/uim_1.8.6.bb b/meta-oe/recipes-support/uim/uim_1.8.6.bb
index 271718e..e7241f1 100644
--- a/meta-oe/recipes-support/uim/uim_1.8.6.bb
+++ b/meta-oe/recipes-support/uim/uim_1.8.6.bb
@@ -4,17 +4,19 @@ LICENSE = "BSD-3-Clause & LGPLv2+"
 LIC_FILES_CHKSUM = "file://COPYING;md5=32463fd29aa303fb2360faeeae17256b"
 SECTION = "inputmethods"
 
-SRC_URI = "http://uim.googlecode.com/files/uim-${PV}.tar.bz2"
+SRC_URI = "https://github.com/uim/uim/releases/download/uim-${PV}/uim-${PV}.tar.bz2"
 
 SRC_URI_append_class-target = " file://uim-module-manager.patch \
     file://0001-fix-bug-for-cross-compile.patch \
     file://0001-Add-support-for-aarch64.patch \
+    file://0001-gtk3-support-Wayland-backend.patch \
+    file://0001-Fix-the-problem-that-the-candidate-window-is-not-sho.patch \
 "
 SRC_URI[md5sum] = "ecea4c597bab1fd4ba98ea84edcece59"
 SRC_URI[sha256sum] = "7b1ea803c73f3478917166f04f67cce6e45ad7ea5ab6df99b948c17eb1cb235f"
 
 DEPENDS = "anthy fontconfig libxft libxt glib-2.0 ncurses intltool"
-DEPENDS_append_class-target = " intltool-native gtk+ gtk+3 uim-native takao-fonts"
+DEPENDS_append_class-target = " intltool-native gtk+3 uim-native takao-fonts"
 
 RDEPENDS_uim = "libuim0 libedit"
 RDEPENDS_uim-anthy = "takao-fonts anthy libanthy0 glibc-utils glibc-gconv-euc-jp"
@@ -31,6 +33,7 @@ EXTRA_OECONF += "--disable-emacs \
     --without-canna \
     --without-mana \
     --without-eb \
+    --without-gtk2 \
 "
 
 CONFIGUREOPTS_remove_class-target = "--disable-silent-rules"
```


なお、当時のmeta-openembeddedのレシピではuimのソースコードをダウンロードすることが出来なったので、上記パッチにはその修正も含まれています。この点については既にOpenEmbeddedプロジェクトに報告済みで、[修正が取り込まれています](https://github.com/openembedded/meta-openembedded/commit/3490d00bff39d667be968d262eaff9d56a5aa0b7#diff-09e0b790d0b008ddd66780c32371f3a5)。

### ビルド方法

ベースのブートイメージのビルド方法は[これまでと同様](https://github.com/webdino/meta-browser/wiki/Firefox-ESR60%E3%83%93%E3%83%AB%E3%83%89%E6%89%8B%E9%A0%86%EF%BC%88iWave-RainboW-G20D-Q7-Yocto-2.0%EF%BC%89)ですので割愛します。
これにuimを追加するには、以下の設定をconf/local.confに追加してcore-image-westonを再作成します。

```
IMAGE_INSTALL_append = " uim uim-common uim-gtk3 uim-anthy "
```


### 動作設定

Anthyを既定のIMとするには、以下の内容で設定ファイル~/.uimを作成します。

```
(define default-im-name 'anthy)
```


firefoxを起動する際に、環境変数`GTK_IM_MODULE=uim`をセットすることでuimが使えるようになります。

```
$ WAYLAND_DISPLAY=wayland-0 GTK_IM_MODULE=uim firefox
```


### まとめ

YoctoのWeston上での日本語入力について、現在の検証状況を紹介しました。こんな記事を書いている暇があったらとっととアップストリームのuimをバージョンアップしてしまいたいところではありますが、すぐには作業に取りかかれないため、まずは社内Wikiの情報を切り貼りして公開してみました。
