---
tags:
- fluentd
- presentation
title: 'PLAZMA OSS Day: TD Tech Talk 2018 – Cooperative works for Fluentd Community'
---
[PLAZMA OSS Day: TD Tech Talk 2018](https://techplay.jp/event/650389) にて登壇しました。
fluent-plugin-kafkaのスループット問題を解消するために開発したkafka-connect-fluentdの紹介とfluent-plugins-nurseryに代表されるFluentdのプラグインを引き取り、メンテナンスしている事例の概要を紹介しました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/cosmo0920/plazma-slide-20180215/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
    <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/cosmo0920/plazma-slide-20180215/" title="Plazma OSS Day slide">Plazma OSS Day slide</a>
  </div>


</div>


### 内容

  * kafka-connect-fluentdについての現状とベンチマーク

  * Fluentdのプラグイン周りのメンテナンスの実績

を紹介しました。

スライドにもありますが、Fluentdのプラグインの作者によるメンテナンスが滞っているプラグインがよく見かけられます。そのため、よく使われているプラグインのうち、作者がメンテナンスをする時間が取れないものに関しては引き取ることにしました。その引き取り場所として、[fluent-plugins-nursery](https://github.com/fluent-plugins-nursery) という organization を作成しました。

#### まとめ

PLAZMA OSS Day: TD Tech Talk 2018で先日リリースしたkafka-connect-fluentdの紹介と、Fluentd周りを良くして行く活動の実績を紹介しました。
