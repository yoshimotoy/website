---
tags:
- ruby
title: コミットログでRetrospectivaと連携
---
[Ruby](http://www.ruby-lang.org/)で実装された[Subversion](https://ja.wikipedia.org/wiki/Subversion)用[^0]リポジトリブラウザ（兼ITS(Issue Tracking System)/[バグ管理システム](https://ja.wikipedia.org/wiki/%E3%83%90%E3%82%B0%E7%AE%A1%E7%90%86%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0)(Bug Tracking System)）として[Retrospectiva](http://retrospectiva.org/)があります。
<!--more-->


Retrospectivaにはコミットログを解析してより便利にRetrospectivaを使うための機能がいくつかあります。Retrospectivaを採用しているプロジェクトであれば、コミットログの書き方をRetrospectivaの解釈できる書き方にすることにより、さらにRetrospectivaを便利に使うことができます。

### チケットへのリンク

RetrospectivaはチケットベースのITS機能を備えています。コミットログには「XXX番のチケットの問題を修正した」というようなログを書くことも多いでしょう。その時、以下のフォーマットでチケットの番号を書くことにより、そのコミットログをRetrospectiva上で見ると該当するチケットにリンクが張られます。（[例](http://retrospectiva.org/changeset/495)）

{% raw %}
```
[#XXX]
```
{% endraw %}

この機能はブラウザ上から変更履歴を見ているときにとても便利です。

### チケットの更新

Retrospectivaには[Extension](http://retrospectiva.org/wiki/Extensions)という拡張機能の仕組みがあります。この仕組みを利用した[SCM Ticket Update](http://retrospectiva.googlecode.com/svn/extensions/1-1/scm_ticket_update/)という拡張機能を導入することにより、コミットログでチケットの状態を更新することができます。

SCM Ticket Updateの導入方法は以下の通りです。（現時点（2008-05-23）のtrunkを利用している場合）

{% raw %}
```
% RAILS_ENV=production ruby script/rxm checkout http://retrospectiva.googlecode.com/svn/extensions/1-1/scm_ticket_update
% RAILS_ENV=production ruby script/rxm install scm_ticket_update
% # Retrospectivaを再起動
```
{% endraw %}

拡張機能をインストールした場合はRetrospectivaを再起動することを忘れないでください。

この拡張機能を入れた後は以下のような書式でコミットログを書くことにより、コミットと一緒にチケットも更新することができます。

チケット#123の状態を修正済みに変更:

{% raw %}
```
クラッシュバグを修正 [#123] (status:fixed)
```
{% endraw %}

チケット#29の割り当てユーザをaliceに変更:

{% raw %}
```
[#29] テストを追加 (assigned:alice)
```
{% endraw %}

他にも以下のような書式が使えます。

{% raw %}
```
[#N] (NAME1:VALUE1 NAME:VALUE2 ...) ログ
```
{% endraw %}

また、もし変更後の値に空白が入っている場合は「"..."」とダブルクォートで囲みます。

{% raw %}
```
[#2929] (status:fixed milestone:"2.9 (バラ)") fix a trivial bug.
```
{% endraw %}

この機能を使うと、コミットした後にブラウザからチケットを変更する作業がなくなるのでチケットのクローズし忘れも減るかもしれません。また、コミットメールで（コミットログから）チケットがクローズされたことが分かるのも便利な点です。

### まとめ

このようにコミットログの書き方を少しRetrospectivaよりにするだけでもっと便利にRetrospectivaが使えるようになります。

Retrospectivaが要求している書き方を使ってもコミットログが見づらくなるわけではないので、少し意識して使ってみてはいかがでしょうか。

[^0]: Gitにも微妙に対応している
