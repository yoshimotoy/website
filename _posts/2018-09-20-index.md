---
tags:
- groonga
- presentation
title: 'db tech showcase Tokyo 2018 - MySQL・PostgreSQLだけで作る高速あいまい全文検索システム #dbts2018'
---
[db tech showcase Toyo 2018](https://www.db-tech-showcase.com/dbts/tokyo)で話すことを事前に宣伝をしておきたかったけど間に合わなかった須藤です。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-tokyo-2018/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-tokyo-2018/" title="MySQL・PostgreSQLだけで作る高速あいまい全文検索システム">MySQL・PostgreSQLだけで作る高速あいまい全文検索システム</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-tokyo-2018/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/dbtechshowcasetokyo2018)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-db-tech-showcase-tokyo-2018)

### 内容

去年は「[MySQL・PostgreSQLだけで作る高速でリッチな全文検索システム]({% post_url 2017-09-08-index %})」というタイトルで話しました。去年はMySQL（Mroonga）・PostgreSQL（PGroonga）で次のことを実現するための具体的なSQLを紹介しました。

  * 全文検索

  * キーワードハイライト

  * 周辺テキスト表示

  * 入力補完

  * 同義語展開

  * 関連文書の表示

  * 構造化データ（オフィス文書・HTML・PDFなど）対応

今年は「MySQL・PostgreSQLだけで作る高速あいまい全文検索システム」というタイトルで話しました。今年も話の流れは同じにしました。あることを実現する具体的なSQLを紹介するというスタイルです。今年はMySQL（Mroonga）・PostgreSQL（PGroonga）で次のことを実現するための具体的なSQLを紹介しました。

  * ヨミガナ検索

  * 同義語展開

  * 電話番号検索

  * ワイン名検索

  * fuzzy検索

今年は「あいまい検索」の実現方法にフォーカスした機能を選びました。同義語展開は去年も紹介したのですが、「あいまい検索」というテーマでは入っていたほうがよさそうだと思ったので入れました。「近傍検索」と「quorumマッチ」は入れられませんでした。

「あいまい検索」というテーマにしたのは今年はこのあたりの機能を強化したからです。「ヨミガナ検索」は今回の発表のために間に合わせました。

### まとめ

去年の内容と組み合わせると全文検索まわりのかなりのことをMySQL・PostgreSQLで実現できます。ぜひ、Mroonga・PGroongaを使ってMySQL・PostgreSQLをさらに活用してください。

Mroonga・PGroongaのサポートが必要な方は[問い合わせフォーム](/contact/?type=groonga)からご相談ください。
