---
tags: []
title: 公式のWindows版PostgreSQLパッケージ用の拡張機能のビルドシステムの作り方
---
PostgreSQLは[各種プラットフォーム用のパッケージを提供](http://www.postgresql.org/download/)しているため、簡単にインストールすることができます。PostgreSQL用の拡張機能のインストール方法も簡単です。拡張機能のバイナリーと設定ファイル[^0]を拡張機能用のディレクトリーに配置して[`CREATE EXTENSION`](https://www.postgresql.jp/document/9.4/html/sql-createextension.html)を実行するだけです。
<!--more-->


[GNU Make](http://www.gnu.org/software/make/)があるシステムでは[PGXS](https://www.postgresql.jp/document/9.4/html/extend-pgxs.html)[^1]という拡張機能のビルドを支援する仕組みを使うことができます。PGXSを使えば[数十行程度](https://github.com/pgroonga/pgroonga/blob/master/Makefile)でGCCを使って拡張機能をビルドする仕組みができます[^2]。

しかし、公式のWindows版PostgreSQLパッケージ[^3]はVisual Studioでビルドされているため、GCCを使うPGXSでは公式のWindows版PostgreSQLパッケージ用の拡張機能をビルドできません。そこで、[CMake](http://www.cmake.org/)を使って公式のWindows版PostgreSQLパッケージ用のビルドシステムの作り方を紹介します。

なお、MinGWを使ってWindows版PostgreSQLをビルドする場合はPGXSを使えます[^4]。また、PostgreSQL本体と一緒に拡張機能をビルドする場合はPostgreSQL自体のビルドシステムを使えます[^5]。ここで紹介する方法は「公式のWindows版PostgreSQLパッケージ用に拡張機能だけをビルドする」ビルドシステムの作り方です。

### 公式のWindows版PostgreSQLパッケージ用拡張機能のビルド方法

ビルドシステムを作るためにはビルド方法を確立していなければいけません。Visual Studio用の拡張機能のビルド方法は[PostgreSQLのWiki](https://wiki.postgresql.org/wiki/Building_and_Installing_PostgreSQL_Extension_Modules#Windows_with_Microsoft_Visual_Studio)にまとまっています。ざっくり言うと次の設定をしたVisual Studioのソリューションファイルを作ってビルドします。

  * DLLを作成する
  * 素のCとしてコンパイルする
  * C++の例外を無効にする
  * includeの検索パスに↓を追加する
    * `include\server\port\win32_msvc`
    * `include\server\port\win32`
    * `include\server`
    * `include`
  * ライブラリーの検索パスに↓を追加する
    * `lib`
  * `postgres.lib`をリンクする

Wikiではこれらの設定をしたプロジェクトファイルを手動で作る前提になっているように見えますが、バージョン管理システムと相性が悪いですし、更新にはVisual Studioが必要になりメンテナンスしづらいのでやりたくありません。ということで、これらの設定をしたVisual Studioでのビルド用のファイルを生成することにします[^6]。

### CMakeを使った公式のWindows版PostgreSQLパッケージ用拡張機能のビルドシステムの使い方

CMakeを使うとVisual Studioでのビルド用のファイルを生成することができます。また、CMakeはCPackというパッケージ作成用の仕組みも用意しているのでビルド済みの拡張機能のパッケージ作成までできます。

CMakeを使った場合は次のようにすればビルドできます。「`%PostgreSQLをインストールしたフォルダー%`」と「`%PostgreSQLのバージョン%`」は適切な値に置き換えます。例えば、「`..\pgsql`」と「`9.4.1-3`」といった具合です。

{% raw %}
```
> cmake . -G "Visual Studio 12 2013 Win64" -DCMAKE_INSTALL_PREFIX=%PostgreSQLをインストールしたフォルダー% -DPOSTGRESQL_VERSION=%PostgreSQLのバージョン%
> cmake --build . --config Release
```
{% endraw %}

ビルドした拡張機能をインストールする場合は次のようにします。

{% raw %}
```
> cmake --build . --config Release --target install
```
{% endraw %}

パッケージを作成する場合は次のようにします。

{% raw %}
```
> cmake --build . --config Release --target package
```
{% endraw %}

簡単ですね。

### CMakeを使った公式のWindows版PostgreSQLパッケージ用拡張機能のビルドシステム

ではCMake用の設定ファイル（`CMakeLists.txt`）を作ります。ベースは次のようになります。ここに前述のVisual Studioでの設定を追加していきます。

{% raw %}
```
cmake_minimum_required(VERSION 3.0.0)

set(PROJECT_NAME "拡張機能名") # 例: "PGroonga"
set(PROJECT_ID "拡張機能のID") # 例: "pgroonga"

set(VENDOR "拡張機能の開発者") # 例: "The PGroonga Project"

set(VERSION_MAJOR "拡張機能のメジャーバージョン") # 例: "0"
set(VERSION_MINOR "拡張機能のマイナーバージョン") # 例: "5"
set(VERSION_PATCH "拡張機能のパッチバージョン")   # 例: "0"
# ↑は.controlから抽出することもできる
# 方法は後述するPGroongaの例を参照すること
set(VERSION_FULL "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

project("${PROJECT_ID}")

# ↓はユーザーが指定した「%PostgreSQLをインストールしたフォルダー%」になる
set(POSTGRESQL_DIR "${CMAKE_INSTALL_PREFIX}"
  CACHE PATH "PostgreSQL binary directory")
# ↓はパッケージを作成するときだけ必要
set(POSTGRESQL_VERSION "unknown"
  CACHE STRING "PostgreSQL version")

set(LIBRARY_NAME "lib${PROJECT_ID}")

set(EXTENSION_DIR "lib")
set(EXTENSION_DATA_DIR "share/extension")
set(DOCUMENT_DIR "share/${PROJECT_ID}")

set(SOURCES
  "ソースコード1.c"
  "ソースコード2.c"
  "...")
```
{% endraw %}

Visual Studioでの設定を再確認します。

  * DLLを作成する
  * 素のCとしてコンパイルする
  * C++の例外を無効にする
  * includeの検索パスに↓を追加する
    * `include\server\port\win32_msvc`
    * `include\server\port\win32`
    * `include\server`
    * `include`
  * ライブラリーの検索パスに↓を追加する
    * `lib`
  * `postgres.lib`をリンクする

順に設定します。まずは「DLLを作成する」です。`SHARED`とファイル名のベース名を拡張機能IDにすることがポイントです。

{% raw %}
```
add_library("${LIBRARY_NAME}" SHARED ${SOURCES})
set_target_properties("${LIBRARY_NAME}"
   PROPERTIES
   OUTPUT_NAME "${PROJECT_ID}")
```
{% endraw %}

次は「素のCとしてコンパイルする」ですが、これは特になにもする必要はありません。

続いて「C++の例外を無効にする」は`/EHsc`オプションを指定します。詳細は[MSDNの/EH（例外処理モデル）のドキュメント](https://msdn.microsoft.com/ja-jp/library/1deeycx5.aspx)を参考にしてください。

{% raw %}
```
set_source_files_properties(${SOURCES}
  PROPERTIES
  COMPILE_FLAGS "/EHsc")
```
{% endraw %}

「includeの検索パスを追加する」は次のようにします。

{% raw %}
```
include_directories(
  "${POSTGRESQL_DIR}/include/server/port/win32_msvc"
  "${POSTGRESQL_DIR}/include/server/port/win32"
  "${POSTGRESQL_DIR}/include/server"
  "${POSTGRESQL_DIR}/include")
```
{% endraw %}

「ライブラリーの検索パスを追加する」は次のようにします。

{% raw %}
```
link_directories(
  "${POSTGRESQL_DIR}/lib")
```
{% endraw %}

「`postgres.lib`をリンクする」は次のようにします。

{% raw %}
```
target_link_libraries("${LIBRARY_NAME}"
  "postgres.lib")
```
{% endraw %}

これで必要な設定が完了しました。簡単ですね。後はインストール先の指定とパッケージ作成用の設定だけです。

インストール先の指定は次のようになります。

{% raw %}
```
install(TARGETS "${LIBRARY_NAME}"
  DESTINATION "${EXTENSION_DIR}")

install(FILES
  "${PROJECT_SOURCE_DIR}/${PROJECT_ID}.control"
  DESTINATION "${EXTENSION_DATA_DIR}")

install(FILES
  "${PROJECT_SOURCE_DIR}/${PROJECT_ID}.sql"
  RENAME "${PGRN_PROJECT_ID}--${VERSION_FULL}.sql"
  DESTINATION "${PGRN_EXTENSION_DATA_DIR}")
```
{% endraw %}

パッケージ作成用の設定は次の通りです。すでに設定した値を使っているだけです。

{% raw %}
```
set(CPACK_GENERATOR "ZIP")
set(CPACK_INCLUDE_TOPLEVEL_DIRECTORY OFF)
set(CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${VERSION_PATCH}")
set(CPACK_PACKAGE_VENDOR "${VENDOR}")
if(CMAKE_CL_64)
  set(PACKAGE_SYSTEM_NAME "x64")
else()
  set(PACKAGE_SYSTEM_NAME "x86")
endif()
set(CPACK_PACKAGE_FILE_NAME
  "${PROJECT_ID}-${VERSION_FULL}-postgresql-${POSTGRESQL_VERSION}-${PACKAGE_SYSTEM_NAME}")

include(CPack)
```
{% endraw %}

まとめると次の通りです。

{% raw %}
```
cmake_minimum_required(VERSION 3.0.0)

set(PROJECT_NAME "拡張機能名") # 例: "PGroonga"
set(PROJECT_ID "拡張機能のID") # 例: "pgroonga"

set(VENDOR "拡張機能の開発者") # 例: "The PGroonga Project"

set(VERSION_MAJOR "拡張機能のメジャーバージョン") # 例: "0"
set(VERSION_MINOR "拡張機能のマイナーバージョン") # 例: "5"
set(VERSION_PATCH "拡張機能のパッチバージョン")   # 例: "0"
# ↑は.controlから抽出することもできる
# 方法は後述するPGroongaの例を参照すること
set(VERSION_FULL "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

project("${PROJECT_ID}")

# ↓はユーザーが指定した「%PostgreSQLをインストールしたフォルダー%」になる
set(POSTGRESQL_DIR "${CMAKE_INSTALL_PREFIX}"
  CACHE PATH "PostgreSQL binary directory")
# ↓はパッケージを作成するときだけ必要
set(POSTGRESQL_VERSION "unknown"
  CACHE STRING "PostgreSQL version")

set(LIBRARY_NAME "lib${PROJECT_ID}")

set(EXTENSION_DIR "lib")
set(EXTENSION_DATA_DIR "share/extension")
set(DOCUMENT_DIR "share/${PROJECT_ID}")

set(SOURCES
  "ソースコード1.c"
  "ソースコード2.c"
  "...")

add_library("${LIBRARY_NAME}" SHARED ${SOURCES})
set_target_properties("${LIBRARY_NAME}"
   PROPERTIES
   OUTPUT_NAME "${PROJECT_ID}")

set_source_files_properties(${SOURCES}
  PROPERTIES
  COMPILE_FLAGS "/EHsc")

include_directories(
  "${POSTGRESQL_DIR}/include/server/port/win32_msvc"
  "${POSTGRESQL_DIR}/include/server/port/win32"
  "${POSTGRESQL_DIR}/include/server"
  "${POSTGRESQL_DIR}/include")

link_directories(
  "${POSTGRESQL_DIR}/lib")

target_link_libraries("${LIBRARY_NAME}"
  "postgres.lib")

install(TARGETS "${LIBRARY_NAME}"
  DESTINATION "${EXTENSION_DIR}")

install(FILES
  "${PROJECT_SOURCE_DIR}/${PROJECT_ID}.control"
  DESTINATION "${EXTENSION_DATA_DIR}")

install(FILES
  "${PROJECT_SOURCE_DIR}/${PROJECT_ID}.sql"
  RENAME "${PGRN_PROJECT_ID}--${VERSION_FULL}.sql"
  DESTINATION "${PGRN_EXTENSION_DATA_DIR}")

set(CPACK_GENERATOR "ZIP")
set(CPACK_INCLUDE_TOPLEVEL_DIRECTORY OFF)
set(CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${VERSION_PATCH}")
set(CPACK_PACKAGE_VENDOR "${VENDOR}")
if(CMAKE_CL_64)
  set(PACKAGE_SYSTEM_NAME "x64")
else()
  set(PACKAGE_SYSTEM_NAME "x86")
endif()
set(CPACK_PACKAGE_FILE_NAME
  "${PROJECT_ID}-${VERSION_FULL}-postgresql-${POSTGRESQL_VERSION}-${PACKAGE_SYSTEM_NAME}")

include(CPack)
```
{% endraw %}

実際に使われているCMakeの設定ファイルは[PGroongaのCMakeLists.txt](https://github.com/pgroonga/pgroonga/blob/master/CMakeLists.txt)を参考にするとよいでしょう。

また、実際に作成したパッケージは[http://packages.groonga.org/windows/pgroonga/](http://packages.groonga.org/windows/pgroonga/)以下にあります。こちらもあわせて参考にしてください。

### まとめ

公式のWindows版PostgreSQLパッケージ用の拡張機能のビルドシステムの作り方を紹介しました。ここで紹介した方法はCMakeを使っています。Visual StudioでビルドしなければいけないときにCMakeは便利なので、そんなときはCMakeを思い出してみてください。

[^0]: 拡張機能のメタデータを書いた`.control`ファイルと拡張機能のインストール方法を書いた`.sql`ファイル。

[^1]: なんの略かわかりませんが、PostgreSQL Extension Systemの略な気はします。

[^2]: ただし、`.deb`や`.rpm`などパッケージを作る仕組みは提供していません。それらは自分で作る必要があります。[PGroongaが使っている仕組み](https://github.com/pgroonga/pgroonga/tree/master/packages)が参考になるかもしれません。

[^3]: EnterpriseDBが提供していますが、これは公式のパッケージなんですよ…ね？

[^4]: PGXSの中身を見ると使えるように見えます。

[^5]: ソースを見ると使えるように見えます。

[^6]: PostgreSQL自体のビルドシステムもこのアプローチのように見えます。
