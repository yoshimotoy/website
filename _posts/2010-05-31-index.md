---
tags:
- ruby
title: 日本Ruby会議2010スポンサーと発表・企画のお知らせ
---
今年も夏に[日本Ruby会議](http://rubykaigi.org/)が開催されますが、昨年に引き続き[今年もスポンサーになりました](http://rubykaigi.org/2010/ja/SponsorsGold#clearcode)。
<!--more-->


[日本Ruby会議2010のトップページ](http://rubykaigi.org/2010/ja)で微妙に公開されていますが、[るりまサーチ](http://rurema.clear-code.com/)について発表する予定です。

いくつかの企画にも参加します。今のところ、[るびまでActiveLdapの記事](http://jp.rubyist.net/magazine/?0027-ActiveLdap)などを書いている[高瀬さん](http://d.hatena.ne.jp/tashen/)のLDAPに関する企画と[Ruby 1.9コミッタQ&A](http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-dev/41400)に参加する予定です[^0]。

日本Ruby会議2010でのクリアコード関連情報のお知らせでした。

Ruby関連といえば、最近、クリアコードも[Asakusa.rb](http://qwik.jp/asakusarb/)デビューしました。

[^0]: どちらかというと企画側で。
