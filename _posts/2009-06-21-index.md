---
tags:
- milter-manager
- ruby
title: オープンソースカンファレンス2009 Hokkaido 北海道情報セキュリティ勉強会枠での資料公開
---
まっちゃだいふくさんに声をかけてもらったことがきっかけで、[オープンソースカンファレンス2009 Hokkaidoのせきゅぽろ枠](http://www.ospn.jp/osc2009-do/modules/eguide/event.php?eid=12)でmilter managerの話をしてきました。声をかけてくれたまっちゃだいふくさん、参加してくれたみなさん、ありがとうございました。
<!--more-->


資料: [milter manager](/archives/osc2009-do/)

また、[Ruby札幌](http://ruby-sapporo.org/)がUstreamで配信してくれたので、動画もあります。

動画: [OSC 2009 Hokkaido milter manager](http://www.ustream.tv/recorded/1685447)

### 内容

今回はmilterとmilter managerの話をする前に、迷惑メール対策の現状と有効な対策方法についても話しました。これは、[第2回静岡ITPro勉強会](http://shizuoka-itpro.techtalk.jp/news/2ndworkshop)での[佐藤さんの公演内容](http://d.hatena.ne.jp/stealthinu/20090615/p1)を参考資料として利用しています。利用を快諾してくれた佐藤さんありがとうございます。札幌のみなさんにも迷惑メール対策の現状と有効な対策方法を伝えられたのではないかと思います。

一般的な迷惑メール対策の話の後にmilterとmilter managerの話をしました。[第2回静岡ITPro勉強会の時]({% post_url 2009-06-16-index %})よりも時間が少ないということもあり、今回はあまり突っ込んだ話をせずに、雰囲気が伝わる程度に抑えました。

話の後、司会をしてくれたまっちゃだいふくさんが今回省略したあたりをフォローしてくれました。ありがとうございます。

まっちゃだいふくさんは勉強会の時間外に、いろいろアドバイスをしてくれます。そのため、発表者として参加したこちらもとても勉強になっています。

そして、[まっちゃだいふくさんがもってきてくれたお菓子](http://d.hatena.ne.jp/ripjyr/20090621/1245539078)はとてもおいしかったです。

### Ruby札幌

せきゅぽろ枠の時間以外はRuby札幌にお世話になりました。

Ruby札幌とせきゅぷろの枠はセミナーに参加したのですが、それ以外の時間はRuby札幌のブースにおじゃまさせてもらいました。[daraさん](http://friendfeed.com/dara)から[buzztter](http://buzztter.com/)のバックエンドをgroongaにしたいということを聞いたので、[Ruby/groonga](http://groonga.rubyforge.org/)で実現するために少し相談しました。Ruby/groonga 0.0.3はbuzztterのバックエンドとして使える機能を提供することになるでしょう。

今回、[ActiveLdap](http://rubyforge.org/projects/ruby-activeldap/)を使っている[島田さん](http://iddy.jp/profile/snoozer05/)以外のRuby札幌の人たちとも話すことができたのはよかったです。ActiveLdapやRSS Makerあたりのレビューにも参加することができました。

今回、Ruby札幌の人たちはとても人柄がよいことがわかりました。とてもすばらしいです。また、[Ruby札幌がRabbitを応援している](http://www.ustream.tv/recorded/1685266)こともすばらしいです。

### まとめ

オープンソースカンファレンス2009 Hokkaidoのせきゅぷろ枠でmilter managerの話をしてきました。この話は、まっちゃだいふくさんのおかげで実現しました。話の後のフォローなどいろいろありがとうございます。

Ruby札幌はすばらしいです。Ruby会議2009ではニュースがあるようですし、その後には札幌Ruby会議02もあるようです。Ruby札幌から目が離せません。
