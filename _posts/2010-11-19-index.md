---
tags: []
title: Ubuntu PCをルータ代わりにして、新しくLANを構築してみる
---
以下のような構成のLANを手早く作りたい、という場面が時々あります。
<!--more-->


{% raw %}
```
既存のLAN（192.168.1.0/24）
   ↑
 ルータ1→新しく作るLAN 1（192.168.200.0/24）
   ↓
 ルータ2→新しく作るLAN 2（192.168.201.0/24）
```
{% endraw %}

クリアコードはOSSの導入サポートも行っていますので、クリーンな検証用環境としてこのような構成のネットワークを新たに用意して作業をする、という事がよくあります。今回は、新たなネットワーク機器を購入せずに普通のPCを組み合わせてこのようなネットワークを構築する方法を紹介したいと思います。

### 前提

現時点では既存のLANのみがあり、そこに新たに2つのLANを別々に構築して冒頭のような構成にしたい、という状況にあるとします。

  * 既存のLANはルータを介して既にインターネットに繋がっている。
  * 新しく作る2つのLANからもインターネットに繋がるようにしたい。
  * 本格的に長期に渡って運用するつもりはなくて、あくまで、2つのLANにまたがる作業の実験・検証用のネットワークとして使いたい。
  * 既存LANのルータの設定はいじりたくない。

実運用するネットワークであれば専用にルータ機器を導入するところですが、単に実験・検証用の環境にするだけなら、既存の機材でやりくりして同様の環境をエミュレートできれば十分な場合がほとんどです。

そこで今回は、*Ubuntu 10.10がインストールされていて既存のLANに接続されているPC 1台を適切に設定して、新しいLAN2つと既存のLANの間で動作するルータ兼サーバとして動作させる*ことによって、冒頭のような構成のネットワークを構築しようと思います。

#### 必要な作業

今回の作業は4つのステップに分ける事ができます。

  1. LAN用のインターフェースを3個持っていて既存のLANに既に繋がっているPC 1台と、新しいPC 2台を使って、新しいLANを2つ構築する。

  1. 既存のLANに繋がっているPCをDHCPサーバにする。これによって、新しいLANに所属する事になる各コンピュータのIPアドレスを自動的に決めさせる事ができる（いちいちIPアドレスを自分で決めなくてもよくなる）。

  1. 既存のLANに繋がっているPCにルータの働きをさせる。これによって、新しいLANからもインターネットに繋がるようになる。

  1. 既存のLANに繋がっているPCをDNSにする。これによって、新しいLANに所属するPCが「www.example.com」のようなホスト名を名前解決できるようになる（Webブラウザ等を普通に使えるようになる）。


#### 用意したもの

  * 既にインターネットに繋がっている、構築済みのLAN 1つ
  * Ubuntu 10.10をインストール済みのPC 1台
    * 無線LANインターフェース（wlan0）と有線LAN用インターフェース（eth0）の2つを持っている。
    * 既存LANには無線LANで接続している。
  * USB接続のLANアダプタ
    * Ubuntu PCにもう1つLAN用インターフェース（eth1）を加えるために使う。
  * CentOS 5.5をインストール済みのPC 2台（せんとくん1号、せんとくん2号）
    * 新しく構築するLANに所属する予定のコンピュータ。
    * OSはUbuntuでもWindowsでも構わない。今回はたまたまCentOSというだけ。
    * CentOSはGnomeも入れてデスクトップ環境で操作できるようにしてある。
    * どちらも有線LAN用インターフェース（eth0）を持っている。

#### できる予定のネットワークの構成

冒頭の構成に基づいて、最終的に以下のようなネットワークにしたいと思います。

{% raw %}
```
192.168.1.0/24
      ↑
     wlan0
      ↑
  [Ubuntu PC]→eth0→192.168.200.0/24→[せんとくん1号]
           └→eth1→192.168.201.0/24→[せんとくん2号]
```
{% endraw %}

### ネットワークの基礎知識を押さえておこう

#### 「192.168.1.0/24」って何？　どういう意味？

先の図では特に説明もせず「192.168.1.0/24」という風な表記を使いましたが、この表記が何を意味しているか分かりますか？（分かる場合はこの節を読み飛ばして構いません。）

「192.168.1.0/24」は、そこに接続しているコンピュータのIPアドレスの範囲が 192.168.1.1〜192.168.1.254 であるコンピュータ同士のネットワーク（LAN）を指しています。

IPv4の世界では、ネットワークに接続する全てのコンピュータに一意なアドレスが与えられ、そのアドレスで個々のコンピュータを識別します。これが「IPアドレス」です。IPv4ではIPアドレスは「0〜255の数字がドット区切りで4つ並んだ数列」で表現されていて、例えば「127.0.0.1」や「192.168.1.1」や「66.249.89.104」といったものもIPアドレスです。

このIPアドレスを2進数で表すと、例えば

  * 192.168.1.0
  * 192.168.1.255
  * 66.249.89.104

というアドレスはそれぞれ

  * 11000000.10101000.00000001.00000000
  * 11000000.10101000.00000001.11111111
  * 01000010.11111001.01011001.01101000

になります。

この時、上の2つのアドレスは先頭から24桁目までが一致していますよね。このような場合に*「24桁目までが一致しているアドレスの範囲を1つのLANとして扱う」という意味の表記が、前出の「/24」です*。

ですから、「192.168.1.0/24」のLANならIPアドレスの範囲は192.168.1.0〜192.168.1.255の256個ですし、「172.16.100.0/24」のLANならIPアドレスの範囲は172.16.100.0〜172.16.100.255の256個ということになります。また、24桁ではなく16桁までが一致している範囲を1つのLANとして扱うのなら、「192.168.0.0/16」という表記で192.168.0.0〜192.168.255.255までの65536個のIPアドレスの範囲が1つのLANになります。

さて、「頭から24桁目までが一緒であればそれを1つのLANとする」というルールを、数字でどうやって表せばよいでしょうか。そのまま「/24」と書く事もできますが、別の表現方法もあります。例えば「2進数で値が1になっている部分が一緒であればそれを1つのLANとする」という取り決めにすれば、2進数で「11111111.11111111.11111111.00000000」と書くことになります。これを10進数に直すと「255.255.255.0」になり、このように、ネットワークの範囲を決定する情報をIPアドレスと同じ形式で表記した物を、*サブネットマスク*と呼びます。

以上の事から、世間でたまに見かける

  * 192.168.1.0/24
  * 192.168.1.0/255.255.255.0
  * ネットワークアドレス 192.168.1.0、サブネットマスク255.255.255.0

という風な表記は全て、実際には同じ事を言い表していると分かります。

なお、「192.168.1.0/24」というLANなら192.168.1.0から192.168.1.255までの256個のアドレスが存在することになりますが、*その範囲の最初のアドレスである「192.168.1.0」*はそのネットワーク自体を表すアドレスとして使われて、*最後のアドレスである「192.168.1.255」*は「そのアドレス宛に送られたメッセージは、ネットワークに属している全てのコンピュータが受け取る」という「ブロードキャストアドレス」として使われます。なので、*実際にそのLANでコンピュータに割り振れるアドレス*（LANに所属できるコンピュータの最大数）は、192.168.1.1から192.168.1.254までの254個ということになります。

#### 1台のコンピュータが複数のIPアドレスを持つ事もある

IPアドレスはネットワーク上にあるコンピュータを識別するためのアドレスで、普通にLANに接続する場合ならコンピュータ1台につきIPアドレスは1つというのが一般的ですが、場合によっては1台のコンピュータが複数のIPアドレスを持つ場合もあります。ルータはその代表例です。

2つのLANにまたがって存在するルータ（あるいはルータの代わりをするPC）は、それぞれのLAN用のIPアドレスを持つ事になります。無線LANのインターフェース（wlan0）で192.168.1.0/24の方に繋がっていて、有線LANのインターフェース（eth0）で新しいLAN（192.168.200.0/24）の方に繋がっているのであれば、

  * wlan0
    * 接続先のネットワーク：192.168.1.0/24
    * 自分のIPアドレス：192.168.1.100
  * eth0
    * 接続先のネットワーク：192.168.200.0/24
    * 自分のIPアドレス：192.168.200.100

といった要領です。

#### 目標の再確認

以上を踏まえると、先程構築したいネットワークの構成として挙げた

{% raw %}
```
既存のLAN（192.168.1.0/24）
   ↑
 ルータ1→新しく作るLAN 1（192.168.200.0/24）
   ↓
 ルータ2→新しく作るLAN 2（192.168.201.0/24）
```
{% endraw %}

は、

{% raw %}
```
既存のLAN（192.168.1.0/24）
（192.168.1.0のネットワークで、192.168.1.1〜192.168.1.254の
  254台のコンピュータが存在しうる）
   ↑
 ルータ1→新しく作るLAN 1（192.168.200.0のネットワークで、
   │                     192.168.200.1〜192.168.200.254の254台の
   │                     コンピュータが存在しうる）
   ↓
 ルータ2→新しく作るLAN 2（192.168.201.0のネットワークで、
                         192.168.201.1〜192.168.201.254の254台の
                         コンピュータが存在しうる）
```
{% endraw %}

という意味だということが分かります。

今回はUbuntu PC1台でルータ1とルータ2を兼用するので、それぞれのLANに接続するインターフェースを考慮に入れると

{% raw %}
```
192.168.1.0/24
      ↑
     wlan0（IPアドレス：192.168.1.100）
      ↑
  [Ubuntu PC]→ルータ1用に使うeth0（IPアドレス：192.168.200.254）
           │    └→192.168.200.0/24
           │
           └→ルータ2用に使うeth1（IPアドレス：192.168.201.254）
                 └→192.168.201.0/24
```
{% endraw %}

という構成になります。

### 1. LANを構築する

それでは実際の作業としてまず、Ubuntu PCとせんとくん1号2号の間でLANを組む所から入りましょう。

#### Ubuntu PCに固定IPを割り当てる

普通に構築済みのLANでクライアントとしてUbuntuを使用する場合、IPアドレスはそのLANのDHCPサーバによって割り当てられた適当な物が使われます。しかし、今回構築するLANではこのUbuntu PC自身をサーバ兼ルータとして使用することになりますので、IPアドレスは新LAN用の物を固定で割り当てておかないといけません。

固定IPの割り当てはUbuntuのネットワークマネージャでも可能ですが、後の項でネットワークマネージャではできない設定を施す必要がありますので、今回はより低レベルな設定用のインターフェースとして、ネットワーク接続の設定ファイル /etc/network/interfaces を直接編集することにします。

これから施す設定は、

  * eth0に固定のIPアドレス 192.168.200.254 を割り当てる
    * 192.168.200.0/24系のネットワークであると設定する
  * eth1に固定のIPアドレス 192.168.201.254 を割り当てる
    * 192.168.201.0/24系のネットワークであると設定する

というものです。

/etc/network/interfaces は、使用したUbuntu PCの元の状態では以下のようになっていました。

{% raw %}
```diff
auto lo
iface lo inet loopback

auto wlan0
```
{% endraw %}

ここに、以下のように追記します。（行頭に「+」がある行が、追加された部分です。「+」そのものは実際のファイルには書かないで下さい。）

{% raw %}
```diff
  auto lo
  iface lo inet loopback

  auto wlan0

+ auto eth0
+ iface eth0 inet static
+   address 192.168.200.254
+   netmask 255.255.255.0
+   broadcast 192.168.200.255
+
+ auto eth1
+ iface eth1 inet static
+   address 192.168.201.254
+   netmask 255.255.255.0
+   broadcast 192.168.201.255
```
{% endraw %}

編集結果を保存したら、この設定を有効にするために、一旦ネットワークマネージャを再起動しましょう。ここでネットワークマネージャを再起動しておかないと、Ubuntuのネットワークマネージャがeth0とeth1を勝手に違う設定で使おうとするために、設定が意図通りに反映されないという事が起こるので、注意して下さい。

{% raw %}
```
$ sudo /etc/init.d/network-manager restart
```
{% endraw %}

/etc/ini.d/network-manager の位置に起動スクリプトが見つからない場合は、Ubuntu PCを再起動してもよいです。

（2014年7月16日追記。Ubuntu 14.04LTS以降の場合は、serviceコマンドを使って以下のようにします：

{% raw %}
```
$ sudo service network-manager restart
```
{% endraw %}

）

##### 確認

固定IPをきちんと割り当てられているかどうかは、Gnome端末でifconfigを実行して確かめられます。

{% raw %}
```
$ ifconfig
eth0      Link encap:イーサネット  ハードウェアアドレス xx:xx:xx:xx:xx:xx  
          inetアドレス:192.168.200.254  ブロードキャスト:192.168.200.255  マスク:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  メトリック:1
          RXパケット:0 エラー:0 損失:0 オーバラン:0 フレーム:0
          TXパケット:3 エラー:0 損失:0 オーバラン:0 キャリア:0
          衝突(Collisions):0 TXキュー長:1000 
          RXバイト:0 (0.0 B)  TXバイト:703 (703.0 B)
          割り込み:16 

eth1      Link encap:イーサネット  ハードウェアアドレス xx:xx:xx:xx:xx:xx  
          inetアドレス:192.168.201.254  ブロードキャスト:192.168.201.255  マスク:255.255.255.0
          UP BROADCAST MULTICAST  MTU:1500  メトリック:1
          RXパケット:0 エラー:0 損失:0 オーバラン:0 フレーム:0
          TXパケット:1 エラー:0 損失:0 オーバラン:0 キャリア:0
          衝突(Collisions):0 TXキュー長:1000 
          RXバイト:0 (0.0 B)  TXバイト:54 (54.0 B)

lo        Link encap:ローカルループバック  
...
```
{% endraw %}

この例のように、「inetアドレス」「ブロードキャスト」「マスク」の箇所に自分で設定したIPアドレスが表示されていれば、固定IPの割り当ては成功です。

##### うまくいかない場合

  * ミスタイプはないか？
  * Ubuntu PCを再起動したか？（再起動しないと、上記の設定がUbuntuのネットワークマネージャとインターフェースを取り合ってしまい、設定が有効にならないことがある。）

#### せんとくん1号・2号にも固定IPを割り当てる

こちらはGUIでやることにします。CentOSでGnomeをデスクトップ環境として利用している前提で、Gnomeの場合の手順を示します。

  1. せんとくん1号のGnomeのメニューを「システム」→「管理」→「ネットワーク」と辿ってネットワーク設定のダイアログを開く。

  1. デバイス「eth0」の項目をダブルクリックして、デバイスの詳細を表示する。

  1. 「固定のIPアドレス設定」を選択して、以下のように入力する。

       * アドレス：192.168.200.100（せんとくん自身にこのアドレスを固定で割り当てる。）
       * サブネットマスク：255.255.255.0
       * デフォルトゲートウェイアドレス：192.168.200.254（ルータのIPアドレス。ここではUbuntu PCのIPアドレスを指定しておく。）
  1. OKボタンを押下してダイアログを閉じる。

  1. 「ファイル」メニューから「保存」を選択して、設定の変更を保存する。

  1. eth0が起動中だったら「停止」ボタンを押下して停止させて、「起動」ボタンで再起動する。eth0が休止中の場合はそのまま「起動」ボタンを押下する。


せんとくん2号の場合は、自身のIPアドレスを192.168.201.100に、デフォルトゲートウェイは192.168.201.254にする点が異なります。

##### 確認

固定IPをきちんと割り当てられているかどうかは、Ubuntu PCの場合と同様に、Gnome端末でifconfigを実行して確かめられます。ただしCentOSの場合、初期状態ではsbinにパスが通っていないので 、<kbd>/sbin/ifconfig</kbd> とフルパスで書く必要があります。

#### 各コンピュータを接続する

ここまでできたら、Ubuntu PCのeth0とせんとくん1号のeth0を*クロスケーブルで*繋ぎます。（ストレートケーブルしか無ければ、間にスイッチングハブを挟むと同じ結果になります。）せんとくん2号のeth0はUbuntu PCのeth1に繋ぎます。

これで、*Ubuntu PCとせんとくん1号の間で192.168.200.0/24のLANが、Ubuntu PCとせんとくん2号の間で192.168.201.0/255のLANが構築されました*。

この時点では、ネットワークの構成は以下のようになっています。

{% raw %}
```
192.168.1.0/24
      ↑
     wlan0（192.168.1.100）
      ↑
  [Ubuntu PC]→eth0（192.168.200.254）
           │    └→192.168.200.0/24→（192.168.200.100）eth0[せんとくん1号]
           │
           └→eth1（192.168.201.254）
                 └→192.168.201.0/24→（192.168.201.100）eth0[せんとくん2号]
```
{% endraw %}

##### 確認

LANを構築できたかどうかは、以下のようにすれば確認できます。

  * Ubuntu PCからせんとくん1号宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.200.100
```
{% endraw %}
  レスポンスが返ってくれば成功。
  * Ubuntu PCからせんとくん2号宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.201.100
```
{% endraw %}
  レスポンスが返ってくれば成功。
  * せんとくん1号からUbuntu PC宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.200.254
```
{% endraw %}
  レスポンスが返ってくれば成功。
  * せんとくん2号からUbuntu PC宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.201.254
```
{% endraw %}
  レスポンスが返ってくれば成功。

##### うまくいかない場合

  * 物理的に接続できているか？
    * LANケーブルはちゃんと挿さっているか？
    * クロスケーブルで繋いでいるか？（ストレートケーブルでPC同士を接続すると通信できない場合がある。）
    * ストレートケーブルの接続でスイッチングハブを経由しているなら、ハブの電源は入っているか？　ハブのパイロットランプ（そのポートの先にコンピュータが繋がっている時に点灯する）は点いているか？
    * せんとくん1号がUbuntu PCのeth1に、せんとくん2号がUbuntu PCのeth0に挿さっていないか？（固定IPなので、間違ったポートに挿していると、せんとくん1号2号側から通信できない。）
  * それぞれのPCはpingに対して応答を返すようになっているか？（セキュリティのためpingに応答を返さないよう設定されている場合がある。）

### 2. Ubuntu PCをDHCPサーバにする

現状では、新しく作ったLANにコンピュータを追加する時に、手動でIPアドレスやDNS等を設定する必要があります。これを自動化するために、Ubuntu PCをDHCPサーバとして動作させましょう。

#### DHCPサーバを立てる

まずUbuntu PCにDHCPサーバをインストールします。

{% raw %}
```
$ sudo aptitude install dhcp3-server
```
{% endraw %}

次に、DHCPサーバの設定を作成します。/etc/dhcp3/dhcpd.conf というファイルの末尾に、以下のような設定を書き加えます。
（2014年7月16日追記。Ubuntu 14.04LTS以降の場合は、 /etc/dhcp/dhcpd.conf を編集します。）

{% raw %}
```diff
# 192.168.200.0/24のLAN用の設定
subnet 192.168.200.0 netmask 255.255.255.0 {
  # IPアドレスの割り当て範囲は192.168.200.2〜192.168.200.20
  range dynamic-bootp 192.168.200.2 192.168.200.20;
  # デフォルトゲートウェイ（今回はUbuntu PC自身）
  option routers 192.168.200.254;
  # サブネットマスク
  option subnet-mask 255.255.255.0;
  # ブロードキャスト用のアドレス
  option broadcast-address 192.168.200.255;
  default-lease-time 6000;
  max-lease-time 72000;
}

# 192.168.201.0/24のLAN用の設定
subnet 192.168.201.0 netmask 255.255.255.0 {
  # IPアドレスの割り当て範囲は192.168.201.2〜192.168.201.20
  range dynamic-bootp 192.168.201.2 192.168.201.20;
  # デフォルトゲートウェイ（今回はUbuntu PC自身）
  option routers 192.168.201.254;
  # サブネットマスク
  option subnet-mask 255.255.255.0;
  # ブロードキャスト用のアドレス
  option broadcast-address 192.168.201.255;
  default-lease-time 6000;
  max-lease-time 72000;
}
```
{% endraw %}

変更を保存したら、DHCPサーバを再起動して設定を反映させます。

{% raw %}
```
$ sudo /etc/init.d/dhcp3-server restart
```
{% endraw %}

（2012年4月25日追記。Ubuntu 11.10以降の場合は、DHCPサーバの起動スクリプトのパスが変わったため、DHCPサーバの再起動の際は以下のコマンドを使用します：

{% raw %}
```
$ sudo /etc/init.d/isc−dhcp-server restart
```
{% endraw %}

）

また、DHCPサーバはプロセスが起動した後で有効化されたLANインターフェースを認識しませんので、/etc/network/interfaces にDHCPサーバの自動再起動のためのコマンドも追記しておくとよいでしょう。

{% raw %}
```diff
  auto eth0
  iface eth0 inet static
    address 192.168.200.254
    netmask 255.255.255.0
    broadcast 192.168.200.255
+   pre-up /etc/init.d/dhcp3-server stop
+   post-up /etc/init.d/dhcp3-server start

  auto eth1
  iface eth1 inet static
    address 192.168.201.254
    netmask 255.255.255.0
    broadcast 192.168.201.255
+   pre-up /etc/init.d/dhcp3-server stop
+   post-up /etc/init.d/dhcp3-server start
```
{% endraw %}

ここでは、そのインターフェースを有効化する前に実行するコマンドをpre-upで、そのインターフェースを有効化した後に実行するコマンドをpost-upで指定しています。

（2012年4月25日追記。こちらも、Ubuntu 11.10の場合はDHCPサーバの起動スクリプトのパスを以下のように書き換える必要があります。

{% raw %}
```diff
  auto eth0
  iface eth0 inet static
    address 192.168.200.254
    netmask 255.255.255.0
    broadcast 192.168.200.255
+   pre-up /etc/init.d/isc−dhcp-server stop
+   post-up /etc/init.d/isc−dhcp-server start

  auto eth1
  iface eth1 inet static
    address 192.168.201.254
    netmask 255.255.255.0
    broadcast 192.168.201.255
+   pre-up /etc/init.d/isc−dhcp-server stop
+   post-up /etc/init.d/isc−dhcp-server start
```
{% endraw %}

）

これで、*Ubuntu PCがDHCPサーバになりました*。

#### 固定IPの割り当てをやめる

DHCPサーバ自身となっているUbuntu PCは固定IPのままにしておく必要がありますが、それ以外の新しいLANに参加するクライアントは、DHCPサーバによる動的なIPアドレスの割り当てを利用できます。せんとくん1号2号の設定を元に戻して、固定IPの割り当てを解除しておきましょう。

  1. せんとくん1号のGnomeのメニューを「システム」→「管理」→「ネットワーク」と辿ってネットワーク設定のダイアログを開く。

  1. デバイス「eth0」の項目をダブルクリックして、デバイスの詳細を表示する。

  1. 「自動的にIPアドレス設定を取得」チェックを入れて「dhcp」を選択し、OKボタンを押下してダイアログを閉じる。

  1. 「ファイル」メニューから「保存」を選択して、設定の変更を保存する。

  1. 「停止」ボタンを押下してeth0を停止させて、「起動」ボタンで再起動する。


せんとくん2号も同じ手順で設定できます。

インターフェースを再起動した段階で、ネットワークの構成はこのようになっているはずです（せんとくん1号2号のIPアドレスが、固定で割り当てた物ではなく、DHCPで割り当てられた物になります）。

{% raw %}
```
192.168.1.0/24
      ↑
     wlan0（192.168.1.100）
      ↑
  [Ubuntu PC]→eth0（192.168.200.254）
           │    └→192.168.200.0/24→（192.168.200.2）eth0[せんとくん1号]
           │
           └→eth1（192.168.201.254）
                 └→192.168.201.0/24→（192.168.201.2）eth0[せんとくん2号]
```
{% endraw %}

#### 確認

DHCPによるIPアドレスの自動割り当てが正常に機能しているかどうかは、せんとくん1号2号のGnome端末でifconfigを実行して確かめられます。DHCPの割り当て範囲に設定している範囲のアドレスのうち最初のアドレス（せんとくん1号は192.168.200.2、2号は192.168.201.2）がせんとくん1号2号のeth0に割り当てられていれば、DHCPサーバが正常に動作していると分かります。

#### うまくいかない場合

  * せんとくん1号2号側でIP情報の取得に失敗する場合、
    * Ubuntu PCのDHCPサーバは動作しているか？
    * DHCPサーバの設定が間違っていないか？
    * DHCPサーバを起動した後でUbuntu PCのeth0やeth1を有効化しなかったか？（その場合、DHCPサーバを再起動してみる。）

### 3. Ubuntu PCをルータにする

現状では、せんとくん1号2号から既存LAN上にあるコンピュータにはpingが通りませんし、せんとくん1号と2号の間でもpingが通りません。これは、3つのLANが物理的には繋がっていても、その接点になっているUbuntu PCが通信を遮断しているためです。Ubuntu PCがルータとして機能していれば、この状態は解消されます。

Ubuntu PCをルータとして働かせるというのは、

  * 192.168.200.0/24や192.168.201.0/24に属しているコンピュータから送信されたパケットを中継して、既存のLANやインターネットに送る。
  * そのポケットに対するレスポンスとして返ってきたパケットを中継して、パケット送信元のコンピュータに送る。

といった働きをさせるということです。これによって、異なるLANにあるコンピュータ同士がお互いに通信できるようになります。

ただ、Ubuntu PCが新しいLANから受け取ったパケットをそのまま既存LANのルータに渡すようになっても、新しいLANのコンピュータはインターネットには接続できません。これは、既存LANのルータが新しく作られた192.168.200.0/24や192.168.201.0/24のLANの存在を知らないせいです。そのため、インターネットから返ってきたレスポンスのパケットに「192.168.200.10宛」と書かれていても、既存LANのルータは「192.168.200.0/24のLANなんて知らないよ」とパケットの配送を拒否してしまいます。

この問題を解消するには、

  * Ubuntu PCをルータとして動作させた上で、既存LANのルータの設定も変更して、192.168.200.0/24宛のパケットを既存LANのルータからルーティングできるようにする。
  * Ubuntu PCをルータとして動作させた上で、NAT（IPマスカレード）機能を使うようにする。

という2つの解決策が考えられます。今回は既存のルータの設定はいじらないで済ませたいので、後者の方法を使うことにします。

NAT（IPマスカレード）を設定すると、

  * Ubuntu PCは、192.168.200.0/24や192.168.201.0/24からのパケットを受け取ると、自分（192.168.1.100）から送信されたパケットであるという風にパケット送信元の情報を書き換えて、既存LANのルータに転送する。
  * インターネットから返ってきたパケットをUbuntu PCが受け取ると、パケットの送信先IPアドレスを実際の送信元である192.168.200.0/24や192.168.201.0/24のコンピュータのIPアドレスに書き変えて、実際の送信元に転送する。

という動作が行われるようになります。（大抵の場合、NTT等からレンタルされているルータも、インターネットとLANの間でこれと同じ事をしています。）

Ubuntu PCをルータとして動作させる設定は、先程も編集した /etc/network/interfaces に記述します。

{% raw %}
```diff
  auto eth0
  iface eth0 inet static
    address 192.168.200.254
    netmask 255.255.255.0
    broadcast 192.168.200.255
+   pre-up /sbin/sysctl net.ipv4.conf.all.forwarding=1
+   pre-up /sbin/iptables -t nat -A POSTROUTING -s 192.168.200.0/24 -j MASQUERADE
    pre-up /etc/init.d/dhcp3-server stop # Ubuntu 10.10の場合。Ubuntu 11.10以降では
                                         # pre-up /etc/init.d/isc-dhcp-server stop
                                         # とする。
    post-up /etc/init.d/dhcp3-server start # Ubuntu 10.10の場合。Ubuntu 11.10以降では
                                           # post-up /etc/init.d/isc-dhcp-server start
                                           # とする。
+   post-down /sbin/iptables -t nat -D POSTROUTING -s 192.168.200.0/24 -j MASQUERADE
+   post-down /sbin/sysctl net.ipv4.conf.all.forwarding=0

  auto eth1
  iface eth1 inet static
    address 192.168.201.254
    netmask 255.255.255.0
    broadcast 192.168.201.255
+   pre-up /sbin/sysctl net.ipv4.conf.all.forwarding=1
+   pre-up /sbin/iptables -t nat -A POSTROUTING -s 192.168.201.0/24 -j MASQUERADE
    pre-up /etc/init.d/dhcp3-server stop # Ubuntu 10.10の場合。Ubuntu 11.10以降では
                                         # pre-up /etc/init.d/isc-dhcp-server stop
                                         # とする。
    post-up /etc/init.d/dhcp3-server start # Ubuntu 10.10の場合。Ubuntu 11.10以降では
                                           # post-up /etc/init.d/isc-dhcp-server start
                                           # とする。
+   post-down /sbin/iptables -t nat -D POSTROUTING -s 192.168.201.0/24 -j MASQUERADE
+   post-down /sbin/sysctl net.ipv4.conf.all.forwarding=0
```
{% endraw %}

ここではpre-upに加えて、そのインターフェースを停止した後に実行するコマンドをpost-downで指定しています。追加した行では以下のようなコマンドを実行しています。

  * <kbd>/sbin/sysctl net.ipv4.conf.all.forwarding=1</kbd> は、初期状態で無効化されているルータ機能を有効にするコマンド。
  * 停止時はその逆の <kbd>/sbin/sysctl net.ipv4.conf.all.forwarding=0</kbd> を実行する。
  * <kbd>/sbin/iptables -t nat -A POSTROUTING -s 192.168.200.0/24 -j MASQUERADE</kbd> は、192.168.200.0/24からのパケットを自分（192.168.1.100）から送られた物として再送する（NAT機能を使う）ためのコマンド。
  * 停止時はその逆の <kbd>/sbin/iptables -t nat -D POSTROUTING -s 192.168.200.0/24 -j MASQUERADE</kbd> を実行して、NAT機能を停止する。

変更を保存したら、以下のコマンドを実行してeth0とeth1のインターフェースを停止→再起動します。

{% raw %}
```
$ sudo ifdown eth0
$ sudo ifdown eth1
$ sudo ifup eth0
$ sudo ifup eth1
```
{% endraw %}

これで、*Ubuntu PCが192.168.200.0/24と192.168.201.0/24に対するルータになりました*。

#### 確認

Ubuntu PCが期待通りのルータになっているかどうかは、以下のようにして確認できます。

  * せんとくん1号からせんとくん2号宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.201.20
```
{% endraw %}
  レスポンスが返ってくれば成功。
  * せんとくん2号からせんとくん1号宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.200.10
```
{% endraw %}
  レスポンスが返ってくれば成功。
  * せんとくん1号から既存LAN上のコンピュータ宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.1.1
```
{% endraw %}
  レスポンスが返ってくれば成功。
  * せんとくん2号から既存LAN上のコンピュータ宛にpingを打ってみる。
  {% raw %}
```
$ ping 192.168.1.1
```
{% endraw %}
  レスポンスが返ってくれば成功。
  * せんとくん1号からインターネット上のコンピュータ宛にpingを打ってみる。
  {% raw %}
```
$ ping 8.8.8.8
```
{% endraw %}
  レスポンスが返ってくれば成功。（※8.8.8.8はGoogle DNSのアドレス）
  * せんとくん2号からインターネット上のコンピュータ宛にpingを打ってみる。
  {% raw %}
```
$ ping 8.8.8.8
```
{% endraw %}
  レスポンスが返ってくれば成功。

#### うまくいかない場合

  * ミスタイプはないか？
  * Ubuntu PCのeth0とeth1をちゃんと起動し直したか？

### 4. Ubuntu PCをDNSにする

ここまでの設定で、せんとくん1号2号はインターネットにも繋がるようになっています。ただし、「www.clear-code.com」のようなホスト名ではサーバにアクセスできず、ホストのIPアドレスを直接指定する必要があります。実際に、例えばせんとくん1号で <kbd>ping www.google.com</kbd> とやっても <samp>ping: unknowon host www.google.com</samp>と言われてしまいます。これは、せんとくん1号が「www.google.comというホスト名に対応するIPアドレスが分からない」と音を上げているためです。

この状態を解消するには、

  * せんとくん1号2号が既存のDNSを参照するように設定する。
  * Ubuntu PCが使用しているDNSを192.168.200.0/24および192.168.201.0/24のネットワーク上のコンピュータにも通知して、それを使わせるようにする。
  * Ubuntu PCをDNSとして動作させた上で、192.168.200.0/24および192.168.201.0/24のネットワーク上のコンピュータにそれを通知して、Ubuntu PCをDNSとして使わせるようにする。

という3つの解決策が考えられます。

せんとくん1号にDNSとして既存のDNSを直接参照させるには、Gnomeメニューの「システム」→「管理」→「ネットワーク」の「DNS」タブで「1番目のDNS」に「192.168.1.1」などの既存DNSのIPアドレスを指定します。これで、<kbd>ping www.google.com</kbd> が通るようになります。

ただ、192.168.200.0/24や192.168.201.0/24にコンピュータを接続する度にDNSの設定を編集するのは面倒です。そこで、DHCPサーバの機能の1つであるDNSのアドレスを通知する機能を使うというのが、2番目の案です。

DHCPでDNSのアドレスを通知する場合は、Ubuntu PC（DHCPサーバ）の /etc/dhcp3/dhcpd.conf に以下のように書き加えます。
（2014年7月16日追記。Ubuntu 14.04LTS以降の場合は、 /etc/dhcp/dhcpd.conf を編集します。）

{% raw %}
```diff
  subnet 192.168.200.0 netmask 255.255.255.0 {
    range dynamic-bootp 192.168.200.2 192.168.200.20;
+   option domain-name-servers 192.168.1.1;
    （略）
  }

  subnet 192.168.201.0 netmask 255.255.255.0 {
    range dynamic-bootp 192.168.201.2 192.168.201.20;
+   option domain-name-servers 192.168.1.1;
    （略）
  }
```
{% endraw %}

しかしながら、この場合でも、DNSのアドレスが192.186.1.1から他のアドレスに変わった場合に面倒な事になります。DNSのアドレスが変わった時には、Ubuntu PCのDHCPサーバの設定をそれに合わせて変更しないといけませんし、何より、192.168.200.0/24と192.168.201.0/24に接続している各コンピュータも、DHCPサーバにもう一度問い合わせを行って情報を更新する必要があります。

そこで、Ubuntu PC自身をDNSとして動作させ、Ubuntu PCの固定IP（これは変動しませんよね）をDHCPで通知するというのが3番目の案になります。

Ubuntu PCをDNSにするといっても、Ubuntu PCのDNS自体にあれこれと設定する必要は今の所ありません。Ubuntu PCでDNSのためのサービスが起動していれば、単純に、新しいLANのクライアントからDNSルックアップの要求が来た時に、さらに上位のDNS（Ubuntu PC自身が参照しているDNS）に問い合わせを丸投げしてくれるようになります。

そのように動作させるためには、Ubuntu PCにbind9（DNS用のサーバ）をインストールします。

{% raw %}
```
$ sudo aptitude install bind9
```
{% endraw %}

次に、Ubuntu PCの /etc/dhcp3/dhcpd.conf に以下のように書き加えて、自身のIPアドレスをDNSとして通知するようにします。
（2014年7月16日追記。Ubuntu 14.04LTS以降の場合は、 /etc/dhcp/dhcpd.conf を編集します。）

{% raw %}
```diff
  subnet 192.168.200.0 netmask 255.255.255.0 {
    range dynamic-bootp 192.168.200.2 192.168.200.20;
+   option domain-name-servers 192.168.200.254;
    （略）
  }

  subnet 192.168.201.0 netmask 255.255.255.0 {
    range dynamic-bootp 192.168.201.2 192.168.201.20;
+   option domain-name-servers 192.168.201.254;
    （略）
  }
```
{% endraw %}

変更を保存したら、DHCPサーバを再起動して設定を反映させます。

{% raw %}
```
$ sudo /etc/init.d/dhcp3-server restart
```
{% endraw %}

また念のため、eth0とeth1が有効になる際に必ずbind9も起動している状態になるよう、/etc/network/interfaces にbind9のサービスの自動起動のためのコマンドも追記しておきましょう。

{% raw %}
```diff
   auto eth0
   iface eth0 inet static
     address 192.168.200.254
     netmask 255.255.255.0
     broadcast 192.168.200.255
     pre-up /sbin/sysctl net.ipv4.conf.all.forwarding=1
     pre-up /sbin/iptables -t nat -A POSTROUTING -s 192.168.200.0/24 -j MASQUERADE
     pre-up /etc/init.d/dhcp3-server stop # Ubuntu 10.10の場合。Ubuntu 11.10以降では
                                          # pre-up /etc/init.d/isc-dhcp-server stop
                                          # とする。
+    pre-up /etc/init.d/bind9 stop
     post-up /etc/init.d/dhcp3-server start # Ubuntu 10.10の場合。Ubuntu 11.10以降では
                                            # post-up /etc/init.d/isc-dhcp-server start
                                            # とする。
+    post-up /etc/init.d/bind9 start
     post-down /sbin/iptables -t nat -D POSTROUTING -s 192.168.200.0/24 -j MASQUERADE
     post-down /sbin/sysctl net.ipv4.conf.all.forwarding=0

   auto eth1
   iface eth1 inet static
     address 192.168.201.254
     netmask 255.255.255.0
     broadcast 192.168.201.255
     pre-up /sbin/sysctl net.ipv4.conf.all.forwarding=1
     pre-up /sbin/iptables -t nat -A POSTROUTING -s 192.168.201.0/24 -j MASQUERADE
     pre-up /etc/init.d/dhcp3-server stop # Ubuntu 10.10の場合。Ubuntu 11.10では
                                          # pre-up /etc/init.d/isc-dhcp-server stop
                                          # とする。
+    pre-up /etc/init.d/bind9 stop
     post-up /etc/init.d/dhcp3-server start # Ubuntu 10.10の場合。Ubuntu 11.10では
                                            # post-up /etc/init.d/isc-dhcp-server start
                                            # とする。
+    post-up /etc/init.d/bind9 start
     post-down /sbin/iptables -t nat -D POSTROUTING -s 192.168.201.0/24 -j MASQUERADE
     post-down /sbin/sysctl net.ipv4.conf.all.forwarding=0
```
{% endraw %}

これで、*Ubuntu PCがDNSになりました*。

最後に、せんとくん1号2号のeth0を再起動して、DHCPサーバからの情報を最新のものに更新しておきましょう（やり方は先程と同じです）。

#### 確認

せんとくん1号2号でDNSを利用できるようになっているかどうかは、以下のようにして確認できます。

  1. Gnomeの「ネットワーク設定」を一旦閉じる。

  1. Gnomeメニューを「システム」→「管理」→「ネットワーク」と辿り、「ネットワーク設定」を開き直す。

  1. 「DNS」タブを選択し、「1番目のDNS」欄に192.168.200.254（または192.168.201.254）が表示されている事を確認する。


また、Ubuntu PCが本当にDNSとして動作しているかどうかは、せんとくん1号2号からdigコマンドでインターネット上のホストの名前でDNSルックアップを試みて、

{% raw %}
```
$ dig www.google.com

; <<>> DiG 9.7.1-P2 <<>> www.google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23336
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;www.google.com.			IN	A

;; ANSWER SECTION:
www.google.com.		69989	IN	CNAME	www.l.google.com.
www.l.google.com.	300	IN	A	66.249.89.104
www.l.google.com.	300	IN	A	66.249.89.99
...
```
{% endraw %}

といった結果が得られるかどうかで確認できます。

なお、この状態であれば、せんとくん1号2号でFirefox等を起動してWebブラウズすることもできるようになっています。

#### うまくいかない場合

digの結果が

{% raw %}
```
$ dig www.google.com

; <<>> DiG 9.7.1-P2 <<>> www.google.com
; (1 server found)
;; global options: +cmd
;; connection timed out; no servers could be reached
```
{% endraw %}

となる場合は、DNSを利用できていないということになります。

  * そもそもUbuntu PCからも名前解決できない状態になっていないか？（上位のDNSが死んでる、など。）
  * bind9のサービスは動作しているか？　<kbd>$ sudo /etc/init.d/bind9 restart</kbd> でサービスを再起動してみる。

### まとめ

以上の内容をまとめると、

{% raw %}
```
192.168.1.0/24
      ↑
     wlan0
      ↑
  [Ubuntu PC]→eth0→192.168.200.0/24
           └→eth1→192.168.201.0/24
```
{% endraw %}

このような形で新しく2つのLANを構築する場合にUbuntu PCに対して実際に行う操作は、

  1. 必要なソフトウェアをインストールする。

     {% raw %}
```
$ sudo aptitude install dhcp3-server bind9
```
{% endraw %}
  1. /etc/dhcp3/dhcpd.conf に以下の内容を書き加える。
      （2014年7月16日追記。Ubuntu 14.04LTS以降の場合は、 /etc/dhcp/dhcpd.conf を編集します。）

     {% raw %}
```diff
subnet 192.168.200.0 netmask 255.255.255.0 {
  range dynamic-bootp 192.168.200.2 192.168.200.20;
  option domain-name-servers 192.168.200.254;
  option routers 192.168.200.254;
  option subnet-mask 255.255.255.0;
  option broadcast-address 192.168.200.255;
  default-lease-time 6000;
  max-lease-time 72000;
}

subnet 192.168.201.0 netmask 255.255.255.0 {
  range dynamic-bootp 192.168.201.2 192.168.201.20;
  option domain-name-servers 192.168.201.254;
  option routers 192.168.201.254;
  option subnet-mask 255.255.255.0;
  option broadcast-address 192.168.201.255;
  default-lease-time 6000;
  max-lease-time 72000;
}
```
{% endraw %}
  1. /etc/network/interfaces に以下の内容を書き加える。

     {% raw %}
```diff
auto eth0
iface eth0 inet static
  address 192.168.200.254
  netmask 255.255.255.0
  broadcast 192.168.200.255
  pre-up /sbin/sysctl net.ipv4.conf.all.forwarding=1
  pre-up /sbin/iptables -t nat -A POSTROUTING -s 192.168.200.0/24 -j MASQUERADE
  post-up /etc/init.d/dhcp3-server stop # Ubuntu 10.10の場合。Ubuntu 11.10では
                                        # pre-up /etc/init.d/isc-dhcp-server stop
                                        # とする。
  pre-up /etc/init.d/bind9 stop
  post-up /etc/init.d/dhcp3-server start # Ubuntu 10.10の場合。Ubuntu 11.10では
                                         # post-up /etc/init.d/isc-dhcp-server start
                                         # とする。
  post-up /etc/init.d/bind9 start
  post-down /sbin/iptables -t nat -D POSTROUTING -s 192.168.200.0/24 -j MASQUERADE
  post-down /sbin/sysctl net.ipv4.conf.all.forwarding=0

auto eth1
iface eth1 inet static
  address 192.168.201.254
  netmask 255.255.255.0
  broadcast 192.168.201.255
  pre-up /sbin/sysctl net.ipv4.conf.all.forwarding=1
  pre-up /sbin/iptables -t nat -A POSTROUTING -s 192.168.201.0/24 -j MASQUERADE
  pre-up /etc/init.d/dhcp3-server stop # Ubuntu 10.10の場合。Ubuntu 11.10では
                                       # pre-up /etc/init.d/isc-dhcp-server stop
                                       # とする。
  pre-up /etc/init.d/bind9 stop
  post-up /etc/init.d/dhcp3-server start # Ubuntu 10.10の場合。Ubuntu 11.10では
                                         # post-up /etc/init.d/isc-dhcp-server start
                                         # とする。
  post-up /etc/init.d/bind9 start
  post-down /sbin/iptables -t nat -D POSTROUTING -s 192.168.201.0/24 -j MASQUERADE
  post-down /sbin/sysctl net.ipv4.conf.all.forwarding=0
# source: diff
```
{% endraw %}
  1. 「sudo /etc/init.d/network-manager restart」もしくは「sudo service network-manager restart」でネットワークマネージャを再起動する、もしくはUbuntu PCを再起動する。


ということになります。

### Ubuntu PCをただのクライアントに戻す場合

Ubuntu PCをこのままの状態にしていると、eth0やeth1について、ケーブルの抜き差しだけでネットワークを自動認識するといったUbuntuのネットワークマネージャの機能を利用できません。また、実験用のLANではない192.168.200.0/24や192.168.201.0/24なネットワークに接続した際に、自分がDHCPサーバとして振る舞ってしまうため、ネットワークが不調になる可能性もあります。なので、必要が無くなったら、ここまでで行った設定を解除しておきましょう。

  1. インターフェースを停止し、ルーティング設定を解除する。

     {% raw %}
```
$ sudo ifdown eth0
$ sudo ifdown eth1
```
{% endraw %}
  1. DHCPサーバとDNSを削除する。

     {% raw %}
```
$ sudo aptitude remove dhcp3-server bind9
```
{% endraw %}
     この操作では設定ファイルは残るので、再度aptitudeでインストールすればまた同じ設定で使えるようになる。設定ファイルも含めて完全に削除したい場合は
     {% raw %}
```
$ sudo aptitude purge dhcp3-server bind9
```
{% endraw %}
     とする。
       * サーバを削除したくないのであれば、
       {% raw %}
```
$ sudo aptitude install sysv-rc-conf
```
{% endraw %}
       でサービス管理用のツールをインストールして、
       {% raw %}
```
$ sudo sysv-rc-conf dhcp3-server off
（※Ubuntu 11.10の場合は sudo sysv-rc-conf isc-dhcp-server off）
$ sudo sysv-rc-conf bind9 off
```
{% endraw %}
       でサーバを恒久的に停止させる。この場合、また必要になった時は
       {% raw %}
```
$ sudo sysv-rc-conf dhcp3-server on
（※Ubuntu 11.10の場合は sudo sysv-rc-conf isc-dhcp-server on）
$ sudo sysv-rc-conf bind9 on
```
{% endraw %}
       で再度サーバを有効化できる。
  1. /etc/network/interfaces に書き加えた部分を全て削除する、またはコメントアウトする（行の頭に「#」を挿入する）。

  1. 「sudo /etc/init.d/network-manager restart」もしくは「sudo service network-manager restart」でネットワークマネージャを再起動する、もしくはUbuntu PCを再起動する。


以上で、Ubuntu PCは元の状態に戻ります。

### 応用

今回は2つのLANを作りたいという前提があるためそのようにしましたが、作るLANの数が1個でも3個でもN個でも、基本的な手順は変わりません。ただし、構築するLANの数を増やすには、ルータになるコンピュータのLAN用インターフェースがその分だけ必要になります。（インターフェースが足らない場合は、ルータ役のコンピュータ自体を増やしたり、今回のようにUSB接続のLANアダプタを付け足したりする必要があります。）

また、これを応用すると、「インターネットに接続できるPCが1台しかない環境で他のPCでもインターネットを利用したい」という時に、「インターネットに接続できる1台をルータとして一時的にLANを組んで、みんなでインターネットを利用する」ということもできます。

### おわりに

ルータやDHCPサーバになれるPCが1台あれば、追加投資をせずにクリーンなLANを必要に応じて作る事ができます。既存のLANに影響を与えずに実験をしてみたいけれどもネットワーク機器が足りないという場合は、今回の事例のように、手元のLinux PCをネットワーク機器の代わりに使用してみてはいかがでしょうか。
