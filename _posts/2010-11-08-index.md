---
tags: []
title: PDFやオフィス文書からテキストを抜き出すツールをテスト公開 - ChupaText 0.5.0
---
[全文検索エンジンgroongaを囲む夕べ #1 : ATND](http://atnd.org/events/9234)の定員が50人から120人に増えましたね。たぶん、何人かはキャンセルするはずなので、今のうちに登録しておくとおそらく参加できるでしょう。興味のある方はお早めに登録してください。
<!--more-->


さて、以前、[サーバ上でPDFやオフィス文書からテキストを抜き出す方法]({% post_url 2010-08-02-index %})を紹介しました。これらを使うことにより様々なフォーマットの文書からテキストを抽出し、[groonga](http://groonga.org/)などの全文検索エンジンを利用して高速に目的の文書を見つけることができます。例えば、ファイルサーバやデスクトップ上にある文書を検索する用途にも有用です。

しかし、文書のフォーマット毎に抽出方法を変えなければいけないため、実際にテキストを抽出する部分（インデクサーの機能の一部）を作る場合に不便です。文書のフォーマットに依らず、同じ方法でテキストを抽出できると便利ですよね。

ということで、文書のフォーマットの違いを意識することなくテキストとメタデータ（タイトル・作成者・作成時刻など）を抽出するツール[ChupaText](http://groonga.rubyforge.org/#about-chupatext)を開発し、テスト公開しました。ライセンスはLGPLv2+なので自由に利用可能です。

### 出力フォーマット

入力文書のフォーマットの違いを意識せずに利用するためには、テキスト抽出後の結果が入力文書のフォーマットに依存せず、統一されていなければいけません。ChupaTextでは文書を入力すると、入力文書のフォーマットに依存せず、以下のようにMIME形式で出力します。

{% raw %}
```
% chupatext /tmp/sample_multi_pages.pdf
URI: file:///tmp/sample_multi_pages.pdf
Content-Type: text/plain; charset=UTF-8
Content-Length: 26
Original-Content-Length: 6145
Creation-Time: 2010-09-27T04:09:17Z
Original-Filename: sample_multi_pages.pdf
Original-Content-Type: application/pdf
Original-Content-Disposition: inline;
 filename=sample_multi_pages.pdf;
 size=6145;
 creation-date=Mon, 27 Sep 2010 04:09:17 +0000

page1
2 ページ目
page3
```
{% endraw %}

出力フォーマットとしてMIMEを選択したのは以下の理由からです。

  * すでに広く利用されているフォーマット
  * 1つの出力で複数のコンテンツを表現可能
  * メタデータ（ヘッダー部分）の内容を拡張可能

1番大きな理由が1番目の理由です。独自に新しいフォーマットを定義するよりも、すでに広く利用されているフォーマットを採用する方が利用しやすくなります。JSONが外部出力のフォーマットとして多く採用されているのも、同様の理由でしょう。

### 構造

ChupaTextはそれぞれのフォーマットからテキストやメタデータを抽出する部分をモジュール化し、新しいフォーマットに対応しやすい構造になっています[^0]。

これからも対応フォーマットを増やす予定ですが、それに適した構造になっているというわけです。

### インストール方法

ChupaTextは様々なフォーマットに対応するために、多くのライブラリを必要とします。そのため、ビルドするのは少し面倒です。以下のライブラリがインストールされている必要があります。

  * [libgsf](http://freshmeat.net/projects/libgsf/): 構造化されたフォーマットを読み書きするためのライブラリ。MS OLE2やzipなどにも対応しており、以下のライブラリの中にもlibgsfを利用しているものが多い。
  * [Poppler](http://poppler.freedesktop.org/): PDFの読み込みと描画機能を提供するライブラリ。
  * [wv](http://wvware.sourceforge.net/): MS Wordファイルを読み込む機能を提供するライブラリ。
  * [Gnumeric](http://projects.gnome.org/gnumeric/)とlibgoffice: MS Excelファイルの読み書き機能を提供するライブラリ。libgofficeの上にGnumericが実装されている。
  * [LibreOffice](http://www.documentfoundation.org/download/)または[OpenOffice.org](http://ja.openoffice.org/): MS PowerPointなどMS OfficeファイルやOpenDocumentフォーマットなどを読み書きするアプリケーション群。
  * [Ruby 1.9.2](http://www.ruby-lang.org/): オブジェクト指向スクリプト言語。Rubyでテキスト抽出部分を記述することができるので、容易に新しいフォーマットに対応できる。

簡単にインストールできるようにするため、Debian GNU/LinuxとUbuntu用のパッケージを用意しました。

  * [Debian GNU/Linuxへのインストール](http://groonga.rubyforge.org/chupatext/ja/install-to-debian.html)
  * [Ubuntuへのインストール](http://groonga.rubyforge.org/chupatext/ja/install-to-ubuntu.html)

Fedora用のパッケージは後で用意するかもしれません。

### まとめ

全文検索システムを開発する場合に有用なテキスト抽出ツールChupaTextを紹介しました。興味のある方は試してみてください。

リポジトリは[GitHub](https://github.com/ranguba/chupatext/)にあります。

[^0]: もう少し詳しく書くと、ChupaTextはC言語で実装されていて、抽出部分がそれぞれ共有ライブラリになっています。そのため、特定のディレクトリにファイルを配置することによりChupaTextをビルドし直さなくても新しく対応フォーマットを追加することができます。また、抽出部分をRubyで書くこともできるため、より簡単に対応フォーマットを追加することができます。
