---
tags:
- fluentd
title: Fluentd-kubernetes-daemonsetのElasticsearchイメージでILMを使う
---
### はじめに

クリアコードは[Fluentd](https://www.fluentd.org/)の開発に参加しています。
<!--more-->


Fluentdにはプラグインというしくみがあり、たくさんのプラグインが開発されています。
Fluentdはログ収集ソフトウェアということから[kubernetes](https://kubernetes.io/ja/)（以下k8sと略記）にも載せることができます。
Fluentdの開発元が公式に出しているk8sでのログ収集の仕組みの一つとしてFluentdのDaemonSetを提供しています。

筆者畑ケは[ElasticsearchのILM](https://www.elastic.co/jp/blog/implementing-hot-warm-cold-in-elasticsearch-with-index-lifecycle-management)対応を最近fluent-plugin-elasticsearchに入れました。[^0]
筆者が対応したILMをFluentdのDaemonSetでも有効化して動かすことができたので、報告します。

ILMを有効化していると、古くなったインデックスを定期的に消すというオペレーションをElasticsearch自体に任せることができ、Elasticsearchのクラスターの管理の手間を減らせます。

### k8sのkind

まず、Fluentdのログ収集を解説する前に、k8sに少し触れておきます。
この記事ではminikube v1.11.0、Kubernetes v1.18.3を想定しています。
k8sではいくつかのリソースの管理方法があります。リソースはオブジェクト毎に名前が付けられており、yamlでオブジェクトの振る舞いを決定します。
ここで、単純なコンテナをk8sにデプロイするには、例えば以下のようにnginxのDeploymentを作成します。

```yaml
apiVersion: apps/v1
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```


これを

```console
$ kubectl apply -f ngix-deployment.yaml
deployment.apps/nginx-deployment created
```


とする事で、nginxのコンテナがk8s上で動作し始めます。

```console
$ kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-6b474476c4-vhtmn   1/1     Running   0          34s
nginx-deployment-6b474476c4-xqr4f   1/1     Running   0          34s
```


動作確認が終わったらDeploymentを片付けておきましょう。

```console
$ kubectl delete deployment nginx-deployment
deployment.apps "nginx-deployment" deleted
```


### k8sのDaemonSetとは

k8sには[DaemonSetというkind](https://kubernetes.io/ja/docs/concepts/workloads/controllers/daemonset/)があり、これはクラスターを構成するNode上にDaemonSetが構成するPodを自動的に配置するために使用されるkindです。

DaemonSetのこの性質を用いる事で、各Node上のログを集めるPodを自動的に配置するFluentdのDaemonSetが作成できます。

#### FluentdのDaemonSet

FluentdのDaemonsetは公式では https://github.com/fluent/fluentd-kubernetes-daemonset にて開発がされています。
執筆時点では以下のイメージがDaemonSet用に提供されています。

  * debian-elasticsearch7

  * debian-elasticsearch6

  * debian-loggly

  * debian-logentries

  * debian-cloudwatch

  * debian-stackdriver

  * debian-s3

  * debian-syslog

  * debian-forward

  * debian-gcs

  * debian-graylog

  * debian-papertrail

  * debian-logzio

  * debian-kafka

  * debian-kinesis

この記事では、debian-elasticsearch7のdocker imageを参照しているfluentd-kubernetes-daemonsetの設定を元にして、ILMを有効化したロギング環境を構築します。

#### FluentdのElasticsearch7 Daemonset

Fluentdでログ収集をした後に、Elasticsearchを用いてログをストアするDaemonSetは例えば、 [fluentd-daemonset-elasticsearch.yaml](https://github.com/fluent/fluentd-kubernetes-daemonset/blob/1dae1ebc18936ca5b2921dc84aa9a98617158bab/fluentd-daemonset-elasticsearch.yaml)です。この設定ではRBACを使っていませんが、簡単のためこのDaemonSetをもとにして構成します。
また、この記事で使用するdebian-elasticsearchのimageのtagは`fluent/fluentd-kubernetes-daemonset:v1.11.1-debian-elasticsearch7-1.3` または `fluent/fluentd-kubernetes-daemonset:v1-debian-elasticsearch`を用いています。
記事が公開されるタイミングではどちらのタグを使用しても大丈夫です。

元のDaemonSetの構成ではElasticsearchのテンプレート設定が入っていないため、テンプレートの設定を[ConfigMap](https://cloud.google.com/kubernetes-engine/docs/concepts/configmap?hl=ja)で表現することにします。

```yaml
apiVersion: v1
data:
  index_template.json: |-
    {
        "index_patterns": [
            "logstash-default*"
        ],
        "settings": {
            "index": {
                "number_of_replicas": "3"
            }
        }
    }
kind: ConfigMap
metadata:
  name: es-template
  namespace: kube-system
---
```


[ConfigMapをk8sではpodからストレージボリュームとして参照する](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#add-configmap-data-to-a-volume)ことが出来ます。

```yaml
      volumes:
      - name: es-template
        configMap:
          name: es-template
```


k8sのvolumeオブジェクトはConfigMapの名前を指定してボリュームとしてPodから認識させます。
このオブジェクトをマウントします。

```yaml
        volumeMounts:
        - name: es-template
          mountPath: /host
          readOnly: true
```


fluent-plugin-elasticsearchのILMの設定を入れていきます。

```yaml
          # ILM parameters
          # ==============
          - name: FLUENT_ELASTICSEARCH_ENABLE_ILM
            value: "true"
          - name: FLUENT_ELASTICSEARCH_ILM_POLICY
            value: '{ "policy": { "phases": { "hot": { "min_age": "0ms", "actions": { "rollover": { "max_age": "1d", "max_size": "5g
b" } } }, "delete": { "min_age": "2d", "actions": { "delete": {}}}}}}'
          - name: FLUENT_ELASTICSEARCH_TEMPLATE_FILE
            value: /host/index_template.json
          - name: FLUENT_ELASTICSEARCH_TEMPLATE_NAME
            value: "logstash-default"
```


上記の設定を入れればElasticsearchのILMの機能を有効化する為に必要な設定が入れられました。
[変更の全体はこのコミット](https://github.com/cosmo0920/fluentd-kubernetes-daemonset/commit/b670921ec7f9c7c7b07a7d0343cee727608dc33c)で見ることができます。

#### 実際に適用してみる

適用する前に、以下の設定を実際のElasticsearchが動いているサーバーの値に書き換えておいてください。

```yaml
          - name:  FLUENT_ELASTICSEARCH_HOST
            value: "elasticsearch-master.default.svc"
          - name:  FLUENT_ELASTICSEARCH_PORT
            value: "9200"
          - name: FLUENT_ELASTICSEARCH_SCHEME
```


##### この記事で実際に使用しているElasticsearchとそのデプロイ方法の概要

この記事で使用しているElasticsearchは[Elastic社がメンテナンスしているhelm charts](https://github.com/elastic/helm-charts/tree/master/elasticsearch)の7.8.0タグを用いてminikubeで作成したk8sにデプロイしました。

k8sの外からElasticsearchのAPIを叩けるように9200ポートのポートフォワードを設定しておきます。

```console
$ kubectl port-forward svc/elasticsearch-master 9200 
Forwarding from 127.0.0.1:9200 -> 9200
Forwarding from [::1]:9200 -> 9200
...
```


別のターミナルからElasticsearchの応答を確認します。

```console
$ curl localhost:9200
{
  "name" : "elasticsearch-master-2",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "Vskz9aTjTZSCg8klQMz5mg",
  "version" : {
    "number" : "7.8.0",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "757314695644ea9a1dc2fecd26d1a43856725e65",
    "build_date" : "2020-06-14T19:35:50.234439Z",
    "build_snapshot" : false,
    "lucene_version" : "8.5.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```


Elasticsearch v7.8.0が動作していることが確認できました。

##### kubectlを用いて実際に適用

では、kubectlで実際に適用してみます。

```console
$ kubectl apply -f fluentd-daemonset-elasticsearch.yaml
configmap/es-template created
daemonset.apps/fluentd created
```


Podが動作しているかを確認します。

```console
$ kubectl get pods -n=kube-system
NAME                               READY   STATUS    RESTARTS   AGE
coredns-66bff467f8-pswng           1/1     Running   1          13d
etcd-minikube                      1/1     Running   0          6d1h
fluentd-hqh9n                      1/1     Running   0          7s
kube-apiserver-minikube            1/1     Running   0          6d1h
kube-controller-manager-minikube   1/1     Running   1          13d
kube-proxy-kqllr                   1/1     Running   1          13d
kube-scheduler-minikube            1/1     Running   1          13d
storage-provisioner                1/1     Running   3          13d
```


FluentdのDaemonSetが動作しているのを確認できました。
Fluentdが正常に動いているかをPodのログを見て確認してみます。

```console
$ kubectl logs fluentd-hqh9n -n=kube-system
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-dedot_filter' version '1.0.0'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-detect-exceptions' version '0.0.13'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-elasticsearch' version '4.1.1'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-grok-parser' version '2.6.1'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-json-in-json-2' version '1.0.2'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-kubernetes_metadata_filter' version '2.3.0'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-multi-format-parser' version '1.0.0'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-prometheus' version '1.6.1'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-record-modifier' version '2.0.1'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-rewrite-tag-filter' version '2.2.0'
2020-07-22 08:06:15 +0000 [info]: gem 'fluent-plugin-systemd' version '1.0.2'
2020-07-22 08:06:15 +0000 [info]: gem 'fluentd' version '1.11.1'
2020-07-22 08:06:16 +0000 [info]: using configuration file: <ROOT>
...
<snip>
...
2020-07-22 08:06:16 +0000 [info]: starting fluentd-1.11.1 pid=6 ruby="2.6.6"
2020-07-22 08:06:16 +0000 [info]: spawn command to main:  cmdline=["/usr/local/bin/ruby", "-Eascii-8bit:ascii-8bit", "/fluentd/vendor/bundle/ruby/2.6.0/bin/fluentd", "-c", "/fluentd/etc/fluent.conf", "-p", "/fluentd/plugins", "--gemfile", "/fluentd/Gemfile", "-r", "/fluentd/vendor/bundle/ruby/2.6.0/gems/fluent-plugin-elasticsearch-4.1.1/lib/fluent/plugin/elasticsearch_simple_sniffer.rb", "--under-supervisor"]
2020-07-22 08:06:16 +0000 [info]: adding match in @FLUENT_LOG pattern="fluent.**" type="null"
2020-07-22 08:06:16 +0000 [info]: adding filter pattern="kubernetes.**" type="kubernetes_metadata"
2020-07-22 08:06:16 +0000 [info]: adding match pattern="**" type="elasticsearch"
2020-07-22 08:06:17 +0000 [info]: adding source type="systemd"
2020-07-22 08:06:17 +0000 [info]: adding source type="systemd"
2020-07-22 08:06:17 +0000 [info]: adding source type="systemd"
2020-07-22 08:06:17 +0000 [info]: adding source type="prometheus"
2020-07-22 08:06:17 +0000 [info]: adding source type="prometheus_output_monitor"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: adding source type="tail"
2020-07-22 08:06:17 +0000 [info]: #0 starting fluentd worker pid=18 ppid=6 worker=0
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/kube-scheduler-minikube_kube-system_kube-scheduler-986d31752d921b9cee830917de6372781bd418c4674e7c890ef2ccb082292f50.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/elasticsearch-master-1_default_configure-sysctl-f8b971b868b6536e084453d8890a7677640446a20b8dc6397ed7715ada823be5.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/fluentd-hqh9n_kube-system_fluentd-65a884bccf20d3134d96f836a3a1a9e1116bee9fd01a8298206b54baf9340f84.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/elasticsearch-master-1_default_elasticsearch-6393ceacc5dc158689f5f4c20a3b072c91badab6974469e2652882eadc8b0964.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/elasticsearch-master-0_default_elasticsearch-7a47537855db7869b905cff73434348b7244a3f2a0a2f31b7703fdff864d3838.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/storage-provisioner_kube-system_storage-provisioner-1a2e641e4dfa4470903315c22c369cd02f43b819a44911130cb775b771bf2f42.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/kube-proxy-kqllr_kube-system_kube-proxy-c756169c6e4a9c348719703e49630442f48327552cbaf33a7a61bf6d60ffa3f8.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/elasticsearch-master-0_default_configure-sysctl-5fee5b507865277bf15f8f5412d72f977038b21e657fa39d51f73705edfe8b6b.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/kube-scheduler-minikube_kube-system_kube-scheduler-fdec557596b5c9c38040b9a04753b3407fa51f3e07bd1fea441b8c72bcc33f6a.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/kube-proxy-kqllr_kube-system_kube-proxy-68c7040be16381fa6e8179312482f36c521bb0588053c879138199e2c89deca5.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/etcd-minikube_kube-system_etcd-8ece6d2d408533810f2bc33e9aeeb534ea2781259c8046331b297c446ec24fe9.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/kube-apiserver-minikube_kube-system_kube-apiserver-ffa76a89be8bf37a82e68a36ea165d4db957b2156c277131f592d7b1b9497279.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/coredns-66bff467f8-pswng_kube-system_coredns-3f5612e80916072da46574a47f2b782dce33a536c1fa62d95450d1673ff63105.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/kube-controller-manager-minikube_kube-system_kube-controller-manager-015b2b7520797e279dd4783eeb68fa8b8a26db6ecc1d684f67bfdc13411791e3.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/kube-controller-manager-minikube_kube-system_kube-controller-manager-9a1b2e08d497b4ca2d144e42972b23fad15bcc1beb996a2b8e6fab0aee3999c0.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/elasticsearch-master-2_default_elasticsearch-c179b54e4fdddbb20068269b75cd88325658c98847d6d906d3a4a954860885af.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/coredns-66bff467f8-pswng_kube-system_coredns-3a7f59d8a9cec5ed207b061d89853cd19c3f2388a09a628c590d079c76af0323.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/storage-provisioner_kube-system_storage-provisioner-73c205f99d913ad3915df4004aeab5a03a549c7d00dbc47288bfec9cdedfdcf8.log
2020-07-22 08:06:17 +0000 [info]: #0 [in_tail_container_logs] following tail of /var/log/containers/elasticsearch-master-2_default_configure-sysctl-a4ffd4fa272b2eaaa4bb6daf8e2618ee109fdb9aba51ce9130226823f1be08a0.log
2020-07-22 08:06:17 +0000 [info]: #0 fluentd worker is now running worker=0
```


`#0 fluentd worker is now running worker=0` となるので正常に動作しています。

動作させていくらか経ったElasticsearchのインデックスの状態を確認します。

```console
$ curl localhost:9200/_cat/indices
green open logstash-default-2020.07.22-000004 1BNxJGy2R_u2ETFog8hI5g 1 3     0 0    624b   208b
green open logstash-default-2020.07.25-000002 yesiqdW4Qdi_PD5JEGwctw 1 3     0 0    624b   208b
green open logstash-default-2020.07.27-000002 y0-NGO_3SmmcRRnLNNy9cw 1 3     0 0    624b   208b
green open logstash-default-2020.07.22-000003 B8Z41XVvTpCIjrLewEutMA 1 3    61 0 153.5kb 51.1kb
green open logstash-default-2020.07.21-000001 5JzBvzkgSve-pJBOl-_AOA 1 3  3662 0   4.5mb  1.5mb
green open logstash-default-2020.07.22-000002 hsojlNScTOCaMDKCLlTEjQ 1 3     0 0    624b   208b
green open logstash-default-2020.07.25-000001 L9mPpFjrSYmex27-oUlqsQ 1 3 22798 0    17mb  5.6mb
green open logstash-default-2020.07.28-000001 kYvZ7R_fSVaXcIMBdP7OgA 1 3   132 0 518.8kb  185kb
green open logstash-default-2020.07.27-000001 CJTNpX4lQf6frlc5v1aHIg 1 3 62061 0  45.2mb 15.1mb
green open logstash-default-2020.07.25-000003 g_oSzQTRS42Pn5DZm8NSMA 1 3     0 0    624b   208b
```


logstash-default-2020.07.22-000001のインデックスは作成されてから2日以上経っているので消されていることがわかります。
インデックスがILMで管理されているかどうかを確認します。

```console
$ curl localhost:9200/logstash-2020.07.28/_ilm/explain | jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   550  100   550    0     0  36666      0 --:--:-- --:--:-- --:--:-- 36666
{
  "indices": {
    "logstash-default-2020.07.28-000001": {
      "index": "logstash-default-2020.07.28-000001",
      "managed": true,
      "policy": "logstash-policy",
      "lifecycle_date_millis": 1595898121210,
      "age": "4.37h",
      "phase": "hot",
      "phase_time_millis": 1595898121544,
      "action": "rollover",
      "action_time_millis": 1595898216494,
      "step": "check-rollover-ready",
      "step_time_millis": 1595898216494,
      "phase_execution": {
        "policy": "logstash-policy",
        "phase_definition": {
          "min_age": "0ms",
          "actions": {
            "rollover": {
              "max_size": "5gb",
              "max_age": "1d"
            }
          }
        },
        "version": 7,
        "modified_date_in_millis": 1595398857585
      }
    }
  }
}
```


`"managed": true,`とあるため、このインデックスはILMにより管理されています。
試しにElasticsearchに検索リクエストを飛ばしてみましょう。

```console
$ curl -XGET -H "Content-Type: application/json" localhost:9200/logstash-2020.07.28/_search -d '{"size": 2, "sort": [{"@timestamp": "desc"}]}' | jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2502  100  2457  100    45   5906    108 --:--:-- --:--:-- --:--:--  6014
{
  "took": 391,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 242,
      "relation": "eq"
    },
    "max_score": null,
    "hits": [
      {
        "_index": "logstash-default-2020.07.28-000001",
        "_type": "_doc",
        "_id": "OXzek3MBhNn_Y01M22q7",
        "_score": null,
        "_source": {
          "log": "2020-07-28 05:21:57 +0000 [info]: #0 [filter_kube_metadata] stats - namespace_cache_size: 2, pod_cache_size: 2, namespace_cache_api_updates: 4, pod_cache_api_updates: 4, id_cache_miss: 4\n",
          "stream": "stdout",
          "docker": {
            "container_id": "65a884bccf20d3134d96f836a3a1a9e1116bee9fd01a8298206b54baf9340f84"
          },
          "kubernetes": {
            "container_name": "fluentd",
            "namespace_name": "kube-system",
            "pod_name": "fluentd-hqh9n",
            "container_image": "fluent/fluentd-kubernetes-daemonset:v1.11.1-debian-elasticsearch7-1.3",
            "container_image_id": "docker-pullable://fluent/fluentd-kubernetes-daemonset@sha256:af33317d3b8723f71843b16d1721a3764751b1f57a0fe4242a99d1730de980b0",
            "pod_id": "fdfd5807-8164-4907-a1a9-9b782c3eb97e",
            "host": "minikube",
            "labels": {
              "controller-revision-hash": "57997fd64d",
              "k8s-app": "fluentd-logging",
              "pod-template-generation": "1",
              "version": "v1"
            },
            "master_url": "https://10.96.0.1:443/api",
            "namespace_id": "5c1df0cc-d72b-4c24-a4af-a8d595d62713"
          },
          "@timestamp": "2020-07-28T05:21:57.002062084+00:00",
          "tag": "kubernetes.var.log.containers.fluentd-hqh9n_kube-system_fluentd-65a884bccf20d3134d96f836a3a1a9e1116bee9fd01a8298206b54baf9340f84.log"
        },
        "sort": [
          1595913717002
        ]
      },
      {
        "_index": "logstash-default-2020.07.28-000001",
        "_type": "_doc",
        "_id": "gWLek3MByRVHYKQQ2yC5",
        "_score": null,
        "_source": {
          "log": "2020-07-28 05:21:56.801939 I | mvcc: finished scheduled compaction at 59275 (took 3.825907ms)\n",
          "stream": "stderr",
          "docker": {
            "container_id": "8ece6d2d408533810f2bc33e9aeeb534ea2781259c8046331b297c446ec24fe9"
          },
          "kubernetes": {
            "container_name": "etcd",
            "namespace_name": "kube-system",
            "pod_name": "etcd-minikube",
            "container_image": "k8s.gcr.io/etcd:3.4.3-0",
            "container_image_id": "docker-pullable://k8s.gcr.io/etcd@sha256:4afb99b4690b418ffc2ceb67e1a17376457e441c1f09ab55447f0aaf992fa646",
            "pod_id": "27093604-c8b7-4b22-a0df-c7eebe63afb3",
            "host": "minikube",
            "labels": {
              "component": "etcd",
              "tier": "control-plane"
            },
            "master_url": "https://10.96.0.1:443/api",
            "namespace_id": "5c1df0cc-d72b-4c24-a4af-a8d595d62713"
          },
          "@timestamp": "2020-07-28T05:21:56.802153342+00:00",
          "tag": "kubernetes.var.log.containers.etcd-minikube_kube-system_etcd-8ece6d2d408533810f2bc33e9aeeb534ea2781259c8046331b297c446ec24fe9.log"
        },
        "sort": [
          1595913716802
        ]
      }
    ]
  }
}
```


確かにElasticsearchにk8s内部で発生したログがストアされていることが確認出来ました。

### まとめ

FluentdのDaemonSetにより、k8s内部のログをILMを有効化してElasticsearchにストアするやり方を解説しました。
この記事の方法でElasticsearchのILMを有効化する場合、helmを使っているのであればElasticvsearchをデプロイする際に注意点があります。[helmの公式リポジトリでデプロイできるElasticsearch](https://github.com/helm/charts/tree/master/stable/elasticsearch)は古く、
[Elasticsearchの開発元のもの](https://github.com/elastic/helm-charts/tree/master/elasticsearch)を使ってhelmでElasticsearchのクラスターをデプロイしてください。

また、記事中で使用しているILMのポリシーは2日経ったらインデックスを消去するという単純なものですが、実際のプロダクション環境ではhotのあとにwarmやcold状態を挟んでdeleteに移行するポリシーを作成するよう検討してください。

当社では、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Fluentdサポートサービス](/services/fluentd.html)を提供しています。Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるSIer様、サービス提供事業者様は、[お問い合わせフォーム](/contact/)よりお問い合わせください。

[^0]: fluent-plugin-elasticsearch v4.1.1でILM関連のバグは直しました。
