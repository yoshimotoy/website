---
tags:
- fluentd
title: Fluentd UIのFluentd v1対応のロードマップ
---
[fluentd-ui](https://github.com/fluent/fluentd-ui)というFluentdの設定を管理できるWebアプリケーションがあります。
Fluentd v1 がリリースされる前から機能の追加やFluentdの新しい機能への対応はされていませんでした。
<!--more-->


手を入れる前は、以下のような状態でした。

  * Rails 4.2.8

  * Fluentd v0.12

  * Vue.js v0.11.4

  * sb-admin-v2

  * filter非対応

  * label feature非対応

  * systemd非対応

  * reload非対応

  * Fluentd v1に対応していないプラグインがおすすめプラグインに載っている

2018年4月中旬から手を入れ始めて、新しいバージョンをいくつかリリースしました。

  * v0.4.5

    * 約一年ぶりのリリース

    * Rails 4.2.10に更新

    * 使用しているgemを一通り更新

    * poltergeistからheadless chromeに移行

  * v1.0.0-alpha.1

    * Rails 5.2.0に更新

    * Fluentd v1のサポートを開始し、Fluentd v0.12以前のサポートをやめた

    * Vue.js v2.5.16 に更新

    * startbootstrap-sb-admin 4.0.0 に更新

    * JavaScript まわりを sprockets から webpacker に移行 [^0]

  * v1.0.0-alpha.2

    * v1.0.0-aplha.1で動かない箇所があったのを修正した

今後の予定は以下の通りです。

  1. `Fluent::Config::ConfigureProxy#dump_config_definition`で得られる情報を利用して、設定UIを構築する

  1. 複雑な設定を持つプラグインは個別にフォームを作成し、使いやすくする

  1. owned plugin (parserやformatterなど)の設定方法について検討し、実装する

  1. テストの修正

  1. filterサポート

  1. label featureサポート

  1. おすすめプラグインの更新

  1. [issues](https://github.com/fluent/fluentd-ui/issues)の対応

3まで完了したらalpha.3かbeta.1をリリースする予定です。

[^0]: CSSはsprocketsのまま
