---
tags: []
title: サポートエンジニアNight vol.3
---
「サポートエンジニア」と聞いて、皆さんはどのような職務内容を想像するでしょうか。ついたてで仕切られたブースに大勢の人が並んで座っている？　開発経験の浅い、あるいは経験が無い人が、マニュアルに従って問い合わせを右から左にさばいている？　突っ込んだ事を質問されたら「仕様です」で煙に巻く？　エンドユーザー向けの無料の電話サポートだとそのような形態のサポートもあるようですが、BtoB（法人向け）のテクニカルサポートは、それとはだいぶ状況が異なります。
<!--more-->


去る2018年7月18日、そのようなサポート業務に関わる人達の情報交換を目的としたイベントである[サポートエンジニアNight vol.3](https://techplay.jp/event/676489)が開催されました。[発起人の方による背景説明](https://blog.torut.tokyo/entry/2018/07/11/085308)では、BtoBのテクニカルサポートが技術的な問題解決力を要求される物であり、ビジネス上も非常に重要な役割となっている事が語られています。

当社は自社Webサービスは運営しておらず、またパッケージ製品の販売も行っていませんが、WebブラウザのFirefoxやE-mailクライアントのThunderbird、全文検索エンジンのGroonga、自社開発したmilter managerのほか、ご相談に応じて様々なフリーソフトウェアのテクニカルサポートを法人向けに提供しています。その過程で必要となる「調査の仕方」や「開発元へのフィードバックの仕方」などはフリーソフトウェア開発一般の知見と言う事ができ、ノウハウも広く知られていますが、それらをお客様にサービスとして提供しビジネスとして成立させる部分については、我流でどうにか回してきたというのが正直なところです。ノウハウとして確立しているとは言い難く、新たに採用したメンバーにサポート業務に関わってもらう際には特に、知見の伝達に苦労しています。この記事では、サポートエンジニアNight vol.3で発表されていた知見について、当社のサポートビジネスでの知見も交えつつまとめてみます。

（なお、会場入り時間の関係でTreasure Dataとはてなの事例については発表を聞けておりません。）

### Sider, Inc.の事例

<script async class="speakerdeck-embed" data-id="4f81dc31680943fb9650c58d318b0ff4" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>


#### 専任のサポート担当者をいつ置くべきかという問題

GitHub上でのプルリクエストに対して自動コードレビューを行うサービスである[Sider](https://sider.review/ja)では、元々専任のサポート担当者は置かれておらず、開発チームの人員がサポートを兼任するという体制だったそうです。その後、サポートにかかる負荷の増大を受けて、段階的にサポート体制を改善していったという事が述べられていました。

  * 専任のサポート担当者がいない時期には、「気付いた人が対応する」というモデルから始まった。

    * 問い合わせがあると、その情報が社内のSlackに流れてくるので、それに気がついた人が対応する。

    * このモデルだと、重い開発タスクが発生しているときにサポートに手が回らない事があった。

  * 週替わりで担当者をローテーションする方式（ラウンドロビン方式）にしてみた。

    * このモデルでも、担当者が不在の場合は結局元と同様に「誰か気付いた人が代わりに対応」する必要があった。

  * 専任のサポートエンジニアという役割を設ける事にした。

    * 重めの開発タスクは振られなくなった。

    * ドキュメントのメンテナンスは職責外だが（おそらくサポートの過程で発覚した不備は）対応している。

    * 開発タスクも、期限が切られていない（重くない）物については対応している。

    * サービスのバックエンドとして様々なOSSを使用しているため、サポートの中でそのOSS製品にある不具合に遭遇した場合には、アップストリームへのフィードバック（コントリビュート）も行う。

    * サービスの性質上、サポート業務上でも様々なコードに触れる必要がある。

具体的な数字として、直近7.5ヶ月での対応件数は291件だったそうです。1ヶ月の営業日を仮に20日と仮定すると、291÷(20×7.5)≒4.2で1営業日あたり4件強のサポートをこなしている計算になるでしょうか。テクニカルサポートでコンスタントに1日4件の問い合わせを1人でさばくというのは、当社の場合は相当忙しいという印象[^0]です。

#### サポートをやりやすくするための工夫

サポートを効率よくこなすための方法としては、以下のような事が語られていました。

  * ユーザー側から見えるステータスとは別に、内部的にはエラーのステータスがさらに細かく分かれており、それを見ればだいたいの見当がつくようになっている。

  * Inspectlet（行動トラッキングツール）を採用しており、UIに関する問い合わせにおいて、ユーザーが実際に行った操作がどのようなものであったかを調べられるようにしている。

  * 様々な手法を駆使し、なるべくユーザーの手を煩わせないで問題を解決できるようにしている。

エラー情報をリッチな物にしておく事は、ソフトウェア開発において一般的に大事な事です。というのも、最初に伝えられるエラーに十分な情報が出力されていれば、そこからの原因調査が楽になるからです。エラー情報が不十分だと、まず同じ現象を再現させる所から取りかからなくてはならず、再現できなけれは調査は大幅に困難なものとなります。当社のFirefox/Thunderbirdサポートにおいても、Firefox自体が持つエラーコンソールの内容や、より低レベルのログの収集を依頼する事が多々あります。

#### サポート品質をどう定めるべきかという問題

現状の問題点としては、以下のような事が述べられていました。

  * サポート品質の指標が定まっていないため、「なるべく早く返す」「なるべく詳しく説明する」といった事について、担当者個人の裁量任せとなってしまっている。

  * 現状ではまだ担当者が一人だけのため、不在時にどうすればよいかについては未解決のままになっている。

  * サービスは世界中で使われるため、問い合わせの10〜20％は海外から寄せられる物であるが、タイムゾーンが異なるため、問い合わせが日本での業務時間外にくる事がある。

    * そのような問い合わせに即時対応しようとすると、労働基準法に抵触する事になるため、即時対応ではなく翌日対応としている。

    * どうしてもという場合には、労働基準法が適用されない管理職の人に対応してもらうという方法がある。

当社のサポートサービスの場合、約款によって「サポートの回答期限はN営業日以内」「サポートの受付時間を何時から何時までと定め、その時間外に発生した問い合わせについては翌営業日を起算日とする」という事を明記しています。基本的には可能な限り早く対応していますが、問い合わせが同時多発的に発生した場合にはそのすべてに1日以内で対応しきれない場合もありますし、担当者が不在の場合もあります。そのため、「諸々の条件を考慮して確実に提供可能なサービスレベル」を先に見積もっておき、そのサービスレベルの範囲内での対応をお約束するという形で、あらかじめお客様に了解を頂いている[^1]という事になります。

### 株式会社レトリバの事例

<div class="slideshare-wide">
<iframe src="//www.slideshare.net/slideshow/embed_code/key/iqVa8dcSVoXj7C" width="640" height="398" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/HidetoMasuoka/ss-106421046" title="ソフトウェアベンチャーのサポートエンジニア" target="_blank">ソフトウェアベンチャーのサポートエンジニア</a> </strong> from <strong><a href="https://www.slideshare.net/HidetoMasuoka" target="_blank">Hideto Masuoka</a></strong> </div>
</div>


音声認識のソフトウェアパッケージを提供している[レトリバ](https://retrieva.jp/)では、「研究部門」で生み出された技術を「製品開発部門」で製品に落とし込み、「営業技術部門」で販売やサポートを行っているそうです。このため、テクニカルサポートは営業技術部門の担当範囲となっていて、他の業務との兼務でサポートを行われているとの事でした。

#### サポートへの問い合わせが減るようにするという事

こちらの会社では、サポートにあたっては以下の事を大事にされているそうです。

  * お客様が困っている問題を迅速に解決する事

  * お客様が困っている課題を製品にフィードバックし改善して、お客様の課題を解決する事

  * お客様にスキルを伝達し、なるべくサポートに問い合わせなくても良いようにする事

一見してユニークなのは3番目の点です。近視眼的に考えれば、お客様からのお問い合わせを受けてさばくのがサポートの仕事である以上、問い合わせ件数を減らすのは自分で自分の仕事を減らして、自分の存在意義を減じる事に思えるかもしれません。ですがより大きな視点で考えると、問い合わせが減る事には以下のようなメリットがあります。

  * 簡単なトラブルはお客様側で自己解決してもらえるようになると、お客様のビジネスがより円滑に進むようになる。

  * 問い合わせが減るとサポート業務が減り、その分他の事に時間を割けるようになる。

  * 簡単で退屈な作業が減って、困難な少数の仕事にじっくり取り組めるようになると、純粋な技術者視点においてやりがいが増す。

  * 問い合わせに多く答える事が直接的な収益増につながらない契約形態なのであれば、問い合わせ対応に要する工数と収益には負の相関関係が成立する。端的に言うと、問い合わせが発生しない方が助かる。

パッケージの販売や製品の使用権のサブスクリプション契約を伴わない、純粋なサポート契約のみを行っている当社の場合でも、これらの点はメリットになっています。というのも、当社のサポート契約はインシデントサポート[^2]なので、上記の4番目の点と同じ事が言えるからです[^3]。

ただ、どの程度まで「お客様を育てる」かは場合によりけりであるとのことでした。サポート提供側の狙い通りに自己解決できるようになってくれるお客様ばかりではなく、中には、これについて教えてくれるのであればあれもこれも教えてほしい、と本来のサポート範囲以上の事を求めてくるタイプのお客様もいるそうで、そのようなケースではある程度のところで対応を打ち切る必要もあるようです。当社のサポートにおいても、障害の発生原因を切り分けて対応責任の有無を明確にする事を心がけています。

#### サポート提供者もお客様も、どちらも「完璧」ではないという事を前提にした運用

また、それ以外のトピックとしては、過去のサポート対応事例はその都度チケットに記録を残しておき、同じお客様から同じ問い合わせがあった時に時間をかけずに回答したり、何月何日に回答済みと返したりできるようにしておく、という事も行っているとのお話がありました。お客様側も人間である以上、忘れるという事はありますし、また担当の方が変わった時に十分な情報が引き継がれていなかったり、複数の担当の方がいる場合に情報が随時共有されていなかったりという事は起こりえます。またサポート担当者が複数名いる場合には、サポート提供者の側でも情報を共有しておく必要があります。

当社でも、サポートのお問い合わせはすべてRedmineのチケットで管理しており、基本的な運用は以下のような形を取る事が多いです。

  * 新しい問い合わせが発生した場合、問い合わせ内容を説明文として、新しくチケットを作成する。

  * その問い合わせに対して回答を返し終えたら、回答文を注記として加える形で、ステータスを「解決」に設定する。

  * 回答に対し再度質問があった場合や、回答の中で「この点を調査した結果をお知らせください」と記載した部分について情報をご連絡いただいた場合、その問い合わせや情報を注記やチケットへの添付ファイルとした上で、ステータスを「フィードバック」に設定する。

  * ステータスが「解決」または「終了」でないチケットは、すべて「こちらがボールを持っており、こちらから連絡（回答）しなければならない」状態として取り扱う。

ただし、当社では、お客様からのお問い合わせのメールを元にRedmineに起票して、それ以後のやりとりをそのチケット上で管理するという場合と、お客様自身の手でRedmineに直接起票を頂く場合の両方をお客様ごとに切り替えています。メールベースでのサポートの場合、メールの本文がすべてRedmineのチケットの情報として記録される形になっており、過去に回答済みの情報はRedmineの検索機能を使って見つける事になります。

### Herokuの事例

<script async class="speakerdeck-embed" data-id="382697a78c7d474a8552ec10e6fe7ca7" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>


#### チケットベースでのサポート運用

[Heroku](https://www.heroku.com/)では、サブスクリプションモデルである事からユーザーの離脱率を下げる事が重要視されているようで、単なる「問い合わせ窓口」にとどまらず、サポートの質を高める事を強く意識しているという事が述べられていました。

  * サポートの受付はすべてチケットの起票を以て行う運用をとっている。

    * チケットが起票されると、サポートエンジニアが閲覧しているSlackに通知が流れるようになっている。

  * 第1担当者から第3担当者までの3人1ユニットを「その週のサポート担当ユニット」として、週替わりでローテーションしている。

    * その週の担当ユニットの中では原則として第1担当者が新規チケットを拾い上げるが、第1担当者が不在であれば第2担当者が、第2担当者も不在であれば第3担当者が拾い上げる。

    * サポートエンジニアは世界中の各地に在住しており、ユニット間・チーム間の連絡はインターネット越しに英語で行っている。

  * 日本語での問い合わせチケットは、日本語の問い合わせであることを示す専用の分類になっている。

    * 日本語で問い合わせがあった場合、それを英訳して、通常のサポートチケットとして起票し直す。

    * 回答が出たら、それを適宜日本語に翻訳し直して、元の問い合わせに対する回答とする。

当社のサポートでお客様に直接Redmineに起票を頂くケースは、お客様自身が開発者やサーバーの運用担当者である場合に取る事が多く、ユーザー層がITエンジニアであり要領が掴めている事が期待されるHerokuと事情が似通っていると言えそうです。

#### 日本語の表現の問題

日本語でのサポートがシステム上は特別な扱いになっているというのは、英語アレルギーの人が多いという日本固有の事情でしょうか。問い合わせ内容を日本語から英語へ・回答を英語から日本語へ翻訳する際には、以下の点が難しいとの事を述べておられました。

  * 直訳すればよいという物ではなく、ケースバイケースで時には大幅な「意訳」も求められる。

    * 書かれていない事を補ったり、書いてある内容を言い換えたりしなくてはならない。

  * サポートの回答に特有の表現などがあるためか、ビジネス日本語・敬語の取り扱いに苦慮している。

    * 「これを読んでからメールを書けば失礼がない」というような参考書が求められている。

当社のサポートサービスは日本国内のお客様が対象なので、敬語については同様の悩みを持っています。当社では特にビジネスメール研修のような事は実施していないため、サポート経験が浅い担当者が書いた回答文は手直しが必要な場合もあり、ある程度慣れるまではベテランサポート担当者が回答をレビューするという体制をとっています。教科書のようなテキストとして使える参考書籍があれば、当社でも是非導入したいところです。

#### サービスへのフィードバック

問い合わせに対する回答以外での顧客満足度に寄与する取り組みについても紹介がありました。

  * サービスのステータス（負荷の状況、障害の発生状況や復旧状況）は積極的・自発的に公表するようにしている。

    * 障害が発生しない安定したサービス、であるかのように装おうとして障害の発生を隠そうとしても、ユーザーのリテラシーが高いためすぐにボロが出る。

    * それよりも、正確な情報を公開しておく事の方が信頼に繋がる。

  * 同じ問い合わせが何度も発生する状況は、サービスの異常や不備の兆候と考えて、サービス運用に適宜フィードバックする。

  * FAQ（よくある質問と回答）というレベルに至っていなくても、受けた問い合わせに基づく特定の事例でも、ナレッジとして文書にまとめて公開する。

    * ナレッジからサービス・製品にフィードバックする場合もある。

先のレトリバ社の事例でも同様の話があったと思われますが、同じ問い合わせが度々発生するというのは不調や不備の兆候と考えるのが妥当で、FAQを用意して終わりと片付けて良いものではありません。皆が躓く石があるのであれば、まずはそれを取り除くのがベストで、避け方をノウハウとして周知するのはあくまで次善の策です。サービス品質やユーザー体験の向上のための情報が集まる窓口として、サポートが大事な役割を担っているという事がよく分かる事例ですね。

当社の場合も、自社開発製品でないフリーソフトウェアのサポートにおいて未知の不具合に遭遇する事は度々あり、原則としてそれらは[アップストリームにもフィードバックするよう努めています](/blog/category/feedback.html)。また、Redmineにお客様自身で起票を頂くケースでは、ナレッジ蓄積・共有の場としてRedmineに付属のWikiをお客様自身がお使いになっている場合もあります。

#### かなり割り切った評価システム

サポートそのものの品質に関わる話として、得られたサポートに対する顧客からの評価・フィードバックという物があります。満足度を5段階評価で入力するなど、この種のフィードバックの収集方法には様々なやり方があり得ますが、詳細なアンケートをとろうとすればするほど顧客側の負担が増えてしまうため、どこまで質問するかというのは非常に難しい問題です。

Herokuでは（一旦10段階評価を導入したものの意味が無かったので改めた、というような経緯があったわけではなく、）当初から単純な「良かった or 悪かった」の2値入力にしているとの事でした。これについて、サポートエンジニアの方々は元気がない時に「良かった」評価を眺めて元気を出すというような「使い方」もされているそうです。製品開発での「リリース」のような分かりやすい成果が見えにくいサポート職において、従事する人自身のモチベーションを高める一つの方法として参考になる事例だと言えるでしょう。

#### 密なコミュニケーションを心がける事

会社のメンバーが各国に離れていることから、エンジニア間でのコミュニケーションには、メール、Slack、Google Hangoutでのビデオチャットなど様々な方法を併用しているそうです。中でも、特に文字情報でのコミュニケーションにおいては、「思っていて表情には出ているが、文字には現れない」というような、その通信手段上では伝達されない情報からくる微妙な齟齬をなくすために、大げさなくらいに感情や状況を報告するという、いわゆるオーバーコミュニケーションを心がけているという事が語られていました。

それでも全く対面した事がないままでコミュニケーションを続けていると伝わらない部分が出てきてしまうそうで、年に何回かというペースで全社的に一カ所に実際に集まり、お互いに顔を見て対面しながら話す機会を設けて、感謝の言葉などのやりとりをしているそうです。

#### サポートにおける、スピードと品質のトレードオフについて

質疑応答の際には、Herokuの有償サポートの「1時間や30分といった時間の中での回答を保証」というプランについて、「回答の速度と正確性のどちらを優先しているのか？」という質問が寄せられていました。これについては明確に「正確性を優先している」とのことで、回答内容に確信を持てない時などは、時間がかかる旨を伝えた上できちんと調べてから回答をしているそうです。そのように時間がかかるとお客様からの評価が下がるのでは？　と心配する所ですが、実際の所は、回答の質が高ければユーザーからの評価が悪くなるという事は無いという感触があるそうです。

当社の場合、調査に非常に時間がかかるようなお問い合わせを受けた時には、まず1時間から数時間という一定時間の範囲内で判明した事実を元に、「現時点ではここまでは分かっていますが、ここから先は未調査領域のため不明です。N時間の範囲内で継続調査を実施しますか？」とその都度判断を仰ぐという運用をとっています。これは、お客様によって、あるいは同じお客様でも状況によって、回答のスピードと正確性・詳細さのどちらを優先するかが変わるためです[^4]。

### Microsoftの事例

Microsoft社の事例紹介では、リクルーティング用の資料を引用しながら、キャリアとしてのサポートエンジニア[^5]という点に焦点を当てて話されていました。

#### Microsoft社内でのサポートエンジニアの位置付け

Microsoft社内では、サポートエンジニアが属するセクションは開発やセールスなどの他のセクションとは別の、独立性の高い組織として存在しているそうです。

  * 職務上、開発部門にコンタクトをとるための正式な権限や、ソースコードのリポジトリを閲覧するための権限などが与えられている。技術の最後の砦、ゴールキーパーと言えるような位置づけである。

  * ミッションを一言で言うと「Microsoftのファンを作る仕事」。

  * 国ごとのチームの結びつきが強い他部署に比べると、サポートエンジニアは「全世界のサポートエンジニアが属する1つの集団」という性質が強い。

    * サポートエンジニア全員が書き込めるOneNoteを使用して情報共有・集積を図っている。

Microsoftのサポートエンジニアが本当にこれらの権限を持っているという事は、過去の対応事例からも分かります。過去、当社のお客様がMicrosoftとサポート契約を結んでいたことから、Windows上でFirefoxを使用した際に発生する希な現象について調査を行った事がありましたが、Firefoxのソースコードを調査した結果を踏まえた当社の回答に対し、Microsoft側もソースコードレベルでの調査結果を出してきてくれて、責任範囲が明確になったという事が実際にありました。

国際的なコミュニケーションという点では、発表中では特にアジア圏の各国にまたがったコミュニケーションについて紹介されていました。アジア各国のスタッフは、英語ネイティブでない人ばかり（基本的に第二言語として英語を使っている人ばかり）なので、音声での会話よりも文字での読み書きの方が得意という人が多いそうです。そのため、メール、チャット等、連絡事項は基本的に文章にするという文化があるとの事でした。

#### 社内でサポートエンジニアをどう評価するか

Herokuの事例でも触れられていましたが、Microsoftでもサポートエンジニアの評価の仕組みは独特な物になっているそうです。

  * 回答の数が多いからといって優秀と評価されるわけではない。

  * 車輪の再発名は評価されない。既にある物は、極力そのまま使う事が評価される。

  * 一人で抱え込むよりも、他の人に助けを求めている方が評価が高くなるようになっている。

    * 他者を巻き込んで、他者の成功に貢献する事が評価に繋がる。

    * 「誰々さんのおかげで回答できた」「誰々さんが書いてくれたドキュメントに助けられた」といったフィードバックを行う社内ツール[^6]があり、そのデータが評価の指標になる。

当社の場合、会社自体の人数規模が非常に小さい事もあって、厳密なデータに基づく評価制度という物はまだありません。しかし、ある程度以上の規模になり「お互いの存在を知らない」スタッフ同士という関係があり得る状態になってくると、公正な運用のためにも数値に基づく評価制度は必要になってくるでしょう。

#### サポートエンジニアのキャリアに求められる物、得られる物

以上のような職務内容から、Microsoftにおけるサポートエンジニアには以下の能力が求められ、また、職務を通じてこれらの能力が高まるという事が語られていました。

  * 技術力（調査力、問題解決力）

  * コミュニケーション力（交渉力、チームワーク力）

  * 英語力

今回発表を見る事ができたいくつかの企業においても、また当社においても、これらは同様の事が言えます。テクニカルサポートの業務で行う事は、既にあるプロダクトのバグ報告や質問に対応する際に行う調査・検証の作業と、かなりの部分が共通しています。一般的に言って、バグ修正の作業の大部分は調査が占めており、調査の正否が修正の正否を決めると言っても過言ではないでしょう。また、新機能の開発などにおいてエンドユーザーや顧客の要望を的確に吸い上げるためにはコミュニケーション力が求められますし、スケジュール調整や、その機能を次のリリースに含めるかどうかなど、意見や利害の調整・交渉が必要になる場面もあります。最新情報を調査するためには、翻訳を待っていては遅すぎるため英語で元資料を参照する事が欠かせませんし、トラブルの類似事例調査についても、日本語だけで調査するよりも英語で公表されている事例も調査範囲に含められた方が、より有用な情報に辿り着きやすいです。

こうして考えると、テクニカルサポートは開発に特化したスキルセットとはまた異なる、技術とコミュニケーションの総合力が求められる業務であると言えるのではないでしょうか。

### まとめ

以上、サポートエンジニアNight vol.3にて聞く事ができた他社でのテクニカルサポートの事例を、当社で提供しているテクニカルサポートの運用事例を交えつつご紹介しました。

具体的なプロダクトやサービスという成果が目に見える形で世に発表される「開発」や、売り上げが数字として出る「営業」に比べると、サービスの安定した稼働を支える「運用」や、トラブル発生時の問い合わせに対応する「サポート」という分野は、成果が可視化されにくい性質があります。職務の性質上公に語る事ができない情報も多いので、どういう人がどういう働き方をしているのかという情報もあまり表に出てこず、また、メディアに大々的に取り上げられてモデルケースになるような人物もそうそういません。学生さんなどにとっては「イメージしにくい」という印象だけでもあればまだいい方で、そもそも存在すら意識されないという事になってしまうのも致し方ないでしょう[^7]。

実際のサポートエンジニアは、業務を遂行する上でITエンジニアとしての総合力が求められる仕事と言えます。突き抜けた開発力には自信がないが、技術もコミュニケーションもそつなくこなせる、というジェネラリスト型の人は、自分の特性を活かせる職としてサポートエンジニアにも目を向けてみてはいかがでしょうか？

[^0]: 当社では各メンバーがサポート専任ではなく開発案件等と兼任している事が多い事と、対象製品のソースコードを深く調査するようなケースでは集中力の消費が激しい事から、実際に1日で対応可能な上限はこのくらいになる場合が多いです。

[^1]: このような取り決めを一般的に「Service Level Agreement（SLA）」と言います。

[^2]: 平たく言うと、回数券のような契約形態。問い合わせ1件につきいくらという都度支払い/従量制とは異なり、あらかじめ決まった回数/時間の問い合わせが可能な権利を販売する、という形をとります。

[^3]: ただ、サポート契約を単独で提供している当社の場合には、この点を推し進めると「これだけ製品が安定していて問い合わせも日常的には発生しなくなったなら、もうサポート契約は不要なのでは？」とお客様が判断され、サポート契約が継続されなくなる、という可能性は出てきます。そのような状況においてのサポート契約は「いざ何かトラブルが起こったときのための保険」としての性質が強まってくるため、常日頃からどんなトラブルでもきちんと解決できるという信頼を積み重ねていく事がなおさら重要だと言えるでしょう。

[^4]: 回答のスピードだけを気にして、未調査の事についてまで思い込みで不正確な回答をしてしまうと、その回答を信じて運用したお客様に不利益を与えてしまい、ひいては自社の信用も損ないます。また、もし知っていたら教えて欲しいという程度のつもりだった質問に対して、勝手な忖度を行って何十時間と先走った調査をして工数を消費してしまうと、それをインシデントとして消費されればお客様の不利益になりますし、かといってインシデント消費なしでの無償対応とすればやはり自社の不利益になります。最初に簡単な範囲で一次回答を返して以後の判断をゆだねるというのは、自社にとってもお客様にとっても最も不利益が発生しにくい方法ではないか、というのが当社の見解です。

[^5]: Microsoft社内では「Customer Service and Support」という呼び方になるそうです。

[^6]: 残念ながら、これ自体は公開されておらず、製品としても販売されていないとのこと。

[^7]: そういう理由から「あこがれの職業」にもなりにくく、多くの会社でサポートエンジニアの採用や育成は難しい課題となっているようです。
