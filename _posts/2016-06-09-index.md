---
tags:
- groonga
- presentation
title: 'MySQLとPostgreSQLと日本語全文検索2：初心者向けMroonga・PGroonga情報 #mypgft'
---
6月9日に「[MySQLとPostgreSQLと日本語全文検索2](https://groonga.doorkeeper.jp/events/41770)」というイベントを開催しました。今回も[DMM.comラボ](http://labo.dmm.com/)さんに会場を提供してもらいました。当日のツイートは[MySQLとPostgreSQLと日本語全文検索2 - Togetterまとめ](http://togetter.com/li/985580)にまとまっています。
<!--more-->


[2月9日に開催した1回目のイベント]({% post_url 2016-02-09-index %})では次の2つのことについて紹介しました。

  * MySQLでの日本語全文検索の歴史と実現方法

  * PostgreSQLでの日本語全文検索の歴史と実現方法

2回目となる今回は次のことについて紹介しました。

  * MySQL 5.7 InnoDBの日本語全文検索機能の1歩進んだ使い方

    * 発表資料：[MySQL 5.7 InnoDB 日本語全文検索（その２）](http://www.slideshare.net/yoyamasaki/mysql-57-innodb)

  * [pg_bigm](http://pgbigm.osdn.jp/)の1歩進んだ使い方

    * 発表資料：TODO（じきに公開されるはずです。）

  * [Mroonga](http://mroonga.org/ja/)・[PGroonga](http://pgroonga.github.io/ja/)の1歩進んだ使い方

    * 発表資料：[初心者向けMroonga・PGroonga情報](https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search-2/)

  * PGroongaの利用事例（[VVAULT AUDIT](http://vvault.jp/product/vvault-audit/index.html)での利用事例）

    * 発表資料：[VVAULT AUDITにおけるPGROONGAの利用](http://www.slideshare.net/NaokiTakami/vvault-audit-pgroonga)

ここではMroonga（むるんが）とPGroonga（ぴーじーるんが）の1歩進んだ使い方について少し紹介します。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search-2/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search-2/" title="初心者向けMroonga・PGroonga情報">初心者向けMroonga・PGroonga情報</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search-2/)

  * [スライド（SlideShare）](http://www.slideshare.net/kou/mysql-and-postgresql-and-japanese-full-text-search-2)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-mysql-and-postgresql-and-japanese-full-text-search-2)

今回の発表ではMroongaとPGroongaのオススメの使い方およびレプリケーションまわりについて紹介しました。

### Mroongaのオススメの使い方

Mroongaは次のように使うのがオススメです。

  * できればストレージモードを使う

  * テーブル定義はデフォルトのパラメーターを使う（デフォルトでいい感じになるように調整してあります。）

  * 検索するときは`IN BOOLEAN MODE`と`*D+`プラグマを使う

詳細はスライドに書いていますが、以下のようなSQLで使うということです。

```sql
CREATE TABLE items (
  title text,
  FULLTEXT INDEX (title)
  -- ↑をCOMMENTでカスタマイズできるが
  --   まずはデフォルトで使うのがオススメ
) ENGINE=Mroonga
  DEFAULT CHARSET=utf8mb4;

SELECT * FROM items
  WHERE MATCH (title)
        --        ↓ *D+プラグマを使ってデフォルトでANDにする
        AGAINST ('*D+ 激安 人気' IN BOOLEAN MODE);
        --                      ↑ ブーリアンモードを使う
        --                         Web検索エンジンのような使い勝手になる
```


また、ストレージモードを使うための工夫としてスレーブだけをMroongaにしてレプリケーションする構成とその設定方法も紹介しました。ストレージモードではトランザクションを使えませんが、この構成にするとトランザクションを使わなくてもよくなります。

### PGroongaのオススメの使い方

PGroongaは次のように使うのがオススメです。

  * 主キーを用意する

  * インデックス定義はデフォルトで使う（デフォルトでいい感じになるように調整してあります。）

  * 検索は`search_path`を設定した上で`@@`演算子を使う

詳細はスライドに書いていますが、以下のようなSQLで使うということです。

```sql
CREATE TABLE items (
  -- ↓主キーを用意する
  id integer PRIMARY KEY,
  title text,
);
CREATE INDEX pgroonga_items_index
  ON items
  --              ↓ インデックスに主キーを含める
  USING pgroonga (id, title);
  -- パラメーターは指定せずデフォルトで使う

ALTER DATABASE db1
  -- ↓ search_pathにpgroongaスキーマを入れ、pg_catalogスキーマよりも先にする
  SET search_path TO "$user",public,pgroonga,pg_catalog;

SELECT *,
       -- ↓検索スコアーを取得（このために主キーが必要）
       pgroonga.score(items) AS score
  FROM items
  --          ↓ @@演算子を使う
  WHERE title @@ "激安 人気"
  ORDER BY score DESC;
```


PGroongaのレプリケーションについても説明しました。

PGroongaはPostgreSQL標準のレプリケーション機能を使えませんが、[pglogicalと組み合わせたレプリケーション]({% post_url 2016-03-22-index %})は使えます。なお、PostgreSQL 9.6以降ではPostgreSQL標準のレプリケーション機能を使えるようにPGroongaの開発を進めています。

PostgreSQLでの日本語全文検索を実現する方法としてPGroongaに興味のある方は、PGroongaをWindows用のサーバーログ管理ソフトである[VVAULT AUDIT](http://vvault.jp/product/vvault-audit/index.html)での利用事例「[VVAULT AUDITにおけるPGROONGAの利用](http://www.slideshare.net/NaokiTakami/vvault-audit-pgroonga)」も合わせて参照してください。

### まとめ

6月9日に開催された「[MySQLとPostgreSQLと日本語全文検索2](https://groonga.doorkeeper.jp/events/41770)」でのMroongaとPGroongaの発表内容について紹介しました。

MySQL・PostgreSQLで日本語全文検索したい場合はぜひこれらを検討してみてください。
