---
tags: []
title: Let’s Encryptをcloudapp.azure.comで使おうとするときのハマりどころ
---
### はじめに

誰でもSSL/TLS証明書を無償で取得できることを標榜しているサービスとして、[Let's Encrypt](https://letsencrypt.org/)があります。2016年4月に正式サービスの提供が開始されたことから、実際に試してみた人も多いことでしょう。
<!--more-->


今回は、Let's Encryptをcloudapp.azure.comで使おうとするときのハマりポイントを紹介します。

> 2016/06/17 Kazuhiro NISHIYAMAさんに記事内容の誤りを[letsencrypt の Rate Limit について](http://blog.n-z.jp/blog/2016-06-17-letsencrypt.html)というエントリで指摘していただきました。ありがとうございます。Rate Limitの回避方法の言及が不十分だった箇所、適用例外についての記述が誤っている箇所、ステージング環境を利用する場合のコマンドラインオプションがある旨の3点について記述をあらためました。


### cloudapp.azure.comについて

cloudapp.azure.comはAzureで仮想マシンを立ちあげて、公開するときの既定のドメインです。
次のようなルールでDNSを設定できます。このうちロケーションは東日本リージョンであればjapaneastになります。

  * <指定の名前>.<ロケーション>.cloudapp.azure.com

### cloudapp.azure.comでLet's Encrypt

お使いのディストリビューションにcertbotがあればそれを、ない場合や最新のcertbotが利用したければ、[certbot](https://certbot.eff.org/)のサイトからクライアントをダウンロードします。

```
$ wget https://dl.eff.org/certbot-auto
$ chmod a+x certbot-auto
```


あとは次のようにダウンロードしたスクリプトを実行するだけ、と思うかも知れません。

```
$ ./certbot-auto
```


Azure上とはいえ、Linuxディストリビューションを使う分には何も問題なさそうではありますが、ここにハマりポイントがあります。

実際に実行してみると、次のようなエラーに遭遇しました。（2016年6月時点）

```
There were too many requests of a given type :: Error creating new cert :: Too many certificates already issued for: azure.com
Please see the logfiles in /var/log/letsencrypt for more details.
```


エラーメッセージで検索してみると、このエラーに遭遇している人が他にもいることがわかりました。

  * [Too many certificates already issued for: azure.com](https://community.letsencrypt.org/t/too-many-certificates-already-issued-for-azure-com/15404/2)

この投稿からたどれるリンクには、Let's Encryptの制限に関するものがありました。

  * [Rate Limits for Let’s Encrypt](https://community.letsencrypt.org/t/rate-limits-for-lets-encrypt/6769)

細かな制限はいろいろありますが、どうやら特定ドメインに対しては、週に20個のSSL/TLSサーバー証明書しか発行しないようです。そのため*.cloudapp.azure.comなどのように、皆がこぞってSSL/TLSサーバー証明書を取得しに行くような場合、見事に制限にひっかかってしまいます。

これを回避するには別途独自にドメインを取得したり、Dynamic DNSやIaaSなどを利用している場合はサービスプロバイダーに対応（後述）してもらったりしなければなりません。

> 2016/06/17訂正：DDNSやIaaSについての言及を追加。


### 適用例外はありますか？

*.cloudapp.azure.comで上記の制限がかかるとなると、大分厳しいように思えます。

申請すれば、制限を緩和してもらえるというような情報もありましたが、本当にそういったものはあるのでしょうか。

実際に疑問に思って質問しているケースがありました。

  * [Where is the rate limit exemption request form?](https://community.letsencrypt.org/t/where-is-the-rate-limit-exemption-request-form/16185/9)

LE Tech AdvisorであるJacob氏の発言を読む限りでは、残念ながら個別に申請して例外扱いしてもらう方法はなさそうです。

ただし、後から教えてもらったのですが、[DDNS Rate Limited](https://github.com/certbot/certbot/issues/1607)や[Too many certificates already issued for dynamic dns provider sub domain](https://github.com/certbot/certbot/issues/2186)で言及されているように、次の条件を満たせば例外扱いされるようです。

  * サービスプロバイダー（azure.comなのでMicrosoft）によってドメインを明示的に[Public Suffix List](https://publicsuffix.org/)に入れてもらう。具体的には[Guideline](https://github.com/publicsuffix/list/wiki/Guidelines)に従って手続をしてもらい、[`public_suffix_list.dat`](https://publicsuffix.org/list/public_suffix_list.dat)へazure.comを追加する [^0]

  * 更新されたリストを[Let's Encryptのサーバー](https://github.com/letsencrypt/boulder)にも反映してもらう

ただし、上記の条件を満たすのはすぐにというわけにはいきません。

> 2016/06/17訂正：個別に申請する方法がない点、サービスプロバイダーに対応してもらう方法を追記。


### 更新や再取得の場合は？

ここまで厳しいのは、あくまで新規取得の場合です。
更新する場合や、再取得の場合は影響はないようです。

### 正式版じゃなくてもいいから試してみたい

正式なSSL/TLS証明書じゃなくても、certbotを試してみたい、というのであればステージング環境があります。

  * [Testing Against the Let’s Encrypt Staging Environment](https://community.letsencrypt.org/t/testing-against-the-lets-encrypt-staging-environment/6763)

こちらは、ステージング環境だけあって、制限がゆるいです。*.cloudapp.azure.comでも問題なくSSL/TLSサーバー証明書を取得できます。

その場合には次のコマンドラインオプションを使います。

```
  --test-cert, --staging
                        Use the staging server to obtain test (invalid) certs;
                        equivalent to --server https://acme-
                        staging.api.letsencrypt.org/directory (default: False)

```


> 2016/06/17訂正：パッチをあてなくてもコマンドラインオプションでできるので、そちらを紹介するように訂正。


この方法で取得した場合、認証局が「Fake LE Intermediate X1」になります。
正式版の「Let's Encrypt Authority X3」ではありません。アクセスするにはブラウザのセキュリティ例外などの設定が必要です。

### まとめ

今回は、Let's Encryptを*.cloudapp.azure.comの配下で取得しようとすると、制限にひっかかってしまうというハマりポイントの事例を紹介しました。
ただし、この情報は2016年6月時点のものなので今後改善されるかも知れません。

[^0]: 実際にazurewebsites.netやazure-mobile.net、cloudapp.netは登録されています。
