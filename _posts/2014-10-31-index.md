---
title: Debianでパッケージをリリースできるようにしたい - そしてDebianへ
tags:
  - debian
---
### はじめに

以前、オープンソースのカラムストア機能付き全文検索エンジンである[Groonga](http://groonga.org/ja)をDebianに入れるために必要な作業について、最初のとっかかりであるWNPPへのバグ登録やDebianらしいパッケージかどうかチェックするためのLintian、mentors.debian.netの使いかたについて紹介記事を書きました。
<!--more-->


  * [Debianでパッケージをリリースできるようにしたい - WNPPへのバグ登録]({% post_url 2014-03-07-index %})
  * [Debianでパッケージをリリースできるようにしたい - よりDebianらしく]({% post_url 2014-04-03-index %})
  * [Debianでパッケージをリリースできるようにしたい - mentors.debian.netの使いかた]({% post_url 2014-07-02-index %})

今回は、GroongaのDebianリポジトリ入りを目指す作業がようやく実を結び、Debian（unstable）に入ったことから、unstableに入るまでどんな流れだったのかを紹介します。

### mentors.debian.netにパッケージをアップロードしたその後は

mentors.debian.netにパッケージをアップロードした後のざっくりとした流れは以下の通りです。

  * スポンサーにパッケージをチェックしてもらう
  * スポンサーにパッケージをNew Queueにアップロードしてもらう
  * ftp-masterにパッケージをチェックしてもらう
  * ftp-masterがパッケージをオフィシャルリポジトリへアップロード

Debian開発者である[やまね](https://wiki.debian.org/HidekiYamane)さんによるスライドに参考となる図があります。
以下の図は「なれる！ Debian開発者 〜 45分でわかる？ メンテナ入門」の26ページのものです。

<div class="slideshare">
  <iframe src="//www.slideshare.net/slideshow/embed_code/10137088?startSlide=26" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen>
  </iframe>
  <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/henrich_d/debian-45" title="なれる! Debian開発者 〜 45分でわかる? メンテナ入門" target="_blank">なれる! Debian開発者 〜 45分でわかる? メンテナ入門</a> </strong> from <strong><a href="//www.slideshare.net/henrich_d" target="_blank">Hideki Yamane</a></strong>
  </div>
</div>


以降では、それぞれのステップを簡単に説明します。

### スポンサーにパッケージをチェックしてもらう

mentors.debian.netにアップロードしたパッケージはスポンサーにその内容をチェックしてもらわなければなりません。
Groongaの場合はDebian開発者である[やまね](https://wiki.debian.org/HidekiYamane)さんにお願いしました。

パッケージの内容にいくつか不備があったので、その点を修正してmentors.debian.netにアップロードしなおすというのを何度か行う必要がありました。

このあたりのやまねさんとのやりとりは [ITP: groonga -- Fulltext search engine](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=732055)から参照できます。

dch --teamにしていなかったり、debian/copyrightの記述が不適切だったりしていたりと、細々とした修正をしています。
この作業はスポンサーによるチェックが通るまで続きます。

### スポンサーにパッケージをNew Queueにアップロードしてもらう

スポンサーであるやまねさんのチェックを通ったら、次は[New Queue](https://ftp-master.debian.org/new.html)へのアップロードです。Groongaは新規パッケージなので、Debian開発者に行ってもらう必要があります。これはスポンサーであるやまねさんにお願いしました。

パッケージを扱う権限については以下の図が参考になります。

<div class="slideshare">
  <iframe src="//www.slideshare.net/slideshow/embed_code/10137088?startSlide=48" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen>
  </iframe>
  <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/henrich_d/debian-45" title="なれる! Debian開発者 〜 45分でわかる? メンテナ入門" target="_blank">なれる! Debian開発者 〜 45分でわかる? メンテナ入門</a> </strong> from <strong><a href="//www.slideshare.net/henrich_d" target="_blank">Hideki Yamane</a></strong>
  </div>
</div>


### ftp-masterにパッケージをチェックしてもらう

パッケージがNew Queueにアップロードされたら、あとはftp-masterにチェックしてもらえるのを待ちます。
Groongaの場合は、チェックしてもらうまでに3ヶ月ほどかかりました。

ただし、その3ヶ月間でLintianのチェックが追加されたため、あえなく初回はrejectされてしまいました。
一旦rejectされたら、再度パッケージを修正して、mentors.debian.netにアップロードするところからやりなおしです。

Groongaの場合は、バンドルしているjavascriptやcssのcopyrightが明記されていなかったので、再度のftp-masterによるチェックでrejectされてしまうという残念なことがありました。二度目のftp-masterによるチェック待ちは1ヶ月ほどでした。

New Queue待ちは結構長いため、ftp-masterによるチェックがすんなり通るためには、ライセンスまわりを特に入念にチェックするのをお勧めします。

### ftp-masterがパッケージをオフィシャルリポジトリへアップロード

ftp-masterによるチェックが通ったら、ようやくオフィシャルリポジトリにパッケージがアップロードされます。
debian/changelogに （Closes: #732055）などとしてBTSの番号を書いておくとアップロードと前述のバグが同時にクローズされます。

日々パッケージがアップロードされている様子は [Debian upload](https://twitter.com/DebianUpload) で知ることもできます。

こうして、普段unstableを常用している人はapt-getでGroongaをインストールできるようになりました。

### まとめ

今回はmentors.debian.netにパッケージをアップロードしてからDebianのオフィシャルリポジトリに入るまでの流れを簡単に紹介しました。
Debianオフィシャルリポジトリに入ったとはいえ、まだunstableにしかパッケージはありません。安定版のDebian（wheezy）には入っていないので、wheezyでは、Groongaプロジェクトで提供しているパッケージをインストールする必要があります。

今後は安定版でもbackportsからインストールして使えるようにするというのを目指すのと、肉の日リリースに継続的に追従していくのを頑張っていきます。
