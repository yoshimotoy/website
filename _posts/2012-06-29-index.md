---
tags: []
title: groonga 2.0.4で新しくgroonga-httpdが追加されました
---
### はじめに

groonga 2.0.4が6月29日にリリースされました。そのリリースの変更点の1つとして新しい実行ファイルである`groonga-httpd`が追加されました。
<!--more-->


今回は`groonga-httpd`について説明するとともに、簡単に特長を紹介します。

### `groonga-httpd`について

groongaには、`groonga`という実行ファイルが含まれています。`groonga-httpd`は`groonga`と関連しているので、まずは`groonga`について説明します。

`groonga`を使うことで、ローカルにあるgroongaのデータベースファイルに対してテーブル作成、レコード追加、全文検索などの処理を行うことができます。その他にも`groonga`はサーバーモードとして実行することもでき、リモートからも同様にテーブル作成、レコード追加、全文検索などの処理を行うことが可能です。リモートとの通信には、HTTPか独自プロトコルであるGQTPを利用します。

`groonga-httpd`はその`groonga`のHTTPでのサーバーモードの代わりとして利用できるものです。

`groonga`のHTTPサーバーの実装は独自なものですが、`groonga-httpd`は[nginx](http://www.nginx.org/)を使っています。そのためnginxの特長がそのまま`groonga-httpd`にも当てはまります。具体的には、高性能であること、nginxモジュールで様々な機能が提供されていること、HTTPの仕様にしっかり準拠していることなどが挙げられます。

`groonga-httpd`の詳細については、[groongaのドキュメント](http://groonga.org/docs/executables/groonga-httpd.html)を参照してください。

### 使ってみる

上で説明したように、`groonga-httpd`でのみサポートされている機能としてHTTPのKeep-Aliveがあります。Keep-Aliveを有効にすることでクエリごとの接続、切断処理が省略され性能が向上します。そのことを確認してみましょう。

まず、[groonga-httpdのドキュメント](http://groonga.org/docs/executables/groonga-httpd.html)に書いてあるように`groonga-httpd`をセットアップします。

そのあと、次のように`ab`でKeep-Aliveの無効の時と有効の時とで性能を測定してみます。

Keep-Aliveが無効の時:

{% raw %}
```
$ ab -c 100 -n 100000 'http://0.0.0.0:10041/d/status'
Requests per second:    15135.00 [#/sec] (mean)
Time per request:       6.607 [ms] (mean)
Time per request:       0.066 [ms] (mean, across all concurrent requests)
```
{% endraw %}

Keep-Aliveが有効の時:

{% raw %}
```
$ ab -c 100 -n 100000 -k 'http://0.0.0.0:10041/d/status'
Requests per second:    23285.10 [#/sec] (mean)
Time per request:       4.295 [ms] (mean)
Time per request:       0.043 [ms] (mean, across all concurrent requests)
```
{% endraw %}

無効の時と比べ、有効にした時は、Requests per secondから約1.5倍の性能向上が確認できます。

`groonga`のHTTPサーバーに対しても同様に性能を測定してみます。

Keep-Aliveが無効の時:

{% raw %}
```
$ ab -c 100 -n 100000 'http://0.0.0.0:10041/d/status'
Requests per second:    20933.84 [#/sec] (mean)
Time per request:       4.777 [ms] (mean)
Time per request:       0.048 [ms] (mean, across all concurrent requests)
```
{% endraw %}

Keep-Aliveが有効の時:

{% raw %}
```
$ ab -c 100 -n 100000 -k 'http://0.0.0.0:10041/d/status'
Requests per second:    20683.93 [#/sec] (mean)
Time per request:       4.835 [ms] (mean)
Time per request:       0.048 [ms] (mean, across all concurrent requests)
```
{% endraw %}

`groonga`のHTTPサーバーはKeep-Aliveに対応していないので、有意義な性能差は確認できません。

今回の性能測定の結果はHTTPの通信処理だけの性能差ということになり、クエリの処理全体でみると性能差は縮まります。しかし今回のリリースではnginxとgroongaを組み合わせた最初のリリースです。さらなる性能向上や機能追加はこれからということになっていますのでご期待ください。

### まとめ

groonga 2.0.4で新しく追加された`groonga-httpd`について簡単に紹介しました。
