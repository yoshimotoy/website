---
tags:
  - apache-arrow
title: 2018年末にApache Arrow関連の話を聞けるイベント
---
[Apache Arrow](https://arrow.apache.org/)の開発に参加している須藤です。
<!--more-->


2018年11月、12月にApache Arrow関連の話を聞けるイベント（私がApache Arrow関連の話をするイベント）がいくつかあるので紹介します。どれも開催場所は東京です。

### 2018-11-17（土）13:30- RubyData Tokyo Meetup

11月17日（今週の土曜日！）開催の[RubyData Tokyo Meetup](https://speee.connpass.com/event/105127/)でApache Arrowの話をします。Apache Arrow全体の話というよりRubyに特化したApache Arrowの話をします。RubyでApache Arrowを使いたいという人に向いているイベントです。

このイベントでは最近Apache Arrowのコミッターになった[@shiro615](https://github.com/shiro615)の話も聞けます。Apache Arrowのコミッターになるまでの話です。

このイベントではApache Arrow以外にもディープラーニング・高速行列演算・可視化をRubyで実現するための話を聞けます。Rubyでデータを扱いたい人にオススメのイベントです。

### 2018-12-04（火）18:00- Apache Arrow - データ処理ツールの次世代プラットフォーム

12月4日（火）開催の[Apache Arrow - データ処理ツールの次世代プラットフォーム](https://ossforum.connpass.com/event/109902/)でApache Arrowの話をします。こちらはRubyに特化せずにApache Arrow全体の話をします。基本的なことから話すので、Apache Arrowの名前を聞いたことがあってよさそうな雰囲気はするけどまだよく知らない、という人でも大丈夫です。

質疑応答の時間が長めにあるので、Apache Arrowの情報収集をしたいという人はこのイベントがオススメです。

今回紹介するイベントの中で唯一平日夜の開催なので、平日の方が参加しやすいという人はこのイベントがオススメです。

### 2018-12-08（土）13:30- Apache Arrow東京ミートアップ2018

12月8日（土）開催の[Apache Arrow東京ミートアップ2018](https://speee.connpass.com/event/103514/)でApache Arrowの話をします。このイベントでもApache Arrow全体の話をします。12月4日のイベントとの違いは開発者向けの話かどうかです。このイベントではユーザー寄りというより開発者寄りの話をします。

このイベントの目的は開発に参加する人を増やすことです。対象プロダクトはApache Arrowおよび「Apache Arrowを活用するとうれしくなりそうなプロダクト」もろもろです。そのため、開発者寄りの話になります。

このイベントではApache Arrow本体の話だけでなく、次の「Apache Arrowを活用するとうれしくなりそうなプロダクト」の話もあります。

  * [Apache Spark](https://spark.apache.org/)

  * [Multiple-Dimension-Spread](https://github.com/yahoojapan/multiple-dimension-spread)

  * R関連プロダクト

  * Ruby関連プロダクト

  * Python関連プロダクト

Apache Arrowおよびこれらのプロダクトの開発に参加したい人にオススメのイベントです。

このイベントの前半はApache Arrowおよびこれらのプロダクトの話を聞く時間で、後半は実際に開発に参加しはじめる時間です。参加しはじめるというのは、パッチを書きまくるというわけではなく、どこから始めるのがよさそうかを相談しはじめるという意味です。

このイベントにはApache Arrowの開発に参加している人（私も含む）、Apache Sparkの開発に参加している人、[Red Data Tools](https://red-data-tools.github.io/ja/)に参加している人、[pandas](https://pandas.pydata.org/)の開発に参加している人などすでに開発に参加している人も多く参加します。そのような人たちと相談してイベントの時間内で最初の一歩を踏み出せるようにします。

さらに、このイベントの後にいくつかフォローアップイベントを開催します。こうすることで、このイベントで開発に参加し始めたあとも継続して開発できることを狙っています。

### まとめ

2018年11月、12月に東京でApache Arrow関連の話を聞けるイベントを3つ紹介しました。それぞれ少しずつ趣向が違うので、適切なイベントを選んで参加してみてください。
