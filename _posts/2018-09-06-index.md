---
tags:
- feedback
title: fcitx-mozcの入力モードを外部プロセスから制御する
---
### はじめに

何年か前に、とある案件で「[fcitx-mozc](https://gitlab.com/fcitx/mozc)の入力モードを自分のアプリケーションから制御したい」というご要望をお客様から承りました。そのときに、[fcitx-dbus-status](https://github.com/clear-code/fcitx-dbus-status)というFcitx用アドオンを実装して、このご要望にお応えしました。
<!--more-->


今回はこのアドオンについて紹介します。

### fcitx-dbus-statusとは

fcitx-dbus-statusはFcitxで動作している入力メソッドのステータスをD-Bus経由で取得したり、変更したりできるようにするためのFcitxアドオンです。`dbus-send`コマンドや`dbus-monitor`コマンドを使えば、シェルスクリプトで入力メソッドのステータスを制御したり監視したりすることもできます。

主にMozcを想定して開発しましたが、他の入力メソッドでも使用することができます。

### インストール方法

おそらくどのディストリビューションでもfcitx-dbus-statusのパッケージは用意されていないでしょうから、自分でビルドしてインストールする必要があります。

例えばUbuntu 18.04では以下のような手順でインストールすることができます。

```console
$ sudo apt install g++ cmake fcitx-libs-dev libdbus-1-dev fcitx-module-dbus
$ git clone git@github.com:clear-code/fcitx-dbus-status.git
$ cd fcitx-dbus-status
$ mkdir build
$ cd build
$ cmake ..
$ make
$ sudo make install
```


インストールが完了したら、アドオンが確実に読み込まれるようにシステムを再起動した方が良いでしょう。

### 使用方法

scriptsディレクトリ以下にMozc用のサンプルスクリプトがあります。

たとえばMozcの入力モードを全角カタカナに変更したい場合には以下のコマンドを実行します。

```
$ ./scripts/set-mozc-composition-mode.sh katakana
```


現在のモードを取得したい場合は以下のコマンドを実行します。

```
$ ./scripts/get-mozc-composition-mode.sh
```


モードの変更をリアルタイムで検知したい場合は以下のコマンドを実行します。

```
$ ./scripts/monitor-status.sh
```


### まとめ

[fcitx-dbus-status](https://github.com/clear-code/fcitx-dbus-status)について紹介しました。なお、ステータス変更のリアルタイム検知を実現するにはFcitx側の変更も必要だったので、事前にFcitxのメーリングリストで実装方針を作者の方に相談した上で、[パッチを提案](https://github.com/fcitx/fcitx/pull/271)しています。この修正は既に取り込まれており、最近のLinuxディストリビューションではこの変更が含まれたバージョンを利用できるようです。
