---
title: 'PGroongaの並列インデックス構築の並列度を最大にする方法'
author: komainu8
tags:
- groonga
---

[PGroonga]のメンテナンスもしている堀本です。

PGroongaは4.0.0からPostgreSQL 17以降を使っている場合に限り、複数のCPUコアを活用してインデックスの構築を並列で実行できます。
（PostgreSQL 17より前のバージョンでは動作しないので注意してください。）
並列に実行するため、インデックス構築速度の高速化が期待できますが、場合によっては思ったほど高速にならない可能性もあります。

今回は、PGroonga 4.0.0を使ってもあまりインデックス構築が高速にならない人向けの記事です。

[PGroonga]: https://pgroonga.github.io/ja/

<!--more-->

### はじめに

前述の通り、PGroonga 4.0.0以降ではインデックスの構築を複数のCPUコアを利用して並列に実行できます。

どのくらい並列で実行するかは、PostgreSQLがおすすめする並列度を使っていますが、
PostgreSQLがおすすめする並列度は控えめなので、あまり高速にならない可能性があります。

しかし、PGroongaはPostgreSQLがおすすめしてくる並列度ではなく、全力で並列に実行するように設定を変更できます。
PGroonga 4.0.0を使ってもあまりインデックス構築が高速にならない人は、これから紹介する方法を使って高速になるか試してみてください。

具体的には、PostgreSQL 17の環境変数に[GRN_N_WORKERS_DEFAULT]を設定する必要があるので、以降では、この環境変数を設定する方法を紹介します。

[GRN_N_WORKERS_DEFAULT]: https://groonga.org/dev/ja/docs/reference/command/n_workers.html#specified-by-environment-variable-grn-n-workers-default

### PostgreSQLに環境変数を設定する

ということで、PostgerSQL 17に環境変数`GRN_N_WORKERS_DEFAULT`を設定しましょう。
ここで、「え、どうやって？」と思った人はこの先に進んでください。そう思わなかった人は、この先の情報は不要ですので、`GRN_N_WORKERS_DEFAULT`に`-1`を設定して高速になるか試してみてください。

それでは、UbuntuにインストールされたPGroongaを例に環境変数を設定していきましょう。
環境変数は以下のように設定します。

1. `/etc/postgresql/17/main/environment`に`GRN_N_WORKERS_DEFAULT = -1`を追記します。
   （`systemd edit`を使って環境変数を設定するのではなく、`/etc/postgresql/17/main/environment`に環境変数を記載する点に注意してください。）

2. `systemctl restart postgresql@17-main`を実行してサービスを再起動します。(`systemctl reload`では反映されない点に注意してください。)

値が反映されているかどうかは、`pgroonga_command('status')`で確認できます。
色々な値が出力されますが、注目するのは`default_n_workers`と`n_workers`の2つだけです。

今回の例では、`GRN_N_WORKERS_DEFAULT = -1`を設定したので、以下の2つのSQLの結果が`-1`になっていれば設定した値が反映されています。
（`pgroonga_command('status')`だと、不要なパラメーターまで出力されてしまうので、以下の例では確認に必要なパラメーターのみを出力するようにしています。）

```sql
SELECT pgroonga_command('status')::jsonb->1->'default_n_workers' as default_n_workers;
 default_n_workers 
-------------------
 -1
(1 row)

SELECT pgroonga_command('status')::jsonb->1->'n_workers' as n_workers;
 n_workers 
-----------
 -1
(1 row)
```

上記のように、`GRN_N_WORKERS_DEFAULT`に`-1`を設定することで、全力で並列に実行するようになります。
この状態で、再度インデックス構築速度が速くなっているか確認してみてください。

### おわりに

今回は、UbuntuにインストールされたPGroongaを例に、PGroongaの並列インデックス構築の並列度を最大にする方法を紹介しました。

もし、PGroongaを使っていてなにかお困りのことがあれば、[お問い合わせ]({% link contact/index.md %})よりご連絡ください。
