---
title: 2022年のApache ArrowのPMC chairの成果
author: kou
tags:
  - apache-arrow
---

2022年の[Apache Arrow](https://arrow.apache.org/)のPMC chairだった須藤です。役割を終え、PMC chairが変わったので私のPMC chairの成果をまとめます。

私がApache ArrowのPMC chairになった経緯やPMC chairってなに？そもそもPMCってなに？などは[代表取締役の須藤がApache ArrowのPMC chairに就任]({% post_url 2022-01-27-apache-arrow-pmc-chair %})を参照してください。

<!--more-->

### PMC chairの仕事

PMC chairの主な仕事は次の通りです。

* [Apache Software FoundationのBoard](https://www.apache.org/foundation/board/)への定期レポート
* 新しいPMCメンバーを招待

PMC chairになる人はそうそういないと思うのでこれらを説明してもだれかの役に立つとは思えませんが私ががんばったことをアピールするために説明します。

#### Boardレポート

「Apache Software FoundationのBoard」というのはApache Software Foundationの取締役会です。取締役会は毎月開催されていてApache Software Foundation傘下のプロジェクトは3ヶ月に一回プロジェクトの健康状態を報告することになっています。これを提出するのがPMC chairです。

WesさんがPMC chairのときはGoogle Docsでみんなで報告内容を揉んでいたのですが、私はGoogle Docsが苦手なのでGitHubのpull requestで報告内容を揉むことにしました。

* 2022-04の分： https://github.com/apache/arrow/pull/12759
* 2022-07の分： https://github.com/apache/arrow/pull/13525
* 2022-10の分： https://github.com/apache/arrow/pull/14357

話題は期間内の[`dev@arrow.apache.org`](https://lists.apache.org/list.html?dev@arrow.apache.org)の内容を確認しながらそれっぽいと思ったものをピックアップしていました。私は流暢に英語を書けないので短いサマリーとしてまとめるのが大変でした。

#### 新しいPMCメンバーの招待

Apache Software FoundationではPMCメンバーを追加する手順が決まっています。簡単に言うと次のような感じです。

1. 既存PMCメンバーのだれかが新しいPMCメンバーによさそうな人を選ぶ
2. 1.の人がよさそうかどうかをPMCが投票して決める
3. いいね！となったらPMC chairが1.の人を招待
4. 1.の人が承諾したらPMC chairが1.の人をPMCメンバーに追加

PMC chairが全部やるわけではないんですねぇ。1.と2.はPMCの1メンバーとして取り組みます。PMC chairだからといって意見が通りやすいとかはありません。3.と4.だけがPMC chairとしての仕事です。

なお、私は7人のPMCメンバーを招待しました。そのうち私が提案した人は1人です。


### PMC chairとしてではない2022年の成果

2022年のPMC chairとして私の成果は↑の通りなのですが、2022年はApache Arrowの1開発者としてもがんばったのでついでにそれも自慢します。

どのくらいがんばったかと言うと各リリースで一番コミットした人・マージした人になるくらい頑張りました。Apache Arrowは[リリースごとにコミット数とマージ数をカウントしている](https://arrow.apache.org/release/)ので見てみましょう。

| バージョン | リリース日 | コミット数 | コミット数順位 | マージ数 | マージ数順位 |
| ---------- | ---------- | ---------- | -------------- | -------- | ------------ |
| 7.0.0      | 2022-02-03 | 49         | 2              | 73       | 3            |
| 8.0.0      | 2022-05-06 | 39         | 3              | 84       | 3            |
| 9.0.0      | 2022-08-03 | 65         | 1              | 126      | 1            |
| 10.0.0     | 2022-10-26 | 68         | 1              | 126      | 1            |

前半は2位・3位ですが、後半は1位です。これは、今年の4月からApache Arrowの開発に使える時間が増えたからです。具体的に言うと、[Apache Arrowの最新情報（2022年5月版）]({% post_url 2022-05-13-latest-apache-arrow-information %})に書いた通り、[Voltron Dataさん](https://voltrondata.com/)が資金援助してくれています。（私のApache Arrow関連の開発にお金を払ってくれています。）Apache Arrowの開発を仕事にしたい人は[採用情報]({% link recruitment/index.md %})、特に[Apache Arrow関連業務の採用情報]({% link recruitment/apache-arrow.md %})を参照してください。

では、これらのコミットでどのような改良をしたかを自慢します。

#### GLib/Rubyバインディングの改良

私がApache Arrowの開発に参加している一番のモチベーションはRubyです。Rubyでもデータ処理できるようにしたくてApache Arrowの開発に参加しています。そのためにRubyから使えるApache Arrowの機能を増やしています。具体的には今年は次のことをできるようにしました。

* より多くの[計算関数](https://arrow.apache.org/docs/cpp/compute.html)を使えるようにした
* JOINできるようにした
* HTTPSでアクセスできるApache Arrow/Apache Parquet/CSVファイルを直接読み込めるようにした
* Google Cloud StorageにあるApache Arrow/Apache Parquet/CSVファイルを直接読み込めるようにした
* いろんな処理が簡単に書けるようにした
  * 説明するのが難しいんですが、いい感じにオブジェクトを変換する仕組みを用意してこれで動くといいなという感じに書いたら動くようになっています。たとえば、前はApache Arrow用のオブジェクトを明示的に作らないといけなかったところを`Hash`やキーワード引数から自動で構築するようになったとかです。
* BigDecimal連携を強化した
* Apache Arrow Flight SQLを使えるようにした
* Apache Arrow DataFusionを使えるようにした
* Apache Arrow Database Connectivity（ADBC）を使えるようにした

#### Valaのサポート

私は[Vala](https://vala.dev/)を使っていないのですがValaから使いたいという人がいたのでサポートしました。ValaはGNOME方面で使われているC#のようなプログラミング言語です。GLibバインディングでちょろっとがんばるとValaからもいい感じに使えるのでやっておきました。

#### リリースまわりの改良

Apache Arrowは言語に依存しない（= いろんな言語で使える）ことが売りの1つなのでたくさんの言語実装（バインディングを含む）があります。Apache ArrowはRust実装・Julia実装以外は3-4ヶ月に1回のペースで一緒にリリースしています。各言語ごとにリリース・パッケージまわりのツール・文化が異なるのでリリース作業は大変です。タグを打ったら終わりという感じではありません。

そのため、今のApache Arrowは実質「最新バージョンのみサポート」という状況になっています。「3-4ヶ月で新しいバージョンがリリースされる」という事実と組み合わせると、「Apache Arrowユーザーは3-4ヶ月ごとにバージョンアップしないといけない」ということになります。しかも今のApache Arrowは3-4ヶ月ごとのリリースで大なり小なり互換性が壊れます。多くの場合は影響を受けませんが、Apache Arrowユーザーは「3-4ヶ月ごとにApache Arrow関連のコードの変更を含んだバージョンアップ作業が必要になるかも」というメンテナンスコストがあるということです。

エンタープライズ環境で使われるプロダクトでは複数のリリースラインをサポートしたり、さらに、各リリースラインが1年以上メンテナンスされることも驚くことではありません。これは頻繁なバージョンアップが向かないユースケースがあるためです。そのようなユースケースでもApache Arrowを使うためにはもう少しApache Arrowのメンテナンス期間を長くしないといけません。

メンテナンス期間を長くするためにはメンテナンスコストを下げる仕組みあるいはだれかがそれなりのメンテナンスコストを払う必要があります。私は前者の方が好きなので少しずつそのあたりを進めていました。

具体的にはまずは普通のリリース作業の自動化を進めています。メンテナンスの中にはリリース作業も含まれています。リリース作業の自動化は普通のリリースのコストを下げるだけではなくメンテナンスコストも下げます。

このあたりに関してメーリングリストでも議論しているので興味がある人はこちらの議論に参加してください。あるいはクリアコードに入って仕事としてこのあたりを整備したい！でもいいですよ！

* [\[DISCUSS\] Maintenance policy](https://lists.apache.org/thread/7mgwr02h1f7zvghym1kljyb32s50vx1o)

#### Linux向けパッケージまわりのメンテナンス

私はApache ArrowのDebian/Ubuntu/AlmaLinux/CentOS Stream向けのパッケージもメンテナンスしているのですが、今年は次のバージョンのサポートを追加・削除しました。基本的に新しいバージョンが出たらサポートして、EOLになったら削除しています。

* 削除：Debian GNU/Linux buster
* 削除：Ubuntu 21.04
* 削除：Ubuntu 21.10
* 追加：Ubuntu 22.04
* 追加：Ubuntu 22.10
* 追加：AlmaLinux 9
* 追加：Amazon Linux 2 aarch64
* 追加：CentOS Stream 8
* 追加：CentOS Stream 9

RedHat Enterprise Linux系のパッケージでは複数のバージョンのApache Arrowパッケージをインストールできるようにしたのがハイライトです。前述の通り、Apache Arrowは3-4ヶ月に1回非互換入りでリリースされます。これまでのパッケージは1つのバージョンしかインストールできなかったので、Apache Arrowがリリースされた直後にApache Arrowに依存したパッケージが壊れていました。Apache Arrowのパッケージは新しいのにApache Arrowに依存したパッケージはまだ古いApache Arrowが必要だからです。複数のバージョンのApache Arrowパッケージを同時にインストールできるようにすることによりApache Arrowに依存したパッケージがすぐに最新のApache Arrowに対応しなくてもよくなりました。

説明が抽象的すぎてなにが問題なのかわかってもらえない気がするので具体例を出すと、GroongaのRedHat Enterprise Linux系のパッケージがいい感じになりました。

#### Conanサポート

[Conan](https://conan.io/)というのはC++用のパッケージマネージャーです。Apache Arrowパッケージもあるのですが、大きなパッチを抱えているのでApache Arrow本体の方を改良してConanのApache Arrowパッケージのパッチを0にしようとしています。10.0.0でApache Arrow本体の方の作業は終わったので今はConanのApache Arrowパッケージにpull requestを送って少しずつパッチを減らしている最中です。パッチがなくなるとApache Arrowパッケージのバージョンアップが簡単になってリリースコストも減るのです。

#### Julia実装のサポート

Apache ArrowのJulia実装はApache Arrowの他の実装と開発スタイルが違いすぎて https://github.com/apache/arrow に入っては出て、入っては https://github.com/apache/arrow-julia に移動して、となかなかいい感じの開発スタイルを見つけられずにいました。そのあたりが今はなんかいい感じの開発スタイルに落ち着いています。これは私のがんばりがあったからです！もちろん私だけががんばったわけではなくて私以外の人達もがんばったんですが、私も結構がんばったと思うんですよ！

今はいい感じに落ち着いたので2023年もいい感じに開発が継続していくはずです。

#### ビルドシステムの改良

ここでいうビルドシステムとは主にCMakeベースのビルドシステムのことです。C++実装やPythonバインディングがCMakeを使っています。これを改良するというのは今どきっぽいCMakeの使い方にするとか依存ライブラリーをいい感じに組み込めるようにするとかです。

実はC++実装のCMakeパッケージが微妙だったのがこの数年のもやもやだったのですが、ついに今年いい感じにできました。すっきり！

Apache Arrowの開発にはCMakeが得意というか好きというか苦手意識がないというかそういう人があまりいないので、ビルドシステムをいじるのが好きな私がやっていました。リリースまわりもそうですがビルドシステムまわりも人手が足りていないのでチャンス（？）ですよ！

RバインディングはC++実装のバインディングなのでCMakeベースのビルドシステムが関係することがあります。そのため、たまにRバインディングのビルドまわりもいじっていました。

Java実装のJNI関連のビルドシステムもCMakeベースなのですがこのあたりも整理していい感じにしました。このおかげでmacOS用・Windows用のJNIのバイナリーも`.jar`の中に入るようになりました。GandivaやApache Arrow DatasetなどがJNIを使っているのですが、macOS・Windowsでもそれらを使えるようになったということです。

#### ADBCのリリースまわりのサポート

そろそろADBCの最初のリリース（0.1.0）があるのですが、その仕組みづくりやリリース作業の一部を手伝っています。

ADBCはRuby on Railsユーザーにも恩恵がありそうなので楽しみにしていてください。来年はActive RecordのADBCアダプターを作ったりPostgreSQLのApache Arrow Flight SQLサーバーインターフェイスを作ったりしたいと思っています。

### まとめ

Apache ArrowのPMC chairとして過ごした2022年のApache Arrow関連の活動の成果を自慢しました。PMC chairとしての役割は終わりましたが今後もPMCメンバーの1人として開発に参加してRubyでもいい感じにデータ処理できるようにしていきます。

Rubyでもいい感じにデータ処理したい！と思った人は[Red Data Tools](https://red-data-tools.github.io/ja/)で一緒に取り組みましょう。[Red Data Toolsのチャット](https://gitter.im/red-data-tools/ja)で待っています。

なお、[次のPMC chairはAndrew Lambさん](https://lists.apache.org/thread/f46owgtf0c0888tb7qjjrstrgth42181)です。Andrewさんは[InfluxDB](https://www.influxdata.com/products/influxdb-overview/)を開発している[InfluxData](https://www.influxdata.com/)で働いています。次世代のInfluxDBである[InfluxDB IOx](https://github.com/influxdata/influxdb_iox)で使っているApache ArrowのRust実装（Apache Arrow DataFusionなど関連プロジェクトも含む）のためにApache Arrowの開発に参加しています。（私はそうなんだろうなぁと思っています。）非常にアクティブで、コードをよく書くのはもちろん、レビューや議論にも積極的に参加するし、新しいコントリビューターを増やすことにも力を入れている立派な人です。来年もApache Arrowプロジェクトは成長しそうですね！
