---
tags:
- fluentd
title: Kafka Connector for Fluentdのベンチマーク
---
[PLAZMA OSS Day: TD Tech Talk 2018 – Cooperative works for Fluentd Community]({% post_url 2018-02-16-index %}) の補足記事その3です。
<!--more-->


fluent-plugin-kafkaのアウトプットプラグインをkafka-connect-fluentdで置き換えられるようにするためには、機能だけでなく性能も重要なので性能を比較した結果をまとめます。

### 構成

GCP上にn1-standard-2(vCPU 2, memory 7.5GB)で構築しています。OSイメージはdebian-cloud/debian-9です。

![システム構成図]({{ "/images/blog/20180301_0.png" | relative_url }} "システム構成図")

  * client

    * fluent-benchmark-client が入っているホスト

  * Fluentd

    * in_forward + fluent-plugin-kafkaのoutputプラグインの入ったホスト

    * Fluentdはtd-agent3で入れた1.0.2

  * Kafka

    * Kafka + kafka-connect-fluentd が入っているホスト

  * metrics

    * Fluentd + influxdb + grafana が入っているホスト

    * Fluentdはtd-agent3で入れた1.0.2

    * influxdb + grafana はDockerイメージを使用している

### ベンチマークシナリオ

今回はfluent-plugin-kafkaとkafka-connect-fluentdの比較ができればいいので、送信するレコードはfluent-benchmark-clientのデフォルトのものを使用します。
JSONで表現すると以下のものとなります。MessagePackだと50byteです。

```
{ "message": "Hello, Fluentd! This is a test message." }
```


また、スループットを比較したいのでout_kafka, out_kafka_buffered, out_kafka2の各パラメータはデフォルト値のままにします。
ただし、デフォルトだと遅すぎるため、out_kafka2に関しては `buffer/flush_thread_count`を3に変更しています。

kafka-connect-fluentdもworker pool sizeは1としますが、`KAFKA_HEAP_OPTS="-Xmx4G -Xms4G"` を指定しています。

fluent-benchmark-client のパラメータは以下の通り、5分間負荷をかけるようにしました。
`--n-events-per-sec`に与える値を変えると、負荷を大きくしたり小さくしたりできます。

```
fluent-benchmark-client \
        --host=$host \
        --port=$port \
        --max-buffer-size=4g \
        --flush-interval=10 \
        --n-events-per-sec=$1 \
        --period=5m
```


[fluentd-benchmark](https://github.com/fluent/fluentd-benchmark)にあるシナリオを参考にして、`--n-events-per-sec`の値を決めました。

  * 1000 events/sec

  * 10000 events/sec

  * 30000 events/sec

  * 50000 events/sec

### ベンチマーク結果

|                        | 10000 events/sec                                                                                    | 30000 events/sec or 50000 events/sec                                                                |
|------------------------|-----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| out_kafka              | [![out_kafka 10k events/sec]({{ "/images/blog/20180301_1.png" | relative_url }} "out_kafka 10k events/sec")](/blog/images/20180301_1.png)              | [![out_kafka 30k events/sec]({{ "/images/blog/20180301_2.png" | relative_url }} "out_kafka 30k events/sec")](/blog/images/20180301_2.png)              |
| out_kafka_buffered     | [![out_kafka_buffered 10k events/sec]({{ "/images/blog/20180301_3.png" | relative_url }} "out_kafka_buffered 10k events/sec")](/blog/images/20180301_3.png)     | [![out_kafka_buffered 30k events/sec]({{ "/images/blog/20180301_4.png" | relative_url }} "out_kafka_buffered 30k events/sec")](/blog/images/20180301_4.png)     |
| out_kafka2             | [![out_kafka2 10k events/sec]({{ "/images/blog/20180301_5.png" | relative_url }} "out_kafka2 10k events/sec")](/blog/images/20180301_5.png)             | [![out_kafka2 50k events/sec]({{ "/images/blog/20180301_6.png" | relative_url }} "out_kafka2 50k events/sec")](/blog/images/20180301_6.png)             |
| FluentdSourceConnector | [![FluentdSourceConnector 50k events/sec]({{ "/images/blog/20180301_7.png" | relative_url }} "FluentdSourceConnector 50k events/sec")](/blog/images/20180301_7.png) | [![FluentdSourceConnector 50k events/sec]({{ "/images/blog/20180301_8.png" | relative_url }} "FluentdSourceConnector 50k events/sec")](/blog/images/20180301_8.png) |

out_kafkaとout_kafka_bufferedは30000events/secでout_kafka2とFluentdSourceConnectorは50000events/secです。

それぞれのグラフは上から順に、CPU使用率、メモリ使用量、Kafkaが処理したイベント数（直近の1分間の平均、5分間の平均、15分間の平均）、Kafkaが処理したイベント数の合計です。

「Kafkaが処理したメッセージ数の合計＝指定した秒間イベント数×5分」になっていれば、イベントはKafkaで処理できています。

|                        | 送信イベント数(events/sec) | 平均受信イベント数(events/sec) | CPU usage | 全部処理できた？ | 備考                        |
|------------------------|----------------------------|--------------------------------|-----------|------------------|-----------------------------|
| out_kafka              |                      10000 | 10000                          |   40%-50% | ○               |                             |
| out_kafka              |                      30000 | 2万弱                          |   90%以上 | ×               |                             |
| out_kafka_buffered     |                      10000 | 10000                          |    0%-85% | ○               |                             |
| out_kafka_buffered     |                      30000 | 2万弱                          |      100% | ×               |                             |
| out_kafka2             |                      10000 | 10000                          |   40%-50% | ○               |                             |
| out_kafka2             |                      50000 | -                              |      100% | ×               | Fluentdのバッファがあふれた |
| FluentdSourceConnector |                      10000 | 10000                          |   20%以下 | ○               |                             |
| FluentdSourceConnector |                      50000 | 5万弱                          |   平均66% | ○               |                             |

fluent-plugin-kafkaよりもkafka-connect-fluentdの方がメッセージのスループットが約2.5倍高かった。
また、同じイベンント数を処理する場合でもkafka-connect-fluentdの方がCPUの使用率が低かった。

メモリー使用量については、Fluentd側が正しく測定できていなさそうなので追加の検証が必要です。

### まとめ

kafka-connect-fluentdとfluent-plugin-kafkaの性能を比較して、kafka-connect-fluentdの方が速いことがわかりました。

kafka-connect-fluentdは、Kafka Connecotr APIのマルチタスクをサポートしていなかったり、fluent-plugin-kafkaと同じ形式で書き込むようにしていたりするので、まだまだ性能を向上する余地があります。
