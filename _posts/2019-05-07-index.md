---
tags:
- mozilla
title: 2019年5月4日前後から発生しているFirefoxのアドオン無効化問題の詳細な解説
---
[「Firefox」でインストール済みアドオンが利用不能になる問題が発生中 - 窓の杜](https://forest.watch.impress.co.jp/docs/news/1182954.html)や[Firefox 66.0.4 リリース、拡張機能全滅問題を修正 | スラド IT](https://it.srad.jp/story/19/05/05/2252212/)などで既報の通り、2019年5月4日前後から一般向けのリリース版Firefoxでアドオンを一切使用できない状態が発生していました。緊急の修正版としてリリースされたFirefox 66.0.4およびFirefox ESR60.6.2で現象は既に解消されており、また、既にインストール済みのFirefoxに対してもホットフィックスが順次展開されていますが、本記事では今回の問題の技術的な詳細を解説します。
<!--more-->


### ユーザー側で取れる対応

まず最初に、ユーザー側で取れる対応をまとめておきます。

  * Firefox 66.0.4以降またはFirefox ESR60.6.2以降に更新する。

  * 「Firefox調査」を通じて `hotfix-update-xpi-signing-intermediate-bug-1548973` が自動的に反映されるのを待つ。

  * [有効期限が更新された中間証明書のファイル](https://hg.mozilla.org/releases/mozilla-release/raw-file/5b264ffa56e752df9a66c8a781e06fde51ada9e8/security/apps/addons-public-intermediate.crt)をダウンロードし、「オプション」→「プライバシーとセキュリティ」→「証明書」→「証明書を表示」で表示される証明書マネージャにおいて、「認証局証明書」の一覧の「インポート」ボタンから認証局証明書としてインポートする。

  * `about:config` で隠し設定の `xpinstall.signatures.required` を `false` に設定する。（Firefox ESR、Developer Edition、Nightlyのみ有効で、Firefox 48以降の通常のリリース版Firefoxではこの方法は無効です）

Mozillaによるサポートが終了したバージョンのFirefoxでは「Firefox調査」を通じての修正の反映が行われないため、Firefoxの更新、証明書の手動でのインポート操作、または `about:config` での隠し設定の変更が必要になります。

修正が反映される前のバージョンのFirefoxを起動してアドオンが無効化されてしまった場合、証明書のインポートや `about:config` での暫定的対応を行った後でも「アドオンが依然として無効で、且つ、ユーザー操作で再度有効化できないまま」となる場合があります。これは、アドオンが署名の期限切れで強制的に無効化されたという情報がユーザープロファイル内に残留しているために発生する現象です。この状況に陥った場合、Firefoxを終了した状態でユーザープロファイル内の `extensions.json` を削除して状態をリセットすると、アドオンが自動的に有効化されます（あるいは、手動操作で有効化できる状態でアドオンマネージャに表示されるようになります）。

### アドオンの電子署名の基本のおさらい

Firefoxでは、[Firefox 43以降のバージョンからアドオンのインストールパッケージの電子署名が必須となっています](https://dev.mozilla.jp/2015/02/extension-signing-safer-experience/)。

Firefoxのアドオンはその特性上、認証局証明書の差し替えや通信の読み取りなど、Firefox上でありとあらゆる事ができる物でした[^0]。これではユーザーを困らせたりユーザーの機密情報を狙ったりする事を企む攻撃者にとって格好の標的となり得るため、その種の攻撃を防ぐために、Firefox 43で以下のような仕組みが導入されました。

  * すべてのアドオンは、[Mozilla Add-ons](https://addons.mozilla.org/)でダウンロード可能な形で公開する物も、それ以外も、審査のため一旦Mozilla Add-onsのサービスにファイルをアップロードしなくてはならない。

  * アップロードされたファイルは、エディター権限を持つ人の目視による審査を受け、受理された場合にのみMozillaによって電子署名が施される。電子署名により、エディターが審査した時のファイルと同じ内容のファイル（＝安全が確認されたファイル）である事が保証される。

  * ユーザー環境のFirefoxは、入手したアドオンのパッケージの電子署名を検証し、妥当な署名である事を確認できた（＝エディターが審査した通りの内容のファイルであると確認できた＝安全が確認された）段階で初めてそのアドオンを使用可能にする。

ただ、これではアドオンの開発中や、社内用として機密情報を含めたアドオンを使いたい場合など、審査用にファイルを供出できないケースで支障があります。そのため、以下のような救済措置が存在しています。

  * この電子署名の検証は `xpinstall.signatures.required` という隠し設定を `false` にして無効化できる。

    * ただし、Firefox 48以降のバージョンでは、法人向けのFirefox ESRと開発者向けのDeveloper Edition、Nightlyでのみ有効で、通常リリース版のFirefoxではこの設定は機能しません。

  * `about:debugging` から一時的に読み込まれたアドオンに対しては、電子署名の検証は行われない。

### アドオンの電子署名の詳細

ここでさらっと「電子署名が施される」と述べましたが、具体的には何が行われているのでしょうか。[IE View WE](https://addons.mozilla.org/firefox/addon/ie-view-we/)の場合を例に取って解説します。

「インストール」ボタンからダウンロードできるインストールパッケージ `ie_view_we-1.3.2-fx.xpi` の実体はZIP形式のアーカイブですが、これを展開して取り出された中にある `META-INF` というフォルダとその内容がまさに電子署名の実体です。それぞれの内容は以下の通りです。

  * `META-INF/manifest.mf`: パッケージ内に含まれる各ファイルのハッシュ値の一覧

  * `META-INF/mozilla.mf`: パッケージ全体のハッシュ値

  * `META-INF/mozilla.rsa`: DER形式の電子署名データ

このうちの `mozilla.rsa` には、以下の情報が含まれています。

  * 各ファイルやパッケージ全体の情報と、Mozillaが持つ秘密鍵を組み合わせて導出した署名データ

  * 署名データの導出に使われた秘密鍵に対応する公開鍵、の情報を含む証明書

ユーザー環境のFirefoxは、実際のアドオンのパッケージに含まれるファイルの内容と公開鍵を使って署名データを導出し、それが `mozilla.rsa` に含まれる物（秘密鍵を使って導出された物）と一致するかどうかを以て、パッケージの内容が改竄されていないことを確認することができます。

この `mozilla.rsa` に含まれる証明書は、`openssl` コマンドを使うと以下のようにして内容を確認できます。

```console
$ openssl pkcs7 -inform DER -text -print_certs < ie_view_we-1.3.2-fx/META-INF/mozilla.rsa
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1551780906376321839 (0x158908833dcb5b2f)
    Signature Algorithm: sha384WithRSAEncryption
        Issuer: C=US, O=Mozilla Corporation, OU=Mozilla AMO Production Signing Service, CN=signingca1.addons.mozilla.org/emailAddress=foxsec@mozilla.com
        Validity
            Not Before: Mar  5 10:15:06 2019 GMT
            Not After : Mar  4 10:15:06 2020 GMT
        Subject: C=US, ST=CA, L=Mountain View, O=Addons, OU=Production, CN=ieview-we@clear-code.com
        Subject Public Key Info:
            （省略）
        X509v3 extensions:
            （省略）
    Signature Algorithm: sha384WithRSAEncryption
         （省略）
-----BEGIN CERTIFICATE-----
（省略）
-----END CERTIFICATE-----

Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1048580 (0x100004)
    Signature Algorithm: sha384WithRSAEncryption
        Issuer: C=US, O=Mozilla Corporation, OU=Mozilla AMO Production Signing Service, CN=root-ca-production-amo
        Validity
            Not Before: May  4 00:09:46 2017 GMT
            Not After : May  4 00:09:46 2019 GMT
        Subject: C=US, O=Mozilla Corporation, OU=Mozilla AMO Production Signing Service, CN=signingca1.addons.mozilla.org/emailAddress=foxsec@mozilla.com
        Subject Public Key Info:
            （省略）
        X509v3 extensions:
            （省略）
    Signature Algorithm: sha384WithRSAEncryption
         （省略）
-----BEGIN CERTIFICATE-----
（省略）
-----END CERTIFICATE-----
```


ここでは2つの証明書が出力されています。

  * `CN=ieview-we@clear-code.com` である証明書（`CN=signingca1.addons.mozilla.org` によって発行された）：そのアドオン固有の物として発行されたコード署名証明書で、パッケージの電子署名に使われた物

  * `CN=signingca1.addons.mozilla.org` である証明書（`CN=root-ca-production-amo` によって発行された）：中間証明書

`CN=root-ca-production-amo` の証明書は [`addons-public.crt` というファイル名でFirefoxのリポジトリに含まれている自己署名証明書](https://hg.mozilla.org/releases/mozilla-release/file/5b264ffa56e752df9a66c8a781e06fde51ada9e8/security/apps/addons-public.crt)で、[ビルド時にソースコード内に組み込まれ](https://hg.mozilla.org/releases/mozilla-release/file/5b264ffa56e752df9a66c8a781e06fde51ada9e8/security/apps/moz.build#l41)、[組み込みの証明書として認識される](https://hg.mozilla.org/releases/mozilla-release/file/5b264ffa56e752df9a66c8a781e06fde51ada9e8/security/apps/AppTrustDomain.cpp#l60)、ビルトインのルート証明書の1つです[^1]。

ここで注目するべきなのは、`mozilla.rsa` に含まれる証明書のうち2番目、中間証明書の方の有効期限です。当該箇所を以下に抜粋します。

```
Not Before: May  4 00:09:46 2017 GMT
Not After : May  4 00:09:46 2019 GMT
```


これを見ると、有効期限が2017年5月4日0時9分46秒（UTC）から2019年5月4日0時9分46秒（UTC）となっています。日本標準時はUTCより9時間進んでいるので、日本では2019年5月4日の9時過ぎに有効期限を迎えた事になります。この瞬間以降、この中間証明書を含むアドオンのパッケージはすべて「期限切れの中間証明書に基づく証明書で署名されたファイル」と見なされ、「署名を検証できない」＝「エディターが審査した通りの内容のファイルであると確認できない」＝「安全が確認されていない」と判断されるため、新規インストールは拒絶され、インストール済みの物は無効化されるという結果になっていたわけです。

### Mozillaが取った対応

上記の事を踏まえ、Mozillaでは[いくつかの対応が検討されたようです](https://bugzilla.mozilla.org/show_bug.cgi?id=1549010)。[Firefoxのリリースブランチ](https://hg.mozilla.org/releases/mozilla-release/graph/tip)と[ESR60用ブランチ](https://hg.mozilla.org/releases/mozilla-esr60/graph/tip)に実際に投入された変更を追うと、以下の3通りの方法が試された模様です。

  1. [コード署名証明書の検証時にだけ現在時刻を2019年4月27日に巻き戻す](https://hg.mozilla.org/releases/mozilla-release/rev/9cdb06fa51891f31c4371b3d06d8e46148b5237a)

  1. [新しい中間証明書をハードコードし、それを証明書データベースに追加する](https://hg.mozilla.org/releases/mozilla-release/rev/848b15028562c6757748070f637e0e4f0bbb5f65)[^2]

  1. [新しい中間証明書をハードコードし、`CN=root-ca-production-amo` の証明書に基づく証明書ツリーを検証する時に新しい中間証明書を代わりに使う](https://hg.mozilla.org/releases/mozilla-release/rev/5b264ffa56e752df9a66c8a781e06fde51ada9e8)

緊急のリリースとなったFirefox 66.0.4とFirefox ESR60.6.2には、この2番目の対応が反映されています。また、動的な反映が可能である事から、統計情報の収集・ユーザーのFirefox使用状況の収集を行うための仕組みを使って、この2番目の方法と同じ手順でインストール済みのFirefoxに暫定的対策を行う（有効な中間証明書を自動インポートする）という対応が展開されています。Firefox 66.0.3以前やFirefox ESR60.6.1以前を使用中の環境では、この対応が反映された事によってアドオンが再び使用可能になっているはずです。「オプション」→「プライバシーとセキュリティ」→「証明書」→「証明書を表示」で認証局証明書の一覧を表示し、`Mozilla Corporation` 配下に `signingca1.addons.mozilla.org` が表示されていれば、この対応が既に反映された状態であるという事を確認できます。

[![（スクリーンショット：認証局証明書の一覧に中間証明書が現れている状態）]({{ "/images/blog/20190507_0.png" | relative_url }} "（スクリーンショット：認証局証明書の一覧に中間証明書が現れている状態）")]({{ "/images/blog/20190507_0.png" | relative_url }})

一方、Firefox 66.0.5とFirefox ESR60.6.3には3番目の対応が採用されており、2番目の対応は不要になったとして取り除かれています[^3]。

### 中間証明書の更新では解消されない問題

現時点では以上の通りの暫定的な対応がなされていますが、今回発生したすべての問題が解決されたわけではなく、一部のアドオンではさらに手動での対応が必要です。その中でも、アナウンスされている未解決の問題の1つである、[一部のアドオンが中間証明書の更新後も有効にならない件](https://bugzilla.mozilla.org/show_bug.cgi?id=1549129)は、ここまでで解説した電子署名の仕組みが大きく関係しています。

前述の説明の通り、アドオンは中間証明書によって発行されたそのアドオン固有の証明書によって電子署名を施された状態になっています。この証明書はアドオンごとの固有の識別子（ID）に基づいて発行されていますが、[EPUBReader](https://addons.mozilla.org/firefox/addon/epubreader/)などの一部のアドオンでは、そもそもこの固有の識別子が開発元によって指定されていません。そのため、Mozilla Add-onsのシステムが自動的に一意なIDを割り当てているのですが、アドオンのファイルの最終更新日が変わったなどの契機で自動割り当てされたIDが代わってしまうと、中間証明書の更新後であっても、そもそも電子署名が不正と見なされてしまうという状況が発生します。そのため、アドオンを手動で再インストールして、電子署名に記録されたIDとアドオンの一時的なIDが一致する状態を作り直す必要があるという訳です。

他にも、コンテナータブに固有の情報が失われる、検索エンジンや起動時のホームページなどアドオンによって変更されたはずのFirefox側の設定が初期状態に戻ったままになる、といった問題が残されているようです。これらの問題が解決された場合、それらもまたホットフィックスとして展開されたり、もしくは緊急の修正版としてリリースされたりする可能性がありますので、引き続き注視が必要です。

### 暫定的な対応を取った後の注意点

冒頭に記載したとおり、Firefox ESR、Developer EditionおよびNightlyにおいては、 `xpinstall.signatures.required` による電子署名の検証の無効化によって今回の問題を回避できます。しかしながら、それは同時に、この設定と同時に導入された安全確認の仕組みを無効化する事をも意味します。企業での運用でユーザーによるアドオンのインストールを禁止していて、導入されるアプリケーションの影響範囲が厳密に制御されている状況であればよいのですが、個人でESR版を使用している場合、悪意の攻撃者によるアドオンが知らず知らずのうちに読み込まれるというリスクがあります。

この方法を取った場合は、Firefoxが更新されたり、ホットフィックスが反映されたりした事を確認できたら、早急に証明書の検証を有効化する事を強くお勧めします。

### まとめ

以上、2019年5月4日以降で発生しているFirefoxのアドオン無効化問題の技術的な詳細を解説しました。

当社では[Firefoxの法人向けテクニカルサポートを有償で提供しています](/services/mozilla/menu.html)が、実際にお客様の環境でも影響が出ている例があります。

今回の問題に見られるような証明書の期限切れに起因する問題は、度々世間を騒がせています。記憶に新しい所では、2018年から2019年にかけても以下のニュースがありました。

  * [「うんこボタン」全品交換の理由 - ITmedia NEWS](https://www.itmedia.co.jp/news/articles/1811/28/news077.html)

  * [世界的な携帯電話の通信障害--原因はエリクソンのソフトウェア証明書 - ZDNet Japan](https://japan.zdnet.com/article/35129783/)

  * [米国政府機関の閉鎖、ウェブサイトのセキュリティにも悪影響--証明書の期限切れで - ZDNet Japan](https://japan.zdnet.com/article/35131262/)

証明書のように有効期限が存在するデータに依存するシステムを運用する場合、有効期限の到来には充分な余裕を持って対応できるような仕組みを作っておく必要がある、という事を思い知らされますね。

[^0]: これは従来型のアドオン（いわゆるXULアドオン）での話です。Firefox 57以降のバージョンで使用できるアドオン（WebExtensionsベースのアドオン）では、できる事が技術的に制限されているため、危険性は大幅に減じています。

[^1]: <a href='https://hg.mozilla.org/releases/mozilla-release/log/5b264ffa56e752df9a66c8a781e06fde51ada9e8/security/apps/addons-public.crt'>編集履歴</a>によると、このルート証明書は2015年に電子署名の仕組みが導入された時に追加されて以降特に変更された様子はありません。ルート証明書自体は2025年3月15日まで有効なので当面は問題無く利用できますが、このファイルがもし今後更新されないままであれば、2025年に再び同様のトラブルが起こる事になるでしょう。その場合、署名の検証を無効化できなくなっている現在のリリース版Firefoxをサポート切れのまま使い続けていると、アドオンを再度有効化する手立てが全く無くなるという事にもなりかねません。これらの観点からも、サポート切れのバージョンのFirefoxを使い続ける事にはリスクがあり、原則としてサポートが継続しているバージョンを使い続ける事が望ましいという事が言えます。

[^2]: 各アドオンの署名データに含まれる中間証明書と同じSubjectの中間証明書がFirefoxの証明書データベースにも含まれるようになり、以後はそちらが優先的に認識されるために中間証明書の期限切れ問題が解消する、という理屈です。

[^3]: 一度証明書データベース登録された中間証明書は、明示的に削除しない限りはFirefox 66.0.5への更新後もそのまま残ります。
