---
tags:
- fluentd
title: Fluentd v0.12.16でプラグインの設定をコマンドラインから確認する方法
---
[Fluentd v0.12.16 has been released | Fluentd](http://www.fluentd.org/blog/fluentd-v0.12.16-has-been-released)でも、詳しく紹介されていない機能ですがFluentd v0.12.16からはコマンドライン上でプラグインがどんな設定項目を持っているか確認することができるようになりました。
<!--more-->


例えば`fluent-plugin-s3`のアウトプットプラグインであれば以下のようにして使うことができます。

```text
$ fluentd --show-plugin-config output:s3
2015-10-09 15:25:56 +0900 [info]: Show config for output:s3
2015-10-09 15:25:56 +0900 [info]: 
path: string: <"">
use_server_side_encryption: string: <nil>
aws_key_id: string: <nil>
aws_sec_key: string: <nil>
aws_iam_retries: integer: <5>
s3_bucket: string: <nil>
s3_region: string: <"us-east-1">
s3_endpoint: string: <nil>
s3_object_key_format: string: <"%{path}%{time_slice}_%{index}.%{file_extension}">
store_as: string: <"gzip">
auto_create_bucket: bool: <true>
check_apikey_on_start: bool: <true>
proxy_uri: string: <nil>
reduced_redundancy: bool: <false>
storage_class: string: <"STANDARD">
format: string: <"out_file">
acl: string: <:private>
assume_role_credentials
 role_arn: string: <nil>
 role_session_name: string: <nil>
 policy: string: <nil>
 duration_seconds: integer: <nil>
 external_id: string: <nil>
instance_profile_credentials
 retries: integer: <nil>
 ip_address: string: <nil>
 port: integer: <nil>
 http_open_timeout: float: <nil>
 http_read_timeout: float: <nil>
shared_credentials
 path: string: <nil>
 profile_name: string: <nil>
```


`--show-plugin-config`の引数は`<プラグインの種類>:<プラグインの名前>`で指定します。
現時点ではプラグインの種類は以下の4種類あります。

  * input

  * output

  * filter

  * buffer

ほとんどの場合、プラグインの名前は`fluent-plugin-s3`であれば`s3`のようにプラグインのGemの名前から`fluent-plugin-`を除いたものになります。

`--show-plugin-config`にはいくつか制限事項もあります。

  * fluentdでロードできないプラグインの設定項目を確認することはできません

  * `config_param`のブロックを使って設定を書いている場合は、ブロックによって設定されるデフォルト値などは確認できません

  * プラグインの`configure`メソッドで動的にデフォルト値を設定している場合も確認できません

### プラグイン作者の方へ

fluentd v0.12.16 から使える`--show-plugin-config`オプションですが、プラグイン側で`config_param`に`desc`オプションを書いてあげると`--show-plugin-config`したときに`desc`に指定した説明が表示されます。

`desc`オプションを追加しても`--show-plugin-config`を持たないバージョンのfluentdでも問題なく動作します。

なので`--show-plugin-config`で表示される情報を充実させたい場合は以下のように説明を書いてください。

```ruby
module Fluent
# ...省略
  class S3Output < Fluent::TimeSlicedOutput
    Fluent::Plugin.register_output('s3', self)

    # ...省略

    config_param :path, :string, :default => "",
                 :desc => %Q(path prefix of the files on S3. Default is ""(no prefix))
    config_param :use_server_side_encryption, :string, :default => nil
    config_param :aws_key_id, :string, :default => nil, :secret => true,
                 :desc => "AWS access key id. This parameter is required when your agent is not running on EC2 instance with an IAM Role."
    config_param :aws_sec_key, :string, :default => nil, :secret => true,
                 :desc => "AWS secret key. This parameter is required when your agent is not running on EC2 instance with an IAM Role."
    # ...省略

```


説明を追加すると以下のように表示されます。

```text
 bundle exec fluentd --show-plugin-config output:s3
2015-10-09 16:02:35 +0900 [info]: Show config for output:s3
2015-10-09 16:02:35 +0900 [info]: 
path: string: <""> # path prefix of the files on S3. Default is ""(no prefix)
use_server_side_encryption: string: <nil>
aws_key_id: string: <nil> # AWS access key id. This parameter is required when your agent is not running on EC2 instance with an IAM Role.
aws_sec_key: string: <nil> # AWS secret key. This parameter is required when your agent is not running on EC2 instance with an IAM Role.
aws_iam_retries: integer: <5>
# ... 省略
```


Fluentd本体にバンドルされているプラグインでも`desc`オプションに対応しているプラグインはまだありません。自作のプラグインに対応するついでにFluentd本体にPull Requestを送ってみるのはどうでしょうか？
