---
tags:
- ruby
title: Rails 2.3.2でActiveLdapを使う
---
2009/3/24時点でのActiveLdapの最新リリースは1.0.2ですが、ActiveLdap 1.0.2はRails 2.3.2には対応していません。これは、ActiveLdap 1.0.2の方がRails 2.3.2より早くリリースされたからです。基本的に、ActiveLdapはリリース時点での最新のRailsに対応していますが、未来のRailsには対応できていません。
<!--more-->


### Rails 2.3.2とRuby 1.9.1とActiveLdap

さて、そんなActiveLdapですが、trunkではRails 2.3.2に対応しています。（[Issue 18 - ruby-activeldap - [Rails 2.3 Support] :: Running WEBrick Hangs - Google Code](http://code.google.com/p/ruby-activeldap/issues/detail?id=18)）

また、Rails 2.3.2だけではなく、Ruby 1.9.1にも対応しています。（[Issue 20 - [Ruby 1.9 Support] :: Running Tests - Google Code](http://code.google.com/p/ruby-activeldap/issues/detail?id=20)）
対応の過程で[Alexey.Chebotar](http://code.google.com/u/Alexey.Chebotar/)さんが、Ruby/LDAPをRuby 1.9.1に対応しました。

### Ruby 1.9.1対応Ruby/LDAP

[Ruby/LDAP](http://ruby-ldap.sourceforge.net/)はOpenLDAPのRubyバインディングで最終リリースが2006/8/9です。Alexeyさん以外にも[Ruby 1.9.1対応した成果をフィードバック](http://sourceforge.net/tracker/index.php?func=detail&aid=2622809&group_id=66444&atid=514521)している人がいるのですが、まだ対応してもらえていないようです。

Alexeyさんもメンテナの方に連絡をとってみたということですが、まだ対応してもらえていないようです。パッチの形で転がっているよりも、パッチが取り込まれた形の方が利用しやすいということと、AlexeyさんがRuby/LDAPをメンテナンスする意志があるということだったので、ActiveLdapのリポジトリにRuby 1.9.1対応版のRuby/LDAPが入っています。（[リポジトリ](http://ruby-activeldap.googlecode.com/svn/ldap/trunk/)）Ruby/LDAPをRuby 1.9.1で使いたいという方はこれを使ってみるとよいと思います。

### Rails 2.3.2とActiveLdapのtrunk

少し脱線しましたが、ActiveLdapのtrunkをRailsアプリケーションで使う方法がActiveLdapプロジェクトのWikiにあります。（[UsingTrunkWithRailsJa](http://code.google.com/p/ruby-activeldap/wiki/UsingTrunkWithRailsJa)）
Rails 2.3.2でActiveLdapを使いたい場合はこの方法を参考にしてください。

当初は次のリリースは1.1.0と考えていたのですが、1.1.0の前に、Rails 2.3.2に対応している現時点のものを1.0.3をリリースすることも考えています。いくつか予定されていた項目がまだ実現されていないので1.1.0を出すことはできないのですが、その前にRails 2.3.2対応版を出すことには価値があるかもしれないと思うようになってきました。[^0]

1.1.0に予定されていた項目の1つにドキュメントの整備というものがあります。この項目もまだ実現できていない項目の1つなのですが、現在、[id:tashen](http://d.hatena.ne.jp/tashen/)さんが[チュートリアルの日本語訳](http://code.google.com/p/ruby-activeldap/wiki/TutorialJa)を行ってくれています。（ありがとうございます！）

ActiveLdapは活発に開発が行われているライブラリなので、RubyでLDAPを便利に操作したい場合はActiveLdapを使うことを検討してみてください。そして、より使いやすく有用なライブラリにするために、バグ修正やドキュメント作成などでプロジェクトに協力してくれる人を募集しています。お待ちしています。

[^0]: Rails 2.3.2に対応していないというIssueが2つ登録されたりしたので。
