---
tags:
- mozilla
title: Mozilla Firefox/Thunderbirdの法人利用者向けFAQを編纂中です
---
現在、Mozilla Japan主導でFirefoxとThunderbirdの法人利用者向け情報の整理が進められています。
クリアコードもMozilla Japanサポートパートナーとしてこの作業に協力しており、弊社内で蓄積されてきた技術情報を積極的にFAQ項目へフィードバックしております。
<!--more-->


この作業はGitHub上の公開のリポジトリで行われており、前述のFAQも、現状の最新版をWeb上で参照できるようになっています。

  * [Firefox/Thunderbirdの法人利用者向けFAQ（ソース）](https://github.com/mozilla-japan/enterprise/blob/master/FAQ.md)

現在は上記のGitHub上のリポジトリでしか参照することができませんが、近いうちに他の情報も併せて普通のWebサイトとして公開される見込みです。

また、せっかくのGitHubですので、「こんな項目もあった方がよい」といった意見や追加の情報がありましたら、随時プルリクエストの形でお送り頂ければ幸いです。

### 背景

サポートパートナーとしてMozilla製品のサポート事業を展開している弊社では、FirefoxやThunderbirdの導入を検討されている法人のお客様からのお問い合わせを頂くことが度々あります。
その際、実際にお話を伺ってみると、いくつかのお客様で共通したご質問・ご要望を頂くことが多いという事実がありました。

ところが、調査してみると、Web上にはそれらの要望に応えられるような直接的な情報はなかなか見つかりませんでした。
（だからこそ弊社にお問い合わせ頂いたというわけなのですが……）
これは、一般的な個人ユーザと法人ユーザではFirefoxやThunderbirdに求めるカスタマイズの性質が異なることが多く、[Mozilla サポート](https://support.mozilla.org/ja/home)のような個人のボランティアベースでのナレッジには、「法人のシステム管理担当者の方が社内にFirefoxやThunderbirdを展開する」という観点での有用な情報が集まりにくい傾向があるからだと考えられます。

一方で、サポート事業を継続的に行ってきた中で弊社内に蓄積されてきた技術情報について、一般向けに整理し公開するための適切な場が存在しないという問題もありました。
いくつかの情報は[Mozilla Developer Network](https://developer.mozilla.org/ja/)やこのククログで断片的に公開していますが、残念ながら、上記のような観点で参照しやすい状態にはなっていません。

### FAQの編纂方針

[現在編纂中のFAQ](https://github.com/mozilla-japan/enterprise/blob/master/FAQ.md)では、「法人のシステム管理担当者の方が社内にFirefoxやThunderbirdを展開する」という具体的なユースケースを前提として項目を検討し、記載しています。
これにより、今までは弊社のようなサポートパートナーでなければ応えられなかったような法人ユーザ特有のカスタマイズの要望を、ユーザ自身（法人内のシステム管理担当者の方）の手である程度までは自己解決できるようになるものと思われます。

現時点では、目に見えてニーズの多い物からということで、デスクトップ環境でFirefox（24ESR以降のバージョン）を利用する場合の情報を優先してFAQ項目を編纂しています。
将来的には、モバイル環境での利用やThunderbird向けのFAQ項目も拡充していく予定です。

### FAQで紹介しているアドオンやその他のソフトウェアについて

弊社では省力化その他の観点から、FirefoxやThunderbirdのカスタマイズ内容について、ニーズが多い物は可能な限り設定ファイルやアドオンの組み合わせによってカスタマイズを完結できるようにする方針を持っています。
その結果として相当数のアドオンを開発・公開していますが、そのような方針に基づいて開発されているため、個々のアドオンは単体では利用し辛かったり、利用方法が分かりにくかったりする部分がありました。

上記のFAQでは、具体的な要望に対して「その場合はこのアドオンを使うとよい」「アドオンをこのように設定するとよい」という形で利用法を紹介しています。
また弊社開発のアドオン以外や、アドオンを使用しない場合のカスタマイズ方法についても同様に紹介しており、運用の都合に合わせた最適な方法を選択できるように記述する事を意識しています。

なお、FAQで紹介しているアドオンは以下のページから閲覧・入手することができます。

  * [Mozilla Add-ons上の弊社アカウントにおける公開済みアドオンの一覧](https://addons.mozilla.org/firefox/user/clearcode-inc/)（レビューが完了し、公開が承認されているアドオンのみが表示されます）
  * [GitHub上の弊社アカウントにおけるプロジェクト一覧](https://github.com/clear-code/)（アドオン以外のプロジェクトも表示されます）

具体的には、今回のFAQに関連する以下のアドオンはすべて公開済みです。

  * [Always Default Client](https://addons.mozilla.org/firefox/addon/always-default-client/)
  * [AutoConfiguration Hook](https://addons.mozilla.org/firefox/addon/autoconfiguration-hook/)
  * [Cert Importer](https://addons.mozilla.org/firefox/addon/cert-importer/)
  * [Customizable Shortcuts](https://addons.mozilla.org/firefox/addon/customizable-shortcuts/)
  * [Disable about:config](https://addons.mozilla.org/firefox/addon/disable-aboutconfig/)
  * [Disable Addons](https://addons.mozilla.org/firefox/addon/disable-addons/)
  * [Disable Auto Update](https://addons.mozilla.org/firefox/addon/disable-auto-update/)
  * [Disable Sync](https://addons.mozilla.org/firefox/addon/disable-sync/)
  * [Do Not Save Password](https://addons.mozilla.org/firefox/addon/do-not-save-password/)
  * [Flex Confirm Mail](https://addons.mozilla.org/thunderbird/addon/flex-confirm-mail/)
  * [Force Addon Status](https://addons.mozilla.org/firefox/addon/force-addon-status/)
  * [globalChrome.css](https://addons.mozilla.org/firefox/addon/globalchromecss/)
  * [Hide Option Pane](https://addons.mozilla.org/firefox/addon/hide-option-pane/)
  * [History Prefs Modifier](https://addons.mozilla.org/firefox/addon/history-prefs-modifier/)
  * [IMAPキャッシュの自動消去（Clear IMAP Cache）](https://addons.mozilla.org/thunderbird/addon/clear-imap-local-cache/)
  * [Only Minor Update](https://addons.mozilla.org/firefox/addon/only-minor-update/)
  * [Permissions Auto Registerer](https://addons.mozilla.org/firefox/addon/permissions-auto-registerer/)
  * [UI Text Overrider](https://addons.mozilla.org/firefox/addon/ui-text-overrider/)
  * [Windowsショートカットの直接実行（Open Windows Shortcuts Directly）](https://github.com/clear-code/openshortcuts/releases)（危険なアドオンであるためMozilla Add-onsサイトでの公開は不可能とのこと）
  * [Winmail Opener Bridge](https://addons.mozilla.org/thunderbird/addon/winmail-opener-bridge/)
  * [ローカルファイルからのリンク挿入（Insert Link from Local File）](https://addons.mozilla.org/thunderbird/addon/insert-link-from-local-file/)
  * [不正なアドレスの警告表示パッチ（Patch to Alert Invalid Addresses）](https://addons.mozilla.org/thunderbird/addon/patch-to-alert-invalid-addr/)
  * [添付ファイルの文字エンコーディングの自動判別（Attachemnt Encoding Detector）](https://addons.mozilla.org/thunderbird/addon/attachemnt-encoding-detecto/)

また、これらのアドオンや設定ファイルを同梱した組織内向けカスタマイズ済みFirefox・Thunderbirdのインストーラを作成するための仕組みとして、弊社では「Fx Meta Installer」というソフトウェアも開発・公開しています。
こちらも併せてご参照下さい。

  * [Fx Meta Installer](https://github.com/clear-code/fx-meta-installer)
  * [Fx Meta Installerのチュートリアル]({% post_url 2012-11-07-index %})

### まとめ

  * Mozilla Japanと協力して、FirefoxおよびThunderbirdの法人利用者向けFAQをGitHub上で編纂・公開しています。
    * Mozilla製品の法人利用に関しては、今後も情報公開を進めていく予定です。
    * 他にも有用な情報をお持ちの方のご協力をお待ちしております。
  * FAQの公開に併せて、FAQで紹介しているアドオンのMozilla Add-onsでの公開を開始しました。
    * 未登録のアドオンも順次登録していく予定です。

クリアコードでは、Mozilla製品の導入を検討中あるいは導入済みの法人ユーザさまからのお問い合わせを引き続きお待ちしております。
