---
tags: []
title: Launchpadを利用してパッケージを公開するには
---
### はじめに

オープンソースのカラムストア機能付き全文検索エンジンのひとつに[Groonga](http://groonga.org/ja)があります。Groongaを使うと全文検索機能付き高性能アプリケーションを開発することができます。
<!--more-->


Groongaでは各種ディストリビューション向けにパッケージを提供[^0]しています。そのため、比較的簡単に[インストール](http://groonga.org/ja/docs/install.html)できるようになっています。

今回はGroongaのパッケージを公開するのに独自リポジトリでの提供をやめ、[Launchpad](https://launchpad.net/)を利用してパッケージを公開するようにした話を紹介します。

#### Launchpadに移行する理由

Groongaは各種ディストリビューション向けに独自にリポジトリを提供しています。リリースの際にはchroot環境でパッケージをビルドする仕組みになっています。これはこれで良いのですが、Groongaのビルドには最低2GBのメモリが必要ですし、Debian/Ubuntu/CentOS向けにビルドしていることやアーキテクチャの違いなども含めると、毎回結構な数のパッケージをビルドしていることになります。ビルドするためのPCの負荷もなかなかのものです。並列で走らせてもビルドが完了するまでに時間もかかります。ビルドのために別のPCを確保したので、PCの負荷については改善できましたが、リリースのためのコストという面では問題がありました。

そこで、Ubuntuではパッケージのビルドとリポジトリのホスティングを請け負ってくれるPPA（Personal Package Archive）という仕組みを利用することにしました。

Launchpadに移行するメリットは次のとおりです。

  * ソースパッケージさえアップロードしてしまえばあとは勝手にビルドしてくれる
  * パッケージのリポジトリを公開するサーバーを自前でメンテナンスしなくても良い
  * 手元にパッケージをビルドするための環境（chroot）を維持する必要がない
  * リポジトリのためのディスク容量を節約できる

一方、Launchpadに移行するデメリットは次のとおりです。

  * パッケージができるまで長いと半日くらい待たされることがある
  * EOLなリリース向けにはパッケージを作成できない

Groongaの場合はEOLになったらすぐにサポートもやめているので、上記のデメリットは問題になりません。デメリットよりも移行することで得られるメリットが勝っているので移行することにしました。

### Launchpadでパッケージを公開するには

Launchpadでパッケージを公開するのに必要な情報は[Packaging PPA BuildingASourcePackage](https://help.launchpad.net/Packaging/PPA/BuildingASourcePackage)にあります。公開するための手順を、最初にする作業と毎回する作業とに分けて説明します。

#### 最初にする作業

ここでは一度だけする必要がある作業を説明します。

  * Launchpadへのユーザー登録
  * Launchpadへの公開鍵の登録
  * CoCへの署名
  * チームの登録
  * チームへのユーザー登録
  * PPAの登録
  * dputの設定

##### Launchpadへのユーザー登録

まずは、Launchpadのアカウントが必要です。[Create an account](https://login.launchpad.net/+new_account)でアカウントを作成します。

##### Launchpadへの公開鍵の登録

Launchpadのアカウントでログインして、プロファイルのページからPGPの鍵を登録します。
[Ubuntu Weekly Recipe 第46回　PPAの活用](http://gihyo.jp/admin/serial/01/ubuntu-recipe/0046?page=2)の「公開鍵の登録」を参照すると良いでしょう。

##### CoCへの署名

PPAを利用するにはCode of Conductへの署名が必要です。具体的な手順としては[Ubuntu Weekly Recipe 第46回　PPAの活用](http://gihyo.jp/admin/serial/01/ubuntu-recipe/0046)の「CoCへの署名」を参照すると良いでしょう。

ただし、[ubuntuパッケージをlaunchpad ppaで公開する方法](http://d.hatena.ne.jp/bellbind/20100918/1284779708)にも書いてあるように、CoCテキストのダウンロードは、ブラウザで表示されている指定されたテキストをコピーしてそれに署名するやりかたに変更になっています。

##### チームの登録

Groongaの場合は、リリース担当者が変わることもあるので、[Groongaチーム](https://launchpad.net/~groonga)を作ることにしました。チームを作るにはログインした状態で[Register a team](https://launchpad.net/people/+newteam)で登録します。

##### チームへのユーザー登録

リリース担当者を必要に応じてチームのページから[Add member](https://launchpad.net/~groonga/+addmember)で登録します。

##### PPAの登録

チームを作成した時点では、PPAは利用できるようになっていません。そこで公式リリース版とベータ版用途の次の2つのPPAを作成することにしました。

  * ppa （正式リリース版）
  * nightly （ベータ版、テスト用途）

##### dputの設定

後述しますが、ソースパッケージをアップロードするのにdputを使います。そのため、~/.dput.cfに設定を追加しておきます。

{% raw %}
```
[groonga-ppa]
fqdn = ppa.launchpad.net
method = ftp
incoming = ~groonga/ppa/ubuntu
login = anonymous
allow_unsigned_uploads = 0

[groonga-nightly]
fqdn = ppa.launchpad.net
method = ftp
incoming = ~groonga/nightly/ubuntu
login = anonymous
allow_unsigned_uploads = 0
```
{% endraw %}

#### 毎回する作業

ここではリリースごとにする作業を説明します。

##### パッケージの作成とアップロード

最初にする作業をすべて終えたら、いよいよパッケージのアップロードにうつります。

Groongaの場合には、すでにdebパッケージをビルドするためにdebian/*ファイル群があったので、それらを利用してソースパッケージをビルドしました。

必要な手順は次のとおりです。

  1. ソースアーカイブ（groonga-X.Y.Z.tar.gz）をリネーム（groonga_X.Y.Z.orig.tar.gz）する

  1. ソースアーカイブを展開する

  1. debianディレクトリを展開したディレクトリにコピーする

  1. dchコマンドでディストリビューションとバージョンを更新する（dch --distribution --newversionオプションを指定）

  1. debuild -S -saでソースパッケージをビルドする

  1. dput groonga-ppa （.changesファイルを指定）でLaunchpadにアップロード


上記を対象となるリリース（precise/saucy/trusty）ごとに行います。
きちんとアップロードできるとアップロードを受理したことを通知するメールが届きます。あとはビルドが完了するまで待つだけです。

パッケージのビルド結果は[View package details](https://launchpad.net/~groonga/+archive/ppa/+packages)から確認できますし、もしビルドに失敗した場合には通知メールが届きます。

よくあるのは、controlファイルにビルドに必要な依存関係の記述が抜けていたりすることです。失敗した場合の通知メールにはビルドログへのリンクもありますし、上記詳細ページをたどることでもログを確認できます。ビルドに失敗したソースパッケージはDelete packagesというリンクにアクセスすることで削除可能です。

Groongaの場合には、ここまでの手順をまとめてRubyスクリプトにして（upload.rb）省力化しています。

{% raw %}
```ruby
def upload(code_name)
  in_temporary_directory do
    FileUtils.cp(@source_archive.to_s,
                 "#{@package}_#{@version}.orig.tar.gz")
    run_command("tar", "xf", @source_archive.to_s)
    directory_name = "#{@package}-#{@version}"
    Dir.chdir(directory_name) do
      FileUtils.cp_r(@debian_directory.to_s, "debian")
      deb_version = "#{current_deb_version.succ}~#{code_name}1"
      run_command("dch",
                  "--distribution", code_name,
                  "--newversion", deb_version,
                  "Build for #{code_name}.")
      run_command("debuild", "-S", "-sa", "-pgpg2", "-k#{@pgp_sign_key}")
      run_command("dput", @dput_configuration_name,
                  "../#{@package}_#{deb_version}_source.changes")
    end
  end
end
```
{% endraw %}

上記は[upload.rb](https://github.com/groonga/groonga/blob/master/packages/ubuntu/upload.rb)の抜粋です。

### Launchpadへの移行の影響

Launchpadでパッケージを公開するようにしたことで、従来aptのリポジトリを登録してもらっていた手順が次のように変更になりました。

{% raw %}
```
% sudo apt-get -y install software-properties-common
% sudo add-apt-repository -y universe
% sudo add-apt-repository -y ppa:groonga/ppa
% sudo apt-get update
```
{% endraw %}

これでGroongaをインストールするのに、従来通りにapt-getで簡単にインストールできるようになりました。

{% raw %}
```
% sudo apt-get -y install groonga
```
{% endraw %}

#### まとめ

今回はGroongaのパッケージを公開するのに独自リポジトリでの提供をやめ、Launchpadを利用してパッケージを公開するようにした話を紹介しました。
すでにdebパッケージがつくれる状態であれば、対象となるリリースごとに必要とされるライブラリに違いがあったり、他のソースパッケージが必要だったりしない限り、Launchpadでパッケージを公開するのは比較的容易です。まだソースアーカイブしか公開していないけど、ゆくゆくはUbuntu向けにパッケージもきちんと提供したいというソフトウェアがあれば、Launchpadを活用してみてはいかがでしょうか。

[^0]: Debian/Ubuntu/CentOSの場合はpackages.groonga.orgでリポジトリを提供していて、Fedoraの場合にはFedora公式リポジトリからインストールできるようになっています。
