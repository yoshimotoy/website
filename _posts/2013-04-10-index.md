---
tags: []
title: Fedoraプロジェクトで新規パッケージをリリースする方法
---
### はじめに

今回は、[Fedoraプロジェクト](https://fedoraproject.org)で新規パッケージをリリースする方法を、[Cutter](http://cutter.sourceforge.net/index.html.ja)を[リリースしたときの経験](https://bugzilla.redhat.com/show_bug.cgi?id=887778)をもとに紹介します。
<!--more-->


Cutterプロジェクトでは、従来はFedora向けに独自にリポジトリを用意してRPMを提供していました。しかし、この方法では、ユーザーがCutterをインストールするときにリポジトリを追加登録する手間がかかり、不便でした。
そこで、FedoraでCutterを利用したいユーザーがもっと簡単に導入できるように、FedoraプロジェクトからRPMを提供するようにしました[^0]。

### 新規パッケージリリースまでの流れ

新規パッケージの公開までの流れについては、[Join the package collection maintainers](https://fedoraproject.org/wiki/Join_the_package_collection_maintainers)に詳細がまとめられています。

このドキュメントは[日本語訳](https://fedoraproject.org/wiki/Join_the_package_collection_maintainers/ja)もあります。
ところどころ内容が最新版に追従していませんが、参考としては十分です。

まずはじっくり上記ドキュメントを読まれることをお勧めしますが、簡単に紹介すると以下のようになります。

  * Bugzillaアカウントの取得
  * Fedora Account System（FAS）のアカウントの取得
  * メーリングリストへの参加
  * レビューリクエストの準備
  * レビューリクエストの投稿
  * レビューリクエストの承認
  * SCMリクエスト
  * パッケージのインポート
  * パッケージのコミット
  * パッケージのビルド
  * パッケージのアップデート
  * パッケージのテスト
  * パッケージのリリース

それでは、順を追って説明していきます。

#### Bugzillaアカウントの取得

まずは[Red Hat Bugzilla](https://bugzilla.redhat.com/)のアカウントを取得します。

Bugzillaのアカウントは[New Account](https://bugzilla.redhat.com/createaccount.cgi)のリンクから作成します。

このアカウントは後述するレビューリクエストを行うときに必要になります。

#### Fedora Account System（FAS）のアカウントの取得

Fedoraプロジェクトでパッケージをメンテナンスするためには、FASアカウントと呼ばれるメンテナンスのためのアカウントが必要です。

FASアカウントは[Fedora Account System](https://admin.fedoraproject.org/accounts/)にアクセスし[New Account](https://admin.fedoraproject.org/accounts/user/new)のリンクから作成します。

このアカウントはレビューリクエストを行う際や、レビューリクエストが承認されたあとのパッケージのインポートやコミット等のリポジトリ関連の作業に使います。

##### Contributors License Agreement（CLA）への同意

FASアカウントを登録したら、Contributors License Agreement（貢献者ライセンス同意書、以下CLA）に同意する必要があります。

同じくFASアカウントでログインした状態から complete the CLA のリンクをたどり、ライセンスを読んだうえで、同意します。

詳しくは[1.5. CLA に署名する](http://docs.fedoraproject.org/ja-JP/Fedora_Contributor_Documentation/1/html/Translation_Quick_Start_Guide/sect-Translation_Quick_Start_Guide-Accounts_and_Subscriptions-Signing_the_CLA.html)を参照してください。

![FASアカウントでCLAに同意した状態のスクリーンショット]({{ "/images/blog/20130410_0.png" | relative_url }} "FASアカウントでCLAに同意した状態のスクリーンショット")

CLAに同意すると、図の状態になります。

##### 公開鍵の登録

FASアカウントを登録したら、ログイン後、"My Account"メニューよりSSHの公開鍵を登録します。

ここで登録した鍵を用いて後述するfedorapeople.orgやリポジトリへとアクセスします。

後々の便利のために、~/.ssh/configに以下の設定を追加しておくと良いでしょう（IdentityFile ~/.ssh/id_rsa.fedora.pkeyの設定は個々の環境で読み替えてください）。

{% raw %}
```
HOST *.fedoraproject.org fedorapeople.org *.fedorahosted.org
     IdentityFile ~/.ssh/id_rsa.fedora.pkey
```
{% endraw %}

#### メーリングリストへの参加

これは必須ではありませんが、他のパッケージのレビューを見ることで、パッケージング作業についてより深く学べます。

  * [package-review](https://admin.fedoraproject.org/mailman/listinfo/package-review)

他にもいくつか参考になるメーリングリストが存在します。
必要に応じて参加すると良いでしょう。

<table>
 <tr><th>メーリングリスト</th><th>説明</th></tr>
 <tr><td><a href="https://lists.fedoraproject.org/mailman/listinfo/devel-announce">devel-announce</a></td><td>アナウンス向けのメーリングリスト。流量が少ない。最近だとFedora 19のAlphaフリーズのアナウンスが流れた。</td></tr>
 <tr><td><a href="https://lists.fedoraproject.org/mailman/listinfo/devel">devel</a></td><td>開発者向けのメーリングリスト。流量が多い。</td></tr>
 <tr><td><a href="https://admin.fedoraproject.org/mailman/listinfo/package-announce">package-announce</a></td><td>パッケージのリリースアナウンス向けのメーリングリスト。パッケージがstable入りしたのを知るために登録する。</td></tr>
</table>


#### レビューリクエストの準備

新規パッケージを登録するためには、レビューというプロセスを経る必要があります。
レビューをしてもらうには以下の情報が必要です。

  * specファイルのURL
  * SRPMファイルのURL
  * FASアカウント名

specファイルやSRPMファイルの場所は一般にアクセス可能な場所であれば、どこでも構いません。

Cutterの場合は[Sourceforge.net](http://sourceforge.net/)を利用していたので、[一時ディレクトリ](http://sourceforge.net/projects/cutter/files/tmp/)を用意して、そこにファイルをアップロードしていました。

レビューリクエストを行うまえに、あらかじめパッケージに問題がないか確認する方法をいくつか紹介します。

  * rpmlintによるチェック
  * Kojiによるビルドチェック
  * mockによるビルドチェック
  * fedora-reviewによるチェック

##### rpmlintによるチェック

パッケージに問題がないか確認する最もシンプルな方法です。

{% raw %}
```
$ rpmlint -i *.rpm
```
{% endraw %}

パッケージのレビューに関しては[ReviewGuidelines](http://fedoraproject.org/wiki/Packaging:ReviewGuidelines)というドキュメントがあります。
このドキュメントには、レビュワーがしなければいけないことの1つとして「rpmlintの実行」が記載されています。

あらかじめrpmlintで報告されている警告やエラーを潰しておくと、レビューしてもらうときの余計なやりとりを減らすことができます[^1]。

##### Kojiによるビルドチェック

Fedoraプロジェクト向けにRPMパッケージをビルドするシステムとして[Koji](https://fedoraproject.org/wiki/Koji)があります。

Kojiを使うと複数のリリースもしくは複数のアーキテクチャ向けにRPMパッケージのビルドがうまくいくか確認することができます[^2]。

Kojiを使うにはいくつかセットアップが必要です。

Kojiについての詳細は[Using the Koji build system](https://fedoraproject.org/wiki/Using_the_Koji_build_system)を参照してください。

まずはfedora-packagerをインストールします。

{% raw %}
```
$ sudo yum install fedora-packager
```
{% endraw %}

次にfedora-packager-setupを実行します。このとき、FASアカウントの入力が必要です。
また、ブラウザ向けのクライアント証明書のパスワードの入力も必要です。

{% raw %}
```
$ fedora-packager-setup

Setting up Fedora packager environment
You need a client certificate from the Fedora Account System, lets get one now
FAS Username: kenhys
FAS Password: 
Saved: /home/kenhys/.fedora-server-ca.cert
Linking: ~/.fedora-server-ca.cert to ~/.fedora-upload-ca.cert
Setting up Browser Certificates
Enter Export Password:
Verifying - Enter Export Password:

Browser certificate exported to ~/fedora-browser-cert.p12
To import the certificate into Firefox:

Edit -> Preferences -> Advanced
Click "View Certificates" 
On "Your Certificates" tab, click "Import" 
Select ~/fedora-browser-cert.p12
Type the export passphrase you chose earlier

Once imported, you should see a certificate named "Fedora Project".
Your username should appear underneath this.

You should now be able to click the "login" link at http://koji.fedoraproject.org/koji/ successfully.

importing the certificate is optional and not needed for daily use.
you should also import the ca cert at ~/.fedora-upload-ca.cert
```
{% endraw %}

パスワードを入力し終わると処理が進みます。

fedora-packager-setupコマンドは以下の4つのファイルをホームディレクトリへと保存します[^3]。

  * .fedora-server-ca.cert
  * .fedora-upload-ca.cert（.fedora-server-ca.certへのシンボリックリンク）
  * .fedora.cert
  * fedora-browser-cert.p12

この手順は通常一度だけ必要です[^4]。

これで、Kojiを使う準備が整いました。

Kojiを使ってビルドを試すには、以下の様に--scratchオプションを与えてkojiコマンドを実行します。

{% raw %}
```
$ koji build --scratch f18 cutter-1.2.2-4.fc18.src.rpm
```
{% endraw %}

KojiでのビルドはすべてFedoraプロジェクトのビルドサーバーで行うので、他のビルドとの兼ね合いからビルドが完了するまでそれなりに時間がかかります。

##### Mockによるビルドチェック

依存関係などが適切に処理されているかについては[Mock](http://fedoraproject.org/wiki/Projects/Mock)によるビルドを行うことで確認できます。

KojiもMockを使って実現しています。

Mockではローカルにchroot環境を構築してRPMをビルドするようになっています。

常用しているFedoraの環境とは異なりクリーンな環境でビルドを行うため、依存関係の抜け等をチェックすることができます[^5]。

Mockを使うにはいくつかセットアップが必要です。

まずはmockグループに作業ユーザ（任意）を追加します。この作業は初回のみ必要です。

{% raw %}
```
$ sudo usermod -a -G mock （作業ユーザーアカウント）
```
{% endraw %}

次にmock環境を初期化します。この作業はビルドを試したい環境ごとに行う必要があります。

{% raw %}
```
$ mock -r fedora-18-x86_64 --init
```
{% endraw %}

最後にmockの特定環境でビルドするには以下のコマンドを実行します。

{% raw %}
```
$ mock -r fedora-18-x86_64 --no-clean cutter-1.2.2-4.fc18.src.rpm
```
{% endraw %}

mockコマンドはビルド結果のログを以下に生成します。また、ビルドしたRPMも同じ階層に保存します。

{% raw %}
```
/var/lib/mock/fedora-18-x86_64/result/build.log
```
{% endraw %}

Mockの詳細については[Using Mock to test package builds](http://fedoraproject.org/wiki/Using_Mock_to_test_package_builds)を参照してください。

##### fedora-reviewによるチェック

[fedora-review](https://fedorahosted.org/FedoraReview/)という専用のツールを使って確認することもできます。
以下のようにしてインストールします。

{% raw %}
```
$ sudo yum install fedora-review
```
{% endraw %}

fedora-reviewはレビュワー向けのツールなので、パッケージに関するより細かいレポート結果を得ることができます。
ローカル環境で実行するには以下のようにSRPMを指定します。

{% raw %}
```
$ fedora-review --rpm-spec --name cutter-1.2.2-4.fc18.src.rpm
```
{% endraw %}

実行が終了すると、fedora-reviewコマンドは（パッケージ名）/review.txtを生成します。
review.txtの内容は以下のように、チェックする項目とその結果を含んでいます。

{% raw %}
```
Package Review
==============

Key:
[x] = Pass
[!] = Fail
[-] = Not applicable
[?] = Not evaluated
[ ] = Manual review needed

Issues:
=======
- Package do not use a name that already exist
  Note: A package already exist with this name, please check
  https://admin.fedoraproject.org/pkgdb/acls/name/cutter
  See: https://fedoraproject.org/wiki/Packaging/NamingGuidelines#Conflicting_Package_Names

===== MUST items =====

C/C++:
[ ]: Package does not contain kernel modules.
[ ]: Package contains no static executables.
[ ]: Development (unversioned) .so files in -devel subpackage, if present.
     Note: Unversioned so-files in private %_libdir subdirectory (see
     attachment). Verify they are not in ld path.
[x]: Header files in -devel subpackage, if present.
[x]: ldconfig called in %post and %postun if required.
[x]: Package does not contain any libtool archives (.la)
[x]: Rpath absent or only used for internal libs.

Generic:
[ ]: Package is licensed with an open-source compatible license and meets
     other legal requirements as defined in the legal section of Packaging
以下省略  
```
{% endraw %}

#### specファイルやSRPMのアップロードの注意事項

[Join the package collection maintainers](https://fedoraproject.org/wiki/Join_the_package_collection_maintainers)では、[Upload Your Package](https://fedoraproject.org/w/index.php?title=Join_the_package_collection_maintainers&action=edit&section=13)というセクションで、specやSRPMのアップロード先として[fedorapeople.org](http://fedorapeople.org/)を紹介しています。
しかし、最初のレビューリクエストを投稿する時点では、fedorapeople.orgへファイルをアップロードする権限がありません。

fedorapeople.orgへのアクセスするには、自分のFASアカウントを後述するpackagerグループに追加してもらう必要がありますが、これはパッケージのレビューが承認された後になるからです。

そのため、この時点ではどこか他の場所にspecやSPRMをアップロードして、レビューしてもらえる状態にする必要があります。

#### レビューリクエストの投稿

specファイルとSRPMファイルが用意できたら、[Bugzilla](https://bugzilla.redhat.com/)へレビューリクエストを投稿します。

レビューを投稿するときの雛形は、専用の入力フォームとして[fedora-review](https://bugzilla.redhat.com/bugzilla/enter_bug.cgi?product=Fedora&format=fedora-review)があります。

![レビュー雛形画面]({{ "/images/blog/20130410_1.png" | relative_url }} "レビュー雛形画面")

{% raw %}
```
Spec URL: <spec info here>
SRPM URL: <srpm info here>
Description: <description here>
Fedora Account System Username:
```
{% endraw %}

Cutterの場合は以下のようなレビューリクエストを投稿しました。

{% raw %}
```
Spec URL: http://sourceforge.net/projects/cutter/files/tmp/cutter.spec
SRPM URL: http://sourceforge.net/projects/cutter/files/tmp/cutter-1.2.2-1.fc17.src.rpm

Description: 
Cutter is a xUnit family Unit Testing Framework for C.
Cutter provides easy to write test and easy to debug code environment.
Fedora Account System Username: kenhys
```
{% endraw %}

  * Spec URL: SPECファイルのURLを記述します。
  * SRPM URL: SRPMファイルのURLを記述します。
  * Description: パッケージの説明を記述します。
  * Fedora Account System Username: ここにあらかじめ取得したFASアカウント名を記述します。

##### レビューリクエストの注意事項

レビューリクエストを行う際には、以下の点に注意する必要があります。

  * FE-NEEDSPONSORフラグの設定
  * fedora-reviewフラグの設定

新規パッケージを投稿するときには、FE-NEEDSPONSORフラグを設定しなければなりません。

これはパッケージのメンテナンスの権限を有するpackagerグループのうち、より高度な権限を持つsponsorという権限を持っている人にレビューをしてもらう必要があることを示します。

FE-NEEDSPONSORフラグは、レビューリクエストの左側にあるBlocks:という項目を編集します。

![FE-NEEDSPONSORフラグ]({{ "/images/blog/20130410_2.png" | relative_url }} "FE-NEEDSPONSORフラグ")

fedora-reviewフラグを設定するには、レビューリクエストのFlags:という項目を編集します。

![Flags(edit)]({{ "/images/blog/20130410_3.png" | relative_url }} "Flags(edit)")

Flags:は登録したレビューリクエスト画面の右側に項目があります。

Flags:の隣りの(edit)をクリックするといくつかプルダウンが表示されます。

![Flags]({{ "/images/blog/20130410_4.png" | relative_url }} "Flags")

このうち、fedora-reviewという項目があるので、プルダウンから?を選択して更新します。
fedora-reviewに設定できる値とその意味は以下の通りです。

<table>
  <tr><th>fedora-reviewフラグ</th><th>説明</th></tr>
  <tr><td>?</td><td>レビューリクエスト中</td></tr>
  <tr><td>+</td><td>レビューが承認された</td></tr>
  <tr><td>-</td><td>レビューが却下された</td></tr>
</table>


FE-NEEDSPONSORが設定されているレビューリクエストのみを[一覧で参照](https://bugzilla.redhat.com/showdependencytree.cgi?id=FE-NEEDSPONSOR)することもできます。

#### レビューリクエストの承認

レビューリクエストを投げたものの、それがそのまますんなり通るとは限りません。

Cutterの場合もレビューリクエストを最初に投げてから何度かspec記述上の問題点をレビュワーに指摘され、修正するというサイクルを繰り返しました。

レビュワーは、指摘事項の根拠となるドキュメントを示しながら、問題点をどう修正すべきかをコメントしてくれます。

例えば、Cutterの場合だと、指摘された問題点の一つにBuildRoot:タグを指定していたというものがありました。

{% raw %}
```
> BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u} -n)

https://fedoraproject.org/wiki/Packaging:Guidelines#BuildRoot_tag

The "fedora-review" tool complains about this tag, too.
```
{% endraw %}

すべての指摘事項を修正し、レビューを通ると以下の状態になります。

  * レビュワーがレビューリクエストにAPPROVEDというレビューコメントをつける（レビュワーによって表現は多少異なる場合があります）。
  * レビュワーがレビューリクエストのfedora-reviewフラグを+に変更する。
  * レビュワーがレビューリクエストした人を[packagerグループ](https://fedoraproject.org/wiki/How_to_get_sponsored_into_the_packager_group)に追加する。

packagerグループに入るとFedoraのサーバーへアクセスできるようになります[^6]。

fedora-reviewフラグが+になったら、次のステップ「SCMリクエスト」に進みます。

#### SCMリクエストについて

パッケージのレビューが通ったら次にやることは、新規パッケージのためにリポジトリを用意することです。

リポジトリについては、レビューリクエストのバグのコメントに[SCMリクエスト](https://fedoraproject.org/wiki/Package_SCM_admin_requests)と呼ばれる作業依頼のコメントを投稿します。

Cutterの場合は以下のようなSCMリクエストを行いました。

{% raw %}
```
New Package SCM Request
=======================
Package Name: cutter
Short Description: Unit Testing Framework for C/C++
Owners: kenhys
Branches: f18
InitialCC:
```
{% endraw %}

  * Package Name: パッケージ名を記述します。
  * Short Description: パッケージの説明を記述します。specのSummary:を書くなどすると良いでしょう。
  * Owners: 自身のFASアカウントを記述します。
  * Branches: リポジトリに追加するブランチを記述します。Cutterは当時のリリースであるFedora18（f18）ブランチのみ指定しました。
  * InitialCC: 他にもメンテナとして追加したい人のFASアカウントを記述します。

SCMリクエストを行ったあとは、しばらく待つ必要があります。
Cutterの場合は一日程度でした。
リポジトリを作成する権限のある人が、定期的にSCMリクエストをチェックし、順次対応してくれているようです。

SCMリクエストが処理され、リポジトリの準備が完了すると、レビューリクエストに以下のようなコメントが書き込まれます。

{% raw %}
```
Git done (by process-git-requests).
```
{% endraw %}

またfedora-cvs granted:という件名で、通知メールがあらかじめ登録しておいたアカウントに届きます。

これで、specとSRPMをコミットするためのリポジトリが準備できました。

次のステップ「パッケージのインポート」へと進みます。

これ以降の作業（「パッケージのインポート」から「パッケージのアップデート」まで）は、master（rawhide）[^7]とSCMリクエストで指定したブランチごとにくりかえす必要があります。

作業しているブランチによって一部手順が異なりますが、詳細については「ブランチごとの作業まとめ」として後述します。

#### パッケージのインポート

パッケージのインポートでは以下の作業が必要です。

  * SSHアクセスの確認
  * fedpkgのインストール
  * リポジトリのclone
  * SRPMのインポート

以下、順に説明していきます。

##### SSHアクセスの確認

レビューが通った後、レビュワーによりpackagerグループに追加してもらえます。
packagerグループに所属しているとFedoraのサーバーへとアクセスすることができます。

packagerグループに所属しているかどうかは、FASの"My Account"メニューを表示したときに表示される所属グループリストの"Fedora Packager Git Commit Group"でわかります。ここのStatus欄が"Approved"になっていると、packagerグループに所属していることになります。

![Fedora Packager Git Commit Group]({{ "/images/blog/20130410_5.png" | relative_url }} "Fedora Packager Git Commit Group")

以降の作業ではサーバーへのアクセスをともなうので、FASアカウントを作成したときに用意した秘密鍵を用いて、SSHでログインできるか確認します。

{% raw %}
```
$ ssh-add 秘密鍵のファイルパス
$ ssh -vvv -l （FASアカウント名） fedorapeople.org
```
{% endraw %}

SSHアクセスに成功したら、実際にパッケージのインポートを順に行っていきます。

##### fedpkgのインストール

Fedoraプロジェクトではパッケージのビルドやアップロードなどのメンテナンス作業にfedpkgというコマンドを使います。

fedpkgコマンドを使うには、あらかじめfedpkgパッケージをインストールする必要があります。

前述のKojiによるビルド確認を試している場合は、fedora-packagerのインストール時に、既に（パッケージの依存関係により）fedpkgをインストールしているはずです。
そうでない場合には以下の手順でインストールします。

{% raw %}
```
$ sudo yum install fedpkg
```
{% endraw %}

fedpkgのインストールが完了したら、次のステップ「リポジトリのclone」に進みます。

##### リポジトリのclone

まずは作業用のリポジトリをcloneします。
作業用のディレクトリを用意して、そこにcloneするのが良いでしょう。

{% raw %}
```
$ fedpkg clone （パッケージ名）
```
{% endraw %}

上記コマンドを実行することでリポジトリをcloneすることができます。

Cutterの場合は以下のように作業ディレクトリを用意してcloneしました。

{% raw %}
```
$ mkdir -p $HOME/work/fedora-scm
$ cd $HOME/work/fedora-scm
$ fedpkg clone cutter
$ cd cutter
```
{% endraw %}

##### SRPMのインポート

リポジトリのcloneができたら、SRPMを以下のコマンドにてインポートします。

{% raw %}
```
$ fedpkg import （SRPMのパス）
```
{% endraw %}

fedpkg importコマンドは、SRPMからspecファイルやパッチ、ソースアーカイブを抽出し、作業ディレクトリへと展開します。
また、.gitignoreやハッシュ値を書きこんだsourcesファイルも生成します。

Cutterの場合は以下のようになりました。

{% raw %}
```
$ ls -1
cutter-1.2.2.tar.gz
cutter-configure-gtk-debug-detection.diff
cutter-test-use-upper-case-gdk-literal.diff
cutter.spec
sources
```
{% endraw %}

ここまでで、パッケージのインポートが完了しました。

次のステップ「パッケージのコミット」へと進みます。

#### パッケージのコミット

パッケージのインポートが完了したら、差分（新規追加）を確認した上でコミットします。
また、pushしてローカルのコミット内容をアップストリームへと反映します。

{% raw %}
```
$ git commit -m "Initial import (#XXXXXX)." 
$ git push
```
{% endraw %}

pushが完了したら、次のステップ「パッケージのビルド」へと進みます。

#### パッケージのビルド

パッケージのビルドを行うにはfedpkg buildを実行します。

CutterでFedora 18ブランチ向けにビルドしたときは、以下のようになりました[^8]。

{% raw %}
```
$ fedpkg build
Building cutter-1.2.2-4.fc18 for f18-candidate
Created task: 4941489
Task info: http://koji.fedoraproject.org/koji/taskinfo?taskID=4941489
Watching tasks (this may be safely interrupted)...
4941489 build (f18-candidate, /cutter:5978273caa55d3d45ed8e760efc15919b6133ab9): free
4941489 build (f18-candidate, /cutter:5978273caa55d3d45ed8e760efc15919b6133ab9): free -> open (buildvm-17.phx2.fedoraproject.org)
  4941490 buildSRPMFromSCM (/cutter:5978273caa55d3d45ed8e760efc15919b6133ab9): free
  4941490 buildSRPMFromSCM (/cutter:5978273caa55d3d45ed8e760efc15919b6133ab9): free -> open (buildvm-16.phx2.fedoraproject.org)
  4941490 buildSRPMFromSCM (/cutter:5978273caa55d3d45ed8e760efc15919b6133ab9): open (buildvm-16.phx2.fedoraproject.org) -> closed
  0 free  1 open  1 done  0 failed
  4941492 buildArch (cutter-1.2.2-4.fc18.src.rpm, i686): free
  4941491 buildArch (cutter-1.2.2-4.fc18.src.rpm, x86_64): open (buildvm-18.phx2.fedoraproject.org)
  4941492 buildArch (cutter-1.2.2-4.fc18.src.rpm, i686): free -> open (buildvm-07.phx2.fedoraproject.org)
  4941492 buildArch (cutter-1.2.2-4.fc18.src.rpm, i686): open (buildvm-07.phx2.fedoraproject.org) -> closed
  0 free  2 open  2 done  0 failed
  4941491 buildArch (cutter-1.2.2-4.fc18.src.rpm, x86_64): open (buildvm-18.phx2.fedoraproject.org) -> closed
  0 free  1 open  3 done  0 failed
  4941510 tagBuild (noarch): free
  4941510 tagBuild (noarch): free -> open (buildvm-21.phx2.fedoraproject.org)
  4941510 tagBuild (noarch): open (buildvm-21.phx2.fedoraproject.org) -> closed
  0 free  1 open  4 done  0 failed
4941489 build (f18-candidate, /cutter:5978273caa55d3d45ed8e760efc15919b6133ab9): open (buildvm-17.phx2.fedoraproject.org) -> closed
  0 free  0 open  5 done  0 failed
4941489 build (f18-candidate, /cutter:5978273caa55d3d45ed8e760efc15919b6133ab9) completed successfully
```
{% endraw %}

ビルドが成功したので、次のステップ「パッケージのアップデート」へと進みます。

#### パッケージのアップデート

Fedoraプロジェクトではパッケージの追加/更新/削除やパッケージの公開・管理を行うためのシステムとして[Bohdi](http://fedoraproject.org/wiki/Bodhi)を使っています。

BohdiはビルドしたパッケージをFedora ユーザーが参照するyumリポジトリへと反映します。

Bohdiへビルドしたパッケージを反映させるには、パッケージのアップデートを行います。

パッケージのアップデートをするには以下のコマンドを実行します。

{% raw %}
```
$ fedpkg update
```
{% endraw %}

fedpkg updateを実行するとエディタが起動するので、アップデートに関する項目を編集します。
何のパッケージの、どんな更新なのかといったメタ情報を記述します。

必須なのはtypeで、新規パッケージの場合にはnewpackageを指定します。

Cutterの場合は以下のようにしました。

{% raw %}
```
[ cutter-1.2.2-4.fc18 ]

# bugfix, security, enhancement, newpackage (required)
type=newpackage

# testing, stable
request=testing

# Bug numbers: 1234,9876
bugs=887778

# Description of your update
notes=Unit Testing Framework for C/C++

# Enable request automation based on the stable/unstable karma thresholds
autokarma=True
stable_karma=3
unstable_karma=-3

# Automatically close bugs when this marked as stable
close_bugs=True

# Suggest that users restart after update
suggest_reboot=False
```
{% endraw %}

編集結果を保存するとfedpkg updateコマンドがその内容をBohdiへと送ります。

{% raw %}
```
Creating a new update for  cutter-1.2.2-4.fc18 
Password for kenhys: 
Creating a new update for  cutter-1.2.2-4.fc18 
Update successfully created
================================================================================
     cutter-1.2.2-4.fc18
================================================================================
    Release: Fedora 18
     Status: pending
       Type: newpackage
      Karma: 0
    Request: testing
       Bugs: 887778 - Review Request: cutter - A Unit Testing Framework for C
      Notes: Unit Testing Framework for C/C++
  Submitter: kenhys
  Submitted: 2013-02-09 10:07:19
   Comments: bodhi - 2013-02-09 10:07:47 (karma 0)
             This update has been submitted for testing by kenhys.

  https://admin.fedoraproject.org/updates/cutter-1.2.2-4.fc18
```
{% endraw %}

#### ブランチごとの作業まとめ

「パッケージのインポート」から「パッケージのアップデート」までは作業しているブランチごとに繰り返す必要があります。
ただし、作業ブランチによっては、やるべきこと、やらなくていいことがあります。

そこで、ここまでの作業手順を一旦まとめます。Cutterの場合には（当時Fedora 19ブランチはなかったので）、masterブランチとFedora 18ブランチについて作業しました。

<table>
 <tr><th>作業</th><th>masterブランチ</th><th>Fedora 18ブランチ</th></tr>
 <tr><td>パッケージのインポート</td><td>必要</td><td>SRPMのインポートは不要。代わりにmasterブランチからマージする。</td></tr>
 <tr><td>パッケージのコミット</td><td>必要</td><td>必要（pushのみ）</td></tr>
 <tr><td>パッケージのビルド</td><td>必要</td><td>必要</td></tr>
 <tr><td>パッケージのアップデート</td><td>不要</td><td>必要</td></tr>
 <tr><td>パッケージのテスト</td><td>不要</td><td>必要</td></tr>
 <tr><td>パッケージのリリース</td><td>不要</td><td>必要</td></tr>
</table>


まず、masterブランチ向けに「パッケージのインポート」から「パッケージビルド」まで行いました。
次にfedpkg switch-branch f18でFedora 18のブランチに切り替え、masterブランチからマージした後、パッケージのリリースまで行いました。

上記を具体的な実行コマンド列で比較すると以下のようになります。

masterブランチの場合

{% raw %}
```
$ git import （SRPMのパス）
$ git commit
$ git push
$ fedpkg build
```
{% endraw %}

Fedora 18ブランチの場合

{% raw %}
```
$ fedpkg switch-branch f18
$ git merge master
$ git push
$ fedpkg build
$ fedpkg update
```
{% endraw %}

Fedora 18ブランチの手順では以下のことを行っています。

  * fedpkg switch-branch f18でrawhideと呼ばれるmasterブランチからf18ブランチに切り替える
  * git merge masterでmasterブランチの修正をf18ブランチにとりこむ
  * git pushでコミットをアップストリームに反映する
  * fedpkg buildでf18向けにパッケージをビルドする
  * fedpkg updateでf18向けにビルドしたパッケージをBohdiへと反映する

ここまでの作業がブランチごとに完了したら、次のステップ「パッケージのテスト」へと進みます。

#### パッケージのテスト

fedpkg updateでBohdiへとパッケージを更新するようにしむけても、それで終わりではありません。

投稿したRPMパッケージが一般ユーザーが利用しているyumリポジトリへと反映されるまでには、いくつかの段階があります。

<table>
  <tr><th>Bohdiのステータス</th><th>説明</th></tr>
  <tr><td>pending</td><td>リポジトリ反映前</td></tr>
  <tr><td>testing</td><td>testingリポジトリへ反映されている</td></tr>
  <tr><td>stable</td><td>一般ユーザーが利用するyumリポジトリへ反映されている</td></tr>
</table>


RPMパッケージは一旦キューに入れられるのでpending状態になり、その後testingリポジトリ[^9]へと反映されます。

そしてtestingリポジトリで一定評価を得たパッケージがstable[^10]に入る仕組みになっています。

その際の評価はKarmaと呼ばれる仕組みによって行います。

Bohdiにアカウントを有しているユーザーは、Webインターフェースからパッケージに対するコメントをすることができます。
コメントするときには、あわせてパッケージを評価することができます。
この評価欄の集計がKarmaです。

ユーザーがパッケージに対してコメントするときに 「Works for me」にチェックを入れると+1となり、「Does not work」にチェックを入れると-1の評価となります。

![Bohdiコメント欄]({{ "/images/blog/20130410_6.png" | relative_url }} "Bohdiコメント欄")

デフォルトのKarmaは0からスタートします。
BohdiはKarmaが+3になったパッケージをstableリポジトリへと反映し、逆に-3に達したパッケージをtestingリポジトリから削除します。

![testing状態のCutter]({{ "/images/blog/20130410_7.png" | relative_url }} "testing状態のCutter")

このKarmaの閾値についてはfedpkg update実行時にその都度メタ情報として設定できます[^11]。

この仕組みの詳細については、[QA:Updates Testing](https://fedoraproject.org/wiki/QA:Updates_Testing)を参照してください。

#### パッケージのリリース

一定評価（stable_karma）を得られたパッケージは、Bohdiが自動的に一般ユーザーが利用するyumリポジトリ（stableリポジトリ）へと反映します。

一定評価を得られていないパッケージであっても、一定期間（2週間程度）経過すると、手動操作でyumリポジトリへと反映することができます。

その場合は、パッケージをアップロードした人がBohdiのインターフェースから作業します。
該当パッケージの情報ページで「Mark as stable」をクリックするとyumリポジトリへ反映することができます。

![mark as stable]({{ "/images/blog/20130410_8.png" | relative_url }} "mark as stable")

### まとめ

新規パッケージをリリースするまでに必要な情報については、すでにFedoraプロジェクトによって文書化されています。
また、一部には日本語訳も用意されています。

ただし、実際にやってみないことにはわからないこともあります。

そこで今回は具体例をもとに実際にパッケージをリリースするところまでの流れを紹介しました。

フリーソフトウェアの開発元がかならずしも使っているディストリビューション向けにRPMパッケージを提供してくれるとは限りません。

ソースアーカイブからRPMを作成してくれる[checkinstall](http://asic-linux.com.mx/~izto/checkinstall/)などもあるので、手元の環境ではRPM化して管理しているという人もいるかも知れません。

しかし、自作RPMパッケージを手元でだけ管理している状態では、他の人もRPM化して管理したい場合には、また同様の手順を踏むことになります。
自分は問題を解決したけど、また再度同じ問題にぶつかる人がいるという状況に変化はありません。

成果物については手元の環境だけで眠らせておかずにディストリビューションへと還元するのをおすすめします。

今回は、新規にパッケージを登録するところまでを書きました。
パッケージは登録して終わりではありません。継続してメンテナンスしていくことも重要です。

既存パッケージを更新する方法については、またの機会に記事にしたいと思います。

### 参考:パッケージメンテナンス関連の文書

  * [Join the package collection maintainer](https://fedoraproject.org/wiki/Join_the_package_collection_maintainers)
  * [Join the package collection maintainersの日本語訳](https://fedoraproject.org/wiki/Join_the_package_collection_maintainers/ja)
  * [Package Review Process](http://fedoraproject.org/wiki/Package_Review_Process)
  * [ReviewGuidelines](http://fedoraproject.org/wiki/Packaging:ReviewGuidelines)
  * [Package SCM admin requests](https://fedoraproject.org/wiki/Package_SCM_admin_requests)
  * [Using Mock to test package builds](http://fedoraproject.org/wiki/Using_Mock_to_test_package_builds)
  * [QA:Updates Testing](https://fedoraproject.org/wiki/QA:Updates_Testing)

[^0]: 1.2.2をリリースして以降。

[^1]: rpmlintによりスペルミスとして警告されていても、固有名詞のため問題ない場合もあります。また、rpmlintはそれぞれのRPM単位でのチェックのため、メインパッケージに含まれていれば良いライセンス類がサブパッケージに含まれていないと警告として扱われたりします。

[^2]: 例えば現在リリースされているFedora 18だけではなく、リリース前のFedora 19などでもビルド確認が行えます。

[^3]: fedora-browser-cert.p12についてはブラウザ経由でKojiへアクセスするときに必要です。セットアップ手順は上記のメッセージの通り証明書のインポートを行います。

[^4]: 証明書の有効期限が切れたら更新する必要はあります。

[^5]: BuildRequires:の指定が抜けているなど。

[^6]: 例えば、fedorapeople.orgに割り当てられたディスク領域を使うことができます。次回以降のレビューリクエストを行うときはこちらにspecファイルとSRPMを置くと良いでしょう。

[^7]: [Rawhide](https://fedoraproject.org/wiki/Releases/Rawhide)はFedoraの開発版を意味し、masterブランチのことです。

[^8]: masterブランチのビルドのときのログが残っていなかったのでFedora 18向けにビルドしたときのログです。

[^9]: testingリポジトリは、テスター向けのリポジトリです。標準では有効になっていないので、testingリポジトリのパッケージを実際にインストールするには別途有効にする必要があります。

[^10]: 一般ユーザーが利用するyumリポジトリ

[^11]: stableリポジトリへ反映される基準値はstable_karmaで、削除される基準値はunstable_karmaで設定できます。それぞれデフォルト値は3と-3です。
