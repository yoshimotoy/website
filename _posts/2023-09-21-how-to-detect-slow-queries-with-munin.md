---
title: Muninを使ってGroongaのスロークエリーを検出する
author: komainu8
tags:
  - groonga
---
[Groonga](https://groonga.org/ja/)の[サポートサービス]({% link services/groonga.md %})を担当している堀本です。

今回は、[Munin](https://munin-monitoring.org/)(ムニン)というサーバー監視ツールを使ってGroongaのスロークエリーを検出する方法を紹介します。

<!--more-->

検索サービスを提供しているサイトでは、クエリーの実行時間がサイトの利便性に大きな影響を及ぼします。
皆さんも、Webサイトで買い物をする際の商品検索や、図書館で蔵書を探す際の蔵書検索などで検索に10秒もかかっていたら、
そのサイトや検索サービスは使いたくなくなりますよね？

そのため、検索サービスにおいては検索のボトルネックを検出し改善していくことは非常に重要です。
したがって、常に遅い検索クエリーが存在するかどうかを監視しておきたくなります。

世の中には、検索に関する情報だけではなく、CPUやメモリー使用量など様々なコンピューターリソースを監視できるツールがたくさんあります。
今回はその中でMuninという監視ツールを使ってGroongaのスロークエリーを監視、検出します。
なぜ、Muninなのかというと、GroongaにはこのMuninにデータを送るためのプラグインがすでに実装されているからです。
デフォルトでは無効ですが、`groonga-munin-plugins`というパッケージをインストールすることで使えるようになります。

そのため、GroongaユーザーはMuninと接続するためのプラグインを自分で作ることなく、Groongaのスロークエリーを監視できるのです。

では、以下で実際に環境を構築し、スロークエリーを監視、検出する仕組みを作っていきましょう！
今回は、AlmaLinux 9 上で環境構築を行います。

### Munin-ServerとMunin-Nodeのセットアップ

まず最初にMuninをインストールして、環境構築を初めましょう。
Muninは、情報を収集する対象のサーバーにインストールする`munin-node`と各サーバーから収集した情報を集約するサーバーに
インストールする`munin`の２つのパッケージがあります。

`munin`をインストールするサーバーのことを、以後"Munin-Server"と呼ぶことにします。
`munin-node`をインストールするサーバーのことを、以後"Munin-Node"と呼ぶことにします。

今回は以下のような環境を構築します。
Munin-Nodeには、Groongaが動いており、Munin-Nodeの情報をMunin-Serverが定期的に取得する構成になっています。
Munin-Nodeとなるサーバーには、すでにGroongaがインストール済みの状態とします。

```
----------------------------         ---------------------------
| Munin-Server             |  -----> | Munin-Node              |
| IP Address: 192.168.2.1  |         | IP Address: 192.168.2.2 |
----------------------------         | host_name: groonga.node |
                                     ---------------------------
```

では、まずはMunin-Serverのセットアップを行います。

以下の手順で`munin`パッケージをインストールします。

```console
$ sudo dnf install -y epel-release
$ sudo dnf install -y --enablerepo=epel --enablerepo=crb munin
```

収集したデータをWebブラウザー経由で確認するため、Webサーバーを起動しましょう。
また、不意の再起動やメンテンナンスによる再起動に備えて、サーバー起動時に自動でWebサーバーが起動するように設定しておきましょう。

Webサーバーは`munin`パッケージをインストールした際に自動でインストールされています。

```console
$ sudo systemctl enable httpd
$ sudo systemctl start httpd
```

ここまでで、Munin-Serverのインストールは完了です。
次は、情報収集の対象であるMunin-Nodeのセットアップを実施します。

以下の手順で、`munin-node`パッケージをインストールします。

```console
$ sudo dnf install -y epel-release
$ sudo dnf install -y --enablerepo=epel --enablerepo=crb munin-node
```

インストールが完了したら`munin-node`を起動しましょう。
`munin-node`も不意の再起動やメンテンナンスによる再起動に備えて、サーバー起動時に自動で起動するように設定しておきましょう。

```console
$ sudo systemctl enable munin-node
$ sudo systemctl start munin-node
```

Munin-NodeにはすでにGroongaがインストールされているので、次はGroongaとMuninを接続するためのプラグインをインストールします。
もし、Groongaをまだインストールしていない場合は、以下のURLを参照してインストールしてください。

* [Groonga - インストール](https://groonga.org/ja/docs/install.html)

また、GroongaのMuninプラグインをインストールするには、Groonga HTTPサーバーが起動している必要があります。
もし、まだgroonga-server-httpをインストールしていない場合は以下の手順でインストールし、Groonga HTTPサーバーを起動してください。

```console
$ sudo dnf install -y groonga-server-http
$ sudo systemctl enable groonga-server-http
$ sudo systemctl start groonga-server-http
```

GroongaのMuninプラグインは以下の手順でインストールします。

```console
$ sudo dnf install -y --enablerepo=epel groonga-munin-plugins
```

インストール完了するとGroongaのMuninプラグインは有効になります。
以下の手順でプラグインが有効になったかどうかを確認できます。

```console
$ munin-node-configure | grep groonga
```

以下のように`|yes|`と表示されていればGroongaのMuninプラグインは有効になっています。

```
groonga_cpu_load_          | yes  | httpd
groonga_cpu_time_          | yes  | httpd
groonga_disk_              | yes  | gqtp http httpd
groonga_memory_            | yes  | httpd
groonga_n_records_         | yes  | http httpd
groonga_query_performance_ | yes  | httpd
groonga_status_            | yes  | http httpd
groonga_throughput_        | yes  | http httpd
```

最後に、以下の手順で`munin-node`を再起動します。

```console
$ sudo systemctl restart munin-node
```

ここまでで、GroongaのMuninプラグインを有効化できたので、Groongaの情報をMunin-Serverに送る準備はできました。
次は、Munin-ServerとMunin-Nodeの接続です。

### Munin-ServerとMunin-Nodeの接続

MuninはMunin-ServerからMunin-Nodeに対して接続し情報を収集します。
そのため、Munin-Node側でMunin-Serverからの接続を許可する必要があります。

以下の手順でMunin-Serverからの接続を許可します。
なお、Munin-Server - Munin-Node間の通信はすでにできるようになっている前提です。
もし、ファイヤーウォール等でMunin-Server - Munin-Node間の通信をブロックしているなら、解除してください。

1. Munin-Nodeの`/etc/munin/munin-node.conf`に以下の記述を追加します。

```
host_name groonga.node
allow ^192\.168\.2\.1$
```

上記の記述を追記することでMunin-Serverからアクセスが可能になります。
上記の`192.168.2.1`のところはMunin-Serverのアドレスを指定してください。
Munin-Serverのアドレスは正規表現で記述してください。

host_name は任意の名前で問題ありません。Munin-Serverで情報を収集するサーバーを識別するために使います。

2. 設定を有効にするため、`munin-node`を再起動します。

```console
$ sudo systemctl restart munin-node
```

次は、情報を収集する側、つまりMunin-ServerにMunin-Nodeを登録します。
Munin-ServerにMunin-Nodeを登録しないと、どのサーバーに情報を収集しに行くのかわからないためです。

以下の手順でMunin-ServerにMunin-Nodeを登録します。

Munin-Serverの`/etc/munin/munin-conf.d/local.conf`に以下の記述を追加します。

```
[groonga.node]
    address 192.168.2.2
    use_node_name yes
```

`[]`の中には、ホスト名(munin-node.confに設定したhost_name)を指定します。
`use_node_name`はyesにすることで、munin-node.confに設定したhost_nameを表示します。
noにすると、Munin-Nodeのホスト名を表示するので、わかりやすい方を設定してください。

ここまでで、Munin-ServerとMunin-Nodeの接続は完了し、Groongaの情報が収集できているはずです。
Muninは定期的にMunin-Nodeから情報を収集しますが、手動で収集することもできます。
動作確認も兼ねて以下の手順で、手動更新を実施しGroongaの情報を収集できるか試してみましょう。

```console
$ sudo -u munin munin-cron
```

上記を実行すると情報が更新されます。
以下のアドレスにWebブラウザーでアクセスし、情報が収集されているか確認してみましょう。

http://Munin-ServerのIPアドレス/munin/

今回の例では、以下のようになります。

http://192.168.2.1/munin/

上記のアドレスにアクセスすると、Munin-Serverに登録されている全部のMunin-Nodeが表示されます。
今回登録したMunin-Nodeの情報のみ見たい場合は、以下のアドレスにアクセスしてください。

http://Munin-ServerのIPアドレス/munin/node/Munin-Nodeのホスト名/index.html

今回の例では、以下のようになります。

http://192.168.2.1/munin/node/groonga.node/index.html

### アラートの設定

次に、Groongaのクエリー実行時間がある値を超えたらアラートを出すように設定します。
こうすることで、例えば1秒以上かかるクエリーを検出することができます。

アラートの設定は、以下の手順で実施します。

Munin-Serverの`/etc/munin/munin-conf.d/local.conf`に以下の記述を追加します。

監視対象のホストの設定に以下の内容を追記します。

```
プラグイン名.監視項目名.警告レベル（warning または critical） しきい値
```

今回は、クエリーの実行時間でアラートを設定したいので、プラグイン名はクエリーの実行時間を取得する`groonga_query_performance_httpd`を指定します。
監視項目名には、以下の3つが設定できます。

* `longest`
* `average`
* `median`

`longest`は、最大値を表します。つまり監視期間の中で最も遅いクエリーの時間です。
`average`は、監視期間中のクエリー実行時間の平均値を表します。
`median`は、監視期間中のクエリー実行時間の中央値を表します。

今回はスロークエリーがあるかどうかを確認したいので、`longest`を指定します。
警告レベルは重要度に応じて変更してください。今回の例では`warning`とします。
しきい値はクエリーの実行時間を指定します。今回は1秒以上のクエリーがあったらアラートを出したいので1000とします。
`groonga_query_performance_httpd`で収集される値はミリ秒単位のため、1秒以上のクエリーを検出する場合は1000を指定する必要があります。

今回の例では、以下のような記述になります。

```
[groonga.node]
    address 192.168.2.2
    use_node_name yes
    groonga_query_performance_httpd.longest.warning 1000
```

以上で、Muninを使ってスロークエリーを監視、検出することができるようになりました！
Groongaをお使いの環境でスロークエリーを検出する仕組みをお探しの方は、是非上記の方法を試してみてください。
